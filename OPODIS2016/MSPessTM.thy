theory MSPessTM
imports "Transitions" "Interface"
begin

datatype PC =
    read_begin1  
  | read_begin2  
  | read_begin3  
  | write_begin1 
  | write_begin2 
  | write_begin3 
  | write_begin4 
  | write_begin5 
  | write_begin6 
  | read_read1(loc : L)   
  | read_read2(loc : L)  
  | read_read3(loc : L)   
  | read_read4(loc : L)  
  | read_read5(loc : L)  
  | write_read1(loc : L) 
  | write_read2(loc : L) 
  | write_read3(loc : L) 
  | write_read4(loc : L) 
  | write_read5(loc : L)  
  | write_read6(loc : L)  
  | write_write1(loc :L)(val: V) 
  | read_commit1  


  | write_commit0  
  | write_commit1  
  | write_commit2
  | write_commit3a
(*   | write_commit3 
 *)
  | write_commit4 
(*  | write_commit5 *)
  | write_commit6 
(*  | write_commit7 *)
  | write_commit8 
  | write_commit9 
  | write_commit10
  | write_commit11
  | write_commit12
  | write_commit13
  | write_commit14

datatype Status =
    NotStarted
  | Ready
  | pending: Pending(pc: PC)
  | BeginResponding
  | ReadResponding V
  | WriteResponding
  | CommitResponding
  | Committed
  
datatype 'a option2 =
  Val (the2: 'a) | Reading | Idle


type_synonym InternalAction = PC

record State =
  status :: "T => Status"
  write_set :: "T => L => V option"
  global_version :: "nat"
  txn_version :: "T \<Rightarrow> nat option2"
  writer_waiting :: "T \<Rightarrow> bool"
  store :: "L => V"
  version :: "L => nat"
  lock :: "bool" (* False = taken *)
  progress_seen :: "T \<Rightarrow> bool"
  temp :: "T \<Rightarrow> nat"
  (* Auxiliary variables *)
  active_writer :: "T option"
  committing_writer :: "T option"


definition txn_ver :: "State \<Rightarrow> T \<Rightarrow> nat"
  where   "txn_ver s t \<equiv> case (txn_version s t) of Val n \<Rightarrow> n"

definition ws_val :: "State \<Rightarrow> T \<Rightarrow> L \<Rightarrow> V"
  where
  "ws_val s t l \<equiv> case (write_set s t l) of Some v \<Rightarrow> v"


(* Update functions *)

definition update_status :: "T \<Rightarrow> Status \<Rightarrow> State \<Rightarrow> State"
  where
  "update_status t st s \<equiv>
    s\<lparr> status := ((status s) (t := st)) \<rparr>"

definition update_write_set :: "T \<Rightarrow> L \<Rightarrow> V option \<Rightarrow> State \<Rightarrow> State"
  where
  "update_write_set t l v s \<equiv> 
    s\<lparr> write_set := ((write_set s) (t := ((write_set s t) (l := v))))\<rparr>"

definition update_txn_version :: "T \<Rightarrow> (nat option2) \<Rightarrow> State \<Rightarrow> State "
  where
  "update_txn_version t n s \<equiv>       s \<lparr> txn_version := ((txn_version s) (t := n)) \<rparr>"

definition update_writer_waiting :: "T \<Rightarrow> bool \<Rightarrow> State \<Rightarrow> State"
  where
  "update_writer_waiting t b s \<equiv> s\<lparr> writer_waiting := ((writer_waiting s) (t := b)) \<rparr>"

definition update_store :: "L \<Rightarrow> V \<Rightarrow> State \<Rightarrow> State"
  where
  "update_store l v s \<equiv>
    s\<lparr> store := ((store s) (l := v)) \<rparr>"

definition update_version :: "L \<Rightarrow> nat \<Rightarrow> State \<Rightarrow> State"
  where
  "update_version l n s \<equiv>
    s\<lparr> version := ((version s) (l := n)) \<rparr>"

definition update_progress_seen :: "T \<Rightarrow> bool \<Rightarrow> State \<Rightarrow> State"
  where
  "update_progress_seen t b s \<equiv>
    s\<lparr> progress_seen := ((progress_seen s) (t := b)) \<rparr>"

definition update_temp :: "T \<Rightarrow> nat \<Rightarrow> State \<Rightarrow> State"
  where
  "update_temp t n s \<equiv>
    s\<lparr> temp := ((temp s) (t := n)) \<rparr>"

definition set_active_writer :: "T \<Rightarrow> State \<Rightarrow> State"
  where
  "set_active_writer t s \<equiv>  s\<lparr> active_writer := Some t \<rparr>"

definition unset_active_writer :: "State \<Rightarrow> State"
  where
  "unset_active_writer s \<equiv>  s\<lparr> active_writer := None \<rparr>"

definition set_committing_writer :: "T \<Rightarrow> State \<Rightarrow> State"
  where
  "set_committing_writer t s \<equiv>  s\<lparr> committing_writer := Some t \<rparr>"

definition unset_committing_writer :: "State \<Rightarrow> State"
  where
  "unset_committing_writer s \<equiv>  s\<lparr> committing_writer := None \<rparr>"

definition falsify_writer_waiting_and_active_writer :: "T \<Rightarrow> State \<Rightarrow> State"
  where
  "falsify_writer_waiting_and_active_writer t s \<equiv> s\<lparr> writer_waiting := ((writer_waiting s) (t := False)) \<rparr> ;; set_active_writer t"

(* initial state *)

    
definition start :: "State => bool"
  where
  "start s \<equiv>
       (\<forall> t. status s t = NotStarted)
     \<and> (\<forall> t. \<not> progress_seen s t)
     \<and> global_version s = 1
     \<and> lock s = True
     \<and> (\<forall> t. txn_version s t = Idle \<and> writer_waiting s t = False \<and> write_set s t = empty)
     \<and> store s : mem_initial
     \<and> active_writer s = None 
     \<and> committing_writer s = None
     \<and> store s = (\<lambda> l. v0)"
     

(* preconditions state *)


definition ext_pre :: "State \<Rightarrow> T \<Rightarrow> Interface.Action \<Rightarrow> bool"
  where
  "ext_pre s0 t a \<equiv>
    (case a of
        BeginInv \<Rightarrow> status s0 t = NotStarted
      | BeginResp \<Rightarrow> status s0 t = BeginResponding
      | CommitInv \<Rightarrow> status s0 t = Ready 
      | CommitResp \<Rightarrow> status s0 t = CommitResponding
      | Cancel \<Rightarrow> False
      | Abort \<Rightarrow> False
      | ReadInv l \<Rightarrow> status s0 t = Ready 
      | ReadResp v \<Rightarrow> status s0 t = (ReadResponding v)
      | WriteInv l v \<Rightarrow> status s0 t = Ready  \<and> t : TWriter 
      | WriteResp \<Rightarrow> status s0 t = WriteResponding)
      "
  
definition ext_eff :: "State \<Rightarrow> T \<Rightarrow> Interface.Action \<Rightarrow> State"
  where
  "ext_eff s t a \<equiv>
   (case a of
        BeginInv \<Rightarrow>
           (if t : TWriter then
             (s ;; update_status t (Pending write_begin1))
            else if t : TReader then
             (s ;; update_status t (Pending read_begin1)) else s)
      | BeginResp \<Rightarrow> 
          s ;; update_status t Ready
      | CommitInv \<Rightarrow>
           (if t : TWriter then
             (s ;; update_status t (Pending write_commit0))
            else if t : TReader then
             (s ;; update_status t (Pending read_commit1)) else s)
      | CommitResp \<Rightarrow>  s ;; update_status t Committed
      | ReadInv l \<Rightarrow>
           (if t : TWriter then
             (s ;; update_status t (Pending (write_read1 l)))
            else if t : TReader then
             (s ;; update_status t (Pending (read_read1 l))) else s)
      | ReadResp v \<Rightarrow> s ;; update_status t Ready
      | WriteInv l v \<Rightarrow> s ;; update_status t (Pending (write_write1 l v))
      | WriteResp \<Rightarrow> s ;; update_status t Ready)"
  
definition tau_pre :: "State \<Rightarrow> T \<Rightarrow> InternalAction \<Rightarrow> bool"
  where
  "tau_pre s t i \<equiv>
    status s t = Pending i"


definition test_and_branch :: "T \<Rightarrow> bool \<Rightarrow> Status \<Rightarrow> Status \<Rightarrow> State \<Rightarrow> State"
  where
  "test_and_branch t b strue sfalse s \<equiv> 
      if b then s ;; update_status t strue
      else s ;; update_status t sfalse
  "

definition await :: "T \<Rightarrow> bool \<Rightarrow> Status \<Rightarrow> State \<Rightarrow> State"
  where
  "await t b ps s \<equiv> 
      if b then s ;; update_status t ps 
      else s
  "

definition set_to_list :: "'a set \<Rightarrow> 'a list"
  where "set_to_list s = (SOME l. set l = s)"


lemma  set_set_to_list:
   "finite s \<Longrightarrow> set (set_to_list s) = s"
unfolding set_to_list_def by (metis (mono_tags) finite_list some_eq_ex)

definition quiescent_for :: "nat option2 \<Rightarrow> nat \<Rightarrow> bool"
  where
  "quiescent_for tver glob_ver \<equiv>
    case tver of Val n \<Rightarrow> n = glob_ver | Idle \<Rightarrow> True | Reading \<Rightarrow> False"

definition pending_writes :: "State \<Rightarrow> T \<Rightarrow> L set"
  where
  "pending_writes s t \<equiv> {l . (\<exists> v . write_set s t l = Some v \<and> store s l \<noteq> v)}"

definition tau_eff :: "State \<Rightarrow> T \<Rightarrow> InternalAction \<Rightarrow> State"
  where
  "tau_eff s t i \<equiv>
    case i of
        read_begin1 \<Rightarrow> s ;; update_txn_version t Reading
                         ;; update_status t (Pending read_begin2)
      | read_begin2 \<Rightarrow> s ;; update_temp t (global_version s)
                         ;; update_status t (Pending read_begin3)
      | read_begin3 \<Rightarrow> s ;; update_txn_version t (Val ((temp s) t))
                         ;; update_status t BeginResponding
      | write_begin1 \<Rightarrow> s ;; update_writer_waiting t True
                                    ;; update_status t (Pending write_begin2)
      | write_begin2 \<Rightarrow> 
          if (\<not> (writer_waiting s t))
          then s ;; update_status t (Pending write_begin5) (* ;; set_active_writer t *)
          else s ;; update_status t (Pending write_begin3)
      | write_begin3  \<Rightarrow> 
          if lock s
          then s\<lparr> lock := False\<rparr> 
                 ;; update_status t (Pending write_begin4)
                 ;; set_active_writer t
          else s ;; update_status t (Pending write_begin2)
      | write_begin4  \<Rightarrow> s ;; update_writer_waiting t False
                                    ;; update_status t (Pending write_begin5)
      | write_begin5  \<Rightarrow> s ;; update_temp t (global_version s)
                                     ;; update_status t (Pending write_begin6)
      | write_begin6  \<Rightarrow> s ;; update_txn_version t (Val ((temp s) t))
                                     ;; update_status t BeginResponding
      | read_read1 l  \<Rightarrow> 
          s ;; test_and_branch 
                t (progress_seen s t) (Pending (read_read5 l)) (Pending (read_read2 l))
      | read_read2 l  \<Rightarrow>  
          s ;; test_and_branch 
                t (version s l \<noteq> txn_ver s t) (Pending (read_read5 l)) (Pending (read_read3 l))
      | read_read3 l  \<Rightarrow> 
          s ;; await t (txn_ver s t \<noteq> global_version s) (Pending (read_read4 l))
      | read_read4 l  \<Rightarrow> s ;; update_progress_seen t True 
                                     ;; update_status t (Pending (read_read5 l))
      | read_read5 l  \<Rightarrow> s ;; update_status t (ReadResponding (store s l))
      | write_read1 l \<Rightarrow> 
          s ;; (case write_set s t l of
                   Some v \<Rightarrow> update_status t (ReadResponding v)
                 | None \<Rightarrow> update_status t (Pending (write_read2 l)))
      | write_read2 l  \<Rightarrow> 
          s ;; test_and_branch 
                t (progress_seen s t) (Pending (write_read6 l)) (Pending (write_read3 l))
      | write_read3 l  \<Rightarrow> 
          s ;; test_and_branch 
                t (version s l \<noteq> txn_ver s t) (Pending (write_read6 l)) (Pending (write_read4 l))
      | write_read4 l  \<Rightarrow> 
          s ;; await t (txn_ver s t \<noteq> global_version s) (Pending (write_read5 l))
      | write_read5 l  \<Rightarrow> 
          s ;; update_progress_seen t True
            ;; update_status t (Pending (write_read6 l))
      | write_read6 l  \<Rightarrow>
          s ;; update_status t (ReadResponding (store s l))
      | write_write1 l v \<Rightarrow> 
          s ;; update_write_set t l (Some v)
            ;; update_status t WriteResponding 
      | read_commit1  \<Rightarrow> 
          s ;; update_txn_version t Idle
            ;; update_status t CommitResponding 
      | write_commit0  \<Rightarrow> 
          s ;; update_temp t (txn_ver s t)
            ;; update_status t (Pending write_commit1)
      | write_commit1  \<Rightarrow> 
          if odd (temp s t)
          then s ;; update_status t (Pending write_commit4)
          else s ;; update_status t (Pending write_commit2)
      | write_commit2  \<Rightarrow> 
          if (temp s t \<noteq> global_version s)
          then s ;; update_status t (Pending write_commit3a) 
          else s
      | write_commit3a  \<Rightarrow> 
          s ;; update_temp t (global_version s)
            ;; update_status t (Pending write_commit4)
      | write_commit4  \<Rightarrow> 
          let nv = (temp s t) + 1 ; WS = dom (write_set s t)
          in 
          s ;; 
          (if \<exists> l . l \<in> WS \<and> version s l \<noteq> nv
          then let l = (SOME l . l \<in> WS \<and> version s l \<noteq> nv)
               in update_version l nv
          else update_status t (Pending write_commit6))
       | write_commit6  \<Rightarrow> 
           s \<lparr> global_version :=  (temp s t) + 1 \<rparr>
             ;; update_status t (Pending write_commit8)  ;; set_committing_writer t
       | write_commit8  \<Rightarrow>   
           s ;; test_and_branch 
                  t (\<exists> w .  writer_waiting s w) (Pending write_commit9) (Pending write_commit10)
       | write_commit9  \<Rightarrow> 
          let t' = (SOME w. writer_waiting s w \<and> w \<noteq> t) in
           s ;; update_status t (Pending write_commit11)
             ;; update_writer_waiting t' False 
             ;; set_active_writer t'
       | write_commit10  \<Rightarrow> 
           s\<lparr> lock :=  True\<rparr> ;; update_status t (Pending write_commit11)
                            ;; unset_active_writer
       | write_commit11  \<Rightarrow>
           s ;; await t (\<forall> t' . quiescent_for (txn_version s t') (1 + temp s t)) (Pending write_commit12)
       | write_commit12  \<Rightarrow>
          if pending_writes s t \<noteq> {}
          then s \<lparr> store := single_writeback (store s) (write_set s t)\<rparr>
          else s ;; update_status t (Pending write_commit13)
      | write_commit13  \<Rightarrow> 
          s \<lparr> global_version :=  (temp s t) + 2\<rparr> 
            ;; update_status t (Pending write_commit14)
            ;; unset_committing_writer
      | write_commit14  \<Rightarrow> 
          s ;; update_txn_version t Idle
            ;; update_status t CommitResponding"


definition mspesstm_pre :: "State \<Rightarrow> T \<Rightarrow> InternalAction Event \<Rightarrow> bool"
  where
  "mspesstm_pre s t e \<equiv> case e of Internal i \<Rightarrow> tau_pre s t i | External a \<Rightarrow> ext_pre s t a"

definition mspesstm_eff :: "State \<Rightarrow> T \<Rightarrow> InternalAction Event \<Rightarrow> State"
  where
  "mspesstm_eff s t e \<equiv> case e of Internal i \<Rightarrow> tau_eff s t i | External a \<Rightarrow> ext_eff s t a"
  
definition MSPessTM :: "(State, InternalAction) DAut"
  where
  "MSPessTM \<equiv> \<lparr> DAut.start = start,
                DAut.pre = mspesstm_pre,
                DAut.eff = mspesstm_eff \<rparr>"

definition shared_inv :: "State \<Rightarrow> bool"
  where
  "shared_inv s \<equiv>
     (lock s \<longrightarrow> active_writer s = None)
   \<and> (committing_writer s = None \<longleftrightarrow> odd(global_version s))
   \<and> (\<forall> t. status s t = Committed \<longrightarrow> txn_version s t = Idle)
"

definition eff_txn_ver :: "State \<Rightarrow> T \<Rightarrow> nat"
  where
  "eff_txn_ver s t \<equiv>
    case (txn_version s t) of
       Idle \<Rightarrow> if status s t = Pending write_begin6 then temp s t else global_version s
     | Reading \<Rightarrow> if status s t = Pending read_begin3 then temp s t else global_version s
     | Val n \<Rightarrow> n"

definition glb_cleared :: "State \<Rightarrow> T \<Rightarrow> bool"
  where
  "glb_cleared s t \<equiv>
       eff_txn_ver s t = global_version s"

definition txn_ver_states :: "State \<Rightarrow> T \<Rightarrow> bool"
  where
  "txn_ver_states s t \<equiv>
       eff_txn_ver s t \<le> global_version s \<and> global_version s \<le> 2 + eff_txn_ver s t
    \<and> (2 + eff_txn_ver s t = global_version s \<longrightarrow> committing_writer s \<noteq> None)"


definition is_waiting_writer :: "State \<Rightarrow> T \<Rightarrow> bool"
  where
  "is_waiting_writer s t \<equiv>
       t : TWriter
     \<and> committing_writer s \<noteq> Some t
     \<and> \<not> progress_seen s t
     \<and> write_set s t = Map.empty
     \<and> (\<not> writer_waiting s t \<longrightarrow>
                   active_writer s = Some t
                 \<and> \<not> lock s)"

definition is_active_writer :: "State \<Rightarrow> T \<Rightarrow> bool"
  where
  "is_active_writer s t \<equiv>
       t : TWriter
     \<and> txn_version s t \<notin> {Idle, Reading}
     \<and> txn_ver_states s t
     \<and> active_writer s = Some t
    \<and> (progress_seen s t \<longrightarrow> txn_ver s t < global_version s)
     \<and> committing_writer s \<noteq> Some t
     \<and> (odd (txn_ver s t) \<longrightarrow> committing_writer s = None \<and> txn_ver s t = global_version s )
     \<and> (even(txn_ver s t) \<and> txn_ver s t \<noteq> global_version s \<longrightarrow> committing_writer s = None)
"

definition is_intermediate_writer :: "State \<Rightarrow> T \<Rightarrow> bool"
  where
  "is_intermediate_writer s t \<equiv>
       t : TWriter
     \<and> txn_version s t \<notin> {Idle, Reading}
     \<and> active_writer s = Some t
     \<and> committing_writer s = Some t"

definition write_set_ver :: "State \<Rightarrow> T \<Rightarrow> nat \<Rightarrow> bool"
  where
  "write_set_ver s t v \<equiv>
     \<forall> l \<in> dom (write_set s t) . version s l = v"

definition is_active_reader :: "State \<Rightarrow> T \<Rightarrow> bool"
  where
  "is_active_reader s t \<equiv>
        t : TReader
      \<and> txn_version s t \<notin> {Idle, Reading}
      \<and> txn_ver_states s t
    \<and> (progress_seen s t \<longrightarrow> txn_ver s t < global_version s)"

definition is_active_txn :: "State \<Rightarrow> T \<Rightarrow> bool"
  where
  "is_active_txn s t \<equiv>
      (t : TWriter \<longrightarrow> is_active_writer s t)
    \<and> (t : TReader \<longrightarrow> is_active_reader s t)"

definition txn_inv :: "PC Event \<Rightarrow> State \<Rightarrow> T \<Rightarrow> bool"
  where
  "txn_inv e s t \<equiv>
    mspesstm_pre s t e \<longrightarrow>
     (case e of

        External BeginInv \<Rightarrow>
               committing_writer s \<noteq> Some t \<and>
               \<not> progress_seen s t \<and>
               txn_version s t = Idle \<and>
                write_set s t = Map.empty
      | External BeginResp \<Rightarrow> is_active_txn s t
      | External (ReadResp v) \<Rightarrow> is_active_txn s t
      | External WriteResp \<Rightarrow> is_active_writer s t
      | External CommitResp \<Rightarrow> txn_version s t = Idle

      | Internal read_begin1 \<Rightarrow>  t : TReader  \<and> \<not> progress_seen s t \<and>
               txn_version s t = Idle
      | Internal read_begin2 \<Rightarrow> t : TReader \<and> txn_version s t = Reading \<and> \<not> progress_seen s t
      | Internal read_begin3 \<Rightarrow>
               t : TReader \<and>
               txn_version s t = Reading \<and>
               txn_ver_states s t \<and>
               \<not> progress_seen s t

      | Internal write_begin1 \<Rightarrow>
               t : TWriter \<and>
               committing_writer s \<noteq> Some t \<and>
               \<not> progress_seen s t \<and>
               txn_version s t = Idle \<and>
                write_set s t = Map.empty
      | Internal write_begin2 \<Rightarrow>
               is_waiting_writer s t \<and>
               txn_version s t = Idle
      | Internal write_begin3  \<Rightarrow>
               is_waiting_writer s t \<and>
               txn_version s t = Idle
      | Internal write_begin4  \<Rightarrow>
             is_waiting_writer s t \<and>
             active_writer s = Some t \<and>
             txn_version s t = Idle
      | Internal write_begin5  \<Rightarrow>
             is_waiting_writer s t \<and>
             \<not> writer_waiting s t \<and>
               txn_version s t = Idle
      | Internal write_begin6  \<Rightarrow>
            is_waiting_writer s t
          \<and> \<not> writer_waiting s t
          \<and> txn_ver_states s t
          \<and> (odd (temp s t) \<longrightarrow> committing_writer s = None)
          \<and> (even(temp s t) \<and> temp s t \<noteq> global_version s \<longrightarrow> committing_writer s = None)
          \<and> txn_version s t = Idle

      | Internal (read_read1 l)  \<Rightarrow> is_active_reader s t
      | Internal (read_read2 l)  \<Rightarrow> is_active_reader s t
      | Internal (read_read3 l)  \<Rightarrow> is_active_reader s t
      | Internal (read_read4 l)  \<Rightarrow> is_active_reader s t \<and> txn_ver s t < global_version s
      | Internal (read_read5 l)  \<Rightarrow>
               is_active_reader s t
             \<and> (  progress_seen s t
                \<or> version s l \<noteq> txn_ver s t)

      | Internal (write_read1 l) \<Rightarrow> is_active_writer s t
      | Internal (write_read2 l)  \<Rightarrow> is_active_writer s t \<and> l \<notin> dom(write_set s t)
      | Internal (write_read3 l)  \<Rightarrow> is_active_writer s t \<and> l \<notin> dom(write_set s t)
      | Internal (write_read4 l)  \<Rightarrow> is_active_writer s t \<and> l \<notin> dom(write_set s t)
      | Internal (write_read5 l)  \<Rightarrow>
             is_active_writer s t \<and> txn_ver s t < global_version s \<and> l \<notin> dom(write_set s t)
      | Internal (write_read6 l)  \<Rightarrow>
               is_active_writer s t
             \<and> l \<notin> dom(write_set s t)
             \<and> (  progress_seen s t
                \<or> version s l \<noteq> txn_ver s t)

      | Internal (write_write1 l v) \<Rightarrow> is_active_writer s t

      | Internal read_commit1  \<Rightarrow> is_active_reader s t

      | Internal write_commit0  \<Rightarrow>
              is_active_writer s t
      | Internal write_commit1  \<Rightarrow>
              is_active_writer s t
            \<and> temp s t = txn_ver s t
      | Internal write_commit2  \<Rightarrow>
              is_active_writer s t
            \<and> even(temp s t)
            \<and> temp s t = txn_ver s t
      | Internal write_commit3a  \<Rightarrow>
             t : TWriter
            \<and> txn_version s t \<notin> {Idle, Reading}
            \<and> temp s t = txn_ver s t
            \<and> active_writer s = Some t
            \<and> committing_writer s = None
      | Internal write_commit4  \<Rightarrow>
             t : TWriter
            \<and> txn_version s t \<notin> {Idle, Reading}
            \<and> active_writer s = Some t
            \<and> committing_writer s = None
            \<and> temp s t = global_version s
      | Internal write_commit6  \<Rightarrow>
              t : TWriter
            \<and> txn_version s t \<notin> {Idle, Reading}
            \<and> active_writer s = Some t
            \<and> committing_writer s = None
            \<and> temp s t = global_version s
            \<and> write_set_ver s t (1 + global_version s)
      | Internal write_commit8  \<Rightarrow>
              is_intermediate_writer s t
             \<and> 1 + temp s t = global_version s
            \<and> write_set_ver s t (global_version s)
       | Internal write_commit9  \<Rightarrow>
              is_intermediate_writer s t
            \<and> 1 + temp s t = global_version s
            \<and> write_set_ver s t (global_version s)
       | Internal write_commit10 \<Rightarrow>
              is_intermediate_writer s t
            \<and> 1 + temp s t = global_version s
            \<and> write_set_ver s t (global_version s)
       | Internal write_commit11  \<Rightarrow>
              t : TWriter
            \<and> committing_writer s = Some t
            \<and> 1 + temp s t = global_version s
            \<and> write_set_ver s t (global_version s)
       | Internal write_commit12  \<Rightarrow>
              t : TWriter
            \<and> committing_writer s = Some t
            \<and> 1 + temp s t = global_version s
            \<and> (\<forall> t'. t \<noteq> t' \<longrightarrow> glb_cleared s t')
            \<and> write_set_ver s t (global_version s)
      | Internal write_commit13  \<Rightarrow>
              t : TWriter
            \<and> committing_writer s = Some t
            \<and> 1 + temp s t = global_version s
             \<and> (\<forall> t'. t \<noteq> t' \<longrightarrow> glb_cleared s t')
            \<and> write_set_ver s t (global_version s)
      | Internal write_commit14  \<Rightarrow> True

      | _ \<Rightarrow> is_active_txn s t)"

lemmas unfold_txn_props =
  txn_inv_def is_active_reader_def is_active_writer_def is_active_txn_def
  is_waiting_writer_def is_intermediate_writer_def

definition R :: "T \<Rightarrow> State \<Rightarrow> State \<Rightarrow> bool"
  where
  "R t s0 s1 \<equiv>
     status s1 t = status s0 t
   \<and> write_set s1 t = write_set s0 t
   \<and> txn_version s1 t = txn_version s0 t
   \<and> progress_seen s1 t = progress_seen s0 t
   \<and> temp s1 t = temp s0 t

   \<and> (\<forall> l. version s1 l = version s0 l \<or> global_version s0 < version s1 l)

   \<and> (writer_waiting s1 t = writer_waiting s0 t
      \<or> (  writer_waiting s0 t
         \<and> \<not> writer_waiting s1 t
         \<and> \<not> lock s1
         \<and> committing_writer s0 \<noteq> None
         \<and> active_writer s1 = Some t
         \<and> even(global_version s0)))

   \<and> (active_writer s0 = Some t \<longrightarrow>
           active_writer s1 = active_writer s0
         \<and> lock s1 = lock s0
         \<and> version s1 = version s0
         \<and> (committing_writer s0 = None \<longrightarrow> committing_writer s1 = None))

   \<and> (committing_writer s0 = Some t \<longrightarrow>
          global_version s1 = global_version s0
        \<and> store s1 = store s0
        \<and> version s1 = version s0
        \<and> (\<forall> t'. t \<noteq> t' \<and> glb_cleared s0 t' \<longrightarrow> glb_cleared s1 t'))

   \<and> (committing_writer s1 = committing_writer s0 \<longrightarrow> global_version s1 = global_version s0)

   \<and> (committing_writer s1 \<noteq> committing_writer s0 \<longrightarrow>
         global_version s1 = 1 + global_version s0
       \<and> (committing_writer s1 = None \<longrightarrow> eff_txn_ver s0 t = global_version s0)
       \<and> (committing_writer s1 \<noteq> None \<longrightarrow> committing_writer s0 = None))

   \<and> (committing_writer s1 = Some t \<longleftrightarrow> committing_writer s0 = Some t)
"


lemma Event_split:
  "\<lbrakk> b = External BeginInv \<Longrightarrow> P;
     b = External BeginResp \<Longrightarrow> P;
     \<And> l. b = External (ReadInv l) \<Longrightarrow> P;
     \<And> v. b = External (ReadResp v) \<Longrightarrow> P;
     \<And> l v. b = External (WriteInv l v) \<Longrightarrow> P;
     b = External WriteResp \<Longrightarrow> P;
     b = External CommitInv \<Longrightarrow> P;
     b = External CommitResp \<Longrightarrow> P;
     b = External Cancel \<Longrightarrow> P;
     b = External Abort \<Longrightarrow> P;

     b = Internal read_begin1 \<Longrightarrow> P;
     b = Internal read_begin2 \<Longrightarrow> P;
     b = Internal read_begin3 \<Longrightarrow> P;
     b = Internal write_begin1 \<Longrightarrow> P;
     b = Internal write_begin2 \<Longrightarrow> P;
     b = Internal write_begin3 \<Longrightarrow> P;
     b = Internal write_begin4 \<Longrightarrow> P;
     b = Internal write_begin5 \<Longrightarrow> P;
     b = Internal write_begin6  \<Longrightarrow> P;
     \<And> l. b = Internal (read_read1 l) \<Longrightarrow> P;
     \<And> l. b = Internal (read_read2 l) \<Longrightarrow> P;
     \<And> l. b = Internal (read_read3 l) \<Longrightarrow> P;  
     \<And> l. b = Internal (read_read4 l) \<Longrightarrow> P;
     \<And> l. b = Internal (read_read5 l) \<Longrightarrow> P;
     \<And> l. b = Internal (write_read1 l) \<Longrightarrow> P;
     \<And> l. b = Internal (write_read2 l) \<Longrightarrow> P;
     \<And> l. b = Internal (write_read3 l) \<Longrightarrow> P;
     \<And> l. b = Internal (write_read4 l) \<Longrightarrow> P;
     \<And> l. b = Internal (write_read5 l) \<Longrightarrow> P;
     \<And> l. b = Internal (write_read6 l) \<Longrightarrow> P;
     \<And> l v. b = Internal (write_write1 l v) \<Longrightarrow> P;
     b = Internal read_commit1 \<Longrightarrow> P;
     b = Internal write_commit0 \<Longrightarrow> P;
     b = Internal write_commit1 \<Longrightarrow> P;
     b = Internal write_commit2 \<Longrightarrow> P;
     b = Internal write_commit3a \<Longrightarrow> P;
(*      b = Internal write_commit3 \<Longrightarrow> P;
 *)
     b = Internal write_commit4 \<Longrightarrow> P;
(*     b = Internal write_commit5 \<Longrightarrow> P;*)
     b = Internal write_commit6 \<Longrightarrow> P;
(*     b = Internal write_commit7 \<Longrightarrow> P; *)
     b = Internal write_commit8 \<Longrightarrow> P;
     b = Internal write_commit9 \<Longrightarrow> P;
     b = Internal write_commit10 \<Longrightarrow> P;
     b = Internal write_commit11 \<Longrightarrow> P;
     b = Internal write_commit12  \<Longrightarrow> P;
     b = Internal write_commit13 \<Longrightarrow> P;
     b = Internal write_commit14 \<Longrightarrow> P\<rbrakk>
    \<Longrightarrow>
    P"
apply(cases rule: Event.exhaust[where y=b])
using Action.exhaust_sel apply blast
using PC.exhaust_sel by blast


(* Unfolding rules and lemma groups *)
    
lemmas unfold_trans =
  tau_pre_def tau_eff_def
  ext_pre_def ext_eff_def
  mspesstm_eff_def
  mspesstm_pre_def
  update_status_def
  update_txn_version_def
  update_writer_waiting_def
  falsify_writer_waiting_and_active_writer_def
  update_progress_seen_def
  update_temp_def
  update_write_set_def
  update_store_def
  update_version_def
  set_committing_writer_def
  set_active_writer_def
  unset_committing_writer_def
  unset_active_writer_def
  test_and_branch_def
  await_def
  pending_writes_def

lemmas unfold_queries =
  start_def 

lemmas unfold_mspesstm =
  unfold_queries
  unfold_trans
  Utilities.all_utilities
  MSPessTM_def
  TReader_def
 
lemmas all_simps = 
  unfold_queries
  unfold_trans
  Utilities.all_utilities
  RWMemory.all_simps
  MSPessTM_def
  TReader_def

lemmas unfold_eff_txn_ver =
  txn_ver_def eff_txn_ver_def

lemmas unfold_txn_ver_states =
  txn_ver_states_def unfold_eff_txn_ver

definition clean_txn_ver :: "State \<Rightarrow> T \<Rightarrow> PC Event \<Rightarrow> bool"
  where
  "clean_txn_ver s t e \<equiv>
   mspesstm_pre s t e \<and>
   e \<notin> {External BeginInv, External CommitResp,
        Internal read_begin1, Internal read_begin2, Internal read_begin3,
        Internal write_begin1, Internal write_begin2,
        Internal write_begin3, Internal write_begin4, Internal write_begin5,
        Internal write_begin6,
        Internal write_commit3a, Internal write_commit4, (* Internal write_commit5, *) 
        Internal write_commit6, (*, Internal write_commit7 *)
        Internal write_commit8,
        Internal write_commit9,
        Internal write_commit10,
        Internal write_commit11,
        Internal write_commit12,
        Internal write_commit13,
        Internal write_commit14}"

lemma txn_inv_implies_txn_ver_leq_global_ver:
  assumes tinv: "txn_inv a s t" and
          clean: "clean_txn_ver s t a"
     shows "txn_ver s t \<le> global_version s"
using assms
apply(cases rule: Event_split[where b=a],
            simp_all add: unfold_txn_props unfold_txn_ver_states
                          all_simps clean_txn_ver_def
                     split: option2.split if_splits Status.split Event.split PC.split)
by (smt lessI less_SucI less_or_eq_imp_le option2.simps)+

lemma shared_inv_preserved:
  assumes "txn_inv a s at" and
          "shared_inv s" and
          "mspesstm_pre s at a"
  shows "shared_inv (mspesstm_eff s at a)"
proof -
 {fix ia
  assume c: "a = Internal ia"
  from c assms have ?thesis
 apply(cases ia)
 apply(simp_all add: all_simps shared_inv_def)
 apply(simp_all add: all_simps shared_inv_def unfold_txn_props split: option.split)
by (metis even_Suc)
}
moreover
 {fix ea
  assume c: "a = External ea"
  from c assms have ?thesis
  by (cases ea, auto simp add: all_simps
             shared_inv_def txn_inv_def)}
ultimately show ?thesis using assms apply(cases rule: Event_split[where b=a]) by blast+
qed

lemma glb_clear_stable_other:
  assumes "txn_inv a s at" and
          "mspesstm_pre s at a" and
          "global_version (mspesstm_eff s at a) = global_version s" and
          "glb_cleared s t" and
          "t \<noteq> at"
   shows "glb_cleared (mspesstm_eff s at a) t"
using assms apply(cases rule: Event_split[where b=a])
apply(simp_all add: all_simps txn_inv_def glb_cleared_def unfold_eff_txn_ver
            txn_ver_states_def
            split: if_splits option.split option2.split)
by fastforce+

lemma glb_clear_stable_self:
  assumes "txn_inv a s at" and
          "shared_inv s" and
          "mspesstm_pre s at a" and
          "committing_writer s \<noteq> None" and
          "global_version (mspesstm_eff s at a) = global_version s" and
          "glb_cleared s at"
   shows "glb_cleared (mspesstm_eff s at a) at"
using assms apply(cases rule: Event_split[where b=a])
apply(simp_all add: all_simps
            shared_inv_def unfold_txn_props glb_cleared_def unfold_eff_txn_ver
            txn_ver_states_def
            split: if_splits option.split option2.split)
by force+

lemma glb_clear_stable:
  assumes "txn_inv a s at" and
          "shared_inv s" and
          "mspesstm_pre s at a" and
          "committing_writer s = Some c" and
          "global_version (mspesstm_eff s at a) = global_version s"
   shows "\<forall> t. c \<noteq> t \<and> glb_cleared s t \<longrightarrow> glb_cleared (mspesstm_eff s at a) t"
using assms glb_clear_stable_self glb_clear_stable_other
by fastforce

lemma Committed_implies_not_enabled:
  "mspesstm_pre s t e \<Longrightarrow> status s t \<noteq> Committed"
by(cases rule: Event_split[where b=e], auto simp add: all_simps)

lemma R_guaranteed:
  assumes "txn_inv a s at" and
          "shared_inv s" and
          "mspesstm_pre s at a" and
          "t \<noteq> at"
  shows "R t s (mspesstm_eff s at a)"
proof -
 {assume c: "a = Internal write_commit1 \<and> odd(txn_ver s at)"
  from c assms have ?thesis
  by(simp  add: all_simps R_def shared_inv_def unfold_txn_props glb_cleared_def
              split: option.split option2.split)
}
moreover
 {assume c: "a = Internal write_commit1 \<and> even(txn_ver s at) \<and> committing_writer s = None"
  from c assms have ?thesis
 by(simp  add: all_simps R_def shared_inv_def unfold_txn_props glb_cleared_def
              split: option.split option2.split)
}
moreover
 {assume c: "a = Internal write_commit1 \<and> even(txn_ver s at) \<and> committing_writer s \<noteq> None"
  from c assms have ?thesis
 by(simp  add: all_simps R_def shared_inv_def unfold_txn_props glb_cleared_def unfold_eff_txn_ver
              split: option.split option2.split)
}
(* moreover
 {assume c: "a = Internal write_commit3"
  from c assms have ?thesis
  by(auto simp  add: all_simps R_def shared_inv_def unfold_txn_props
                     unfold_eff_txn_ver
              split: option.split)
}
 *)moreover
 {assume c: "a = Internal write_commit13"
  have ev: "eff_txn_ver s t = global_version s"
  using assms c
  by(auto simp add: unfold_txn_props all_simps glb_cleared_def shared_inv_def)
  
  from c ev assms have ?thesis
  by(simp  add: all_simps R_def shared_inv_def unfold_txn_props
                     unfold_eff_txn_ver
              split: option.split if_splits)
}
moreover
 {fix l
  assume c: "a = Internal (write_read4 l) \<and> committing_writer s = None"
  from c assms have ?thesis
  by(simp  add: all_simps R_def  unfold_txn_props
              split: option.split)
}
moreover
 {fix l
  assume c: "a = Internal (write_read4 l) \<and> committing_writer s \<noteq> None"
  from c assms glb_clear_stable[where s=s and c=t and a=a and at=at] have ?thesis
  by(auto simp  add: all_simps R_def shared_inv_def txn_inv_def
              split: option.split if_splits)
}
moreover
 {assume c: "a = Internal write_commit2 \<and> committing_writer s = None"
  from c assms have ?thesis
  by(simp add: all_simps R_def shared_inv_def split: option.split)
}
moreover
 {assume c: "a = Internal write_commit2 \<and> committing_writer s \<noteq> None"
  have cw: "committing_writer s \<noteq> Some at" using c assms by (simp add: all_simps unfold_txn_props)
  from c cw assms have ?thesis
  using glb_clear_stable[where s=s and c=t and a=a and at=at]
  apply clarsimp
  by(simp add: all_simps R_def shared_inv_def txn_inv_def)
}
moreover
 {assume c: "a = Internal write_commit4"
  from c assms have ?thesis
  by(simp add: all_simps R_def shared_inv_def 
               unfold_txn_props glb_cleared_def unfold_eff_txn_ver split: option.split)
}
moreover
 {assume c: "a = Internal write_commit9"
  from c assms have ?thesis
  by(simp add: all_simps R_def shared_inv_def
               unfold_txn_props glb_cleared_def unfold_eff_txn_ver split: option.split)
}
moreover
 {assume c: "a = Internal write_commit10"
  from c assms have ?thesis
  by(simp add: all_simps R_def shared_inv_def unfold_txn_props
               glb_cleared_def unfold_eff_txn_ver split: option.split)
}
moreover
 {assume c: "a = Internal write_commit11"
  from c assms have ?thesis
  by(simp add: all_simps R_def shared_inv_def unfold_txn_props
               glb_cleared_def unfold_eff_txn_ver split: option.split)
}
moreover
 {assume c: "a = Internal write_commit12"
  from c assms have ?thesis
  by(simp add: all_simps R_def shared_inv_def unfold_txn_props glb_cleared_def
                unfold_eff_txn_ver split: option.split)
}
moreover
 {fix ia
  assume c: "a = Internal ia \<and> ia \<notin> {write_commit1, write_commit2, (* write_commit3, *) write_commit4, write_commit9, write_commit10, write_commit11, write_commit12, write_commit13}
                                    \<union> {ia . \<exists> l. ia = write_read4 l}"
  from c assms not_None_eq glb_clear_stable[where c=t and s=s and a=a and at=at] have ?thesis
  apply clarsimp
  apply(cases ia)
  by(simp_all add: all_simps R_def shared_inv_def txn_inv_def
                 split: option2.split option.split)
}
moreover
 {fix ea
  assume c: "a = External ea"
  from c assms glb_clear_stable[where c=t and s=s and a=a and at=at] have ?thesis
 by(clarsimp, cases ea, simp_all add: all_simps txn_inv_def shared_inv_def R_def)
}
ultimately show ?thesis using assms apply(cases rule: Event_split[where b=a])
by blast+
qed


lemma R_preserves_txn_ver_states:
  assumes tver: "txn_ver_states s t" and
          r:  "R t s s'"
  shows "txn_ver_states s' t"
proof -
{assume c: "(eff_txn_ver s t = global_version s \<or> 1 + eff_txn_ver s t = global_version s)
           \<and> committing_writer s' = committing_writer s"
 from r c
 have ?thesis 
 apply(simp add: R_def unfold_txn_ver_states split: option2.split if_splits)
 by (smt Suc_leD not_less_eq_eq option2.simps option2.simps)+
}
moreover
{assume c: "(eff_txn_ver s t = global_version s \<or> 1 + eff_txn_ver s t = global_version s)
           \<and> committing_writer s' \<noteq> committing_writer s \<and> committing_writer s' = None"
 from r c
 have ?thesis 
 apply(simp add: R_def unfold_txn_ver_states split: option2.split if_splits)
apply (metis Suc_n_not_le_n nat_le_linear option2.simps(10) option2.simps(8))
apply (metis Suc_n_not_le_n nat_le_linear option2.simps(8) option2.simps(9))
by force
}
moreover
{assume c: "(eff_txn_ver s t = global_version s \<or> 1 + eff_txn_ver s t = global_version s)
           \<and> committing_writer s' \<noteq> committing_writer s \<and> committing_writer s' \<noteq> None"
 from r c
 have ?thesis 
 apply(simp add: R_def unfold_txn_ver_states split: option2.split if_splits)
by (smt le_Suc_eq nat_le_linear option2.simps option2.simps)+
}
moreover
{assume c: "2 + eff_txn_ver s t = global_version s"
 from r c
 have ?thesis 
 apply(simp add: R_def unfold_txn_ver_states split: if_splits option2.split)
by (smt c nat_le_linear not_None_eq not_le not_less_eq_eq option2.distinct
       option2.simps tver txn_ver_states_def)+
}
ultimately show ?thesis using r tver 
apply(simp add: txn_ver_states_def)
by fastforce
qed

lemma R_preserves_is_waiting_writer:
  assumes "is_waiting_writer s t" and
          "R t s s'"
  shows "is_waiting_writer s' t"
using assms
by(auto simp add: unfold_eff_txn_ver R_def unfold_txn_props)

lemma R_preserves_is_active_writer:
  assumes "is_active_writer s t" and
          "R t s s'"
  shows "is_active_writer s' t"
using assms R_preserves_txn_ver_states[where s=s and s'=s' and t=t]
apply(simp add: R_def)
by (smt is_active_writer_def less_irrefl not_None_eq txn_ver_def)

lemma R_preserves_is_intermediate_writer:
  assumes "is_intermediate_writer s t" and
          "R t s s'"
  shows "is_intermediate_writer s' t"
using assms
by(auto simp add: unfold_eff_txn_ver R_def unfold_txn_props)


lemma R_preserves_write_set_ver:
  assumes "committing_writer s = Some t"
          "write_set_ver s t n" and
          "R t s s'"
  shows "write_set_ver s' t n"
using assms
by(auto simp add: unfold_eff_txn_ver R_def unfold_txn_props write_set_ver_def)


lemma R_preserves_is_active_reader:
  assumes "is_active_reader s t" and
          "R t s s'"
  shows "is_active_reader s' t"
using assms R_preserves_txn_ver_states[where s=s and s'=s' and t=t]
apply(simp add: R_def)
by (metis is_active_reader_def less_SucI txn_ver_def)

lemma R_implies_global_version_steps:
  assumes "R t s s'"
    shows "(global_version s' = global_version s \<or>
            global_version s' = 1 + global_version s)"
using assms by(auto simp add: R_def)

lemma R_preserves_inv:
  assumes "txn_inv a s t" and
          "R t s s'"
  shows "txn_inv a s' t"
proof -
(* {assume c: "a = Internal write_commit5"
  have ?thesis
  using c assms
  apply(simp add: unfold_txn_props all_simps R_def unfold_eff_txn_ver write_set_ver_def)
by force
}
moreover
*)
 {assume c: "a = Internal write_commit6"
  have ?thesis
  using c assms
  by(simp add: unfold_txn_props all_simps R_def unfold_eff_txn_ver write_set_ver_def)
}
moreover
 {fix ia
  assume c: "a = Internal ia \<and>
            ia : {write_commit0, write_commit1, write_commit2, write_commit3a, (* write_commit3, *)
                  write_commit4}"

  from c assms
  R_preserves_is_active_writer[where s=s and s'=s' and t=t]
  R_preserves_write_set_ver[where s=s and s'=s' and t=t]
  R_implies_global_version_steps[where s=s and s'=s' and t=t]
  le_Suc_eq
  have ?thesis
 apply(cases ia)
 apply(simp_all add: txn_inv_def mspesstm_pre_def tau_pre_def txn_ver_def R_def)
by force+
}
moreover
 {fix ia
  assume c: "a = Internal ia \<and>
            ia : {write_commit8, write_commit9,
                  write_commit10, write_commit11, write_commit12,
                  write_commit13}"
  from c assms
  R_preserves_is_intermediate_writer[where s=s and s'=s' and t=t]
  R_preserves_write_set_ver[where s=s and s'=s' and t=t]
  R_implies_global_version_steps[where s=s and s'=s' and t=t]
  le_Suc_eq
  have ?thesis
 apply(cases ia)
 apply(simp_all add: txn_inv_def mspesstm_pre_def tau_pre_def txn_ver_def R_def)
using is_intermediate_writer_def by force+
}
moreover
{fix l
 assume c: "a = Internal (read_read5 l)"
 from c assms
  R_preserves_is_active_reader[where s=s and s'=s' and t=t]
 have ?thesis
apply(simp add: all_simps R_def txn_inv_def unfold_eff_txn_ver)
apply (simp add: eff_txn_ver_def is_active_reader_def txn_ver_states_def)
by (smt leD option2.case_eq_if option2.exhaust_disc)
}
moreover
{fix l
 assume c: "a = Internal (write_read6 l)"
 from c assms
  R_preserves_is_active_writer[where s=s and s'=s' and t=t]
 have ?thesis
apply(simp add: all_simps R_def txn_inv_def unfold_eff_txn_ver)
by (metis (no_types, lifting) is_active_writer_def)
}
moreover
{assume c: "a = Internal write_begin4"
 from c assms
  R_preserves_is_waiting_writer[where s=s and s'=s' and t=t]
 have ?thesis
apply (simp add: all_simps txn_inv_def, clarsimp)
by(simp add: R_def)
}
moreover
{assume c: "a = Internal write_begin6"
 from c assms
  R_preserves_is_waiting_writer[where s=s and s'=s' and t=t]
 have ?thesis
apply (simp add: all_simps txn_inv_def, clarsimp)
apply(simp add: R_def)
by (metis R_preserves_txn_ver_states assms(2) is_waiting_writer_def not_None_eq)
}
moreover
{fix l
 assume c: "a = Internal (read_read4 l)"
 from c assms
 have ?thesis
apply (simp add: all_simps txn_inv_def, clarsimp)
apply(simp add: R_def)
by (metis R_preserves_is_active_reader assms(2) less_SucI txn_ver_def)
}
moreover
{fix l
 assume c: "a = Internal (write_read5 l)"
 from c assms
 have ?thesis
apply (simp add: all_simps txn_inv_def, clarsimp)
apply(simp add: R_def)
by (metis R_preserves_is_active_writer assms(2) is_active_writer_def less_not_refl3 txn_ver_def)
}
moreover
{assume c: "a = Internal write_commit14"
 from c assms
 have ?thesis
by (simp add: all_simps txn_inv_def)
}
moreover
 {fix ia
  assume c: "a = Internal ia \<and>
            ia \<notin> {write_begin4, write_begin6,
                  write_commit0, write_commit1, write_commit2, write_commit3a, (* write_commit3, *)
                  write_commit4, (* write_commit5,*) write_commit6,
                  (*write_commit7,*) write_commit8, write_commit9,
                  write_commit10, write_commit11, write_commit12,
                  write_commit13, write_commit14}
                 \<union>
                 {ia . \<exists> l. ia = read_read4 l \<or> ia = read_read5 l \<or> ia = write_read5 l \<or> ia = write_read6 l}"
  from c assms
  R_preserves_txn_ver_states[where s=s and s'=s' and t=t]
  R_preserves_is_active_writer[where s=s and s'=s' and t=t]
  R_preserves_is_active_reader[where s=s and s'=s' and t=t]
  R_preserves_is_waiting_writer[where s=s and s'=s' and t=t]
  R_implies_global_version_steps[where s=s and s'=s' and t=t]
  le_Suc_eq
  have ?thesis
  apply(cases ia)
  apply(simp add: all_simps R_def txn_inv_def)
  apply(simp add: all_simps R_def txn_inv_def)
  apply(simp add: all_simps R_def txn_inv_def)
  by(force simp add: all_simps R_def txn_inv_def unfold_eff_txn_ver txn_ver_states_def)+
}
moreover
 {fix ea
  assume c: "a = External ea"
  from c assms
  R_preserves_txn_ver_states[where s=s and s'=s' and t=t]
  R_preserves_is_active_writer[where s=s and s'=s' and t=t]
  R_preserves_is_active_reader[where s=s and s'=s' and t=t]
  R_preserves_is_waiting_writer[where s=s and s'=s' and t=t]
  R_preserves_write_set_ver[where s=s and s'=s' and t=t]
  R_preserves_is_intermediate_writer[where s=s and s'=s' and t=t]
  have ?thesis
 apply(cases ea)
apply(simp add: ext_pre_def mspesstm_pre_def R_def txn_inv_def is_active_txn_def)
apply(simp add: ext_pre_def mspesstm_pre_def R_def txn_inv_def is_active_txn_def, metis)
apply(simp add: ext_pre_def mspesstm_pre_def R_def txn_inv_def is_active_txn_def, metis)
apply(simp add: ext_pre_def mspesstm_pre_def R_def txn_inv_def is_active_txn_def)
apply(simp add: ext_pre_def mspesstm_pre_def txn_inv_def)
apply(simp add: ext_pre_def mspesstm_pre_def txn_inv_def)
apply(simp add: ext_pre_def mspesstm_pre_def R_def txn_inv_def is_active_txn_def, metis)
apply(simp add: ext_pre_def mspesstm_pre_def R_def txn_inv_def is_active_txn_def, metis)
apply(simp add: ext_pre_def mspesstm_pre_def R_def txn_inv_def is_active_txn_def, metis)
apply(simp add: ext_pre_def mspesstm_pre_def R_def txn_inv_def is_active_txn_def, metis)
done
}
ultimately show ?thesis using assms apply(cases rule: Event_split[where b=a]) by blast+
qed

lemma txn_ver_states_preserved_self:
  assumes pre: "mspesstm_pre s at a" and
          shared: "shared_inv s" and
          txn: "txn_inv a s at" and
          act: "a \<notin> {External BeginInv, External CommitResp,
                     Internal read_begin1, Internal read_begin2,
                     Internal write_begin1, Internal write_begin2,
                     Internal write_begin3, Internal write_begin4,Internal write_begin5,
                     Internal read_commit1,
                     Internal write_commit4, (*Internal write_commit5,*)
                     Internal write_commit6, (*Internal write_commit7,*)Internal write_commit8,
                     Internal write_commit9,Internal write_commit10,Internal write_commit11,
                     Internal write_commit12,Internal write_commit13,Internal write_commit14}" and
         tver: "txn_ver_states s at"
   shows "txn_ver_states (mspesstm_eff s at a) at"
proof -
{fix l
 assume c: "a = Internal (read_read3 l)"
  from c assms
  have ?thesis
 by(simp add: unfold_eff_txn_ver shared_inv_def all_simps txn_ver_states_def txn_inv_def is_active_txn_def
                            split: if_splits option2.split)
   fastforce
}
moreover
{fix l
 assume c: "a = Internal (write_read1 l) \<and> write_set s at l \<noteq> None"
  from c assms
  have ?thesis
  by (simp add: txn_ver_states_def all_simps unfold_eff_txn_ver unfold_txn_props
                 split: if_splits option2.splits)
}
moreover
{fix l
 assume c: "a = Internal (write_read1 l) \<and> write_set s at l = None"
  from c assms
  have ?thesis
  apply (simp add: txn_ver_states_def all_simps unfold_eff_txn_ver unfold_txn_props
                 split: if_splits)
by (smt Suc_n_not_le_n option2.simps(8) option2.split_sels(2))
}
moreover
{fix l
 assume c: "a = Internal (write_read4 l)"
  from c assms
  have ?thesis
 by(simp add: unfold_eff_txn_ver shared_inv_def all_simps txn_ver_states_def txn_inv_def is_active_txn_def
                            split: if_splits option2.split)
   fastforce
}
moreover
{assume c: "a = Internal write_commit1 \<and> odd(txn_ver s at)"
  from c assms
  have ?thesis
 apply(simp add: unfold_eff_txn_ver shared_inv_def all_simps txn_ver_states_def txn_inv_def is_active_txn_def
                            split: if_splits option2.split)
by force
}
moreover
{assume c: "a = Internal write_commit1 \<and> even(txn_ver s at)"
  from c assms
  have ?thesis
 apply(simp add: unfold_eff_txn_ver shared_inv_def all_simps txn_ver_states_def txn_inv_def is_active_txn_def
                            split: if_splits option2.split)
by force
}
moreover
{assume c: "a = Internal write_commit2"
  from c assms
  have ?thesis
 by(simp add: unfold_eff_txn_ver shared_inv_def all_simps txn_ver_states_def txn_inv_def is_active_txn_def
                            split: if_splits option2.split)
   fastforce
}
(* moreover
{assume c: "a = Internal write_commit3"
  from c assms
  have ?thesis
 by(simp add: unfold_eff_txn_ver shared_inv_def all_simps txn_ver_states_def txn_inv_def is_active_txn_def
                            split: if_splits option2.split)
}
 *)
moreover
 {fix ia
  assume c: "a = Internal ia \<and> ia \<notin> {write_commit1, write_commit2(* , write_commit3 *)}
                                   \<union> {ia . \<exists> l. ia = write_read1 l \<or> ia = write_read4 l \<or> ia = read_read3 l}"
  have etv: "eff_txn_ver (mspesstm_eff s at a) at = eff_txn_ver s at"
    using c assms
     by(cases ia, simp_all add: unfold_eff_txn_ver shared_inv_def all_simps
                             txn_ver_states_def unfold_txn_props
                            split: if_splits option2.split Action.split PC.split)
  from c assms etv
  have ?thesis
  apply(cases ia, simp_all add: unfold_eff_txn_ver shared_inv_def all_simps
                             txn_ver_states_def unfold_txn_props
                            split: if_splits option2.split)
by (smt option.exhaust option2.simps)+
}
moreover
 {fix ea
  assume c: "a = External ea"
  have etv: "eff_txn_ver (mspesstm_eff s at a) at = eff_txn_ver s at"
    using c assms
     by(cases ea, simp_all add: unfold_eff_txn_ver shared_inv_def all_simps
                             txn_ver_states_def unfold_txn_props
                            split: if_splits option2.split Action.split PC.split)
  from c etv assms
  have ?thesis
 by(cases ea, simp_all add: all_simps txn_ver_states_def txn_inv_def is_active_txn_def split: option2.split)
}
ultimately show ?thesis using assms apply(cases rule: Event_split[where b=a])
by blast+
qed

lemma not_idle_reading_committed_implies_glb_cleared:
  assumes "txn_version s t \<notin> {Idle, Reading}" and
          "status s t \<noteq> Committed" and
          cw: "committing_writer s = Some c" and
          cneq: "c \<noteq> t" and
          q: "quiescent_for (txn_version s t) (global_version s)" and
          "txn_inv a s t" and
          "mspesstm_pre s t a"
     shows "glb_cleared s t"
using assms
apply(cases rule: Event_split[where b=a],
       simp_all add: quiescent_for_def glb_cleared_def unfold_eff_txn_ver unfold_txn_props
                  all_simps shared_inv_def txn_ver_states_def split: if_splits)
by (smt Suc_n_not_le_n le_Suc_eq option2.simps option2.split_sels)+


lemma glb_cleared_when:
    assumes txn: "\<forall> a. txn_inv a s t" and
            shared: "shared_inv s" and
            cw: "committing_writer s = Some c" and
            cneq: "c \<noteq> t" and
            q: "quiescent_for (txn_version s t) (global_version s)"
     shows "glb_cleared s t"
proof -
{assume c: "txn_version s t = Idle \<and> status s t \<noteq> Pending write_begin6"
 have ?thesis
 using q c by(auto simp add: quiescent_for_def glb_cleared_def unfold_eff_txn_ver)
}
moreover
{assume c: "txn_version s t = Idle \<and> status s t = Pending write_begin6"
  have ttxn: "txn_inv (Internal write_begin6) s t" using txn by blast
 have ?thesis
 using q c ttxn cw cneq
 by(auto simp add: quiescent_for_def glb_cleared_def unfold_eff_txn_ver unfold_txn_props
                  all_simps txn_ver_states_def)
}
moreover
{assume c: "txn_version s t = Reading"
 have ?thesis
 using q c by(auto simp add: quiescent_for_def glb_cleared_def unfold_eff_txn_ver)
}
moreover
{assume c: "status s t = Committed"
 have ?thesis
 using q c shared by(auto simp add: shared_inv_def quiescent_for_def glb_cleared_def unfold_eff_txn_ver)
}
moreover
{assume c: "txn_version s t \<notin> {Idle, Reading} \<and> status s t \<noteq> Committed"

 have inv_true: "\<exists> a. mspesstm_pre s t a \<and> txn_inv a s t"
 using c txn
 apply(intro exI[where x=
   "case status s t of NotStarted \<Rightarrow> (External BeginInv) | Ready \<Rightarrow> (External CommitInv) | Pending tpc \<Rightarrow> (Internal tpc) | WriteResponding \<Rightarrow> (External WriteResp) | ReadResponding v \<Rightarrow> External(ReadResp v) | BeginResponding \<Rightarrow> External BeginResp | CommitResponding \<Rightarrow> External CommitResp"])
 by(auto simp add: all_simps split: PC.split Event.split Status.split)
 
 have ?thesis
 using assms c inv_true
 not_idle_reading_committed_implies_glb_cleared[where s=s and t=t and c=c]
 by blast
}
ultimately show ?thesis by blast
qed


lemma txn_inv_preserved_self:
  assumes pre: "mspesstm_pre s at a" and
          shared: "shared_inv s" and
          txn: "txn_inv a s at" and
         txnall: "\<forall> a t'. txn_inv a s t'"
   shows "txn_inv b (mspesstm_eff s at a) at"
proof -
 {assume c: "a = Internal write_commit0"
  from c pre shared txn
  have ?thesis
  using txn_ver_states_preserved_self[where s=s and at=at and a=a]
  by(simp add: unfold_eff_txn_ver all_simps unfold_txn_props shared_inv_def 
                   split: Action.split PC.split Event.split option.split option2.split)
}
moreover
 {assume c: "a = Internal write_commit1 \<and> odd(temp s at)"
  from c pre shared txn
  have ?thesis
  by(simp add: unfold_eff_txn_ver all_simps unfold_txn_props shared_inv_def txn_ver_states_def
                   split: Action.split PC.split Event.split option.split option2.split)
}
moreover
 {assume c: "a = Internal write_commit1 \<and> \<not> odd(temp s at)"
  have etv: "eff_txn_ver (mspesstm_eff s at a) at = eff_txn_ver s at"
    using c pre shared txn
     by(simp add: unfold_eff_txn_ver shared_inv_def all_simps
                             txn_ver_states_def unfold_txn_props
                            split: if_splits option2.split Action.split PC.split)
  from c etv pre shared txn
  have ?thesis
  by(simp add: unfold_eff_txn_ver all_simps unfold_txn_props shared_inv_def txn_ver_states_def
                   split: Action.split PC.split Event.split option.split option2.split)
}
moreover
 {assume c: "a = Internal write_commit2 \<and> temp s at = global_version s"
  from c pre shared txn
  have ?thesis
  apply(simp add: unfold_eff_txn_ver all_simps unfold_txn_props shared_inv_def txn_ver_states_def
                   split: Action.split PC.split Event.split option.split)
  by (smt less_irrefl)
}
moreover
 {assume c: "a = Internal write_commit2 \<and> temp s at \<noteq> global_version s"
  have etv: "eff_txn_ver (mspesstm_eff s at a) at = eff_txn_ver s at"
    using c pre shared txn
     by(simp add: unfold_eff_txn_ver shared_inv_def all_simps
                             txn_ver_states_def unfold_txn_props
                            split: if_splits option2.split Action.split PC.split)
  from c etv pre shared txn
  have ?thesis
  using txn_ver_states_preserved_self[where s=s and at=at and a=a]
  by(simp add:  all_simps unfold_txn_props shared_inv_def txn_ver_states_def unfold_eff_txn_ver
                   split: Action.split PC.split Event.split option.split)
}
moreover
 {assume c: "a = Internal write_commit6"
  from c pre shared txn
  have ?thesis
  by(auto simp add: unfold_eff_txn_ver all_simps unfold_txn_props shared_inv_def txn_ver_states_def
                   write_set_ver_def
                   split: Action.split PC.split Event.split option.split)
}
moreover
 {fix l
  assume c: "a = Internal (read_read3 l) \<and> txn_ver s at = global_version s"
  have etv: "eff_txn_ver (mspesstm_eff s at a) at = eff_txn_ver s at"
    using c pre shared txn
     by(simp add: unfold_eff_txn_ver shared_inv_def all_simps
                             txn_ver_states_def unfold_txn_props
                            split: if_splits option2.split Action.split PC.split)
  from c etv pre shared txn
  have ?thesis
  using txn_ver_states_preserved_self[where s=s and at=at and a=a]
  by(simp add: all_simps unfold_txn_props shared_inv_def txn_ver_states_def
                   split: Action.split PC.split Event.split option.split)
}
moreover
 {fix l
  assume c: "a = Internal (read_read3 l) \<and> txn_ver s at \<noteq> global_version s"
  have etv: "eff_txn_ver (mspesstm_eff s at a) at = eff_txn_ver s at"
    using c  pre shared txn
     by(simp add: unfold_eff_txn_ver shared_inv_def all_simps
                             txn_ver_states_def unfold_txn_props
                            split: if_splits option2.split Action.split PC.split)
  from c etv pre shared txn
  have ?thesis
  using txn_ver_states_preserved_self[where s=s and at=at and a=a]
  by(auto simp add: all_simps unfold_txn_props shared_inv_def txn_ver_states_def unfold_eff_txn_ver
                   split: Action.split PC.split Event.split option.split option2.split)
}
moreover
 {fix l
  assume c: "a = Internal (write_read4 l) \<and> txn_ver s at = global_version s"
  from c pre shared txn
  have ?thesis
  using txn_ver_states_preserved_self[where s=s and at=at and a=a]
  by(simp add: unfold_eff_txn_ver all_simps unfold_txn_props shared_inv_def
                   split: Action.split PC.split Event.split option.split)
}
moreover
 {fix l
  assume c: "a = Internal (write_read4 l) \<and> txn_ver s at \<noteq> global_version s"
  from c pre shared txn
  have ?thesis
  using txn_ver_states_preserved_self[where s=s and at=at and a=a]
  by(simp add: unfold_eff_txn_ver all_simps unfold_txn_props shared_inv_def txn_ver_states_def
                   split: Action.split PC.split Event.split option.split option2.split)
  force
}
moreover
 {assume c: "a = Internal write_commit12 \<and> pending_writes s at = {}"
  have ?thesis
  using c  pre shared txn glb_clear_stable[where s=s and c=at and a=a and at=at]
  by(simp add: unfold_eff_txn_ver all_simps unfold_txn_props shared_inv_def txn_ver_states_def
                 write_set_ver_def  
                 split: Action.split PC.split Event.split option.split option2.split)
}
moreover
 {assume c: "a = Internal write_commit12 \<and> pending_writes s at \<noteq> {}"
  have cm: "committing_writer s = Some at" using c pre shared txn
    by (simp add: all_simps txn_inv_def) 
  have gv: "global_version (mspesstm_eff s at a) = global_version s" using c assms
    by (simp add: all_simps txn_inv_def) 
  have ev: "eff_txn_ver (mspesstm_eff s at a) = eff_txn_ver s" using c assms
    by(auto simp add: all_simps txn_inv_def unfold_eff_txn_ver split: if_splits option2.split)
  have ?thesis
  using c pre shared txn cm ev gv glb_clear_stable[where s=s and c=at and a=a and at=at]
  by(simp add: unfold_eff_txn_ver all_simps unfold_txn_props shared_inv_def txn_ver_states_def
                 write_set_ver_def 
                 split: Action.split PC.split Event.split option.split option2.split)
}
moreover
 {assume c: "a = Internal read_begin2"
  from c pre shared txn
  have ?thesis
  by(auto simp add: unfold_eff_txn_ver all_simps unfold_txn_props shared_inv_def txn_ver_states_def
                   split: Action.split PC.split Event.split option.split)
}
moreover
 {assume c: "a = Internal write_begin5"
  from c pre shared txn
  have ?thesis
  by(auto simp add: unfold_eff_txn_ver all_simps unfold_txn_props shared_inv_def txn_ver_states_def
                   split: Action.split PC.split Event.split option.split)
}
moreover
 {assume c: "a = Internal write_begin6"
  from c pre shared txn
  have ?thesis
  apply(simp add: unfold_eff_txn_ver all_simps unfold_txn_props shared_inv_def txn_ver_states_def
                   split: Action.split PC.split Event.split option.split)
by (metis (full_types) even_Suc le_SucE le_antisym option2.simps(10))
}
moreover
 {fix l
  assume c: "a = Internal (read_read1 l)"
  from c pre shared txn
  have ?thesis
  using txn_ver_states_preserved_self[where s=s and at=at and a=a]
  by(cases "progress_seen s at", auto simp add: unfold_eff_txn_ver all_simps unfold_txn_props shared_inv_def txn_ver_states_def
                   split: Action.split PC.split Event.split option.split)
}
moreover
 {fix l
  assume c: "a = Internal (read_read2 l)"
  from c pre shared txn
  have ?thesis
  using txn_ver_states_preserved_self[where s=s and at=at and a=a]
  by(cases "version s l = txn_ver s at", auto simp add: unfold_eff_txn_ver all_simps unfold_txn_props shared_inv_def txn_ver_states_def
                   split: Action.split PC.split Event.split option.split)
}
moreover
 {fix l
  assume c: "a = Internal (read_read5 l)"
  from c pre shared txn
  have ?thesis
  using txn_ver_states_preserved_self[where s=s and at=at and a=a]
  by(auto simp add: unfold_eff_txn_ver all_simps unfold_txn_props shared_inv_def txn_ver_states_def
                   split: Action.split PC.split Event.split option.split)
}
moreover
 {fix l
  assume c: "a = Internal (write_read2 l)"
  from c pre shared txn
  have ?thesis
  using txn_ver_states_preserved_self[where s=s and at=at and a=a]
  by(cases "progress_seen s at", auto simp add: unfold_eff_txn_ver all_simps unfold_txn_props shared_inv_def txn_ver_states_def
                   split: Action.split PC.split Event.split option.split)
}
moreover
 {fix l
  assume c: "a = Internal (write_read3 l)"
  from c pre shared txn
  have ?thesis
  using txn_ver_states_preserved_self[where s=s and at=at and a=a]
  by(cases "version s l = txn_ver s at", auto simp add: unfold_eff_txn_ver all_simps unfold_txn_props shared_inv_def txn_ver_states_def
                   split: Action.split PC.split Event.split option.split)
}
moreover
 {assume c: "a = Internal write_commit11 \<and> (\<forall> t'. quiescent_for (txn_version s t') (1 + temp s at))"

 have cw: "committing_writer s = Some at"
 using txn pre c by(auto simp add: txn_inv_def unfold_mspesstm)

 have temp: "1 + temp s at = global_version s"
 using txn pre c by(auto simp add: txn_inv_def unfold_mspesstm)

 from c pre shared txnall cw temp glb_cleared_when[where s=s and c=at]
  have gc: "\<forall> t'. at \<noteq> t' \<longrightarrow> glb_cleared s t'"
  by simp

  from c pre gc
  have gc_post: "\<forall> t'. at \<noteq> t' \<longrightarrow> glb_cleared (mspesstm_eff s at a) t'"
  by(auto simp add: unfold_mspesstm glb_cleared_def unfold_eff_txn_ver split: option2.split)


  from c pre shared txn cw gc gc_post
  have ?thesis
  by(cases rule: Event_split[where b=b], auto simp add: all_simps unfold_txn_props
              txn_ver_def write_set_ver_def split: option.split)
}
moreover
 {fix l
  assume c: "a = Internal write_commit11 \<and> \<not> (\<forall> t' . quiescent_for (txn_version s t') (1 + temp s at))"
  from c pre shared txn
  have ?thesis
  apply(auto simp add: unfold_eff_txn_ver all_simps unfold_txn_props shared_inv_def txn_ver_states_def
                 glb_cleared_def quiescent_for_def  
                 split: Action.split PC.split Event.split option.split)
 by metis+
}
moreover
 {fix ia
  assume c: "a = Internal ia \<and>
            ia : {write_commit3a, (* write_commit3, *)
                  write_commit4, (*write_commit5,*)
                  (*write_commit7,*) write_commit8, write_commit9,
                  write_commit10,
                  write_commit13, write_commit14}"
  from c pre shared txn
  have ?thesis
  using txn_ver_states_preserved_self[where s=s and at=at and a=a]
  by(cases ia, simp_all add: unfold_eff_txn_ver all_simps unfold_txn_props
                   write_set_ver_def shared_inv_def
                   split: Action.split PC.split Event.split option.split)
}
moreover
 {assume c: "a = Internal read_commit1"
  from c pre shared txn
  have ?thesis
  using txn_ver_states_preserved_self[where s=s and at=at and a=a]
  by(simp add: unfold_eff_txn_ver all_simps unfold_txn_props shared_inv_def
                   split: Action.split PC.split Event.split option.split option2.split)
}
moreover
 {fix l v
  assume c: "a = Internal (write_write1 l v)"
  from c pre shared txn
  have ?thesis
  using txn_ver_states_preserved_self[where s=s and at=at and a=a]
  by(simp add: unfold_eff_txn_ver all_simps unfold_txn_props shared_inv_def
                   split: Action.split PC.split Event.split option.split option2.split)
}
moreover
 {fix ia
  assume c: "a = Internal ia \<and> ia : {read_begin1, read_begin3,
                                     write_begin1, write_begin2, write_begin3, write_begin4}"
  from c  pre shared txn
  have ?thesis
  using txn_ver_states_preserved_self[where s=s and at=at and a=a]
  by(cases ia, simp_all add: unfold_eff_txn_ver all_simps unfold_txn_props shared_inv_def
                   split: Action.split PC.split Event.split option.split option2.split)
}
moreover
 {fix ia l
  assume c: "a = Internal ia \<and> ia : {read_read4 l,
                                     write_read1 l, write_read5 l, write_read6 l}"
  from c  pre shared txn
  have ?thesis
  using txn_ver_states_preserved_self[where s=s and at=at and a=a]
  apply(cases ia, simp_all add: unfold_eff_txn_ver all_simps unfold_txn_props shared_inv_def
                   split: Action.split PC.split Event.split option.split option2.split)
by blast
}
moreover
 {fix ea
  assume c: "a = External ea"
  have etv: "eff_txn_ver (mspesstm_eff s at a) at = eff_txn_ver s at"
    using c pre shared txn
     by(cases ea, simp_all add: unfold_eff_txn_ver all_simps
                             txn_ver_states_def unfold_txn_props
                            split: if_splits option2.split Action.split PC.split)
  from c pre shared txn etv
  have ?thesis
  apply(cases ea)
apply(simp_all add: unfold_eff_txn_ver all_simps unfold_txn_props txn_ver_states_def
                   split: Action.split PC.split Event.split option2.split if_splits)
by force+
}
ultimately show ?thesis
apply(cases rule: Event_split[where b=a])
apply simp_all
by blast+
qed

definition remaining_writes :: "MSPessTM.State \<Rightarrow> L \<Rightarrow> V option"
  where
  "remaining_writes s \<equiv>
    case (committing_writer s) of Some t \<Rightarrow> MSPessTM.write_set s t | None \<Rightarrow> Map.empty"

definition writeback_pc :: "PC set"
  where
  "writeback_pc \<equiv> {(*write_commit7,*) write_commit8,
                   write_commit9, write_commit10,
                   write_commit11, write_commit12}"


lemma writeback_locked:
  assumes inv: "txn_inv (Internal ia) s c" and
           ia: "ia : writeback_pc" and
            cw: "committing_writer s = Some c" and
           stat: "status s c = Pending ia"
    shows "\<forall> l \<in> dom (write_set s c) . version s l = global_version s"
using assms
by(cases ia, simp_all add:  writeback_pc_def txn_inv_def
                write_set_ver_def all_simps)


lemma read_value_not_locked_for_recent_txn:
  assumes shared: "shared_inv s" and
         tinv: "txn_inv (Internal tpc) s t" and
          tpc: "tpc: {read_read5 l, write_read6 l}" and
         tstat: "status s t = Pending tpc" and
          tver: "txn_ver s t = global_version s" and
          cinv: "txn_inv (Internal cpc) s c" and
           cpc: "cpc : writeback_pc" and
          cstat: "status s c = Pending cpc" and
            cw: "committing_writer s = Some c"
    shows "l \<notin> dom (write_set s c)"
proof -

have ldom: "\<forall> l \<in> dom (write_set s c) . version s l = global_version s"
using assms writeback_locked[where s=s and ia=cpc and c=c]
by (simp add: writeback_pc_def)

have etv: "eff_txn_ver s t = txn_ver s t"
using tinv tpc tstat
by (cases tpc, simp_all add: unfold_eff_txn_ver unfold_txn_props all_simps
                     split: option2.split)

have even: "even (global_version s)"
using cinv cw cstat cpc shared
by (cases cpc, simp_all add: all_simps unfold_txn_props writeback_pc_def shared_inv_def)

show ?thesis
using tinv tpc tstat cw ldom etv even tver
apply clarsimp
apply(cases tpc, simp_all add:  writeback_pc_def unfold_txn_props
                write_set_ver_def all_simps txn_ver_states_def unfold_eff_txn_ver
                 split: option2.split if_splits)
by blast+
qed


lemma read_value_not_locked:
  assumes shared: "shared_inv s" and
          tinv: "txn_inv (Internal tpc) s t" and
          tpc: "tpc: {read_read5 l, write_read6 l}" and
         tstat: "status s t = Pending tpc" and
          cinv: "txn_inv (Internal write_commit12) s c" and
          cstat: "status s c = Pending write_commit12" and
            cw: "committing_writer s = Some c"
    shows "l \<notin> dom (write_set s c)"
proof -

have etv: "eff_txn_ver s t = txn_ver s t"
using tinv tpc tstat
by (cases tpc, simp_all add: unfold_eff_txn_ver unfold_txn_props all_simps
                     split: option2.split)

have tver : "txn_ver s t = global_version s"
using cinv cstat etv cw tstat tpc
apply (auto simp add: all_simps unfold_txn_props writeback_pc_def
                             txn_ver_states_def glb_cleared_def eff_txn_ver_def)
apply (metis MSPessTM.Status.inject(1) PC.distinct(765))
by (metis MSPessTM.Status.inject(1) PC.distinct(975))
show ?thesis
using tver assms
read_value_not_locked_for_recent_txn[where s=s and t=t and c=c]
by (auto simp add: writeback_pc_def)
qed

(*
ow_inv_initial aut shared txn;
    ow_inv_preserved_shared aut shared txn;
    ow_inv_preserved_self aut shared txn;
    ow_inv_preserved_other aut shared txn R;
    ow_inv_guarantee aut shared txn R
*)

lemma txn_inv_initial:
  assumes "Transitions.start MSPessTM s"
    shows "txn_inv e s t"
using assms
by(cases rule: Event_split[where b=e],
      simp_all add: txn_inv_def all_simps)

theorem MSPessTM_invariant:
  "invariant (ioa MSPessTM) (\<lambda> s. shared_inv s \<and> (\<forall> t e. txn_inv e s t))"
proof -
 have initial: "ow_inv_initial MSPessTM shared_inv txn_inv"
 using txn_inv_initial
 by(auto simp add: ow_inv_initial_def all_simps shared_inv_def mem_initial_def
                      txn_inv_def split: Action.split PC.split Status.split)

 have pres_shared: "ow_inv_preserved_shared MSPessTM shared_inv txn_inv"
 using shared_inv_preserved by(auto simp add: MSPessTM_def ow_inv_preserved_shared_def)

 have pres_self: "ow_inv_preserved_self MSPessTM shared_inv txn_inv"
 using txn_inv_preserved_self 
 by(auto simp add: MSPessTM_def ow_inv_preserved_self_def)

 have pres_other: "ow_inv_preserved_other MSPessTM shared_inv txn_inv R"
 using R_preserves_inv
 by(auto simp add: MSPessTM_def ow_inv_preserved_other_def)

 have guarantee: "ow_inv_guarantee MSPessTM shared_inv txn_inv R"
 apply(unfold ow_inv_guarantee_def, clarsimp)
 using R_guaranteed Committed_implies_not_enabled
 by(auto simp add: MSPessTM_def)

show ?thesis using initial pres_shared pres_self pres_other guarantee ow_inv_sound
by blast
qed

end
