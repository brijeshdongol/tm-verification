theory Utilities
imports Main
begin

definition rev_app :: "'s \<Rightarrow> ('s \<Rightarrow> 't) \<Rightarrow> 't" (infixl ";;" 150)
  where
  "rev_app s f \<equiv> f s"

definition when_fn :: "('s \<Rightarrow> 's) \<Rightarrow> bool \<Rightarrow> ('s \<Rightarrow> 's)" (infix "when" 160)
  where
  "when_fn f b \<equiv> if b then f else id"


definition apply_partial :: "('a \<Rightarrow> 'b) \<Rightarrow> ('a \<Rightarrow> 'b option) \<Rightarrow> 'a \<Rightarrow> 'b"
  where
  "apply_partial f pf a \<equiv> case pf a of Some b \<Rightarrow> b | None \<Rightarrow> f a"  

definition opt_some :: "('a \<Rightarrow> bool) \<Rightarrow> 'a option"
  where
  "opt_some P \<equiv> if (\<exists> a. P a) then (Some (THE a. P a)) else None"


(* TODO: shouldn't this already exist? *)
abbreviation spr :: "'s set \<Rightarrow> ('s \<Rightarrow> bool)"
  where
  "spr ss \<equiv> \<lambda> s. s : ss"
  
lemma opt_some_defined:
  "(\<forall> a a'. P a \<and> P a' \<longrightarrow> a = a') \<Longrightarrow> case (opt_some P) of Some a \<Rightarrow> P a | None \<Rightarrow> True"
by (metis opt_some_def option.case_eq_if option.sel theI)

lemma opt_some_eq:
  "(\<forall> a a'. P a \<and> P a' \<longrightarrow> a = a') \<and> P a \<Longrightarrow> opt_some P = Some a"
by (metis opt_some_def the_equality)


lemmas all_utilities = rev_app_def apply_partial_def when_fn_def id_def

end
