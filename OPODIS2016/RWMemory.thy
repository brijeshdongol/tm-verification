theory RWMemory
imports Main Utilities
begin

(* Semantics of RW-Memory objects*)

(* Types for locations, values and transactions. Ideally we could use typedecl
   to declare an uninterpreted type, but then we run into an error where the
   type does not satisfy the class (sort?) equals
*)

type_synonym L = "nat"
type_synonym V = "nat"
consts v0 :: "V"

definition mem_initial :: "(L \<Rightarrow> V) set"
  where
  "mem_initial = {\<lambda> l. v0}"
  
record StoresState =
  stores :: "nat \<Rightarrow> (L \<Rightarrow> V) option"

definition max_index :: "'a StoresState_scheme \<Rightarrow> nat"
  where
  "max_index s = Max (dom (stores s))"
  
definition store_at :: "'a StoresState_scheme \<Rightarrow> nat \<Rightarrow> L \<Rightarrow> V"
  where
  "store_at s n \<equiv> case (stores s n) of Some store \<Rightarrow> store"

definition value_at :: "'a StoresState_scheme \<Rightarrow> nat \<Rightarrow> L \<Rightarrow> V"
  where
  "value_at s n l = store_at s n l"

definition value_for :: "'a StoresState_scheme \<Rightarrow> nat \<Rightarrow> (L \<Rightarrow> V option) \<Rightarrow> L \<Rightarrow> V"
  where
  "value_for s n ws l \<equiv>
      case (ws l) of
          Some v \<Rightarrow> v
        | None \<Rightarrow> value_at s n l"
  
definition latest_store :: "'a StoresState_scheme \<Rightarrow> L \<Rightarrow> V"
  where
  "latest_store s \<equiv> store_at s (max_index s)"
  
definition write_back :: "(L \<Rightarrow> V option) \<Rightarrow> ('a StoresState_scheme) \<Rightarrow> 'a StoresState_scheme"  
  where
  "write_back ws s \<equiv> let n = max_index s;
                     new_store = apply_partial (store_at s n) ws in
                    s\<lparr> stores := (stores s)(n+1 := Some new_store) \<rparr>"

definition single_writeback :: "(L \<Rightarrow> V) \<Rightarrow> (L \<Rightarrow> V option) \<Rightarrow> L \<Rightarrow> V"
  where
  "single_writeback st ws l \<equiv>
    if ws=Map.empty then (st l) else let l1 = (SOME l. l \<in> dom(ws)) in (if l=l1 then the(ws l1) else st l)"

lemma single_writeback_partial_invisible[simp]:
  shows "apply_partial (single_writeback st ws) ws l = apply_partial st ws l"
using assms
apply(simp add: single_writeback_def apply_partial_def)
apply(cases "l : dom ws")
apply auto
apply(unfold single_writeback_def)
by (smt Collect_empty_eq dom_def dom_eq_empty_conv mem_Collect_eq option.case_eq_if tfl_some)

definition update_partial :: "'a \<Rightarrow> L \<Rightarrow> V \<Rightarrow> ('a \<Rightarrow> L \<Rightarrow> V option) \<Rightarrow> 'a \<Rightarrow> L \<Rightarrow> V option"
  where
  "update_partial t l v p \<equiv> p(t := ((p t) (l := (Some v))))"
                    
definition initial_stores :: "'a StoresState_scheme \<Rightarrow> bool"
  where
  "initial_stores s \<equiv>
    (\<exists> store. store : mem_initial \<and>
              stores s = (\<lambda> n . if n = 0 then (Some store) else None))"

definition read_consistent :: "(L \<Rightarrow> V) \<Rightarrow> (L \<Rightarrow> V option) \<Rightarrow> bool"              
  where
  "read_consistent st rs \<equiv>
    \<forall> l. case (rs l) of Some v \<Rightarrow> v = st l | None \<Rightarrow> True"
  

lemmas all_lookups =
  store_at_def value_at_def latest_store_def max_index_def value_for_def read_consistent_def

lemmas all_updates =
  update_partial_def write_back_def

lemmas all_simps =
  all_lookups
  all_updates

lemma max_initial:
  "Max (dom (\<lambda> n. if n = 0 then Some s else None)) = 0"
proof -
 have "dom (\<lambda> n. if n = 0 then Some s else None) = {0}"
 using singleton_conv2 by fastforce

 from this show ?thesis by (simp add: dom_def)
qed

lemma max_store_is_max:
  "\<lbrakk>sts \<noteq> Map.empty;
    finite (dom sts)\<rbrakk>
   \<Longrightarrow>
   Max (dom sts) : dom sts"
  by (auto)
  
lemma max_remove:
  "\<lbrakk>s \<noteq> {};
    finite s;
    n \<noteq> Max s\<rbrakk>
    \<Longrightarrow>
    Max s = Max (s - {n})"
proof cases
assume 
  a0: "n \<in> s" and
  a1: "s \<noteq> {}" and
  a2: "finite s" and
  a3: "n \<noteq> Max s"
show ?thesis 
proof -
from a1 a2 have b1: "Max s \<in> s" by auto
from this a0 a2 have "n \<le> Max s" by (auto simp add: Lattices_Big.linorder_class.Max.coboundedI)
from this a3 have b2: "n < Max s" by auto
from this have b3: "(Max s) = max n (Max s)" by (auto simp add: max_def)
from a0 b1 a3 have b4: "s \<noteq> {n}" by auto
from this a0 a2 b2 b4 have b5: "Max (s - {n}) = max n (Max s)" by (metis Max.remove a3 max_def)
with this b3 show ?thesis by auto
qed
next
assume "\<not> (n \<in> s)"
have "s = s - {n}" by (simp add: `n \<notin> s`)
from this show ?thesis by auto 
next
qed


lemma max_stores_append:
   "\<lbrakk>stores s \<noteq> Map.empty;
     finite (dom (stores s))\<rbrakk>
    \<Longrightarrow>
    Max (dom (stores (write_back ws s))) = Max (dom (stores s)) + 1"
by (metis (no_types, lifting) 
Max_insert Nat.add_0_right One_nat_def add_Suc_right dom_eq_empty_conv dom_fun_upd ext_inject lessI 
less_irrefl less_le_trans max_def max_index_def option.distinct(2) surjective update_convs(1) 
write_back_def)


end
