theory Interface
imports RWMemory
begin


(* Transactions *)

type_synonym T = "nat"
consts TWriter :: "T set"
definition TReader :: "T set" where "TReader = {t . t \<notin> TWriter}"

(* The actions of TM automata *)

datatype Action = 
    begin_inv: BeginInv
  | begin_resp: BeginResp
  | commit_inv: CommitInv
  | commit_resp: CommitResp
  | cancel: Cancel
  | abort: Abort
  | read_inv: ReadInv (loc: L)
  | read_resp: ReadResp (val : V)
  | write_inv: WriteInv (loc: L)(val : V)
  | write_resp: WriteResp

datatype 'i Event =
    External Action
  | Internal 'i

type_synonym 'i TM_Event = "T * 'i Event"

datatype Hidden = Tau

type_synonym IOA_Event = "T * Hidden Event"


datatype 'pc Status =
    NotStarted
  | BeginResponding
  | WriteResponding
  | ReadResponding V
  | CommitResponding
  | AbortPending
  | Ready
  | Committed
  | Aborted
  | Pending 'pc

definition ext_enabled :: "'pc Status \<Rightarrow> Action \<Rightarrow> bool"
  where
  "ext_enabled s a \<equiv>
    (s = NotStarted \<and> a = BeginInv)
    \<or>
    (s = Ready \<and> (\<exists> l v. a = WriteInv l v))
    \<or>
    (s = Ready \<and> (\<exists> l. a = ReadInv l))
    \<or>
    (s = Ready \<and> a = CommitInv)
    \<or>
    (s = Ready \<and> a = Cancel)
    \<or>
    (s = BeginResponding \<and> a = BeginResp)
    \<or>
    (s = WriteResponding \<and> a = WriteResp)
    \<or>
    (s = CommitResponding \<and> a = CommitResp)
    \<or>
    (\<exists> v. s = ReadResponding v \<and> a = ReadResp v)
    \<or>
    (s \<notin> {NotStarted, Ready, CommitResponding, Committed, Aborted} \<and> a = Abort)"


end