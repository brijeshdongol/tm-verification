theory MSPessTMCorrect
imports "MSPessTM" "TMS2"
begin


definition abstract_index :: "nat \<Rightarrow> nat"
  where
  "abstract_index n \<equiv> n div 2"
  

definition committer_pc_state :: "MSPessTM.State \<Rightarrow> T \<Rightarrow> MSPessTM.PC set \<Rightarrow> bool"
  where
  "committer_pc_state cs t pcs \<equiv>
      committing_writer cs = Some t \<and>
      (\<exists> pc \<in> pcs. MSPessTM.status cs t = Pending pc)"


definition shared_rel :: "MSPessTM.State \<Rightarrow> TMS2.State \<Rightarrow> bool"
  where
  "shared_rel cs as \<equiv>
     max_index as = abstract_index (global_version cs) \<and>
     ((store cs = latest_store as \<and>
        committing_writer cs = None)
      \<or> (\<exists> c. store cs = store_at as (max_index as - 1) \<and>
              latest_store as = apply_partial (store cs) (MSPessTM.write_set cs c) \<and>
              committer_pc_state cs c (writeback_pc - {write_commit12}))
      \<or> (\<exists> c. (\<forall> l. l \<notin> dom (MSPessTM.write_set cs c) \<longrightarrow> store cs l = latest_store as l) \<and>
              latest_store as = apply_partial (store cs) (MSPessTM.write_set cs c) \<and>
              committer_pc_state cs c {write_commit12})
      \<or> (\<exists> c. store cs = latest_store as \<and> committer_pc_state cs c {write_commit13})
      )"


definition read_index :: "MSPessTM.State \<Rightarrow> T \<Rightarrow> nat"
  where
  "read_index cs t \<equiv> abstract_index (txn_ver cs t)"
declare read_index_def [simp]

definition read_action :: "MSPessTM.State \<Rightarrow> T \<Rightarrow> L \<Rightarrow> TMS2.InternalAction"
  where
  "read_action cs t l \<equiv> DoRead l (read_index cs t)"

declare read_action_def [simp]

definition step_correspondence :: "MSPessTM.State \<Rightarrow> T \<Rightarrow> MSPessTM.InternalAction \<Rightarrow> TMS2.InternalAction option"
  where
  "step_correspondence cs0 t a \<equiv>
    case a of
        write_write1 l v  \<Rightarrow> Some (DoWrite l v)
      | read_read5 l \<Rightarrow> Some (read_action cs0 t l)
      | write_read1 l \<Rightarrow> if (l : dom (MSPessTM.write_set cs0 t)) then Some(read_action cs0 t l) else None
      | write_read6 l \<Rightarrow> Some(read_action cs0 t l)
      | read_commit1 \<Rightarrow> Some (DoCommitReadOnly (read_index cs0 t))
      | write_commit6  \<Rightarrow> Some DoCommitWriter
      | _ \<Rightarrow> None"


definition beginning_txn :: "MSPessTM.State \<Rightarrow> TMS2.State \<Rightarrow> T \<Rightarrow> nat \<Rightarrow> bool"
  where
  "beginning_txn cs as t ver \<equiv>
      status as t = Interface.BeginResponding \<and>
      begin_index as t \<le> abstract_index ver"

definition inflight_txn ::  "MSPessTM.State \<Rightarrow> TMS2.State \<Rightarrow> T \<Rightarrow> bool"
  where
  "inflight_txn cs as t \<equiv>
      begin_index as t \<le> read_index cs t \<and>
      read_consistent (store_at as (read_index cs t)) (read_set as t) \<and>
      (t : TWriter \<longrightarrow> MSPessTM.write_set cs t = TMS2.write_set as t) \<and>
      (t : TReader \<longrightarrow> TMS2.write_set as t = Map.empty)"


definition inflight_txn_at :: "MSPessTM.State \<Rightarrow> TMS2.State \<Rightarrow> T \<Rightarrow> TMS2.Status \<Rightarrow> bool"
  where
  "inflight_txn_at cs as t ast \<equiv>
      status as t = ast \<and>
      inflight_txn cs as t"

definition txn_rel :: "MSPessTM.PC Event \<Rightarrow> MSPessTM.State \<Rightarrow> TMS2.State \<Rightarrow> T \<Rightarrow> bool"
  where
  "txn_rel e cs as t \<equiv>
    mspesstm_pre cs t e \<longrightarrow>
     (case e of

        External BeginInv \<Rightarrow>
               status as t = Interface.NotStarted
      | External BeginResp \<Rightarrow> inflight_txn_at cs as t Interface.BeginResponding
      | External (ReadResp v) \<Rightarrow> inflight_txn_at cs as t (Interface.ReadResponding v)
      | External WriteResp \<Rightarrow> inflight_txn_at cs as t Interface.WriteResponding
      | External CommitResp \<Rightarrow> status as t =  Interface.CommitResponding

      | Internal read_begin1 \<Rightarrow>
               beginning_txn cs as t (global_version cs)
      | Internal read_begin2 \<Rightarrow>
               beginning_txn cs as t (global_version cs)
      | Internal read_begin3 \<Rightarrow>
               beginning_txn cs as t (temp cs t)

      | Internal write_begin1 \<Rightarrow>
               beginning_txn cs as t (global_version cs)
      | Internal write_begin2 \<Rightarrow>
               beginning_txn cs as t (global_version cs)
      | Internal write_begin3  \<Rightarrow>
               beginning_txn cs as t (global_version cs)
      | Internal write_begin4  \<Rightarrow>
               beginning_txn cs as t (global_version cs)
      | Internal write_begin5  \<Rightarrow>
               beginning_txn cs as t (global_version cs)
      | Internal write_begin6  \<Rightarrow>
               beginning_txn cs as t (temp cs t)

      | Internal (read_read1 l) \<Rightarrow>
               inflight_txn_at cs as t (Interface.Pending (ReadPending l))
      | Internal (read_read2 l) \<Rightarrow>
               inflight_txn_at cs as t (Interface.Pending (ReadPending l))
      | Internal (read_read3 l) \<Rightarrow>
               inflight_txn_at cs as t (Interface.Pending (ReadPending l))
      | Internal (read_read4 l) \<Rightarrow>
               inflight_txn_at cs as t (Interface.Pending (ReadPending l))
      | Internal (read_read5 l) \<Rightarrow>
               inflight_txn_at cs as t (Interface.Pending (ReadPending l))

      | Internal (write_read1 l) \<Rightarrow>
               inflight_txn_at cs as t (Interface.Pending (ReadPending l))
      | Internal (write_read2 l) \<Rightarrow>
               inflight_txn_at cs as t (Interface.Pending (ReadPending l))
      | Internal (write_read3 l) \<Rightarrow>
               inflight_txn_at cs as t (Interface.Pending (ReadPending l))
      | Internal (write_read4 l) \<Rightarrow>
               inflight_txn_at cs as t (Interface.Pending (ReadPending l))
      | Internal (write_read5 l) \<Rightarrow>
               inflight_txn_at cs as t (Interface.Pending (ReadPending l))
      | Internal (write_read6 l) \<Rightarrow>
               inflight_txn_at cs as t (Interface.Pending (ReadPending l))

      | Internal (write_write1 l v) \<Rightarrow>
               inflight_txn_at cs as t (Interface.Pending (WritePending l v))

      | Internal read_commit1 \<Rightarrow>
               inflight_txn_at cs as t (Interface.Pending CommitPending)

      | Internal write_commit0  \<Rightarrow>
               inflight_txn_at cs as t (Interface.Pending CommitPending)
      | Internal write_commit1  \<Rightarrow>
               inflight_txn_at cs as t (Interface.Pending CommitPending)
      | Internal write_commit2  \<Rightarrow>
               status as t = Interface.Pending CommitPending \<and>
               TMS2.write_set as t = MSPessTM.write_set cs t \<and>
               read_consistent (store_at as (max_index as)) (read_set as t)
               
      | Internal write_commit3a  \<Rightarrow>
               status as t = Interface.Pending CommitPending \<and>
               TMS2.write_set as t = MSPessTM.write_set cs t \<and>
               read_consistent (store_at as (max_index as)) (read_set as t)
(*       | Internal write_commit3  \<Rightarrow>
               status as t = Interface.Pending CommitPending \<and>
               TMS2.write_set as t = MSPessTM.write_set cs t \<and>
               read_consistent (store_at as (max_index as)) (read_set as t)
 *)      | Internal write_commit4  \<Rightarrow>
               status as t = Interface.Pending CommitPending \<and>
               TMS2.write_set as t = MSPessTM.write_set cs t \<and>
               read_consistent (store_at as (max_index as)) (read_set as t)
(*       | Internal write_commit5  \<Rightarrow>
               status as t = Interface.Pending CommitPending \<and>
               TMS2.write_set as t = MSPessTM.write_set cs t \<and>
               read_consistent (store_at as (max_index as)) (read_set as t) *)
       | Internal write_commit6  \<Rightarrow>
               status as t = Interface.Pending CommitPending \<and>
               TMS2.write_set as t = MSPessTM.write_set cs t \<and>
               read_consistent (store_at as (max_index as)) (read_set as t)
(*       | Internal write_commit7 \<Rightarrow>
               status as t = Interface.CommitResponding *)
       | Internal write_commit8  \<Rightarrow>
               status as t = Interface.CommitResponding
       | Internal write_commit9  \<Rightarrow>
               status as t = Interface.CommitResponding
       | Internal write_commit10 \<Rightarrow>
               status as t = Interface.CommitResponding
       | Internal write_commit11  \<Rightarrow>
               status as t = Interface.CommitResponding
       | Internal write_commit12  \<Rightarrow>
               status as t = Interface.CommitResponding
      | Internal write_commit13  \<Rightarrow>
               status as t = Interface.CommitResponding
      | Internal write_commit14  \<Rightarrow>
               status as t = Interface.CommitResponding

      | _ \<Rightarrow>
        inflight_txn_at cs as t Interface.Ready
        )"

definition sim_rel :: "MSPessTM.State \<Rightarrow> TMS2.State \<Rightarrow> bool"
  where
  "sim_rel cs as \<equiv> shared_rel cs as \<and> (\<forall> a t. txn_rel a cs as t)"

definition tver_read_index_states :: "MSPessTM.State \<Rightarrow> TMS2.State \<Rightarrow> T \<Rightarrow> bool"
  where
  "tver_read_index_states cs as t \<equiv>
     read_index cs t = max_index as
     \<or> (1 + read_index cs t = max_index as \<and>
        committing_writer cs \<noteq> None)"

lemma tver_constraint:
  assumes tinv: "MSPessTM.txn_inv (Internal cpc) cs t" and
          sharedi: "shared_inv cs" and
          sharedr: "shared_rel cs as" and
             pre: "MSPessTM.tau_pre cs t cpc" and
          pc: "cpc : {read_commit1}
                     \<union> {cpc . \<exists> l. cpc = read_read5 l \<or>  cpc = write_read6 l}"
     shows
     "tver_read_index_states cs as t"
proof -
 have gind: "abstract_index (global_version cs) = max_index as"
 using sharedr by (simp add: shared_rel_def)

 have cm_gv: "odd(global_version cs) \<longleftrightarrow> committing_writer cs = None"
 using sharedi by(simp add: shared_inv_def)

 have etv: "eff_txn_ver cs t = txn_ver cs t"
 using tinv pc pre
 by (cases cpc, simp_all add: unfold_txn_props MSPessTM.all_simps unfold_eff_txn_ver
                  split: option2.split)
 show ?thesis
 using tinv etv pre pc sharedr cm_gv
 apply(cases cpc, simp_all add: tver_read_index_states_def
                                abstract_index_def txn_ver_states_def committer_pc_state_def
                       shared_rel_def unfold_txn_props  MSPessTM.all_simps)
by (smt div2_Suc_Suc even_Suc even_Suc_div_two le_SucE le_antisym)+
qed

lemma read_value_correct_quiescing_committer:
  assumes  tinv: "\<forall> t' a. MSPessTM.txn_inv a cs t'" and
          sharedi: "shared_inv cs" and
          sharedr: "shared_rel cs as" and
             pre: "MSPessTM.tau_pre cs t cpc" and
              pc: "cpc : {read_read5 l, write_read6 l}" and
              cm: "committer_pc_state cs c (writeback_pc - {write_commit12})" and
            cm_stat: "MSPessTM.status cs c = MSPessTM.Pending cmpc"
    shows "store cs l = store_at as (read_index cs t) l"
proof -

 have ttinv: "MSPessTM.txn_inv (Internal cpc) cs t" using tinv by blast
 have ctinv: "MSPessTM.txn_inv (Internal cmpc) cs c" using tinv by blast
 have tver_c: "tver_read_index_states cs as t"
 using ttinv sharedi sharedr pre pc tver_constraint by blast

 have cm_gv: "odd(global_version cs) \<longleftrightarrow> committing_writer cs = None"
 using sharedi by(simp add: shared_inv_def)

 have etv: "eff_txn_ver cs t = txn_ver cs t"
 using ttinv pc pre
 by (cases cpc, simp_all add: unfold_txn_props MSPessTM.all_simps unfold_eff_txn_ver
                  split: option2.split)

 have gind: "abstract_index (global_version cs) = max_index as"
 using sharedr by (simp add: shared_rel_def)

 have s: "store cs = store_at as (max_index as - 1) \<and>
              latest_store as = apply_partial (store cs) (MSPessTM.write_set cs c) \<and>
              committer_pc_state cs c (writeback_pc - {write_commit12}) \<and>
              even(global_version cs)"
 using sharedr cm gind sharedi
 by(auto simp add: shared_rel_def shared_inv_def committer_pc_state_def writeback_pc_def)


 have tver : "txn_ver cs t = global_version cs \<or>
              1 + txn_ver cs t = global_version cs \<or>
              2 + txn_ver cs t = global_version cs"
 using ttinv etv pre pc
 by (auto simp add: txn_ver_states_def unfold_txn_props  MSPessTM.all_simps)

 have ldom: "txn_ver cs t = global_version cs \<longrightarrow> l \<notin> dom(MSPessTM.write_set cs c)"
 using assms ttinv ctinv
 read_value_not_locked_for_recent_txn[where s=cs and c=c and t=t and cpc=cmpc and tpc=cpc and l=l]
 by (auto simp add: committer_pc_state_def MSPessTM.all_simps)

 show ?thesis
 using tver_c tver s ldom cm_gv
 apply (auto simp add: committer_pc_state_def tver_read_index_states_def
                    abstract_index_def)
apply (smt One_nat_def diff_Suc_1 abstract_index_def even_Suc gind odd_Suc_div_two
apply_partial_def domIff latest_store_def ldom option.simps)
apply (smt One_nat_def diff_Suc_1 abstract_index_def even_Suc gind odd_Suc_div_two
apply_partial_def domIff latest_store_def ldom option.simps)
defer
(* apply (smt One_nat_def diff_Suc_1 abstract_index_def even_Suc gind odd_Suc_div_two
apply_partial_def domIff latest_store_def ldom option.simps)
 *)
using abstract_index_def gind apply auto (* TODO cleanup *)

apply (metis One_nat_def diff_Suc_1)
apply (metis One_nat_def diff_Suc_1)
apply (metis One_nat_def diff_Suc_1)
apply (metis One_nat_def diff_Suc_1)
by (metis even_Suc odd_Suc_div_two)
qed


lemma read_value_correct_writeback_committer:
  assumes  tinv: "\<forall> t' a. MSPessTM.txn_inv a cs t'" and
          sharedi: "shared_inv cs" and
          sharedr: "shared_rel cs as" and
             pre: "MSPessTM.tau_pre cs t cpc" and
              pc: "cpc : {read_read5 l, write_read6 l}" and
              cm: "committer_pc_state cs c {write_commit12}"
    shows "store cs l = store_at as (read_index cs t) l"
proof -

 have ttinv: "MSPessTM.txn_inv (Internal cpc) cs t" using tinv by blast
 have ctinv: "MSPessTM.txn_inv (Internal write_commit12) cs c" using tinv by blast

 have etv: "eff_txn_ver cs t = txn_ver cs t"
 using ttinv pc pre
 by (cases cpc, simp_all add: unfold_txn_props MSPessTM.all_simps unfold_eff_txn_ver
                  split: option2.split)

 have gind: "abstract_index (global_version cs) = max_index as"
 using sharedr by (simp add: shared_rel_def)

 have s: "(\<forall> l. l \<notin> dom (MSPessTM.write_set cs c) \<longrightarrow> store cs l = latest_store as l) \<and>
              latest_store as = apply_partial (store cs) (MSPessTM.write_set cs c) \<and>
              committer_pc_state cs c {write_commit12}"
 using sharedr cm gind sharedi
 by(auto simp add: shared_rel_def shared_inv_def committer_pc_state_def)

 have tver : "txn_ver cs t = global_version cs"
 using ttinv etv s ctinv pre pc
 by (auto simp add: all_simps unfold_txn_props committer_pc_state_def
                             txn_ver_states_def glb_cleared_def)

 have ldom: "l \<notin> dom(MSPessTM.write_set cs c)"
 using assms ttinv ctinv
 read_value_not_locked[where s=cs and c=c and t=t and tpc=cpc and l=l]
 by (simp add: committer_pc_state_def MSPessTM.all_simps)

 show ?thesis
 using ldom s gind etv tver
 by (simp add: apply_partial_def latest_store_def)
qed

lemma read_value_correct:
  assumes  tinv: "\<forall> t' a. MSPessTM.txn_inv a cs t'" and
          sharedi: "shared_inv cs" and
          sharedr: "shared_rel cs as" and
             pre: "MSPessTM.tau_pre cs t cpc" and
              pc: "cpc : {read_read5 l, write_read6 l}"
    shows "store cs l = store_at as (read_index cs t) l"
proof -
 have ttinv: "MSPessTM.txn_inv (Internal cpc) cs t" using tinv by blast

 have etv: "eff_txn_ver cs t = txn_ver cs t"
 using ttinv pc pre
 by (cases cpc, simp_all add: unfold_txn_props MSPessTM.all_simps unfold_eff_txn_ver
                  split: option2.split)

 have gind: "abstract_index (global_version cs) = max_index as"
 using sharedr by (simp add: shared_rel_def)
show ?thesis
proof -
{fix c
 assume c: "committer_pc_state cs c (writeback_pc - {write_commit12})"
 have ?thesis using c assms
 read_value_correct_quiescing_committer[where cs=cs and as=as and c=c and t=t]
 by (auto simp add: committer_pc_state_def writeback_pc_def)
}
moreover
{fix c
 assume c: "committer_pc_state cs c {write_commit12}"
 have ?thesis using c assms read_value_correct_writeback_committer by blast
}
moreover
{assume c: "store cs = latest_store as \<and> committing_writer cs = None"

 have i: "read_index cs t = max_index as"
 using ttinv pre pc etv gind c sharedi
 apply(cases cpc, simp_all add: shared_inv_def abstract_index_def unfold_txn_props txn_ver_states_def MSPessTM.all_simps)
apply (metis even_Suc even_Suc_div_two le_SucE le_antisym)
by (metis even_Suc_div_two le_SucE le_antisym)

 have ?thesis
 using c i
 by(simp add: latest_store_def)
}
moreover
{fix c
 assume c: "committer_pc_state cs c {write_commit13}"

 have ttinv: "MSPessTM.txn_inv (Internal cpc) cs t" using tinv by blast
 have ctinv: "MSPessTM.txn_inv (Internal write_commit13) cs c" using tinv by blast

 have s: "store cs = latest_store as \<and> committer_pc_state cs c {write_commit13}"
 using sharedr c gind sharedi
 by(auto simp add: writeback_pc_def shared_rel_def shared_inv_def committer_pc_state_def)

 have etv: "eff_txn_ver cs t = txn_ver cs t"
 using ttinv pc pre
 by (cases cpc, simp_all add: unfold_txn_props MSPessTM.all_simps unfold_eff_txn_ver
                  split: option2.split)

 have tver : "txn_ver cs t = global_version cs"
 using ttinv pc etv c ctinv pre pc
 by (auto simp add: all_simps unfold_txn_props committer_pc_state_def
                             txn_ver_states_def glb_cleared_def)

 have i: "read_index cs t = max_index as"
 using ttinv pre pc etv gind c sharedi tver
 by(cases cpc, simp_all add: shared_inv_def abstract_index_def unfold_txn_props txn_ver_states_def MSPessTM.all_simps)

 have ?thesis
 using s c i
 by(simp add: latest_store_def)
}
ultimately show ?thesis
using sharedr shared_rel_def 
by fastforce
qed
qed

lemma sc_step_behaviour:
  assumes  tinv: "\<forall> a t'. MSPessTM.txn_inv a cs t'" and
      sharedinv: "shared_inv cs" and
          sharedr: "shared_rel cs as" and
          txnrel: "txn_rel (Internal ci) cs as t" and
             pre: "MSPessTM.tau_pre cs t ci" and
              sc:"step_correspondence cs t ci = Some ai"
  shows " (t : TWriter \<and>
           (\<exists> l v. ci = write_write1 l v \<and> ai = DoWrite l v \<and>
           TMS2.status as t = Interface.Pending (TMS2.WritePending l v)) \<and>
           MSPessTM.write_set cs t = TMS2.write_set as t \<and>
           committing_writer cs \<noteq> Some t)
       
        \<or> (t : TReader \<and>
           TMS2.write_set as t = Map.empty \<and>
           (\<exists> l. ci = read_read5 l \<and> ai = read_action cs t l \<and>
            store cs l = store_at as (read_index cs t) l \<and>
            begin_index as t \<le> read_index cs t \<and>
            (read_index cs t = max_index as \<or> 1 + read_index cs t = max_index as) \<and>
            TMS2.status as t = Interface.Pending (TMS2.ReadPending l)) \<and>
            read_consistent (store_at as (read_index cs t)) (read_set as t))
       
         \<or> (t : TWriter \<and>
           (\<exists> l. ci = write_read1 l \<and>
                 l : dom (MSPessTM.write_set cs t) \<and>
                 TMS2.write_set as t l = MSPessTM.write_set cs t l \<and>
                 TMS2.status as t = Interface.Pending (TMS2.ReadPending l) \<and>
                 ai = read_action cs t l \<and>
                 MSPessTM.write_set cs t l = TMS2.write_set as t l) \<and>
           committing_writer cs \<noteq> Some t)
        
        \<or> (t : TWriter \<and>
           (\<exists> l. ci = write_read6 l \<and> ai = read_action cs t l \<and>
           l \<notin> dom(write_set as t) \<and>
            store cs l = store_at as (read_index cs t) l \<and>
            TMS2.status as t = Interface.Pending (TMS2.ReadPending l) \<and>
           MSPessTM.write_set cs t l = TMS2.write_set as t l) \<and>
            begin_index as t \<le> read_index cs t \<and> 
            (read_index cs t = max_index as \<or> 1 + read_index cs t = max_index as) \<and>
            read_consistent (store_at as (read_index cs t)) (read_set as t) \<and>
           committing_writer cs \<noteq> Some t)
        
        \<or> (t : TReader \<and>
           ci = read_commit1 \<and>
           ai = DoCommitReadOnly (read_index cs t) \<and>
           TMS2.status as t = Interface.Pending TMS2.CommitPending \<and>
           begin_index as t \<le> read_index cs t \<and>
           (read_index cs t = max_index as \<or> 1 + read_index cs t = max_index as) \<and>
           read_consistent (store_at as (read_index cs t)) (read_set as t) \<and>
           TMS2.write_set as t = Map.empty)
        
        \<or> (t : TWriter \<and>
           ci = write_commit6 \<and>
           ai = DoCommitWriter \<and>
           TMS2.status as t = Interface.Pending TMS2.CommitPending \<and>
           read_consistent (latest_store as) (read_set as t) \<and>
           TMS2.write_set as t = MSPessTM.write_set cs t \<and>
           (*read_index cs t = max_index as \<and>*)
           abstract_index (temp cs t) = max_index as \<and>
           committing_writer cs = None \<and>
           odd(global_version cs) \<and>
           temp cs t = global_version cs)"
proof -

have ctinv: "MSPessTM.txn_inv (Internal ci) cs t" 
using assms by blast

have rvc: "\<forall> l. ci : {read_read5 l, write_read6 l} \<longrightarrow> store cs l = store_at as (read_index cs t) l"
using assms read_value_correct by blast

have mi: "abstract_index (global_version cs) = max_index as"
using sharedr by(simp add: shared_rel_def)

have tver: "ci : {read_commit1}
                     \<union> {cpc . \<exists> l. cpc = read_read5 l \<or>  cpc = write_read6 l}
           \<longrightarrow> read_index cs t = max_index as \<or> 1 + read_index cs t = max_index as"
using assms ctinv tver_constraint
apply(auto simp add: tver_read_index_states_def)
by blast+

show ?thesis
using ctinv txnrel pre sc rvc mi tver sharedinv
apply(cases ci,
      auto simp add: MSPessTM.all_simps step_correspondence_def
                    inflight_txn_at_def inflight_txn_def txn_rel_def unfold_txn_props option.exhaust domD
                    abstract_index_def shared_inv_def
                   split: option.split if_splits)
done
qed

lemma step_internal_precondition:
  assumes  tinv: "\<forall> a t'. MSPessTM.txn_inv a cs t'" and
      sharedinv: "shared_inv cs" and
          sharedr: "shared_rel cs as" and
          txnrel: "txn_rel (Internal ci) cs as t" and
             pre: "MSPessTM.tau_pre cs t ci" and
              sc:"step_correspondence cs t ci = Some ai"
  shows "TMS2.tms_pre as t (Internal ai)"
proof -
{fix l v
 assume c: "t : TWriter \<and>
           (ci = write_write1 l v \<and> ai = DoWrite l v \<and>
           TMS2.status as t = Interface.Pending (TMS2.WritePending l v)) \<and>
           MSPessTM.write_set cs t = TMS2.write_set as t \<and>
           committing_writer cs \<noteq> Some t"

 have ?thesis
 using txnrel pre c 
 by(simp add: txn_rel_def TMS2.unfold_tms2 MSPessTM.all_simps inflight_txn_at_def inflight_txn_def
                       unfold_eff_txn_ver)
}
moreover
{fix l
 assume c: "t : TReader \<and>
           TMS2.write_set as t = Map.empty \<and>
           (ci = read_read5 l \<and> ai = read_action cs t l \<and>
            store cs l = store_at as (read_index cs t) l \<and>
            begin_index as t \<le> read_index cs t \<and>
            (read_index cs t = max_index as \<or> 1 + read_index cs t = max_index as) \<and>
            TMS2.status as t = Interface.Pending (TMS2.ReadPending l)) \<and>
            read_consistent (store_at as (read_index cs t)) (read_set as t)"

 have ?thesis
 using txnrel pre c 
 by(auto simp add: txn_rel_def TMS2.unfold_tms2 MSPessTM.all_simps inflight_txn_at_def inflight_txn_def
                       unfold_eff_txn_ver)
}
moreover
{fix l
 assume c: "t : TWriter \<and>
           (ci = write_read1 l \<and>
                 l : dom (MSPessTM.write_set cs t) \<and>
                 TMS2.write_set as t l = MSPessTM.write_set cs t l \<and>
                 TMS2.status as t = Interface.Pending (TMS2.ReadPending l) \<and>
                 ai = read_action cs t l) \<and>
           committing_writer cs \<noteq> Some t \<and>
           MSPessTM.write_set cs t l = TMS2.write_set as t l"

 have ?thesis
 using txnrel pre c
 by(simp add: txn_rel_def TMS2.unfold_tms2 MSPessTM.all_simps inflight_txn_at_def inflight_txn_def
                       unfold_eff_txn_ver)
}
moreover
{fix l
 assume c: "t : TWriter \<and>
           (ci = write_read6 l \<and> ai = read_action cs t l \<and>
            store cs l = store_at as (read_index cs t) l \<and>
            TMS2.status as t = Interface.Pending (TMS2.ReadPending l)) \<and>
            begin_index as t \<le> read_index cs t \<and> 
            (read_index cs t = max_index as \<or> 1 + read_index cs t = max_index as) \<and>
            read_consistent (store_at as (read_index cs t)) (read_set as t) \<and>
           committing_writer cs \<noteq> Some t \<and>
           MSPessTM.write_set cs t l = TMS2.write_set as t l"

 have ?thesis
 using txnrel pre c 
 by(auto simp add: txn_rel_def TMS2.unfold_tms2 MSPessTM.all_simps inflight_txn_at_def inflight_txn_def
                       unfold_eff_txn_ver)
}
moreover
{assume c: "t : TReader \<and>
           ci = read_commit1 \<and>
           ai = DoCommitReadOnly (read_index cs t) \<and>
           TMS2.status as t = Interface.Pending TMS2.CommitPending \<and>
           begin_index as t \<le> read_index cs t \<and>
           (read_index cs t = max_index as \<or> 1 + read_index cs t = max_index as) \<and>
           read_consistent (store_at as (read_index cs t)) (read_set as t) \<and>
           TMS2.write_set as t = Map.empty"

 have ?thesis
 using txnrel pre c 
 by(auto simp add: txn_rel_def TMS2.unfold_tms2 MSPessTM.all_simps inflight_txn_at_def inflight_txn_def
                       unfold_eff_txn_ver)
}
moreover
{assume c: "t : TWriter \<and>
           ci = write_commit6 \<and>
           ai = DoCommitWriter \<and>
           TMS2.status as t = Interface.Pending TMS2.CommitPending \<and>
           read_consistent (latest_store as) (read_set as t) \<and>
           TMS2.write_set as t = MSPessTM.write_set cs t \<and>
           abstract_index (temp cs t) = max_index as \<and>
           committing_writer cs = None \<and>
           odd(global_version cs) \<and>
           temp cs t = global_version cs"

 have ?thesis
 using txnrel pre c 
 by(auto simp add: txn_rel_def TMS2.unfold_tms2 MSPessTM.all_simps inflight_txn_at_def inflight_txn_def
                       unfold_eff_txn_ver)
}
ultimately show ?thesis
using sc_step_behaviour[where cs=cs and as=as and t=t and ci=ci and ai=ai] assms
by smt
qed

lemma step_external_precondition:
  assumes txnrel: "txn_rel (External a) cs as t" and
             pre: "MSPessTM.mspesstm_pre cs t (External a)"
  shows "TMS2.tms_pre as t (External a)"
using assms
by (cases a, simp_all add: inflight_txn_at_def inflight_txn_def mspesstm_pre_def MSPessTM.ext_pre_def
             txn_rel_def TMS2.tms_pre_def status_enabled_def ext_enabled_def)

lemma shared_rel_preservation:
  assumes sharedr: "shared_rel cs as" and
            gv: "global_version cs' = global_version cs" and
            cst: "store cs' = store cs" and
            ast: "stores as' = stores as" and
            cw: "committing_writer cs' = committing_writer cs" and
            cws: "\<forall> c. committer_pc_state cs c (writeback_pc \<union> {write_commit13}) \<longrightarrow>
                        MSPessTM.status cs' c = MSPessTM.status cs c \<and>
                        TMS2.write_set as' c = TMS2.write_set as c \<and>
                        MSPessTM.write_set cs' c = MSPessTM.write_set cs c" and
            nemp: "dom(stores as) \<noteq> {} \<and> finite(dom(stores as))"
  shows "shared_rel cs' as'"
proof -
have ai1: "max_index as : dom(stores as)"
using nemp by (auto simp add: max_index_def shared_rel_def )

from this have ai2: "abstract_index(global_version cs) : dom(stores as)"
using sharedr by(simp add: shared_rel_def)

show ?thesis
using assms ai2
apply(simp add: shared_rel_def max_index_def latest_store_def
                      store_at_def committer_pc_state_def writeback_pc_def domIff domD
)
by (metis DiffE insertE)
qed

lemma committer_state_preservation:
  assumes tinv: "MSPessTM.txn_inv a cs t" and
          neq: "t \<noteq> c" and
           cw: "committer_pc_state cs c pcs" and
          pre: "mspesstm_pre cs t a"
     shows "committer_pc_state (mspesstm_eff cs t a) c pcs"
using assms
by(cases rule: MSPessTM.Event_split[where b=a],
     simp_all add: MSPessTM.txn_inv_def MSPessTM.all_simps committer_pc_state_def split: option.split)

lemma external_preserves_shared_rel:
  assumes sharedr: "shared_rel cs as" and
             pre: "MSPessTM.mspesstm_pre cs t (External a)" and
            nemp: "dom(stores as) \<noteq> {} \<and> finite(dom(stores as))"
  shows "shared_rel (mspesstm_eff cs t (External a)) (tms_eff as t (External a))"
using assms
apply(insert shared_rel_preservation
       [where cs=cs and as=as and cs'="mspesstm_eff cs t (External a)" and as'="tms_eff as t (External a)"])
using sharedr nemp apply clarsimp
using pre by(cases a, auto simp add: committer_pc_state_def
                          TMS2.unfold_tms2 MSPessTM.all_simps split: if_splits)


lemma internal_preserves_shared_stutter_no_commit:
  assumes  tinv: "\<forall> a t'. MSPessTM.txn_inv a cs t'" and
      sharedinv: "shared_inv cs" and
          sharedr: "shared_rel cs as" and
          txnrel: "txn_rel (Internal ci) cs as t" and
             pre: "MSPessTM.tau_pre cs t ci" and
              wc: "ci \<notin> writeback_pc \<union> {write_commit13}" and
              sc:"step_correspondence cs t ci = None" and
            nemp: "dom(stores as) \<noteq> {} \<and> finite(dom(stores as))"
   shows "shared_rel (mspesstm_eff cs t (Internal ci)) as"
using pre sc wc shared_rel_preservation
       [where cs=cs and as=as and cs'="mspesstm_eff cs t (Internal ci)" and as'=as]
using sharedr nemp apply clarsimp
by(cases ci, auto simp add: MSPessTM.all_simps
                          step_correspondence_def
                          committer_pc_state_def
                          writeback_pc_def
                  split: option.split)


lemma internal_preserves_shared_stutter_commit:
  assumes  tinv: "MSPessTM.txn_inv (Internal ci) cs t" and
      sharedinv: "shared_inv cs" and
          sharedr: "shared_rel cs as" and
          txnrel: "txn_rel (Internal ci) cs as t" and
             pre: "MSPessTM.tau_pre cs t ci" and
              wc: "ci \<in> writeback_pc \<union> {write_commit13}" and
              sc:"step_correspondence cs t ci = None" and
            nemp: "dom(stores as) \<noteq> {} \<and> finite(dom(stores as))"
   shows "shared_rel (mspesstm_eff cs t (Internal ci)) as"
proof -
 have cm: "committing_writer cs = Some t"
 using wc pre tinv
 by(cases ci, simp_all add: MSPessTM.txn_inv_def MSPessTM.all_simps is_intermediate_writer_def
         writeback_pc_def)

show ?thesis
proof -
(*{assume c: "ci = write_commit7"
 have ?thesis
 using c cm sharedr pre
 by(auto simp add: shared_rel_def MSPessTM.all_simps TMS2.unfold_tms2
   committer_pc_state_def writeback_pc_def)
}
moreover*)
{assume c: "ci = write_commit8"
 have ?thesis
 using c cm sharedr pre
 by(auto simp add: shared_rel_def MSPessTM.all_simps TMS2.unfold_tms2
   committer_pc_state_def writeback_pc_def)
}
moreover
{assume c: "ci = write_commit9"
 have ?thesis
 using c cm sharedr pre
 by(auto simp add: shared_rel_def MSPessTM.all_simps TMS2.unfold_tms2
   committer_pc_state_def writeback_pc_def)
}
moreover
{assume c: "ci = write_commit10"
 have ?thesis
 using c cm sharedr pre
 by(auto simp add: shared_rel_def MSPessTM.all_simps TMS2.unfold_tms2
   committer_pc_state_def writeback_pc_def)
}
moreover
{assume c: "ci = write_commit11"
 have ?thesis
 using c cm sharedr pre
 apply(auto simp add: shared_rel_def MSPessTM.all_simps TMS2.unfold_tms2
   committer_pc_state_def writeback_pc_def)
using domIff by fastforce
}
moreover
{assume c: "ci = write_commit12
 \<and> (\<forall> l\<in> dom(MSPessTM.write_set cs t). MSPessTM.write_set cs t l = Some(store cs l))"

 have st: "\<And> l. store cs l = apply_partial (store cs) (MSPessTM.State.write_set cs t) l"
 using c by(auto simp add: apply_partial_def simp add: domIff option.case_eq_if)

 have wr_set: "\<forall>x v. MSPessTM.State.write_set cs t x = Some v \<longrightarrow> store cs x = v"
 using c by (simp add: apply_partial_def st)

 have cw_post: "committer_pc_state (mspesstm_eff cs t (Internal ci)) t {write_commit13}"
 using c wr_set cm pre by(simp add: MSPessTM.all_simps committer_pc_state_def)

 have ?thesis
 using nemp st wr_set cw_post c cm sharedr pre
 by(auto simp add: shared_rel_def MSPessTM.all_simps TMS2.unfold_tms2
   committer_pc_state_def writeback_pc_def)
}
moreover
{assume c: "ci = write_commit12
 \<and> \<not> (\<forall> l\<in> dom(MSPessTM.write_set cs t). MSPessTM.write_set cs t l = Some(store cs l))"

 have wr_set: "(\<exists> x v. MSPessTM.State.write_set cs t x = Some v \<and> store cs x \<noteq> v)"
 using c by (auto simp add: apply_partial_def)

 have ?thesis
 using nemp c cm wr_set sharedr pre
 apply(auto simp add: shared_rel_def MSPessTM.all_simps TMS2.unfold_tms2
   committer_pc_state_def writeback_pc_def)
apply(auto simp add: single_writeback_partial_invisible[where st="store cs" and ws="MSPessTM.State.write_set cs t"])
by (metis apply_partial_def domD domIff option.simps(4) single_writeback_partial_invisible)
}
moreover
{assume c: "ci = write_commit13"
 have ?thesis
 using c cm sharedr pre tinv sharedinv
 by(auto simp add: shared_rel_def MSPessTM.all_simps TMS2.unfold_tms2
   committer_pc_state_def writeback_pc_def MSPessTM.txn_inv_def shared_inv_def abstract_index_def)
}
ultimately show ?thesis
using wc by (auto simp add: writeback_pc_def)
qed
qed

lemma internal_preserves_shared_rel_step:
  assumes  tinv: "\<forall> a t'. MSPessTM.txn_inv a cs t'" and
      sharedinv: "shared_inv cs" and
          sharedr: "shared_rel cs as" and
          txnrel: "txn_rel (Internal ci) cs as t" and
             pre: "MSPessTM.tau_pre cs t ci" and
              sc:"step_correspondence cs t ci = Some ai" and
            nemp: "dom(stores as) \<noteq> {} \<and> finite(dom(stores as))"
   shows "shared_rel (mspesstm_eff cs t (Internal ci)) (tms_eff as t (Internal ai))"
proof -
{assume c: "t : TWriter \<and>
           (\<exists> l v. ci = write_write1 l v \<and> ai = DoWrite l v \<and>
           TMS2.status as t = Interface.Pending (TMS2.WritePending l v)) \<and>
           MSPessTM.write_set cs t = TMS2.write_set as t \<and>
           committing_writer cs \<noteq> Some t"

 have ?thesis
 using c nemp shared_rel_preservation
       [where cs=cs and as=as and cs'="mspesstm_eff cs t (Internal ci)" and as'="tms_eff as t (Internal ai)"]
 using sharedr apply clarsimp
 by(auto simp add: TMS2.unfold_tms2 MSPessTM.all_simps committer_pc_state_def)
}
moreover
{fix l
 assume c: "t : TReader \<and>
           TMS2.write_set as t = Map.empty \<and>
           (ci = read_read5 l \<and> ai = read_action cs t l \<and>
            store cs l = store_at as (read_index cs t) l \<and>
            begin_index as t \<le> read_index cs t \<and>
            (read_index cs t = max_index as \<or> 1 + read_index cs t = max_index as) \<and>
            TMS2.status as t = Interface.Pending (TMS2.ReadPending l)) \<and>
            read_consistent (store_at as (read_index cs t)) (read_set as t)"

 have ncw: "committing_writer cs \<noteq> Some t"
 using c pre sharedr
 by(auto simp add: shared_rel_def MSPessTM.all_simps committer_pc_state_def
                      writeback_pc_def)

 have ?thesis
 using c pre nemp shared_rel_preservation
       [where cs=cs and as=as and cs'="mspesstm_eff cs t (Internal ci)" and as'="tms_eff as t (Internal ai)"]
 using sharedr apply clarsimp
 using ncw by(auto simp add: TMS2.unfold_tms2 MSPessTM.all_simps committer_pc_state_def)
}
moreover
{assume c: "t : TWriter \<and>
           (\<exists> l. ci = write_read1 l \<and>
                 l : dom (MSPessTM.write_set cs t) \<and>
                 TMS2.write_set as t l = MSPessTM.write_set cs t l \<and>
                 TMS2.status as t = Interface.Pending (TMS2.ReadPending l) \<and>
                 ai = read_action cs t l) \<and>
           committing_writer cs \<noteq> Some t"
 have ?thesis
 using c nemp shared_rel_preservation
       [where cs=cs and as=as and cs'="mspesstm_eff cs t (Internal ci)" and as'="tms_eff as t (Internal ai)"]
 using sharedr apply clarsimp
 by(auto simp add: TMS2.unfold_tms2 MSPessTM.all_simps committer_pc_state_def)
}
moreover
{assume c: "t : TWriter \<and>
           (\<exists> l. ci = write_read6 l \<and> ai = read_action cs t l \<and>
            store cs l = store_at as (read_index cs t) l \<and>
            TMS2.status as t = Interface.Pending (TMS2.ReadPending l)) \<and>
            begin_index as t \<le> read_index cs t \<and> 
            (read_index cs t = max_index as \<or> 1 + read_index cs t = max_index as) \<and>
            read_consistent (store_at as (read_index cs t)) (read_set as t) \<and>
           committing_writer cs \<noteq> Some t"

 have ?thesis
 using c nemp shared_rel_preservation
       [where cs=cs and as=as and cs'="mspesstm_eff cs t (Internal ci)" and as'="tms_eff as t (Internal ai)"]
 using sharedr apply clarsimp
 by(auto simp add: TMS2.unfold_tms2 MSPessTM.all_simps committer_pc_state_def)
}
moreover
{assume c: "t : TReader \<and>
           ci = read_commit1 \<and>
           ai = DoCommitReadOnly (read_index cs t) \<and>
           TMS2.status as t = Interface.Pending TMS2.CommitPending \<and>
           begin_index as t \<le> read_index cs t \<and>
           (read_index cs t = max_index as \<or> 1 + read_index cs t = max_index as) \<and>
           read_consistent (store_at as (read_index cs t)) (read_set as t) \<and>
           TMS2.write_set as t = Map.empty"

 have ncw: "committing_writer cs \<noteq> Some t"
 using c pre sharedr
 by(auto simp add: shared_rel_def MSPessTM.all_simps committer_pc_state_def
                      writeback_pc_def)

 have ?thesis
 using c pre nemp shared_rel_preservation
       [where cs=cs and as=as and cs'="mspesstm_eff cs t (Internal ci)" and as'="tms_eff as t (Internal ai)"]
 using sharedr apply clarsimp
 using ncw by(auto simp add: TMS2.unfold_tms2 MSPessTM.all_simps committer_pc_state_def)
}
moreover
{assume c: "t : TWriter \<and>
           ci = write_commit6 \<and>
           ai = DoCommitWriter \<and>
           TMS2.status as t = Interface.Pending TMS2.CommitPending \<and>
           read_consistent (latest_store as) (read_set as t) \<and>
           TMS2.write_set as t = MSPessTM.write_set cs t \<and>
           abstract_index (temp cs t) = max_index as \<and>
           committing_writer cs = None \<and>
           odd(global_version cs) \<and>
           temp cs t = global_version cs"

 have sw_pc: "committer_pc_state (mspesstm_eff cs t (Internal ci)) t (writeback_pc - {write_commit12})"
 using c pre by(auto simp add: committer_pc_state_def MSPessTM.all_simps writeback_pc_def)

 have c_store: "store (mspesstm_eff cs t (Internal ci)) = store_at (tms_eff as t (Internal ai)) (max_index (tms_eff as t (Internal ai)) - 1)"
 using c pre nemp sharedr by (auto simp add: shared_rel_def max_def committer_pc_state_def MSPessTM.all_simps TMS2.unfold_tms2)

 have l_store: "latest_store (tms_eff as t (Internal ai)) = apply_partial (store (mspesstm_eff cs t (Internal ci))) (MSPessTM.write_set (mspesstm_eff cs t (Internal ci)) t)"
 using c pre nemp sharedr by (auto simp add: shared_rel_def max_def committer_pc_state_def MSPessTM.all_simps TMS2.unfold_tms2)

 have aind: "abstract_index(global_version (mspesstm_eff cs t (Internal ci))) = max_index (tms_eff as t (Internal ai))"
 using c pre nemp sharedr
 by (auto simp add: shared_rel_def max_def committer_pc_state_def MSPessTM.all_simps TMS2.unfold_tms2 abstract_index_def)

 have ?thesis using sharedr sw_pc c_store l_store aind
 by(simp add: shared_rel_def committer_pc_state_def)
}
ultimately show ?thesis
using sc_step_behaviour[where cs=cs and as=as and t=t and ci=ci and ai=ai] assms
apply clarsimp 
by smt
qed


lemma internal_preserves_txn_rel_self_step:
  assumes  tinv: "\<forall> a t'. MSPessTM.txn_inv a cs t'" and
      sharedinv: "shared_inv cs" and
          sharedr: "shared_rel cs as" and
          txnrel: "txn_rel (Internal ci) cs as t" and
             pre: "MSPessTM.tau_pre cs t ci" and
              sc:"step_correspondence cs t ci = Some ai" and
            nemp: "dom(stores as) \<noteq> {} \<and> finite(dom(stores as))"
   shows "txn_rel ci1 (mspesstm_eff cs t (Internal ci)) (tms_eff as t (Internal ai)) t"
proof -
{fix l v
 assume c: "t : TWriter \<and>
           (ci = write_write1 l v \<and> ai = DoWrite l v \<and>
           TMS2.status as t = Interface.Pending (TMS2.WritePending l v)) \<and>
           MSPessTM.write_set cs t = TMS2.write_set as t \<and>
           committing_writer cs \<noteq> Some t"

 have ?thesis
 using txnrel pre c 
 by(cases rule: MSPessTM.Event_split[where b=ci1],
        simp_all add: txn_rel_def TMS2.unfold_tms2 MSPessTM.all_simps inflight_txn_at_def inflight_txn_def
                       unfold_eff_txn_ver)
}
moreover
{fix l
 assume c: "t : TReader \<and>
           TMS2.write_set as t = Map.empty \<and>
           (ci = read_read5 l \<and> ai = read_action cs t l \<and>
            store cs l = store_at as (read_index cs t) l \<and>
            begin_index as t \<le> read_index cs t \<and>
            (read_index cs t = max_index as \<or> 1 + read_index cs t = max_index as) \<and>
            TMS2.status as t = Interface.Pending (TMS2.ReadPending l)) \<and>
            read_consistent (store_at as (read_index cs t)) (read_set as t)"

 have ?thesis
 using txnrel pre c 
 by(cases rule: MSPessTM.Event_split[where b=ci1],
        simp_all add: txn_rel_def TMS2.unfold_tms2 MSPessTM.all_simps inflight_txn_at_def inflight_txn_def
                       unfold_eff_txn_ver)
}
moreover
{fix l
 assume c: "t : TWriter \<and>
           ci = write_read1 l \<and>
            l : dom (MSPessTM.write_set cs t) \<and>
            TMS2.write_set as t l = MSPessTM.write_set cs t l \<and>
            TMS2.status as t = Interface.Pending (TMS2.ReadPending l) \<and>
             ai = read_action cs t l \<and>
             committing_writer cs \<noteq> Some t"

 have ?thesis
 using txnrel pre c nemp
 apply(cases rule: MSPessTM.Event_split[where b=ci1])
  apply(simp_all add: MSPessTM.all_simps TMS2.unfold_tms2 inflight_txn_at_def inflight_txn_def  
                 unfold_eff_txn_ver domD txn_rel_def split: option.split)
apply (smt option.case_eq_if option.distinct option.sel)
by blast
}
moreover
{fix l
 assume c: "t : TWriter \<and>
           (ci = write_read6 l \<and> ai = read_action cs t l \<and>
            store cs l = store_at as (read_index cs t) l \<and>
            TMS2.status as t = Interface.Pending (TMS2.ReadPending l)) \<and>
            begin_index as t \<le> read_index cs t \<and> 
            (read_index cs t = max_index as \<or> 1 + read_index cs t = max_index as) \<and>
            read_consistent (store_at as (read_index cs t)) (read_set as t) \<and>
           committing_writer cs \<noteq> Some t \<and>
           MSPessTM.write_set cs t l = TMS2.write_set as t l \<and>
           l \<notin> dom(write_set as t)"

 have ?thesis
 using pre c txnrel nemp sharedr
 apply(cases rule: MSPessTM.Event_split[where b=ci1])
 by(simp_all add: MSPessTM.all_simps TMS2.unfold_tms2 inflight_txn_at_def inflight_txn_def
                 unfold_eff_txn_ver domD domIff txn_rel_def split: option.split)
}
moreover
{assume c: "t : TReader \<and>
           ci = read_commit1 \<and>
           ai = DoCommitReadOnly (read_index cs t) \<and>
           TMS2.status as t = Interface.Pending TMS2.CommitPending \<and>
           begin_index as t \<le> read_index cs t \<and>
           (read_index cs t = max_index as \<or> 1 + read_index cs t = max_index as) \<and>
           read_consistent (store_at as (read_index cs t)) (read_set as t) \<and>
           TMS2.write_set as t = Map.empty"

 have ?thesis
 using txnrel pre c 
 by(cases rule: MSPessTM.Event_split[where b=ci1],
        simp_all add: txn_rel_def TMS2.unfold_tms2 MSPessTM.all_simps inflight_txn_at_def inflight_txn_def
                       unfold_eff_txn_ver)
}
moreover
{assume c: "t : TWriter \<and>
           ci = write_commit6 \<and>
           ai = DoCommitWriter \<and>
           TMS2.status as t = Interface.Pending TMS2.CommitPending \<and>
           read_consistent (latest_store as) (read_set as t) \<and>
           TMS2.write_set as t = MSPessTM.write_set cs t \<and>
           abstract_index (temp cs t) = max_index as \<and>
           committing_writer cs = None \<and>
           odd(global_version cs) \<and>
           temp cs t = global_version cs"

 have ?thesis
 using txnrel pre c 
 by(cases rule: MSPessTM.Event_split[where b=ci1],
        simp_all add: txn_rel_def TMS2.unfold_tms2 MSPessTM.all_simps inflight_txn_at_def inflight_txn_def
                       unfold_eff_txn_ver)
}
ultimately show ?thesis
using sc_step_behaviour[where cs=cs and as=as and t=t and ci=ci and ai=ai] assms
by smt
qed


lemma internal_preserves_txn_rel_self_stutter:
  assumes  tinv: "MSPessTM.txn_inv (Internal ci) cs t" and
          atinv: "TMS2.txn_inv (External BeginResp) as t" and
      sharedinv: "shared_inv cs" and
          sharedr: "shared_rel cs as" and
          txnrel: "txn_rel (Internal ci) cs as t" and
             pre: "MSPessTM.tau_pre cs t ci" and
              sc:"step_correspondence cs t ci = None" and
            nemp: "dom(stores as) \<noteq> {} \<and> finite(dom(stores as))"
   shows "txn_rel e (mspesstm_eff cs t (Internal ci)) as t"
proof -

  have rcs: "ci \<notin> {read_begin1, read_begin2, read_begin3,
                   write_begin1, write_begin2, write_begin3,
                   write_begin4, write_begin5, write_begin6,
                   (* write_commit3, write_commit7, *)write_commit14} \<and>
             read_consistent (store_at as (read_index cs t)) (read_set as t) \<longrightarrow>
              read_consistent (store_at as (read_index (mspesstm_eff cs t (Internal ci)) t)) (read_set as t)"
  using nemp sc tinv pre
  by(cases ci, simp_all add: read_consistent_def step_correspondence_def
            MSPessTM.unfold_mspesstm unfold_eff_txn_ver unfold_txn_props
            split: option.split)

show ?thesis
proof -
{assume c: "ci : {read_begin1, read_begin2, read_begin3,
                  write_begin1, write_begin2, write_begin3, write_begin4, write_begin5, write_begin6}"
 have ?thesis
 using c txnrel pre rcs atinv tinv
 apply(cases ci, simp_all)
by(cases rule: MSPessTM.Event_split[where b=e],
      simp_all add: txn_rel_def TMS2.unfold_tms2 MSPessTM.unfold_mspesstm inflight_txn_at_def inflight_txn_def
                       unfold_eff_txn_ver beginning_txn_def TMS2.txn_inv_def read_consistent_def
                       Interface.ext_enabled_def MSPessTM.txn_inv_def is_waiting_writer_def
               split: if_splits option.split)+
}
moreover
{fix l
 assume c: "ci : {read_read1 l, read_read2 l, read_read3 l, read_read4 l, read_read5 l,
                  write_read1 l, write_read2 l, write_read3 l, write_read4 l, write_read5 l, write_read6 l}"
 have ?thesis
 using c txnrel pre rcs sc
 apply(cases ci, simp_all)
by(cases rule: MSPessTM.Event_split[where b=e],
      simp_all add: txn_rel_def TMS2.unfold_tms2 MSPessTM.unfold_mspesstm inflight_txn_at_def inflight_txn_def
                       unfold_eff_txn_ver beginning_txn_def TMS2.txn_inv_def 
                       Interface.ext_enabled_def MSPessTM.txn_inv_def is_waiting_writer_def
                       step_correspondence_def domIff
               split: if_splits option.split)+
}
moreover
{fix l v
 assume c: "ci = write_write1 l v"
 have ?thesis
 using c txnrel pre rcs sc
 by(simp_all add: step_correspondence_def)
}
moreover
{fix l v
 assume c: "ci = read_commit1"
 have ?thesis
 using c txnrel pre rcs sc
 by(simp_all add: step_correspondence_def)
}
moreover
{ assume c: "ci : {write_commit0, write_commit1, write_commit2, write_commit3a(* , write_commit3 *)}"

  have gi: "abstract_index (global_version cs) = max_index as"
  using sharedr by (simp add: shared_rel_def)

  have etv: "committing_writer cs \<noteq> Some t \<longrightarrow> eff_txn_ver cs t = txn_ver cs t"
  using tinv pre c by(cases ci, simp_all add: MSPessTM.txn_inv_def
                                   MSPessTM.unfold_mspesstm is_active_writer_def
                                   unfold_eff_txn_ver split: option2.split)

  have ri: "ci = write_commit1 \<longrightarrow> read_index cs t = max_index as"
  using c sharedr sharedinv tinv sharedr pre gi etv
  apply(cases ci, simp_all add: abstract_index_def MSPessTM.txn_inv_def
                    MSPessTM.unfold_mspesstm shared_inv_def shared_rel_def
                    is_active_writer_def txn_ver_states_def inflight_txn_at_def inflight_txn_def)
by (metis even_Suc_div_two le_SucE le_antisym)
  have ?thesis
 using c txnrel pre rcs sc tinv ri
 apply(cases ci, simp_all)
 by(cases rule: MSPessTM.Event_split[where b=e],
      simp_all add: txn_rel_def TMS2.unfold_tms2 MSPessTM.unfold_mspesstm inflight_txn_at_def inflight_txn_def
                       unfold_eff_txn_ver beginning_txn_def TMS2.txn_inv_def 
                       Interface.ext_enabled_def MSPessTM.txn_inv_def is_waiting_writer_def
                       step_correspondence_def domIff is_active_writer_def txn_ver_states_def
                split: if_splits option.split)+
}
moreover
{ assume c: "ci : {write_commit4, (* write_commit5,*) write_commit6}"

  have gi: "abstract_index (global_version cs) = max_index as"
  using sharedr by (simp add: shared_rel_def)


  have ?thesis
 using c txnrel pre  sc tinv
 apply(cases ci, simp_all)
 by(cases rule: MSPessTM.Event_split[where b=e],
      simp_all add: txn_rel_def TMS2.unfold_tms2 MSPessTM.unfold_mspesstm inflight_txn_at_def inflight_txn_def
                       unfold_eff_txn_ver beginning_txn_def TMS2.txn_inv_def 
                       Interface.ext_enabled_def MSPessTM.txn_inv_def is_waiting_writer_def
                       step_correspondence_def domIff is_active_writer_def txn_ver_states_def
                split: if_splits option.split)+
}
moreover
{ assume c: "ci : {(*write_commit7,*) write_commit8, write_commit9, write_commit10,
                   write_commit11, write_commit12, write_commit13, write_commit14}"

  have gi: "abstract_index (global_version cs) = max_index as"
  using sharedr by (simp add: shared_rel_def)


  have ?thesis
 using c txnrel pre  sc tinv
 apply(cases ci, simp_all)
 by(cases rule: MSPessTM.Event_split[where b=e],
      simp_all add: txn_rel_def TMS2.unfold_tms2 MSPessTM.unfold_mspesstm inflight_txn_at_def inflight_txn_def
                       unfold_eff_txn_ver beginning_txn_def TMS2.txn_inv_def 
                       Interface.ext_enabled_def MSPessTM.txn_inv_def is_waiting_writer_def
                       step_correspondence_def domIff is_active_writer_def txn_ver_states_def
                split: if_splits option.split)+
}
ultimately show ?thesis using assms
by(cases ci, auto)
qed
qed


lemma external_preserves_txn_rel_self:
  assumes   txnr: "txn_rel (External a) cs as t" and
             sharedr: "shared_rel cs as" and
             pre: "MSPessTM.mspesstm_pre cs t (External a)" and
            nemp: "dom(stores as) \<noteq> {} \<and> finite(dom(stores as))"
  shows "txn_rel b (mspesstm_eff cs t (External a)) (tms_eff as t (External a)) t"
proof (cases a)
    case BeginInv from this assms show ?thesis
      by (cases rule: MSPessTM.Event_split[where b=b],
          auto simp add: beginning_txn_def shared_rel_def txn_rel_def MSPessTM.all_simps TMS2.unfold_tms2)
    next
    case BeginResp from this assms show ?thesis
      by (cases rule: MSPessTM.Event_split[where b=b],
          auto simp add: txn_ver_def inflight_txn_at_def inflight_txn_def txn_rel_def MSPessTM.all_simps TMS2.unfold_tms2)
    next
    case CommitInv from this assms show ?thesis
      apply(cases rule: MSPessTM.Event_split[where b=b],
          simp_all add: shared_rel_def txn_ver_def inflight_txn_at_def inflight_txn_def txn_rel_def
                        MSPessTM.all_simps TMS2.unfold_tms2
                  split: option.split)
      by (smt option.simps )+
    next
    case CommitResp from this assms show ?thesis
      by (cases rule: MSPessTM.Event_split[where b=b],
          auto simp add: txn_ver_def inflight_txn_at_def inflight_txn_def txn_rel_def MSPessTM.all_simps TMS2.unfold_tms2)
    next
    case Cancel from this pre show ?thesis by (simp add: MSPessTM.all_simps)
    next
    case Abort from this pre show ?thesis by (simp add: MSPessTM.all_simps)
    next
    case (ReadInv l) from this assms show ?thesis
      apply(cases rule: MSPessTM.Event_split[where b=b],
          simp_all add: shared_rel_def txn_ver_def inflight_txn_at_def inflight_txn_def txn_rel_def MSPessTM.all_simps TMS2.unfold_tms2
                  split: option.split)
      by (smt option.simps )+
    next
    case (ReadResp v) from this assms show ?thesis
      by (cases rule: MSPessTM.Event_split[where b=b],
          auto simp add: txn_ver_def inflight_txn_at_def inflight_txn_def txn_rel_def MSPessTM.all_simps TMS2.unfold_tms2)
    next
    case (WriteInv l v) from this assms show ?thesis
      by(cases rule: MSPessTM.Event_split[where b=b],
          simp_all add: shared_rel_def txn_ver_def inflight_txn_at_def inflight_txn_def txn_rel_def MSPessTM.all_simps TMS2.unfold_tms2
                  split: option.split)
    next
    case WriteResp from this assms show ?thesis
      by (cases rule: MSPessTM.Event_split[where b=b],
          auto simp add: txn_ver_def inflight_txn_at_def inflight_txn_def txn_rel_def MSPessTM.all_simps TMS2.unfold_tms2)
  qed


lemma internal_preserves_txn_rel_other_step:
  assumes  tinv: "\<forall> a t'. MSPessTM.txn_inv a cs t'" and
      sharedinv: "shared_inv cs" and
          sharedr: "shared_rel cs as" and
          txnrel: "txn_rel (Internal ci) cs as at" and
             pre: "MSPessTM.tau_pre cs at ci" and
              sc:"step_correspondence cs at ci = Some ai" and
             neq: "t \<noteq> at" and
      other_trel: "txn_rel b cs as t" and
       other_tinv: "MSPessTM.txn_inv b cs t" and
            nemp: "dom(stores as) \<noteq> {} \<and> finite(dom(stores as))"
   shows "txn_rel b (mspesstm_eff cs at (Internal ci)) (tms_eff as at (Internal ai)) t"
proof -
{assume npre: "\<not> mspesstm_pre cs t b"

 have status_pres: "MSPessTM.status (mspesstm_eff cs at (Internal ci)) t = MSPessTM.status cs t"
 using neq
  by(cases rule: MSPessTM.Event_split[where b="Internal ci"])
    (simp_all add: MSPessTM.all_simps split: option.split) 

 have pre_pres: "mspesstm_pre (mspesstm_eff cs at (Internal ci)) t b \<longrightarrow> mspesstm_pre cs t b"
 using status_pres
 by(cases rule: MSPessTM.Event_split[where b="b"])
   (simp_all add: mspesstm_pre_def MSPessTM.tau_pre_def MSPessTM.ext_pre_def)

 from pre_pres npre have ?thesis using neq pre
 by(simp add: txn_rel_def)
}
moreover
{fix l v
 assume c: "at : TWriter \<and>
           (ci = write_write1 l v \<and> ai = DoWrite l v \<and>
           TMS2.status as at = Interface.Pending (TMS2.WritePending l v)) \<and>
           MSPessTM.write_set cs at = TMS2.write_set as at \<and>
           committing_writer cs \<noteq> Some at"

 have ?thesis
 using pre c other_trel neq
 by(cases rule: MSPessTM.Event_split[where b=b],
        auto simp add: txn_rel_def TMS2.unfold_tms2 MSPessTM.all_simps inflight_txn_at_def inflight_txn_def
                       unfold_eff_txn_ver beginning_txn_def)
}
moreover
{fix l
 assume c: "at : TReader \<and>
           TMS2.write_set as at = Map.empty \<and>
           (ci = read_read5 l \<and> ai = read_action cs at l \<and>
            store cs l = store_at as (read_index cs at) l \<and>
            begin_index as at \<le> read_index cs at \<and>
            (read_index cs at = max_index as \<or> 1 + read_index cs at = max_index as) \<and>
            TMS2.status as at = Interface.Pending (TMS2.ReadPending l)) \<and>
            read_consistent (store_at as (read_index cs at)) (read_set as at)"

 have ?thesis
 using pre c other_trel neq
 by(cases rule: MSPessTM.Event_split[where b=b],
        auto simp add: txn_rel_def TMS2.unfold_tms2 MSPessTM.all_simps inflight_txn_at_def inflight_txn_def
                       unfold_eff_txn_ver beginning_txn_def)
}
moreover
{fix l
 assume c: "at : TWriter \<and>
           ci = write_read1 l \<and>
            l : dom (MSPessTM.write_set cs at) \<and>
            TMS2.status as at = Interface.Pending (TMS2.ReadPending l) \<and>
             ai = read_action cs at l \<and>
             committing_writer cs \<noteq> Some at \<and>
             TMS2.write_set as at l = MSPessTM.write_set cs at l"

 have s: "stores (tms_eff as at (Internal ai)) = stores as"
 using c by (auto simp add: TMS2.unfold_all_tms2)

 have ?thesis
 using pre s c other_trel neq
 apply(cases rule: MSPessTM.Event_split[where b=b])
 by(simp_all add: txn_rel_def TMS2.unfold_tms2 MSPessTM.unfold_mspesstm inflight_txn_at_def inflight_txn_def
                       beginning_txn_def unfold_eff_txn_ver domD domIff store_at_def
                       max_index_def
 split: option.split if_splits)
}
moreover
{fix l
 assume c: "at : TWriter \<and>
           (ci = write_read6 l \<and> ai = read_action cs at l \<and>
            store cs l = store_at as (read_index cs at) l \<and>
            TMS2.status as at = Interface.Pending (TMS2.ReadPending l)) \<and>
            begin_index as at \<le> read_index cs at \<and> 
            (read_index cs at = max_index as \<or> 1 + read_index cs at = max_index as) \<and>
            read_consistent (store_at as (read_index cs at)) (read_set as at) \<and>
           committing_writer cs \<noteq> Some at \<and>
           MSPessTM.write_set cs at l = TMS2.write_set as at l \<and>
           l \<notin> dom(write_set as at)"

 have ?thesis
 using pre c other_trel neq
 by(cases rule: MSPessTM.Event_split[where b=b],
        auto simp add: txn_rel_def TMS2.unfold_tms2 MSPessTM.all_simps inflight_txn_at_def inflight_txn_def
                       unfold_eff_txn_ver beginning_txn_def)
}
moreover
{assume c: "at : TReader \<and>
           ci = read_commit1 \<and>
           ai = DoCommitReadOnly (read_index cs at) \<and>
           TMS2.status as at = Interface.Pending TMS2.CommitPending \<and>
           begin_index as at \<le> read_index cs at \<and>
           (read_index cs at = max_index as \<or> 1 + read_index cs at = max_index as) \<and>
           read_consistent (store_at as (read_index cs at)) (read_set as at) \<and>
           TMS2.write_set as at = Map.empty"

 have ?thesis
 using pre c other_trel neq
 by(cases rule: MSPessTM.Event_split[where b=b],
        auto simp add: txn_rel_def TMS2.unfold_tms2 MSPessTM.all_simps inflight_txn_at_def inflight_txn_def
                       unfold_eff_txn_ver beginning_txn_def)
}
moreover
{assume c: "at : TWriter \<and>
           ci = write_commit6 \<and>
           ai = DoCommitWriter \<and>
           TMS2.status as at = Interface.Pending TMS2.CommitPending \<and>
           read_consistent (latest_store as) (read_set as at) \<and>
           TMS2.write_set as at = MSPessTM.write_set cs at \<and>
           abstract_index (temp cs at) = max_index as \<and>
           committing_writer cs = None \<and>
           odd(global_version cs) \<and>
           temp cs at = global_version cs \<and>
           b \<notin> {Internal write_commit2, Internal write_commit3a,
                (* Internal write_commit3,  *)Internal write_commit4, (*Internal write_commit5,*)
                Internal write_commit6}"

 have ri: "clean_txn_ver cs t b \<longrightarrow> read_index cs t \<le> max_index as"
 using c other_tinv sharedr
 txn_inv_implies_txn_ver_leq_global_ver[where s=cs and t=t and a=b]
 apply clarsimp
 by(auto simp add: abstract_index_def max_index_def shared_rel_def)

 have rc: "\<forall> n. n \<le> max_index as \<longrightarrow>
              read_consistent (store_at as n) (read_set as t) \<longrightarrow>
           (let as' = (tms_eff as at (Internal ai)) in
             read_consistent (store_at as' n) (read_set as' t))"
 using c nemp by(auto simp add: TMS2.unfold_all_tms2 split: option.split)

 have rc_ri: "clean_txn_ver cs t b
            \<and> read_consistent (store_at as (read_index cs t)) (read_set as t) \<longrightarrow>
           (let as' = (tms_eff as at (Internal ai)) in
             read_consistent (store_at as' (read_index cs t)) (read_set as' t))"
 using rc ri by blast

 have rc_mi: "read_consistent (store_at as (max_index as)) (read_set as t) \<longrightarrow>
           (let as' = (tms_eff as at (Internal ai)) in
             read_consistent (store_at as' (max_index as)) (read_set as' t))"
 using rc by blast

 have ?thesis
 using pre c other_trel neq rc_ri rc_mi
 apply(cases rule: MSPessTM.Event_split[where b=b],
        simp_all add: txn_rel_def TMS2.unfold_tms2 MSPessTM.unfold_mspesstm inflight_txn_at_def inflight_txn_def
                       unfold_eff_txn_ver beginning_txn_def write_back_def
                       clean_txn_ver_def
            abstract_index_def)
by(smt le_SucI)+
}
moreover
{assume c: "at : TWriter \<and>
           ci = write_commit6 \<and>
           ai = DoCommitWriter \<and>
           TMS2.status as at = Interface.Pending TMS2.CommitPending \<and>
           read_consistent (latest_store as) (read_set as at) \<and>
           TMS2.write_set as at = MSPessTM.write_set cs at \<and>
           abstract_index (temp cs at) = max_index as \<and>
           committing_writer cs = None \<and>
           odd(global_version cs) \<and>
           temp cs at = global_version cs \<and>
           b \<in> {Internal write_commit2, Internal write_commit3a,
                (* Internal write_commit3,  *)Internal write_commit4, (*Internal write_commit5,*)
                Internal write_commit6} \<and>
           mspesstm_pre cs t b"

  have ttinv: "\<forall> a. MSPessTM.txn_inv a cs t"
  using tinv by blast

  have attinv: "MSPessTM.txn_inv (Internal write_commit6) cs at"
  using c tinv by blast

  have t_act: "active_writer cs = Some t"
  using ttinv c
  by (cases rule: MSPessTM.Event_split[where b=b],
                   auto simp add: unfold_txn_props)

  have at_act: "active_writer cs = Some at"
  using c attinv pre by(auto simp add: unfold_txn_props mspesstm_pre_def)

  have ?thesis
  using t_act at_act neq by simp
}
ultimately show ?thesis
using sc_step_behaviour[where cs=cs and as=as and t=at and ci=ci and ai=ai] assms
by smt
qed


lemma external_preserves_txn_rel_other:
  assumes   txnr: "txn_rel b cs as t" and
             neq: "t \<noteq> at" and
             pre: "MSPessTM.mspesstm_pre cs at (External a)" and
            nemp: "dom(stores as) \<noteq> {} \<and> finite(dom(stores as))"
  shows "txn_rel b (mspesstm_eff cs at (External a)) (tms_eff as at (External a)) t"
proof -

 have s: "stores (tms_eff as at (External a)) = stores as"
 using pre by (cases a, auto simp add: TMS2.unfold_all_tms2)

 have rs: "read_set (tms_eff as at (External a)) t = read_set as t"
 using pre by (cases a, auto simp add: TMS2.unfold_all_tms2)

 have ws: "write_set (tms_eff as at (External a)) t = write_set as t"
 using pre by (cases a, auto simp add: TMS2.unfold_all_tms2)

 have stats: "status (tms_eff as at (External a)) t = status as t"
 using neq pre by (cases a, auto simp add: TMS2.unfold_all_tms2)

 have begs: "begin_index (tms_eff as at (External a)) t = begin_index as t"
 using neq pre by (cases a, auto simp add: TMS2.unfold_all_tms2)

 have ri: "read_index (mspesstm_eff cs at (External a)) t = read_index cs t"
 using pre neq by(cases a, simp_all add: MSPessTM.all_simps txn_ver_def)

show ?thesis
proof (cases a)
    case BeginInv from this pre s txnr rs ws stats begs ri neq show ?thesis
      by (cases rule: MSPessTM.Event_split[where b=b],
          simp_all add: beginning_txn_def inflight_txn_at_def inflight_txn_def txn_rel_def
                        MSPessTM.all_simps
              split: option2.split if_splits option.split)
    next
    case BeginResp from this pre s txnr rs ws stats begs ri neq show ?thesis
      by (cases rule: MSPessTM.Event_split[where b=b],
          simp_all add: beginning_txn_def inflight_txn_at_def inflight_txn_def txn_rel_def
                        MSPessTM.all_simps
              split: option2.split if_splits option.split)
    next
    case CommitInv from this pre s txnr rs ws stats begs ri neq show ?thesis
      by (cases rule: MSPessTM.Event_split[where b=b],
          simp_all add: beginning_txn_def inflight_txn_at_def inflight_txn_def txn_rel_def
                        MSPessTM.all_simps
              split: option2.split if_splits option.split)
    next
    case CommitResp from this pre s txnr rs ws stats begs ri neq show ?thesis
      by (cases rule: MSPessTM.Event_split[where b=b],
          simp_all add: beginning_txn_def inflight_txn_at_def inflight_txn_def txn_rel_def
                        MSPessTM.all_simps
              split: option2.split if_splits option.split)
    next
    case Cancel from this pre show ?thesis by (simp add: MSPessTM.all_simps)
    next
    case Abort from this pre show ?thesis by (simp add: MSPessTM.all_simps)
    next
    case (ReadInv l) from this pre s txnr rs ws stats begs ri neq show ?thesis
      by (cases rule: MSPessTM.Event_split[where b=b],
          simp_all add: beginning_txn_def inflight_txn_at_def inflight_txn_def txn_rel_def
                        MSPessTM.all_simps
              split: option2.split if_splits option.split)
    next
    case (ReadResp v) from this pre s txnr rs ws stats begs ri neq show ?thesis
      by (cases rule: MSPessTM.Event_split[where b=b],
          simp_all add: beginning_txn_def inflight_txn_at_def inflight_txn_def txn_rel_def
                        MSPessTM.all_simps
              split: option2.split if_splits option.split)
    next
    case (WriteInv l v) from this pre s txnr rs ws stats begs ri neq show ?thesis
      by (cases rule: MSPessTM.Event_split[where b=b],
          simp_all add: beginning_txn_def inflight_txn_at_def inflight_txn_def txn_rel_def
                        MSPessTM.all_simps
              split: option2.split if_splits option.split)
    next
    case WriteResp from this pre s txnr rs ws stats begs ri neq show ?thesis
      by (cases rule: MSPessTM.Event_split[where b=b],
          simp_all add: beginning_txn_def inflight_txn_at_def inflight_txn_def txn_rel_def
                        MSPessTM.all_simps
              split: option2.split if_splits option.split)
  qed
qed




lemma internal_preserves_txn_rel_other:
  assumes   txnr: "txn_rel b cs as t" and
             neq: "t \<noteq> at" and
             pre: "MSPessTM.mspesstm_pre cs at (Internal a)" and
              sc:"step_correspondence cs at a = None" and
             tinv: "MSPessTM.txn_inv (Internal a) cs at" and
            nemp: "dom(stores as) \<noteq> {} \<and> finite(dom(stores as))"
  shows "txn_rel b (mspesstm_eff cs at (Internal a)) as t"
proof -

 have tm: "temp (mspesstm_eff cs at (Internal a)) t = temp cs t"
 using pre neq by(cases a, simp_all add: MSPessTM.all_simps txn_ver_def split: option.split)

 have tv: "txn_ver (mspesstm_eff cs at (Internal a)) t = txn_ver cs t"
 using pre neq by(cases a, simp_all add: MSPessTM.all_simps txn_ver_def split: option.split)

 have ri: "read_index (mspesstm_eff cs at (Internal a)) t = read_index cs t"
 using tv pre neq by(cases a, simp_all add: MSPessTM.all_simps
                           split: option2.split)

 have gv: "a \<noteq> write_commit13 \<longrightarrow> global_version (mspesstm_eff cs at (Internal a)) = global_version cs"
 using sc tv pre neq by(cases a, simp_all add: MSPessTM.all_simps step_correspondence_def
                           split: option.split option2.split)

 have ws: "MSPessTM.write_set (mspesstm_eff cs at (Internal a)) t = MSPessTM.write_set cs t"
 using neq pre by (cases a, auto simp add: MSPessTM.all_simps split: option.split)

 have st: "MSPessTM.status (mspesstm_eff cs at (Internal a)) t = MSPessTM.status cs t"
 using neq pre by (cases a, auto simp add: MSPessTM.all_simps split: option.split)

 have pre_stable: "mspesstm_pre (mspesstm_eff cs at (Internal a)) t b = mspesstm_pre cs t b"
 using neq pre by (cases a, auto simp add: MSPessTM.all_simps
                                    split: option.split PC.split Event.split Action.split)

show ?thesis
proof -
{fix l
 assume c: "a \<noteq> write_commit13"
 have ?thesis
 using c txnr neq ri tv sc tv gv ws st pre_stable tm
 apply(cases a, clarsimp)
 by(cases rule: MSPessTM.Event_split[where b=b],
          simp_all add: beginning_txn_def inflight_txn_at_def inflight_txn_def txn_rel_def
                        step_correspondence_def
              split: option.split)+
}
moreover
{fix l
 assume c: "a = write_commit13"

 have gvp: "global_version (mspesstm_eff cs at (Internal a)) = 1 + global_version cs"
 using sc tv pre neq tinv c
 by(simp_all add: MSPessTM.all_simps step_correspondence_def MSPessTM.txn_inv_def
                           split: option.split option2.split)

 have ?thesis
 using c txnr neq ri tv sc tv gvp ws st pre_stable tm
 by(cases rule: MSPessTM.Event_split[where b=b],
          auto simp add: beginning_txn_def inflight_txn_at_def inflight_txn_def txn_rel_def
                        step_correspondence_def abstract_index_def
              split: option.split)
}
ultimately show ?thesis using assms
by(cases a, auto)
qed
qed

lemma MSPessTM_inv:
  "reach MSPessTM cs \<longrightarrow> MSPessTM.shared_inv cs \<and> (\<forall> a t. MSPessTM.txn_inv a cs t)"
using MSPessTM_invariant by (auto simp add: invariant_def reach_def)

lemma TMS2_inv:
  "reach TMS2 as \<longrightarrow> stores as \<noteq> Map.empty \<and> finite (dom (stores as))
      \<and> (\<forall> a t. TMS2.txn_inv a as t)"
using total_inv stores_domain_def invariant_def reach_def
by (metis (mono_tags, lifting) Max_in domIff)


lemma sim_rel_preserved_internal_step:
  assumes sr: "sim_rel cs as" and
         creach: "reach MSPessTM cs" and
         areach: "reach TMS2 as" and
          pre: "mspesstm_pre cs at (Internal ic)" and
           sc: "step_correspondence cs at ic = Some ia"
    shows "sim_rel (mspesstm_eff cs at (Internal ic)) (tms_eff as at (Internal ia))"
using
internal_preserves_txn_rel_self_step[where cs=cs and as=as and ci=ic and t=at and ai=ia]
internal_preserves_shared_rel_step[where cs=cs and as=as and ci=ic]
internal_preserves_txn_rel_other_step[where cs=cs and as=as and ci=ic and at=at and ai=ia]
TMS2_inv[where as=as]
MSPessTM_inv[where cs=cs]
assms
by(fastforce simp add: sim_rel_def mspesstm_pre_def)

lemma sim_rel_preserved_internal_stutter:
  assumes sr: "sim_rel cs as" and
         creach: "reach MSPessTM cs" and
         areach: "reach TMS2 as" and
          pre: "mspesstm_pre cs at (Internal ic)" and
           sc: "step_correspondence cs at ic = None"
    shows "sim_rel (mspesstm_eff cs at (Internal ic)) as"
using
internal_preserves_txn_rel_self_stutter[where cs=cs and as=as and ci=ic and t=at]
internal_preserves_txn_rel_other[where cs=cs and as=as and a=ic and at=at]
internal_preserves_shared_stutter_commit[where cs=cs and as=as and ci=ic]
internal_preserves_shared_stutter_no_commit[where cs=cs and as=as and ci=ic]
TMS2_inv[where as=as]
MSPessTM_inv[where cs=cs]
assms
apply(cases ic)
by(fastforce simp add: sim_rel_def mspesstm_pre_def writeback_pc_def)+

lemma start_satisfies_sim:
  assumes "Transitions.start MSPessTM cs"
  shows "shared_rel cs default_start \<and> (\<forall> t. txn_rel a cs default_start t)"
proof -

  have cw: "committing_writer cs = None"
  using assms by (simp add: MSPessTM_def MSPessTM.start_def)

  have ai: "abstract_index (global_version cs) = 0 \<and> max_index default_start = 0"
  using assms apply(simp add: MSPessTM_def MSPessTM.start_def abstract_index_def
                                default_start_def max_index_def)
  by(auto simp add: domD RWMemory.max_initial)

  have st: "store cs = latest_store default_start"
  using assms cw ai
  by(simp add: default_start_def MSPessTM_def MSPessTM.start_def
          latest_store_def max_index_def store_at_def)

  show ?thesis using assms cw ai st
  by(cases rule: MSPessTM.Event_split[where b=a],
       auto simp add: mem_initial_def TMS2.default_start_def
                  sim_rel_def txn_rel_def shared_rel_def
                  abstract_index_def MSPessTM.all_simps)
qed

lemma is_standard_simulation:
 "standard_simulation MSPessTM TMS2 step_correspondence sim_rel"
proof -
  have start: "\<forall> cs. Transitions.start MSPessTM cs \<longrightarrow>
                        (\<exists> as. sim_rel cs as \<and> Transitions.start TMS2 as)"
  apply(rule allI, rule impI, rule exI[where x=TMS2.default_start])
  using default_is_start start_satisfies_sim
  by (fastforce simp add: sim_rel_def)


  have ext_pre: "\<forall> cs as at a. reach MSPessTM cs \<and> reach TMS2 as \<longrightarrow>
       sim_rel cs as \<and>
       pre MSPessTM cs at (External a) \<longrightarrow>
       pre TMS2 as at (External a)"
  using step_external_precondition TMS2_inv MSPessTM_inv
  apply (simp add: sim_rel_def MSPessTM_def)
  by blast

  have int_pre: "\<forall> cs as at ic ia. reach MSPessTM cs \<and> reach TMS2 as \<longrightarrow>
       sim_rel cs as \<and>
       pre MSPessTM cs at (Internal ic) \<and>
       step_correspondence cs at ic = Some ia
        \<longrightarrow>
       pre TMS2 as at (Internal ia)"
  using step_internal_precondition TMS2_inv MSPessTM_inv
  apply (simp add: sim_rel_def MSPessTM_def mspesstm_pre_def)
  by blast

  have ext: "standard_sim_ext_step MSPessTM TMS2 sim_rel"
  apply (unfold standard_sim_ext_step_def)
  using MSPessTM_inv external_preserves_shared_rel ext_pre
        external_preserves_txn_rel_other external_preserves_txn_rel_self TMS2_inv
  apply (simp add: sim_rel_def)
  by (smt DAut.select_convs(2) DAut.select_convs(3) MSPessTM_def TMS2_def TMS2_inv)

  have stutter: "standard_sim_stutter MSPessTM TMS2 step_correspondence sim_rel"
  apply (unfold standard_sim_stutter_def)
  using sim_rel_preserved_internal_stutter
  by (auto simp add: MSPessTM_def)

  have step: "standard_sim_int_step MSPessTM TMS2 step_correspondence sim_rel"
  apply (unfold standard_sim_int_step_def)
  using sim_rel_preserved_internal_step int_pre
  by (auto simp add: MSPessTM_def)

show ?thesis
using start ext stutter step by (fastforce simp add: standard_simulation_def)
qed

theorem MSPessTM_TMS2_trace_inclusion:
  "traces (ioa MSPessTM) \<subseteq> traces (ioa TMS2)"
using standard_simulation_trace_inclusion[OF is_standard_simulation]
by blast

end
