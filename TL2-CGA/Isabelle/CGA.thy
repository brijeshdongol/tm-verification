theory CGA
imports Transitions Interface Utilities
begin

datatype pc =
  Begin
| Commit
| Read
| Write

type_synonym status = "pc Interface.Status"

record state =
  store :: "L \<Rightarrow> V"
  status :: "T \<Rightarrow> status"
  
  (* input parameters *)
  val :: "T \<Rightarrow> V"
  addr :: "T \<Rightarrow> L"

  (* program variable *)
  gvc :: "nat"
  lver :: "L \<Rightarrow> nat"
  ver :: "T \<Rightarrow> nat"
  write_set :: "T \<Rightarrow> L \<Rightarrow> V option"
  read_set :: "T \<Rightarrow> L \<Rightarrow> V option"

definition update_status :: "T \<Rightarrow> status \<Rightarrow> state \<Rightarrow> state" where
  "update_status t st \<equiv> \<lambda>s. s \<lparr> status := ((status s) (t := st))\<rparr>"

definition update_store :: "L \<Rightarrow> V \<Rightarrow> state \<Rightarrow> state" where
  "update_store l v \<equiv> \<lambda>s. s \<lparr> store := ((store s) (l := v)) \<rparr>"

definition update_val :: "T \<Rightarrow> V \<Rightarrow> state \<Rightarrow> state" where
  "update_val t v \<equiv> \<lambda>s. s \<lparr> val := ((val s) (t := v))\<rparr>"

definition update_addr :: "T \<Rightarrow> L \<Rightarrow> state \<Rightarrow> state" where
  "update_addr t l \<equiv> \<lambda>s. s \<lparr> addr := ((addr s) (t := l))\<rparr>"

definition update_gvc :: "nat \<Rightarrow> state \<Rightarrow> state" where
  "update_gvc n \<equiv> \<lambda>s. s \<lparr> gvc := n \<rparr>"

definition update_ver :: "T \<Rightarrow> nat \<Rightarrow> state \<Rightarrow> state" where
  "update_ver t n \<equiv> \<lambda>s. s \<lparr> ver := ((ver s) (t := n))\<rparr>"

definition update_lver :: "L \<Rightarrow> nat \<Rightarrow> state \<Rightarrow> state" where
  "update_lver l n \<equiv> \<lambda>s. s \<lparr> lver := ((lver s) (l := n))\<rparr>"

definition update_write_set :: "T \<Rightarrow> L \<Rightarrow> V \<Rightarrow> state \<Rightarrow> state"
  where
  "update_write_set t l v \<equiv> \<lambda>s. s \<lparr> write_set := update_partial t l v (write_set s)\<rparr>"  

definition update_read_set :: "T \<Rightarrow> L \<Rightarrow> V \<Rightarrow> state \<Rightarrow> state"
  where
  "update_read_set t l v \<equiv> \<lambda>s. s \<lparr> read_set := update_partial t l v (read_set s)\<rparr>"  

definition update_store_set :: "(L \<Rightarrow> V option) \<Rightarrow> state \<Rightarrow> state" where
  "update_store_set ws \<equiv> \<lambda>s. s \<lparr> store := (\<lambda>l. if ws l = None then store s l else the (ws l)) \<rparr>"

definition update_lver_set :: "(L \<Rightarrow> V option) \<Rightarrow> nat \<Rightarrow> state \<Rightarrow> state" where
  "update_lver_set ws n \<equiv> \<lambda>s. s \<lparr> lver := (\<lambda>l. if ws l = None then (lver s l) else n) \<rparr>"

definition start :: "state \<Rightarrow> bool" where
  "start s \<equiv>
      (\<forall>t. status s t = NotStarted)
    \<and> store s \<in> mem_initial
    \<and> gvc s = 0
    \<and> (\<forall> t. ver s t = gvc s)
    \<and> (\<forall> l. lver s l = gvc s) 
    \<and> (\<forall>t l. read_set s t l = None)
    \<and> (\<forall>t l. write_set s t l = None)"

definition cga_pre :: "T \<Rightarrow> pc Event \<Rightarrow> state \<Rightarrow> bool" where
  "cga_pre t e s \<equiv>
      e \<noteq> External Cancel
    \<and> (e = External Abort \<longrightarrow> status s t \<notin> {NotStarted, Ready, CommitResponding, Committed, Aborted})
    \<and> (case e of
           External a \<Rightarrow> ext_enabled (status s t) a
         | Internal a \<Rightarrow> status s t = Pending a)"

fun ext_eff :: "T \<Rightarrow> Action \<Rightarrow> state \<Rightarrow> state" where
  "ext_eff t BeginInv = update_status t (Pending Begin)"
| "ext_eff t BeginResp = update_status t Ready"
| "ext_eff t CommitInv = update_status t (Pending Commit)"
| "ext_eff t CommitResp = update_status t Committed"
| "ext_eff t Abort = update_status t Aborted"
| "ext_eff t (ReadInv l) = update_addr t l \<circ>> update_status t (Pending Read)"
| "ext_eff t (ReadResp v) = update_status t Ready"
| "ext_eff t (WriteInv l v) = update_addr t l \<circ>> update_val t v \<circ>> update_status t (Pending Write)"
| "ext_eff t WriteResp = update_status t Ready"


definition validate :: "T \<Rightarrow> state \<Rightarrow> bool" where
  "validate t s \<equiv> (\<forall>l v. read_set s t l = Some v \<longrightarrow> lver s l \<le> ver s t) "

definition rs_consistent :: "T \<Rightarrow> state \<Rightarrow> bool" where
  "rs_consistent t s \<equiv> (\<forall>l v. (state.read_set s t l = Some v) \<longrightarrow> store s l = v)"

definition rs_consistent_read :: "T \<Rightarrow> state \<Rightarrow> bool" where
  "rs_consistent_read t s \<equiv> (\<forall>l v. (state.read_set s t l = Some v \<and> state.write_set s t l = None) \<longrightarrow> store s l = v)"

fun int_eff :: "T \<Rightarrow> pc \<Rightarrow> state \<Rightarrow> state" where
  "int_eff t Begin = (\<lambda>s. update_ver t (gvc s) s) \<circ>> update_status t BeginResponding"

| "int_eff t Commit = (\<lambda>s.
    if (\<forall>l. write_set s t l = None)
    then (update_status t CommitResponding) s
    else (if (ver s t = gvc s) \<or> validate t s
      then (update_store_set (write_set s t) \<circ>> 
            gvc_update (\<lambda>n. n + 1) \<circ>>
            update_lver_set (write_set s t) ((gvc s) + 1) \<circ>> 
            update_status t CommitResponding) s
      else update_status t AbortPending s))"

| "int_eff t Read = (\<lambda>s.
    let l = (addr s t) in 
      case write_set s t l of
        Some v \<Rightarrow> update_val t v (update_status t (ReadResponding v) s)
        | None \<Rightarrow> if lver s l \<le> ver s t
                  then let v = store s l in 
                        (update_val t v \<circ>> 
                         update_read_set t l v \<circ>> 
                         update_status t (ReadResponding v)) s
                  else update_status t AbortPending s)"

| "int_eff t Write = (\<lambda>s. update_write_set t (addr s t) (val s t) s) \<circ>> 
                          update_status t WriteResponding"

fun cga_eff :: "T \<Rightarrow> pc Event \<Rightarrow> state \<Rightarrow> state" where
  "cga_eff t (Internal a) = int_eff t a"
| "cga_eff t (External a) = ext_eff t a"

definition CGA :: "(state, pc) daut" where
  "CGA \<equiv> \<lparr> daut.start = start, daut.pre = cga_pre, daut.eff = cga_eff \<rparr>"

definition global_inv :: "state \<Rightarrow> bool" where
  "global_inv s \<equiv> \<forall> l . lver s l \<le> gvc s"

definition is_writer :: "state \<Rightarrow> T \<Rightarrow> bool" where
  "is_writer s t \<equiv> (\<exists>l. write_set s t l \<noteq> None)"

definition txn_inv :: "T \<Rightarrow> pc Event \<Rightarrow> state \<Rightarrow> bool" where
  "txn_inv t e s \<equiv> cga_pre t e s \<longrightarrow>
   (case e of
     External BeginInv \<Rightarrow> read_set s t = Map.empty \<and> write_set s t = Map.empty
   | Internal Begin \<Rightarrow> read_set s t = Map.empty \<and> write_set s t = Map.empty
   | External BeginResp \<Rightarrow> read_set s t = Map.empty \<and> write_set s t = Map.empty 
                           \<and> ((ver s t = gvc s) \<longrightarrow> validate t s)
                           \<and> (ver s t \<le> gvc s)
   (*Read*)
   | External (ReadInv l) \<Rightarrow> ((ver s t = gvc s) \<longrightarrow> validate t s) 
                           \<and> (ver s t \<le> gvc s)
                           \<and> ((*ver s t = gvc s*) validate t s \<longrightarrow> rs_consistent t s)

   | Internal Read \<Rightarrow> ((ver s t = gvc s) \<longrightarrow> validate t s) 
                      \<and> (ver s t \<le> gvc s)
                      \<and> ((*ver s t = gvc s*) validate t s \<longrightarrow> rs_consistent t s)

   | External (ReadResp v) \<Rightarrow> v = val s t 
                           \<and> (ver s t = gvc s \<longrightarrow> validate t s) 
                           \<and> (ver s t \<le> gvc s)
                           \<and> ((*ver s t = gvc s*) validate t s \<longrightarrow> rs_consistent t s)
                           \<and> (read_set s t (addr s t) = Some v
                                \<or> write_set s t (addr s t) = Some v)
   (*Write*)
   | External (WriteInv l v) \<Rightarrow> (ver s t = gvc s \<longrightarrow> validate t s) 
                           \<and> ver s t \<le> gvc s
                           \<and> ((*ver s t = gvc s*) validate t s \<longrightarrow> rs_consistent t s)

   | Internal Write \<Rightarrow> ((ver s t = gvc s) \<longrightarrow> validate t s) \<and> (ver s t \<le> gvc s)
                           \<and> ((*(ver s t = gvc s)*) validate t s \<longrightarrow> rs_consistent t s)

   | External WriteResp \<Rightarrow> write_set s t \<noteq> Map.empty \<and> ((ver s t = gvc s) \<longrightarrow> validate t s) 
                           \<and> (ver s t \<le> gvc s)
                           \<and> ((*(ver s t = gvc s)*) validate t s  \<longrightarrow> rs_consistent t s)

   (*Commit*)
   | External CommitInv \<Rightarrow> ((ver s t = gvc s) \<longrightarrow> validate t s) 
                           \<and> (ver s t \<le> gvc s)
                           \<and> ((*ver s t = gvc s*) validate t s \<longrightarrow> rs_consistent t s)


   | Internal Commit \<Rightarrow> ((ver s t = gvc s) \<longrightarrow> validate t s) 
                        \<and> (ver s t \<le> gvc s)
                        \<and> ((*ver s t = gvc s \<or>*)validate t s \<longrightarrow> rs_consistent t s)
                        (*\<and> ((\<exists> l v . write_set s t l = Some v) \<and> validate t s  
                              \<longrightarrow> rs_consistent t s)*)


   | External CommitResp \<Rightarrow>   ((\<forall>l. write_set s t l = None) \<or>
                               (gvc s \<ge> ver s t + 1 
                                \<and> (\<forall> l v. write_set s t l = Some v \<longrightarrow> lver s l \<ge> ver s t + 1)))
                            \<and> (ver s t \<le> gvc s)

   | External Abort \<Rightarrow> True

   | _ \<Rightarrow> (ver s t \<le> gvc s) \<and> ((*ver s t = gvc s \<or>*)validate t s \<longrightarrow> rs_consistent t s))"

lemma Event_split:
  "\<lbrakk>b = External BeginInv \<Longrightarrow> P;
    b = Internal Begin \<Longrightarrow> P;
    b = External BeginResp \<Longrightarrow> P;
    \<And> l. b = External (ReadInv l) \<Longrightarrow> P;
    b = Internal Read \<Longrightarrow> P;
    \<And> v. b = External (ReadResp v) \<Longrightarrow> P;
    \<And> l v. b = External (WriteInv l v) \<Longrightarrow> P;
    b = Internal Write \<Longrightarrow> P;
    b = External WriteResp \<Longrightarrow> P;
    b = External CommitInv \<Longrightarrow> P;
    b = Internal Commit \<Longrightarrow> P;
    b = External CommitResp \<Longrightarrow> P;
    b = External Cancel \<Longrightarrow> P;
    b = External Abort \<Longrightarrow> P \<rbrakk>
   \<Longrightarrow>
    P"
  apply (cases rule: Event.exhaust[where y = b])
  using Action.exhaust_sel apply blast
  using pc.exhaust apply blast
  done

lemmas unfold_updates =
  update_status_def
  update_addr_def
  update_val_def
  update_store_def
  update_read_set_def
  update_write_set_def
  update_store_set_def
  update_lver_set_def
  update_ver_def
  update_lver_def

lemmas cga_simps =
  validate_def
  rs_consistent_def
  rs_consistent_read_def
  txn_inv_def
  cga_pre_def
  CGA_def
  ext_enabled_def
  unfold_updates
  is_writer_def
  global_inv_def


(* Begin *)

lemma txn_inv_pres_begin_inv_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External BeginInv);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (cga_eff at a s)"
   apply (cases rule: Event_split[of b])
   by (auto simp add: cga_simps)

lemma txn_inv_pres_begin_inv_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (External BeginInv);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps)

lemma txn_inv_pres_begin_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (Internal Begin);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps)

lemma txn_inv_pres_begin_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (Internal Begin);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps)

lemma txn_inv_pres_begin_resp_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External BeginResp);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (cga_eff at a s)"
   apply (cases rule: Event_split[of b]) 
   by (auto simp add: cga_simps)

lemma txn_inv_pres_begin_resp_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (External BeginResp);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps)

(* Read *)

lemma txn_inv_pres_read_inv_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External (ReadInv l));
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (cga_eff at a s)"
   apply (cases rule: Event_split[of b])
   by (auto simp add: cga_simps)

lemma txn_inv_pres_read_inv_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = External (ReadInv l);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps)


lemma txn_inv_pres_read_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (Internal Read);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (cga_eff at a s)"
   apply (cases rule: Event_split[of b])
   apply (simp_all add: update_partial_def split: option.split)
   apply (simp add: cga_simps split: option.split)
   apply (simp add: cga_simps split: option.split)
   apply (simp add: cga_simps split: option.split)
   apply (simp add: cga_simps split: option.split)
   apply (simp add: cga_simps split: option.split)
   apply (simp add: cga_simps split: option.split)
   apply (metis (no_types, lifting) RWMemory.all_simps(7) fun_upd_apply option.sel)
   by (simp add: cga_simps split: option.split)+


lemma txn_inv_pres_read_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (Internal Read);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps update_partial_def split: option.split)

lemma txn_inv_pres_read_resp_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External (ReadResp v));
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (cga_eff at a s)"
   apply (cases rule: Event_split[of b])
   by (simp_all add: cga_simps)


lemma txn_inv_pres_read_resp_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = External (ReadResp l);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

(* Write *)

lemma txn_inv_pres_write_inv_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External (WriteInv l v));
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_write_inv_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = External (WriteInv l v);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_write_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (Internal Write);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps update_partial_def)

lemma txn_inv_pres_write_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (Internal Write);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (cga_eff at a s)"
   apply (cases rule: Event_split[of b])
   by (simp add: cga_simps update_partial_def)+

lemma txn_inv_pres_write_resp_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External WriteResp);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_write_resp_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = External WriteResp;
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

(* Commit *)

lemma txn_inv_pres_commit_inv_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External CommitInv);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (cga_eff at a s)"
   apply (cases rule: Event_split[of b]) 
   apply (simp_all add: cga_simps)
   done

lemma txn_inv_pres_commit_inv_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (External CommitInv);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_commit_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (Internal Commit);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (cga_eff at a s)"
   apply (cases rule: Event_split[of b])
   by (simp_all add: cga_simps)

lemma Some_theI: "x = Some y \<Longrightarrow> the x = y"
  by auto

lemma txn_inv_pres_commit_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (Internal Commit);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (cga_eff at a s)"
   apply (cases rule: Event_split[of b];
          simp add: cga_simps le_Suc_eq;
         intro Some_theI conjI impI allI; elim conjE disjE exE)
   apply simp_all
   using not_None_eq by blast+

lemma txn_inv_pres_commit_resp_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External CommitResp);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_commit_resp_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (External CommitResp);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_cancel_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External Cancel);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_cancel_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (External Cancel);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_abort_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External Abort);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_abort_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (External Abort);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma allE2: "\<forall>x. P x \<Longrightarrow> (P x \<Longrightarrow> P y \<Longrightarrow> Q) \<Longrightarrow> Q"
  by auto

lemma start_invariant: "daut.start CGA s \<Longrightarrow> txn_inv t e s"
  apply (cases rule: Event_split[of e])
  apply (simp_all add: txn_inv_def cga_pre_def CGA_def start_def ext_enabled_def)
  apply (erule conjE)+
  by blast

lemma total_inv: "invariant (ioa CGA) (\<lambda>s. global_inv s \<and> (\<forall>e t. txn_inv t e s))"
proof (intro invariant_intro conjI allI)
  fix s
  assume "daut.start CGA s"
  thus "global_inv s"
    by (simp add: CGA_def start_def global_inv_def validate_def)
next
  fix s e t
  assume "daut.start CGA s"
  thus "txn_inv t e s"
    by (rule start_invariant)
next
  fix s
  show "preserved  CGA (\<lambda>s. global_inv s \<and> (\<forall>e t. txn_inv t e s)) s"
    apply (simp only: preserved_def)
    apply (intro allI impI)
    apply (erule conjE)+
    apply (intro conjI allI)
  proof -
    fix t a
    assume "pre CGA t a s" and "global_inv s" and "\<forall>e t. (txn_inv t e s)"
    thus "global_inv (eff CGA t a s)"
      apply (cases rule: Event_split[of a])
      apply (simp add: global_inv_def CGA_def cga_pre_def unfold_updates validate_def le_Suc_eq)+
      apply (rule conjI)
      apply (simp split: option.split)
      apply (simp add: update_read_set_def update_status_def update_val_def)
      apply (simp add: update_status_def option.case_eq_if update_val_def)
      by (simp add: global_inv_def CGA_def cga_pre_def unfold_updates validate_def le_Suc_eq)+
  next
    fix t a e t'
    assume "pre CGA t a s" and "global_inv s" and "\<forall>e t. txn_inv t e s"
    thus "txn_inv t' e (eff CGA t a s)"
      apply (cases "t' = t")
      apply (erule ssubst)
      apply (erule_tac x = e and y = a in allE2)
      apply (erule_tac x = t in allE)
      apply (erule_tac x = t in allE)
      apply (cases rule: Event_split[of a])
      apply (simp_all add: CGA_def del: cga_eff.simps)
      apply (rule txn_inv_pres_begin_inv_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_begin_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_begin_resp_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_inv_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_resp_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_inv_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_resp_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_inv_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_resp_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_cancel_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_abort_self, simp, simp, simp, simp)
      apply (erule_tac x = e and y = a in allE2)
      apply (erule_tac x = t' in allE)
      apply (erule_tac x = t in allE)
      apply (cases rule: Event_split[of a])
      apply (rule txn_inv_pres_begin_inv_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_begin_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_begin_resp_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_inv_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_resp_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_inv_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_resp_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_inv_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_resp_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_cancel_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_abort_other, simp, simp, simp, simp, simp, simp)
      done
  qed
qed

lemma reachable_invariant: "reach CGA s \<Longrightarrow> global_inv s \<and> txn_inv t e s"
  using invariant_elim total_inv by fastforce

end