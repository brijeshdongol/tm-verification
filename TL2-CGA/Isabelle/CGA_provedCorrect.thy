theory CGA_provedCorrect
imports CGA_proved TMS2Var "~~/src/HOL/Eisbach/Eisbach_Tools"
begin

definition step_correspondence :: "CGA_proved.state \<Rightarrow> T \<Rightarrow> CGA_proved.pc \<Rightarrow> TMS2Var.InternalAction option"
  where
  "step_correspondence cs t a \<equiv>
    case a of
        Read \<Rightarrow> (case CGA_proved.write_set cs t (addr cs t) of
                  Some v \<Rightarrow> Some (DoRead (addr cs t) (ver cs t))
                  | None \<Rightarrow> if lver cs (addr cs t) \<le> ver cs t
                            then Some (DoRead (addr cs t) (ver cs t))
                            else None)
        | Write \<Rightarrow> Some (DoWrite (addr cs t) (val cs t))
        | Commit \<Rightarrow> if (\<forall>l. CGA_proved.write_set cs t l = None) 
                    then Some DoCommitReadOnly
                    else (if (ver cs t = gvc cs) \<or> validate t cs 
                          then Some DoCommitWriter 
                          else None)
        | _ \<Rightarrow> None"

lemma scf1: "step_correspondence cs t a = Some DoCommitReadOnly \<longleftrightarrow>
  ((\<forall>l. CGA_proved.write_set cs t l = None) \<and> a = Commit)"
  by (cases a) (auto simp add: step_correspondence_def split: option.split)

lemma scf2: "step_correspondence cs t a = Some DoCommitWriter \<longleftrightarrow>
  ((\<exists>l v. CGA_proved.write_set cs t l = Some v) \<and> ((ver cs t = gvc cs) \<or> validate t cs) \<and> a = Commit)"
  apply (cases a) 
  by (auto simp add: step_correspondence_def validate_def split: option.split)

lemma scf3: "step_correspondence cs t a = Some (DoWrite x v) \<longleftrightarrow>
  (x = addr cs t \<and> v = val cs t \<and> a = Write)"
  by (cases a) (auto simp add: step_correspondence_def split: option.split)

lemma scf4: "step_correspondence cs t a = Some (DoRead x n) \<longleftrightarrow>
  ( (n = ver cs t \<and> x = addr cs t \<and> a = Read)
  \<and> ((\<exists>v. CGA_proved.write_set cs t x = Some v) \<or> (CGA_proved.write_set cs t (addr cs t) = None) \<and> lver cs (addr cs t) \<le> ver cs t))"
  apply (cases a)                                                                       
  by (auto simp add: step_correspondence_def split: option.split)

lemma scf1': "step_correspondence cs t Commit = Some DoCommitReadOnly \<longleftrightarrow>
  ((\<forall>l. CGA_proved.write_set cs t l = None))"
  by (auto simp add: step_correspondence_def split: option.split)

lemma scf2': "step_correspondence cs t Commit = Some DoCommitWriter \<longleftrightarrow>
  ((\<exists>l v. CGA_proved.write_set cs t l = Some v) \<and> ((ver cs t = gvc cs) \<or> validate t cs))"
  by (auto simp add: step_correspondence_def validate_def split: option.split)

lemma scf3': "step_correspondence cs t Write = Some (DoWrite x v) \<longleftrightarrow>
  (x = addr cs t \<and> v = val cs t)"
  by (auto simp add: step_correspondence_def split: option.split)

lemma scf4': "step_correspondence cs t Read = Some (DoRead x n) \<longleftrightarrow>
  ( (n = ver cs t \<and> x = addr cs t)
  \<and> ((\<exists>v. CGA_proved.write_set cs t x = Some v) 
       \<or> (CGA_proved.write_set cs t (addr cs t) = None 
           \<and> lver cs (addr cs t) \<le> ver cs t)))"
  by (auto simp add: step_correspondence_def split: option.split)

lemmas sc_simps = scf1 scf2 scf3 scf4

lemmas sc_simps' = scf1' scf2' scf3' scf4'

lemma step_correspondence_None_cases:
  assumes "step_correspondence cs t a = None"
  shows "(a = Read \<and> (lver cs (addr cs t) > ver cs t) \<and> CGA_proved.write_set cs t (addr cs t) = None)
       \<or> (a = Commit \<and> (ver cs t \<noteq> gvc cs) \<and> \<not> validate t cs \<and> (\<exists>l v. CGA_proved.write_set cs t l = Some v))
       \<or> (a = Begin)"
  using assms
  apply (cases a)
  apply (auto simp add: step_correspondence_def split: option.splits)
  apply (metis option.distinct(1))
  apply (meson not_None_eq)
  apply (meson option.exhaust_sel option.simps(3))
  by (meson not_le_imp_less option.simps(3))

lemma step_correspondence_None:
  assumes "step_correspondence cs t a = None"
  and "a = Read \<Longrightarrow> (lver cs (addr cs t) > ver cs t) \<Longrightarrow> CGA_proved.write_set cs t (addr cs t) = None \<Longrightarrow> P"
  and "\<And>l v. a = Commit \<Longrightarrow> (ver cs t \<noteq> gvc cs) \<Longrightarrow> \<not> validate t cs \<Longrightarrow> CGA_proved.write_set cs t l = Some v \<Longrightarrow> P"
  and "a = Begin \<Longrightarrow> P"
  shows "P"
  by (meson assms(1) assms(2) assms(3) assms(4) step_correspondence_None_cases)

lemma step_correspondence_None_var:
  assumes "step_correspondence cs t a = None"
  and "(lver cs (addr cs t) > ver cs t) \<Longrightarrow> CGA_proved.write_set cs t (addr cs t) = None \<Longrightarrow> P Read"
  and "\<And>l v. (ver cs t \<noteq> gvc cs) \<Longrightarrow> \<not> validate t cs \<Longrightarrow> CGA_proved.write_set cs t l = Some v \<Longrightarrow> P Commit"
  and "P Begin"
  shows "P a"
  by (metis assms(1) assms(2) assms(3) assms(4) step_correspondence_None)

definition global_rel :: "CGA_proved.state \<Rightarrow> State \<Rightarrow> bool" where
  "global_rel cs as \<equiv> 
          (gvc cs = max_index as) 
          \<and> latest_store as = store cs
          \<and> (\<forall>l k. lver cs l \<le> k \<and> k \<le> max_index as \<longrightarrow> store_at as k l = store cs l)
(*          \<and> (\<forall>l t. lver cs l \<le> ver cs t \<longrightarrow> store_at as (lver cs l) l = store cs l)*)
(*          \<and> (\<forall> t . ((state.status cs t = Pending Begin) \<and> (State.status as t = BeginResponding)) 
                  \<or> (begin_index as t \<le> ver cs t \<and> ver cs t \<le> max_index as))
          *)"

definition in_flight :: "T \<Rightarrow> CGA_proved.state \<Rightarrow> Interface.State \<Rightarrow> bool" where
  "in_flight t cs as \<equiv>
     (state.write_set cs t = State.write_set as t) \<and>
     (State.read_set as t = state.read_set cs t)"

definition read_ver_consistent :: "T \<Rightarrow> CGA_proved.state \<Rightarrow> Interface.State \<Rightarrow> bool" where
  "read_ver_consistent t cs as \<equiv> 
      read_consistent (store_at as (ver cs t)) (read_set cs t)"


definition lver_consistent :: "T \<Rightarrow> CGA_proved.state \<Rightarrow> Interface.State \<Rightarrow> bool" where
 "lver_consistent t cs as \<equiv>
   let l = (addr cs t) in
   (write_set cs t l = None) \<and> (lver cs l \<le> ver cs t)
     \<longrightarrow> (store_at as (ver cs t)) l = (store cs l)"



definition txn_rel :: "T \<Rightarrow> CGA_proved.pc Event \<Rightarrow> CGA_proved.state \<Rightarrow> State \<Rightarrow> bool"
  where
  "txn_rel t e cs0 as0 \<equiv> in_flight t cs0 as0 \<and>
     (cga_pre t e cs0 \<longrightarrow>
       (case e of
          External BeginInv \<Rightarrow> State.status as0 t = NotStarted
        | Internal Begin \<Rightarrow>
               State.status as0 t = BeginResponding
        | External BeginResp \<Rightarrow> 
               State.status as0 t = BeginResponding
             \<and> (begin_index as0 t \<le> ver cs0 t)
             \<and> (ver cs0 t \<le> max_index as0)
        | Internal Read \<Rightarrow> 
               State.status as0 t = Pending (ReadPending (addr cs0 t))
             \<and> read_ver_consistent t cs0 as0
             \<and> (begin_index as0 t \<le> ver cs0 t) 
             \<and> (ver cs0 t \<le> max_index as0)
        | External (ReadResp v) \<Rightarrow> 
              State.status as0 t = ReadResponding (val cs0 t) 
            \<and> read_ver_consistent t cs0 as0
             \<and> (begin_index as0 t \<le> ver cs0 t) 
             \<and> (ver cs0 t \<le> max_index as0)
       | Internal Write \<Rightarrow> 
              State.status as0 t = Pending (WritePending (addr cs0 t) (val cs0 t))
            \<and> read_ver_consistent t cs0 as0
             \<and> (begin_index as0 t \<le> ver cs0 t) 
             \<and> (ver cs0 t \<le> max_index as0)
        | External WriteResp \<Rightarrow> 
              State.status as0 t = WriteResponding
            \<and> read_ver_consistent t cs0 as0
            \<and> (begin_index as0 t \<le> ver cs0 t) 
            \<and> (ver cs0 t \<le> max_index as0)
        | Internal Commit \<Rightarrow> State.status as0 t = Pending CommitPending
        | External CommitResp \<Rightarrow> State.status as0 t = CommitResponding
        | External Abort \<Rightarrow> State.status as0 t \<notin> {NotStarted, Ready, CommitResponding, Committed, Aborted}
        | External Cancel \<Rightarrow> False
        | _ \<Rightarrow> State.status as0 t = Ready 
             \<and> read_ver_consistent t cs0 as0
             \<and> (begin_index as0 t \<le> ver cs0 t)
             \<and> (ver cs0 t \<le> max_index as0)))"



definition sim_rel :: "CGA_proved.state \<Rightarrow> State \<Rightarrow> bool"
  where
  "sim_rel cs as \<equiv> global_rel cs as \<and> (\<forall>t a. txn_rel t a cs as)"

named_theorems simps

method double_case_simp for a and b :: "pc Event" declares simps =
  (cases a; (rule CGA_proved.Event_split[where b = b]; simp add: simps))

lemma tms2_reachable_invariant: "reach TMS2Var s \<Longrightarrow> txn_inv t e s"
  using invariant_elim total_inv by fastforce

lemma ws_empty: "(\<forall>l. state.write_set cs t l = None) \<longleftrightarrow> state.write_set cs t = Map.empty"
  by auto

lemmas cga_tms2_simps =
  cga_simps TMS2Var.unfold_tms2 RWMemory.all_simps txn_rel_def

lemmas txn_rel_simps = 
read_ver_consistent_def
lver_consistent_def
global_rel_def cga_tms2_simps all_utilities in_flight_def

lemma txn_rel_self_preserved_stutter_Begin:
  assumes "CGA_proved.global_inv cs0"
  and "CGA_proved.txn_inv at (Internal pc) cs0"
  and "\<forall>c. TMS2Var.txn_inv at c as0"
  and "global_rel cs0 as0"
  and "txn_rel at (Internal pc) cs0 as0"
  and "cga_pre at (Internal pc) cs0"
  and "step_correspondence cs0 at pc = None"
  and [simp]: "pc = Begin"
  shows "txn_rel at b (cga_eff at (Internal pc) cs0) as0"
proof -
  from assms spec[where P="\<lambda> c. TMS2Var.txn_inv at c as0" and x="External BeginResp"]
  have "begin_index as0 at \<le> max_index as0"
    by (simp add: cga_tms2_simps TMS2Var.txn_inv_def)
  thus "txn_rel at b (cga_eff at (Internal pc) cs0) as0" using assms
    by (simp_all add: step_correspondence_def txn_rel_simps split: TMS2Var.splits option.split)
qed

lemma txn_rel_self_preserved_stutter_Commit:
  assumes "CGA_proved.global_inv cs0"
  and "CGA_proved.txn_inv at (Internal pc) cs0"
  and "\<forall>c. TMS2Var.txn_inv at c as0"
  and "global_rel cs0 as0"
  and "txn_rel at (Internal pc) cs0 as0"
  and "cga_pre at (Internal pc) cs0"
  and "step_correspondence cs0 at pc = None"
  and [simp]: "pc = Commit"
  shows "txn_rel at b (cga_eff at (Internal pc) cs0) as0"
using assms
apply (simp_all add: global_rel_def cga_tms2_simps all_utilities split: TMS2Var.splits)
apply safe
apply (simp_all add: step_correspondence_def in_flight_def)
apply (metis assms(7) assms(8) pc.distinct(7) pc.simps(2) step_correspondence_None_cases validate_def)
apply (metis assms(7) assms(8) pc.distinct(7) pc.simps(2) step_correspondence_None_cases)
by (metis assms(7) assms(8) pc.distinct(7) pc.simps(2) step_correspondence_None_cases validate_def)

lemma txn_rel_self_preserved_stutter_Read:
  assumes "CGA_proved.global_inv cs0"
  and "CGA_proved.txn_inv at (Internal pc) cs0"
  and "\<forall>c. TMS2Var.txn_inv at c as0"
  and "global_rel cs0 as0"
  and "txn_rel at (Internal pc) cs0 as0"
  and "cga_pre at (Internal pc) cs0"
  and "step_correspondence cs0 at pc = None"
  and [simp]: "state.write_set cs0 at (addr cs0 at) = None"
  and [simp]: "pc = Read"
  shows "txn_rel at b (cga_eff at (Internal pc) cs0) as0"
proof -
  show ?thesis using assms
    apply (simp_all add: global_rel_def cga_tms2_simps all_utilities split: TMS2Var.splits)
    apply safe
    by (simp_all add: step_correspondence_def in_flight_def validate_def)
qed

lemmas sim_simps =
  cga_simps unfold_tms2 latest_store_def CGA_def TMS2Var.txn_inv_def
  write_back_def max_index_def store_at_def in_flight_def txn_rel_def
  option.case_eq_if global_rel_def value_for_def when_fn_def

lemma txn_rel_Dup: "\<forall>t e. txn_rel t e cs as \<Longrightarrow> txn_rel t e cs as \<and> txn_rel t' e' cs as"
  by auto

lemma update_partials: "txn_rel t e cs as \<Longrightarrow> latest_store as l = store cs l \<Longrightarrow> update_partial t l (latest_store as l) (State.read_set as) t = update_partial t l (store cs l) (state.read_set cs) t"
  by (auto simp add: txn_rel_def in_flight_def update_partial_def)

lemma update_partials2:
  assumes "State.read_set as t = state.read_set cs t"
  and "State.read_set as t' = state.read_set cs t'"
  and "X = Y"
  shows "update_partial t l X (State.read_set as) t' = update_partial t l Y (state.read_set cs) t'"
  using assms
  by (auto simp add: update_partial_def)

lemma ws_dom: "addr cs t \<notin> dom (State.write_set as t) \<longleftrightarrow> State.write_set as t (addr cs t) = None"
  by (simp add: dom_def)

lemma CGA_simulation:
  "standard_simulation CGA TMS2Var step_correspondence sim_rel"
proof (unfold standard_simulation_def, intro conjI allI impI exI)
  fix cs
  assume "daut.start CGA cs"
  thus "sim_rel cs default_start"
  proof (auto simp add: sim_rel_def CGA_def CGA_proved.start_def cga_pre_def)
    fix t e
    assume "\<forall>t. state.status cs t = NotStarted"
    and "store cs \<in> mem_initial"
    and "gvc cs = 0"
    and "\<forall>t l. state.read_set cs t l = None"
    and "\<forall>t l. state.write_set cs t l = None"
    note assms = this
    thus "txn_rel t e cs default_start"
      apply -
      apply (rule CGA_proved.Event_split[where b = e])
      by (auto simp add: txn_rel_def cga_pre_def ext_enabled_def default_start_def ws_empty store_at_def max_index_def dom_def in_flight_def)
    from assms have "(\<forall> t. ver cs t = gvc cs)"    
      by (metis CGA_proved.start_def CGA_def \<open>daut.start CGA cs\<close> daut.select_convs(1))
    from this assms 
    show "global_rel cs default_start"
      by (simp add: global_rel_def default_start_def max_index_def latest_store_def store_at_def dom_def mem_initial_def)
   qed
next
  fix cs
  assume "daut.start CGA cs"
  thus "daut.start TMS2Var default_start"
    by (simp add: CGA_def CGA_proved.start_def Interface.start_def default_start_def initial_stores_def mem_initial_def)
next
  show "standard_sim_ext_step CGA TMS2Var sim_rel"
    unfolding standard_sim_ext_step_def
  proof (intro allI impI, (erule conjE)+, rule conjI)
    fix cs as a t
    assume "sim_rel cs as" and "reach CGA cs"
    and "pre CGA t (External a) cs"
    thus "pre TMS2Var t (External a) as"
      apply (simp add: sim_rel_def)
      apply (erule conjE)
      apply (erule_tac x = t in allE)
      apply (erule_tac x = "External a" in allE)
      apply -
      apply (drule CGA_proved.reachable_invariant[where t = t and e = "External a"])
      apply (cases a)
      apply (simp_all add: CGA_def cga_pre_def tms_pre_def ext_enabled_def sim_rel_def status_enabled_def txn_rel_def CGA_proved.txn_inv_def)
      done
  next
    fix cs as a t
    assume "sim_rel cs as" and "reach CGA cs" and "reach TMS2Var as"
    and "pre CGA t (External a) cs"
    thus "sim_rel (eff CGA t (External a) cs) (eff TMS2Var t (External a) as)"
    proof (simp add: sim_rel_def, intro conjI allI)
      fix t' b
      assume "global_rel cs as \<and> (\<forall>t a. txn_rel t a cs as)"
      hence "txn_rel t' b cs as" and "txn_rel t (External a) cs as"
        by auto
      note assms = this
      have "CGA_proved.txn_inv t (External a) cs" and "CGA_proved.txn_inv t' b cs"  and "CGA_proved.global_inv cs"
      by (simp add: CGA_proved.reachable_invariant \<open>reach CGA cs\<close>)+

      from assms this and `pre CGA t (External a) cs`

      show "txn_rel t' b (eff CGA t (External a) cs) (tms_eff t (External a) as)"
        apply -
        apply (cases "t = t'")
        apply (cases a; rule CGA_proved.Event_split[where b = b])
        apply (simp_all add: max_index_def option.case_eq_if rs_consistent_def CGA_proved.txn_inv_def CGA_proved.global_inv_def read_ver_consistent_def store_at_def read_consistent_def txn_rel_def cga_pre_def ext_enabled_def CGA_def tms_eff_def TMS2Var.ext_eff_def unfold_updates CGA_proved.update_status_def in_flight_def update_addr_def update_val_def)
        apply (cases a; rule CGA_proved.Event_split[where b = b])
        by (simp_all add: max_index_def option.case_eq_if rs_consistent_def CGA_proved.txn_inv_def CGA_proved.global_inv_def read_ver_consistent_def store_at_def read_consistent_def txn_rel_def cga_pre_def ext_enabled_def CGA_def tms_eff_def TMS2Var.ext_eff_def unfold_updates CGA_proved.update_status_def in_flight_def update_addr_def update_val_def)
    next
      assume "global_rel cs as \<and> (\<forall>t a. txn_rel t a cs as)"
      and "reach TMS2Var as"  
      and "pre CGA t (External a) cs"
      note assms = this

      from assms have        
          "TMS2Var.txn_inv t (External a) as"
      by (simp add: tms2_reachable_invariant)

      from this assms show "global_rel (eff CGA t (External a) cs) (tms_eff t (External a) as)"
      apply -
      apply (erule conjE)
      apply (cases a)
      apply (simp_all add: txn_rel_def TMS2Var.txn_inv_def global_rel_def max_index_def latest_store_def store_at_def cga_pre_def ext_enabled_def CGA_def tms_eff_def TMS2Var.ext_eff_def unfold_updates CGA_proved.update_status_def in_flight_def update_addr_def update_val_def)
      done
    qed
  qed
next
  show "standard_sim_stutter CGA TMS2Var step_correspondence sim_rel"
    unfolding standard_sim_stutter_def
  proof (intro impI allI, (erule conjE)+)
    fix cs as a t
    assume "reach CGA cs"
    and absreach: "reach TMS2Var as"
    and "sim_rel cs as"
    and "pre CGA t (Internal a) cs"
    and sc_None: "step_correspondence cs t a = None"

    note assms = this

    from `step_correspondence cs t a = None`
    show "sim_rel (eff CGA t (Internal a) cs) as"

    proof (rule step_correspondence_None)
      assume [simp]: "a = Read"
      and "lver cs (addr cs t) > ver cs t"
      and ws_None: "state.write_set cs t (addr cs t) = None"
      note assms = this
 
      have "global_rel cs as" 
           and "\<forall>t' e. txn_rel t' e cs as" 
        using \<open>sim_rel cs as\<close> sim_rel_def apply auto[1]      
        using \<open>sim_rel cs as\<close> sim_rel_def by auto


      from assms this
      show "sim_rel (eff CGA t (Internal a) cs) as"
      proof (auto simp add: sim_rel_def)
        from `global_rel cs as` and `lver cs (addr cs t) > ver cs t` and ws_None
        show "global_rel (eff CGA t (Internal Read) cs) as"
          by (auto simp add: global_rel_def CGA_def CGA_proved.unfold_updates)
      next
        fix t' e

        have "txn_rel t' e cs as"
          by (simp add: \<open>\<forall>t' e. txn_rel t' e cs as\<close>)

        from this `pre CGA t (Internal a) cs` `lver cs (addr cs t) > ver cs t` and ws_None
        show "txn_rel t' e (eff CGA t (Internal Read) cs) as"
          apply -
          apply (cases "t = t'")
          apply (auto simp add: validate_def stores_domain_def CGA_proved.txn_inv_def txn_rel_def CGA_def CGA_proved.unfold_updates in_flight_def update_partial_def cga_pre_def ext_enabled_def)
          using \<open>\<forall>t' e. txn_rel t' e cs as\<close> cga_pre_def txn_rel_def apply fastforce
          using \<open>\<forall>t' e. txn_rel t' e cs as\<close> cga_pre_def txn_rel_def apply fastforce
          using \<open>\<forall>t' e. txn_rel t' e cs as\<close> cga_pre_def txn_rel_def apply fastforce
          using \<open>\<forall>t' e. txn_rel t' e cs as\<close> cga_pre_def txn_rel_def apply fastforce
          using \<open>\<forall>t' e. txn_rel t' e cs as\<close> cga_pre_def txn_rel_def apply fastforce
          apply (rule CGA_proved.Event_split[where b = e])
          apply (auto simp add: validate_def stores_domain_def CGA_proved.txn_inv_def txn_rel_def CGA_def CGA_proved.unfold_updates in_flight_def update_partial_def cga_pre_def ext_enabled_def)
          apply (rule CGA_proved.Event_split[where b = e])
          apply (auto simp add: validate_def stores_domain_def CGA_proved.txn_inv_def txn_rel_def CGA_def CGA_proved.unfold_updates in_flight_def update_partial_def cga_pre_def ext_enabled_def)
          apply (rule CGA_proved.Event_split[where b = e])
          apply (auto simp add: validate_def stores_domain_def CGA_proved.txn_inv_def txn_rel_def CGA_def CGA_proved.unfold_updates in_flight_def update_partial_def cga_pre_def ext_enabled_def)
          apply (rule CGA_proved.Event_split[where b = e])
          apply (auto simp add: validate_def stores_domain_def CGA_proved.txn_inv_def txn_rel_def CGA_def CGA_proved.unfold_updates in_flight_def update_partial_def cga_pre_def ext_enabled_def)
          apply (rule CGA_proved.Event_split[where b = e])
          apply (auto simp add: validate_def stores_domain_def CGA_proved.txn_inv_def txn_rel_def CGA_def CGA_proved.unfold_updates in_flight_def update_partial_def cga_pre_def ext_enabled_def)
          apply (rule CGA_proved.Event_split[where b = e])
          apply (auto simp add: read_ver_consistent_def validate_def stores_domain_def CGA_proved.txn_inv_def txn_rel_def CGA_def CGA_proved.unfold_updates in_flight_def update_partial_def cga_pre_def ext_enabled_def)
          apply (rule CGA_proved.Event_split[where b = e])
          by (auto simp add: read_ver_consistent_def validate_def stores_domain_def CGA_proved.txn_inv_def txn_rel_def CGA_def CGA_proved.unfold_updates in_flight_def update_partial_def cga_pre_def ext_enabled_def)
      qed
    next
      fix l v
      assume [simp]: "a = Commit" 
      and "ver cs t \<noteq> gvc cs"
      and "\<not> validate t cs"
      and ws_Some: "state.write_set cs t l = Some v"
      have "global_rel cs as" and "\<forall>t' e. txn_rel t' e cs as"
        using \<open>sim_rel cs as\<close> sim_rel_def apply auto[1]      
        using \<open>sim_rel cs as\<close> sim_rel_def by auto


      show "sim_rel (eff CGA t (Internal a) cs) as"
      proof (auto simp add: sim_rel_def)
        from `global_rel cs as` and `\<not> validate t cs` and ws_Some
        show "global_rel (eff CGA t (Internal Commit) cs) as"
        apply (auto simp add: global_rel_def CGA_def CGA_proved.unfold_updates validate_def)
        using sc_None scf2' by force
      next
        fix t' e
        have "txn_rel t' e cs as"
          by (simp add: \<open>\<forall>t' e. txn_rel t' e cs as\<close>)

        from this `pre CGA t (Internal a) cs` `ver cs t \<noteq> gvc cs` `\<not> validate t cs` and ws_Some
        show "txn_rel t' e (eff CGA t (Internal Commit) cs) as"
          apply -
          apply (cases "t = t'")
          apply (auto simp add:   txn_rel_def stores_domain_def 
             CGA_def cga_pre_def ext_enabled_def validate_def
            CGA_proved.unfold_updates in_flight_def update_partial_def)
          using \<open>\<forall>t' e. txn_rel t' e cs as\<close> cga_pre_def pc.simps(14) txn_rel_def apply fastforce
          using \<open>\<forall>t' e. txn_rel t' e cs as\<close> cga_pre_def pc.simps(14) txn_rel_def apply fastforce
          using \<open>\<forall>t' e. txn_rel t' e cs as\<close> cga_pre_def pc.simps(14) txn_rel_def apply fastforce
          using \<open>\<forall>t' e. txn_rel t' e cs as\<close> cga_pre_def pc.simps(14) txn_rel_def apply fastforce
          using \<open>\<forall>t' e. txn_rel t' e cs as\<close> cga_pre_def pc.simps(14) txn_rel_def apply fastforce
          apply (rule CGA_proved.Event_split[where b = e]) 
          apply (auto simp add: read_ver_consistent_def ext_enabled_def CGA_proved.update_status_def split:option.split) 
          apply (rule CGA_proved.Event_split[where b = e]) 
          apply (auto simp add: read_ver_consistent_def ext_enabled_def CGA_proved.update_status_def split:option.split) 
          apply (rule CGA_proved.Event_split[where b = e]) 
          apply (auto simp add: read_ver_consistent_def ext_enabled_def CGA_proved.update_status_def split:option.split) 
          apply (rule CGA_proved.Event_split[where b = e]) 
          apply (auto simp add: read_ver_consistent_def ext_enabled_def CGA_proved.update_status_def split:option.split) 
          apply (rule CGA_proved.Event_split[where b = e]) 
          apply (auto simp add: read_ver_consistent_def ext_enabled_def CGA_proved.update_status_def split:option.split) 
          apply (rule CGA_proved.Event_split[where b = e]) 
          by (auto simp add: read_ver_consistent_def ext_enabled_def CGA_proved.update_status_def split:option.split) 
      qed
    next
      assume [simp]: "a = Begin"
      have "global_rel cs as" and "\<forall>t' e. txn_rel t' e cs as"
        using \<open>sim_rel cs as\<close> sim_rel_def apply auto[1]      
        using \<open>sim_rel cs as\<close> sim_rel_def by auto

      show "sim_rel (eff CGA t (Internal a) cs) as"
      proof (auto simp add: sim_rel_def)
        from `global_rel cs as`
        show "global_rel (eff CGA t (Internal Begin) cs) as"
            by (auto simp add: latest_store_def global_rel_def CGA_def CGA_proved.unfold_updates)  
        next
        fix t' e


        have "txn_rel t' e cs as" 
          and "global_rel cs as" 
          and "TMS2Var.txn_inv t' (External BeginResp) as"
          apply (simp add: \<open>\<forall>t' e. txn_rel t' e cs as\<close>)
          apply(simp add: \<open>global_rel cs as\<close>)
          using absreach tms2_reachable_invariant by blast  
          
        from this `pre CGA t (Internal a) cs`
        show "txn_rel t' e (eff CGA t (Internal Begin) cs) as"
          apply (cases "t = t'")  
          apply (auto simp add: update_ver_def txn_rel_def
            in_flight_def cga_pre_def CGA_def CGA_proved.update_status_def)
          apply (cases e)
          apply (clarsimp, simp add: status_enabled_def TMS2Var.txn_inv_def global_rel_def ext_enabled_def)
          apply (rule CGA_proved.Event_split[where b = e]) 
          apply auto
          using \<open>\<forall>t' e. txn_rel t' e cs as\<close> cga_pre_def txn_rel_def apply fastforce
          apply (simp add: ext_enabled_def status_enabled_def global_rel_def TMS2Var.txn_inv_def)
          apply (rule CGA_proved.Event_split[where b = e]) 
          apply (auto simp add: ext_enabled_def)
          apply (simp add: ext_enabled_def status_enabled_def global_rel_def TMS2Var.txn_inv_def)
          apply (rule CGA_proved.Event_split[where b = e]) 
          apply (auto simp add: ext_enabled_def)
          apply (simp add: ext_enabled_def status_enabled_def global_rel_def TMS2Var.txn_inv_def)
          apply (rule CGA_proved.Event_split[where b = e]) 
          apply (auto simp add: ext_enabled_def)
          apply (simp add: ext_enabled_def status_enabled_def global_rel_def TMS2Var.txn_inv_def)
          apply (rule CGA_proved.Event_split[where b = e]) 
          apply (auto simp add: ext_enabled_def read_ver_consistent_def)
          apply (simp add: ext_enabled_def status_enabled_def global_rel_def TMS2Var.txn_inv_def)
          apply (rule CGA_proved.Event_split[where b = e]) 
          by (auto simp add: ext_enabled_def read_ver_consistent_def)
      qed
    qed
  qed
next
  show "standard_sim_int_step CGA TMS2Var step_correspondence sim_rel"
    unfolding standard_sim_int_step_def
  proof (intro impI allI, (erule conjE)+, intro conjI)
    fix cs as ci ai t
    assume "reach CGA cs" and "reach TMS2Var as"
    and "sim_rel cs as"
    and "pre CGA t (Internal ci) cs"
    and sc: "step_correspondence cs t ci = Some ai"
  
    note case_hyps = this
  
    have "txn_rel t (Internal ci) cs as"
      using \<open>sim_rel cs as\<close> sim_rel_def by auto

    from this and case_hyps(3) and case_hyps(4) and case_hyps(5)
    and CGA_proved.reachable_invariant[OF `reach CGA cs`, where t = t and e = "Internal ci"]
    and tms2_reachable_invariant[OF `reach TMS2Var as`, where t = t and e = "Internal ai"]
    show "pre TMS2Var t (Internal ai) as"
      apply (simp add: sim_rel_def)
      apply (erule conjE)
      apply (erule_tac x = t in allE)
      apply (erule_tac x = "Internal ci" in allE)
      apply (cases ai; simp add: sc_simps)
      apply (simp_all add: global_rel_def sc_simps in_flight_def unfold_tms2 CGA_def txn_rel_def cga_simps TMS2Var.txn_inv_def sim_rel_def)
      apply blast
      apply (simp add: read_consistent_def option.case_eq_if)
      apply auto[1]
      using read_ver_consistent_def by blast
    from case_hyps
    have grel: "global_rel cs as" and trel: "\<forall>t e. txn_rel t e cs as"
      by (auto simp add: sim_rel_def)

    note cga_inv = CGA_proved.reachable_invariant[OF `reach CGA cs`, where t = t and e = "Internal ci"]
    note tms2_inv = TMS2Var.reachable_invariant[OF `reach TMS2Var as`, where t = t and e = "Internal ai"]    

    show "sim_rel (eff CGA t (Internal ci) cs) (eff TMS2Var t (Internal ai) as)"
    proof (simp only: sim_rel_def, intro conjI allI)
      from sc grel trel cga_inv tms2_inv and case_hyps(4)
      show "global_rel (eff CGA t (Internal ci) cs) (eff TMS2Var t (Internal ai) as)"
        apply -
        apply (erule conjE)
        apply (erule_tac x = t in allE)
        apply (erule_tac x = "Internal ci" in allE)
        apply (cases ai; simp add: sc_simps)
        apply (simp_all add: sim_simps)
        apply (intro impI conjI allI)+
        apply simp_all        
        apply (rule ext)
        apply (simp add: apply_partial_def option.case_eq_if)+
        apply (elim conjE)+  
        apply (erule_tac x = l in allE)+
        apply auto[1]
        apply (elim conjE)+  
        apply (erule_tac x = l in allE)+
        apply fastforce
        apply (elim conjE)+  
        apply (erule_tac x = l in allE)+
        apply (simp add: apply_partial_def option.case_eq_if)+
        apply auto[1]      
        apply (elim conjE disjE impE)+  
        apply (intro impI conjI allI)+
        apply (simp add: stores_domain_def dom_def)+
        apply (intro impI conjI allI)+
        apply (simp_all add: dom_def read_consistent_def read_ver_consistent_def apply_partial_def option.case_eq_if)+
        by fastforce+
    next
      fix t' e

      note cga2_inv = CGA_proved.reachable_invariant[OF `reach CGA cs`, where t = t' and e = e]

      have "CGA_proved.txn_inv t (Internal ci) cs" and "CGA_proved.txn_inv t' e cs" and "CGA_proved.global_inv cs"
        by (simp add: cga_inv) (simp add: cga2_inv)+


      (*find_theorems stores_domain

      have "in_flight t' cs as" and "in_flight t cs as"
        using trel txn_rel_def by auto
      from grel trel sc this tms2_inv (*this tms2_inv cga_inv and sc*)
      have "in_flight t' (eff CGA t (Internal ci) cs) (eff TMS2Var t (Internal ai) as)"
        apply (cases ai; simp add: sc_simps; simp)
        apply (simp_all add: in_flight_def cga_simps unfold_tms2 TMS2Var.txn_inv_def write_back_def update_partial_def when_fn_def)
        apply (cases "state.write_set cs t (addr cs t)")
        apply (simp_all add: dom_def  value_for_def value_at_def cga_simps update_partial_def store_at_def max_index_def apply_partial_def)
        apply (intro allI conjI impI; blast?)
        apply (simp_all add: option.case_eq_if)
        apply (intro allI conjI impI; blast?)
        sledgehammer
       

      from grel trel sc this tms2_inv and case_hyps(4)
      have "txn_rel t' (External BeginInv) (eff CGA t (Internal ci) cs) (eff TMS2Var t (Internal ai) as)"
        apply (drule_tac t = t and t' = t' and e = "Internal ci" and e' = e in txn_rel_Dup)
        apply simp
        apply (elim conjE)
        apply (cases ai; simp add: sc_simps; elim conjE exE; rule_tac b = e in CGA_proved.Event_split; simp)
        apply (simp_all add: txn_rel_def validate_def CGA_def global_inv_def TMS2Var.txn_inv_def rs_consistent_def CGA_proved.txn_inv_def cga_pre_def global_rel_def)
        *)

      from grel trel sc this tms2_inv and case_hyps(4)
      show "txn_rel t' e (eff CGA t (Internal ci) cs) (eff TMS2Var t (Internal ai) as)"
        apply -
        apply (drule_tac t = t and t' = t' and e = "Internal ci" and e' = e in txn_rel_Dup)
        apply simp
        apply (elim conjE)
        apply (cases ai; simp add: sc_simps; elim conjE exE; rule_tac b = e in CGA_proved.Event_split; simp)
        apply (simp_all add: txn_rel_def validate_def CGA_def global_inv_def TMS2Var.txn_inv_def rs_consistent_def CGA_proved.txn_inv_def cga_pre_def global_rel_def)
        apply (simp_all add: Interface.update_read_set_def max_index_def store_at_def write_back_def Interface.update_status_def TMS2Var.int_eff_def in_flight_def read_ver_consistent_def read_consistent_def CGA_proved.update_status_def update_store_set_def update_lver_set_def tms_eff_def)
        apply (intro conjI impI update_partials2)
        apply clarify
        apply (simp_all add: stores_domain_def ext_enabled_def)
        apply (intro conjI impI update_partials2)
        apply auto[1]
        apply (simp add: max.coboundedI2)+
        apply (intro conjI impI allI update_partials2)
        apply simp+
        apply (metis Suc_n_not_le_n max.coboundedI2)
        apply (metis Suc_n_not_le_n max.coboundedI2)
        apply (metis Suc_n_not_le_n max.coboundedI2)
        apply (metis Suc_n_not_le_n max.coboundedI2)
        apply (metis Suc_n_not_le_n max.coboundedI2)
        apply (metis Suc_n_not_le_n max.coboundedI2)

        apply (cases "State.write_set as t (addr cs t) = None")
        apply (intro conjI impI update_partials2)+
        apply (simp_all add: when_fn_def  update_val_def CGA_proved.update_status_def CGA_proved.update_read_set_def Interface.update_read_set_def)
        apply (intro conjI impI update_partials2)+
        apply (simp split: option.split)
        apply (simp_all add: update_partial_def store_at_def when_fn_def value_for_def value_at_def update_val_def CGA_proved.update_status_def CGA_proved.update_read_set_def Interface.update_read_set_def)
        apply auto[1]

        apply (cases "State.write_set as t (addr cs t) = None")
        apply (intro conjI impI update_partials2)+
        apply (simp_all add: when_fn_def  update_val_def CGA_proved.update_status_def CGA_proved.update_read_set_def Interface.update_read_set_def)
        apply (intro conjI impI update_partials2)+
        apply (simp split: option.split)
        apply (simp_all add: status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)
        apply auto[11]
        apply (simp split: option.split)
        apply (simp_all add: status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)
        apply (simp_all add: update_partial_def store_at_def when_fn_def value_for_def value_at_def update_val_def CGA_proved.update_status_def CGA_proved.update_read_set_def Interface.update_read_set_def)
        apply auto[1]
        apply (auto simp add: status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[12]
(*27*)
        apply (intro conjI impI update_partials2)+
        apply (auto simp add: store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[24]
(*26*)
        apply (intro conjI impI update_partials2)+
        apply (auto simp add:  store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[32]
(*25*)

        apply (intro conjI impI update_partials2)+
        apply (auto simp add:  store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[9]
        apply (simp add:  store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[9]
        apply clarsimp
        apply (simp add:  store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[9]
        apply clarsimp
        apply (simp split: option.split)
        apply (simp add:  store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[9]
        apply (intro conjI impI allI)+
        apply clarsimp
        using global_rel_def grel apply auto[1]
        apply (smt domI option.case_eq_if option.sel option.the_def)
        apply blast
        apply blast
        apply (auto simp add: store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]       
        apply (simp split: option.split)
        apply clarsimp
        apply (auto simp add:  store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply (simp split: option.split)
        apply (auto simp add:  store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        using global_rel_def grel apply auto[1]
        apply (simp split: option.split)
        apply (auto simp add: store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]       
        using global_rel_def grel apply auto[1]
        apply (simp split: option.split)
        apply (auto simp add: store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]       
        using global_rel_def grel apply auto[1]
        apply (simp split: option.split)
        apply (auto simp add:  store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        using global_rel_def grel apply auto[1]
        apply (auto simp add:  store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[6]
        apply (auto simp add:  store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[2]
        apply (auto simp add:  store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply (simp add:  store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply (simp split: option.split)
        apply (simp add:  store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply (intro conjI impI allI)+
        apply auto[1]
        apply (simp add:  store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply clarsimp
        apply (simp add:  store_at_def  value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        using global_rel_def grel apply auto[1]  
        apply auto[1]
        apply (simp add:  store_at_def  value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply clarsimp        
        apply (simp add:  store_at_def  value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply clarsimp        
        apply (smt option.simps(5))
        apply (simp split: option.split)
        apply (simp add:  store_at_def  value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply clarsimp        
        apply (simp add:  store_at_def  value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply (auto simp add:  store_at_def  value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply (auto simp add:  store_at_def  value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[3]
        apply (simp add:  store_at_def  value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply clarsimp      
        apply (simp split: option.split)  
        apply (simp add:  store_at_def  value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply (intro conjI impI allI)+
        apply clarsimp
        apply blast
        using global_rel_def grel apply auto[1]
        apply (smt domI domIff option.case_eq_if option.sel)
        apply (simp split: option.split)
        apply (auto simp add:  store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply (simp split: option.split)
        apply (intro conjI impI update_partials2)+
        apply (auto simp add: cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply (cases "State.write_set as t (addr cs t) = None")
        apply (auto simp add:  store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply (auto simp add:  store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
(*24*)
        apply (intro conjI impI update_partials2)+
        apply (auto simp add: store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[9]
        apply (cases "State.write_set as t (addr cs t) = None")
        apply (simp split: option.split)
        apply (simp add: store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply (intro conjI impI allI)+
        apply clarsimp
        using global_rel_def grel apply auto[1]
        apply (smt domI option.case_eq_if option.sel option.the_def)
        apply (auto simp add: store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[5]
        apply (auto simp add: store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[5]
        apply (auto simp add: store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[5]
        apply (auto simp add: store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[2]
        apply (auto simp add: store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply (simp add: store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply clarsimp
        apply (simp add: store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply (simp split: option.split)
        apply (intro conjI impI allI)+
        apply (auto simp add: store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply (cases "addr cs t \<notin> dom (State.write_set as t)")
        apply blast
        using global_rel_def grel apply auto[1]
        apply (cases "addr cs t \<notin> dom (State.write_set as t)")
        apply blast
        using global_rel_def grel apply auto[1]
        apply (cases "addr cs t \<notin> dom (State.write_set as t)")
        apply blast
        using global_rel_def grel apply auto[1]
        apply (cases "addr cs t \<notin> dom (State.write_set as t)")
        apply blast
        using global_rel_def grel apply auto[1]
        apply (auto simp add: store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply (cases "addr cs t \<notin> dom (State.write_set as t)")
        apply blast
        apply (smt id_def option.simps(5))
        apply (cases "addr cs t \<notin> dom (State.write_set as t)")
        apply blast
        apply (smt id_def option.simps(5))
        apply (cases "addr cs t \<notin> dom (State.write_set as t)")
        apply blast
        apply (smt id_def option.simps(5))
        apply (cases "addr cs t \<notin> dom (State.write_set as t)")
        apply blast
        apply (smt id_def option.simps(5))
        apply (auto simp add: store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply (auto simp add: store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[3]
        apply (auto simp add: store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply (simp add: store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply (cases "addr cs t \<notin> dom (State.write_set as t)")
        apply blast
        apply (simp add: store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply (simp split: option.split)
        apply (simp add: store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply (intro conjI impI allI)+
        apply simp
        apply clarsimp
        apply (simp add: store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply clarsimp
        using global_rel_def grel apply auto[1]
        apply auto[1]        
        apply clarsimp
        apply (simp add: store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply clarsimp
        apply (smt option.simps(5))
        apply (simp split: option.split)
        apply (auto simp add: store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply (auto simp add: store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
(*23*) 
        apply (intro conjI impI update_partials2)+
        apply (auto simp add: option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[2]
        apply (simp split:option.split)
        apply (auto simp add: option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
        apply presburger
        apply fastforce
        apply presburger
        apply fastforce
        apply (simp add: ws_dom)
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[15]
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[10]
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[10]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[2]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[2]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[2]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[2]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[2]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[2]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[2]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[2]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[2]
(*22*)        
        apply (intro conjI impI update_partials2)+
        apply (auto simp add: option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[8]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[20]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[7]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[4]
(*21*)
        apply (intro conjI impI update_partials2)+
        apply (auto simp add: option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[8]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[20]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[7]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[4]
(*20*)
        apply (intro conjI impI update_partials2)+
        apply (auto simp add: option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[8]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[20]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[7]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[4]
(*19*)
        apply (intro conjI impI update_partials2)+
        apply (auto simp add: option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[8]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[20]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[7]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[4]
(*18*)
        apply (intro conjI impI update_partials2)+
        apply (auto simp add: option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[8]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[2]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[13]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[4]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
(*17*)
        apply (intro conjI impI update_partials2)+
        apply (auto simp add: option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[8]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[2]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[13]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[4]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
(*16*)
        apply (intro conjI impI update_partials2)+
        apply (auto simp add: option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[8]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[2]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[8]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[1]
(*15*)
        apply (intro conjI impI update_partials2)+
        apply (auto simp add: option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[8]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[2]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[8]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[9]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[2]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[2]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[2]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[2]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[2]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[2]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[2]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[2]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[2]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[2]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[2]
        apply (cases "State.write_set as t (addr cs t) = None")        
        apply (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_read_set_def)[2]
(*14*)
        apply (intro conjI impI update_partials2)+
        by (auto simp add: ws_dom option.case_eq_if store_at_def value_at_def update_partial_def status_enabled_def cga_simps when_fn_def Interface.update_write_set_def Interface.update_read_set_def)[18]
    qed
  qed
qed

lemma "traces (ioa CGA) \<subseteq> traces (ioa TMS2Var)"
  using CGA_simulation standard_simulation_trace_inclusion by blast
end