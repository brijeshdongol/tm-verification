theory CGASplitCorrect
imports CGASplit CGA "~~/src/HOL/Eisbach/Eisbach_Tools"
begin

definition step_correspondence :: "CGASplit.state \<Rightarrow> T \<Rightarrow> CGASplit.pc \<Rightarrow> CGA.pc option"
  where
  "step_correspondence cs t a \<equiv>
    case a of
        CGASplit.Read \<Rightarrow> Some Read
        | CGASplit.Write \<Rightarrow> Some Write
        | Commit1 \<Rightarrow> if (CGASplit.write_set cs t) = Map.empty
                     then Some Commit 
                     else None 
        | Commit2 \<Rightarrow> Some Commit
        | CGASplit.Begin \<Rightarrow> Some Begin"

lemma scf1: "step_correspondence cs t ic = Some Read \<longleftrightarrow> ic = CGASplit.Read"
  by (cases ic) (simp_all add: step_correspondence_def)

lemma scf2: "step_correspondence cs t ic = Some Write \<longleftrightarrow> ic = CGASplit.Write"
  by (cases ic) (simp_all add: step_correspondence_def)

lemma scf3: "step_correspondence cs t ic = Some Commit \<longleftrightarrow>
  ((ic = CGASplit.Commit1 \<and> CGASplit.write_set cs t = Map.empty) \<or> ic = CGASplit.Commit2)"
  by (cases ic) (simp_all add: step_correspondence_def)

lemma scf4: "step_correspondence cs t ic = Some Begin \<longleftrightarrow> ic = CGASplit.Begin"
  by (cases ic) (simp_all add: step_correspondence_def)

lemmas sc_simps = scf1 scf2 scf3 scf4

lemma step_correspondence_None_cases:
  "step_correspondence cs t a = None
  \<longleftrightarrow>  (a = Commit1 \<and> (CGASplit.write_set cs t) \<noteq> Map.empty)"
  using assms
  apply (cases a)
  by (auto simp add: step_correspondence_def split: option.splits)


definition global_rel :: "CGASplit.state \<Rightarrow> CGA.state \<Rightarrow> bool" where
  "global_rel cs as \<equiv> 
                CGASplit.store cs = store as
              \<and> CGASplit.lver cs = lver as 
              \<and> CGASplit.addr cs = addr as 
              \<and> CGASplit.val cs = val as 
              \<and> CGASplit.gvc cs = gvc as 
              \<and> CGASplit.lver cs = lver as
              \<and> CGASplit.ver cs = ver as 
              \<and> CGASplit.write_set cs = write_set as
              \<and> CGASplit.read_set cs = read_set as"

definition global_rel_var :: "CGASplit.state \<Rightarrow> CGA.state \<Rightarrow> bool" where
  "global_rel_var cs as \<equiv> 
                (\<forall>l. CGASplit.store cs l = store as l)
              \<and> (\<forall>l. CGASplit.lver cs l = lver as l) 
              \<and> (\<forall>t. CGASplit.addr cs t = addr as t) 
              \<and> (\<forall>t. CGASplit.val cs t = val as t) 
              \<and> CGASplit.gvc cs = gvc as 
              \<and> (\<forall>t. CGASplit.ver cs t = ver as t) 
              \<and> (\<forall>t. CGASplit.write_set cs t = write_set as t)
              \<and> (\<forall>t. CGASplit.read_set cs t = read_set as t)"

lemma global_rel_var: "global_rel cs as = global_rel_var cs as"
  by (auto simp add: global_rel_def global_rel_var_def)

definition txn_rel :: "T \<Rightarrow> CGASplit.pc Event \<Rightarrow> CGASplit.state \<Rightarrow> CGA.state \<Rightarrow> bool"
  where
  "txn_rel t e cs0 as0 \<equiv> 
     (CGASplit.cga_pre t e cs0 \<longrightarrow>
       (case e of
          External BeginInv  \<Rightarrow> CGA.status as0 t = NotStarted
        | Internal CGASplit.Begin \<Rightarrow> CGA.status as0 t = Pending Begin
        | External BeginResp \<Rightarrow> CGA.status as0 t = BeginResponding
        | Internal CGASplit.Read \<Rightarrow> CGA.status as0 t = Pending Read
        | External (ReadResp v) \<Rightarrow> status as0 t = (ReadResponding v)
        | Internal CGASplit.Write \<Rightarrow> status as0 t = Pending Write
        | External WriteResp \<Rightarrow> status as0 t = WriteResponding
        | Internal CGASplit.Commit1 \<Rightarrow> status as0 t = Pending Commit
              (*if (CGASplit.write_set cs0 t =  Map.empty \<or> ws_free t cs0) 
              then status as0 t = Pending Commit
              else status as0 t = AbortPending*)
        | Internal CGASplit.Commit2 \<Rightarrow> status as0 t = Pending Commit
        | External CommitResp \<Rightarrow> status as0 t = CommitResponding
        | External Abort \<Rightarrow> (*status as0 t = AbortPending \<or> status as0 t = Pending Commit *)
                            status as0 t \<notin> {NotStarted, Ready, CommitResponding, Committed, Aborted}
        | _ \<Rightarrow> status as0 t = Ready)
     )"

definition sim_rel :: "CGASplit.state \<Rightarrow> CGA.state \<Rightarrow> bool"
  where
  "sim_rel cs as \<equiv> global_rel cs as \<and> (\<forall>t a. txn_rel t a cs as)"

named_theorems simps

method double_case_simp for a and b :: "CGASplit.pc Event" declares simps =
  (cases a; (rule CGASplit.Event_split[where b = b]; simp add: simps))

lemma cga_reachable_invariant: "reach CGA s \<Longrightarrow> txn_inv t e s"
  using invariant_elim total_inv by fastforce

lemma ws_empty: "(\<forall>l. state.write_set cs t l = None) \<longleftrightarrow> state.write_set cs t = Map.empty"
  by auto

lemmas cga_tms2_simps =
  cga_simps CGASplit.cga_simps RWMemory.all_simps txn_rel_def

lemmas txn_rel_simps = 
global_rel_def cga_tms2_simps all_utilities 

lemmas sim_simps =
  cga_simps latest_store_def CGA_def CGA.txn_inv_def
  write_back_def max_index_def store_at_def txn_rel_def
  option.case_eq_if global_rel_def value_for_def when_fn_def

lemma txn_rel_dup: "\<forall>t a. txn_rel t a cs as \<Longrightarrow> txn_rel t a cs as \<and> txn_rel t' a' cs as"
  by simp

lemma CGASplit_simulation:
  "standard_simulation CGASplit.CGA CGA step_correspondence sim_rel"
proof (unfold standard_simulation_def, intro conjI allI impI)
  fix cs
  assume "daut.start CGASplit.CGA cs"
  thus "\<exists>as. sim_rel cs as \<and> daut.start CGA.CGA as"
    apply (rule_tac x = "\<lparr>store = CGASplit.store cs,
                status = (\<lambda>t. NotStarted),
                val = CGASplit.val cs,
                addr = CGASplit.addr cs,
                gvc = CGASplit.gvc cs,
                lver = CGASplit.lver cs,
                ver = CGASplit.ver cs,
                write_set = CGASplit.write_set cs,
                read_set = CGASplit.read_set cs\<rparr>" in exI)
    apply (intro conjI)
    apply (simp add: sim_rel_def)
    apply (intro conjI)
    apply (simp add: global_rel_def)
    apply (intro allI)
    apply (rule_tac b = a in CGASplit.Event_split)
    apply (simp_all add: txn_rel_def CGASplit.cga_pre_def CGASplit.CGA_def CGASplit.start_def sim_simps)
    by (simp add: start_def CGASplit.start_def CGASplit.CGA_def sim_simps)
next
  show "standard_sim_stutter CGASplit.CGA CGA.CGA step_correspondence sim_rel"
    unfolding standard_sim_stutter_def
  proof (intro impI allI, (erule conjE)+)
    fix cs as ic at
    assume "reach CGASplit.CGA cs"
       and absreach: "reach CGA.CGA as"
       and "sim_rel cs as"
       and "pre CGASplit.CGA at (Internal ic) cs" 
       and sc_None: "step_correspondence cs at ic = None"
    thus "sim_rel (eff CGASplit.CGA at (Internal ic) cs) as"
      apply -
      apply (simp add: step_correspondence_None_cases)
      apply (simp add: sim_rel_def)
      apply (intro conjI)
      apply (elim conjE)
      apply (simp add: CGASplit.update_status_def update_llck_set_def CGASplit.CGA_def sim_rel_def sim_simps split:CGASplit.pc.split Action.split Event.split)
      apply (intro allI)
      apply (elim conjE)
      apply (drule_tac t = at and a = "Internal Commit1" and t' = t and a' = a in txn_rel_dup)
      apply (frule_tac t = at and e = "Internal Commit1" in CGASplit.reachable_invariant)
      apply (drule_tac t = t and e = "a" in CGASplit.reachable_invariant)
      apply (rule_tac b = a in CGASplit.Event_split)
      apply simp_all
      apply (simp_all add: CGASplit.update_status_def CGASplit.cga_simps CGASplit.cga_pre_def ws_free_def update_llck_set_def CGASplit.CGA_def sim_rel_def sim_simps split:CGASplit.pc.split Action.split Event.split)
      by auto
  qed
next
  show "standard_sim_ext_step CGASplit.CGA CGA.CGA sim_rel"
    unfolding standard_sim_ext_step_def
  proof (intro impI conjI allI; (elim conjE)+)
    fix cs as e at
    assume "reach CGASplit.CGA cs"
    and "reach CGA.CGA as"
    and SR: "sim_rel cs as"
    and cga_split_pre: "pre CGASplit.CGA at (External e) cs"

    from this have cga_split_global: "CGASplit.global_inv cs"
    and cga_split_txn: "CGASplit.txn_inv at (External e) cs"
      using CGASplit.reachable_invariant by blast+

    have GR: "global_rel cs as" and TR: "txn_rel at (External e) cs as"
      using SR sim_rel_def by blast+

    from this cga_split_global cga_split_txn cga_split_pre
    show "pre CGA.CGA at (External e) as" 
      by (cases e) (simp_all add: CGASplit.cga_simps CGA.cga_simps CGASplit.CGA_def txn_rel_def global_rel_def)

    show "sim_rel (eff CGASplit.CGA at (External e) cs) (eff CGA.CGA at (External e) as)"
    proof (simp add: sim_rel_def, intro conjI allI)
      from GR TR cga_split_global cga_split_txn cga_split_pre
      show "global_rel (eff CGASplit.CGA at (External e) cs) (eff CGA.CGA at (External e) as)"
        by (cases e) (simp_all add: CGASplit.cga_simps CGA.cga_simps CGASplit.CGA_def txn_rel_def global_rel_def)
    next
      fix t a
      {
        assume "t = at"
        from this and GR TR cga_split_global cga_split_txn cga_split_pre
        have "txn_rel t a (eff CGASplit.CGA at (External e) cs) (eff CGA.CGA at (External e) as)"
          by (cases e; rule_tac b = a in CGASplit.Event_split)
             (simp_all add: CGASplit.cga_simps CGA.cga_simps CGASplit.CGA_def txn_rel_def global_rel_def)
      }
      moreover
      {
        assume two_trans: "t \<noteq> at"

        have "txn_rel t a cs as"
          using SR sim_rel_def by auto

        from this two_trans and GR TR cga_split_global cga_split_txn cga_split_pre
        have "txn_rel t a (eff CGASplit.CGA at (External e) cs) (eff CGA.CGA at (External e) as)"
          by (cases e; rule_tac b = a in CGASplit.Event_split)
             (simp_all add: CGASplit.cga_simps CGA.cga_simps CGASplit.CGA_def txn_rel_def global_rel_def)
      }
      ultimately
      show "txn_rel t a (eff CGASplit.CGA at (External e) cs) (eff CGA.CGA at (External e) as)"
        by blast
    qed
  qed
next
  show "standard_sim_int_step CGASplit.CGA CGA.CGA step_correspondence sim_rel"
    unfolding standard_sim_int_step_def
  proof (intro conjI impI allI; (erule conjE)+)
    fix cs as ic ia at
    assume "reach CGASplit.CGA cs"
    and "reach CGA.CGA as"
    and SR: "sim_rel cs as"
    and cga_split_pre: "pre CGASplit.CGA at (Internal ic) cs"
    and sc: "step_correspondence cs at ic = Some ia"

    from this have cga_split_global: "CGASplit.global_inv cs"
    and cga_split_txn: "CGASplit.txn_inv at (Internal ic) cs"
      using CGASplit.reachable_invariant by blast+

    have GR: "global_rel cs as" and TR: "txn_rel at (Internal ic) cs as"
      using SR sim_rel_def by blast+

    from sc this cga_split_global cga_split_txn cga_split_pre
    show "pre CGA.CGA at (Internal ia) as"
      apply -
      apply (cases ia; simp only: sc_simps; (elim disjE conjE)?)
      by (simp_all add: CGASplit.cga_simps CGA.cga_simps CGASplit.CGA_def txn_rel_def global_rel_def)
      
    show "sim_rel (eff CGASplit.CGA at (Internal ic) cs) (eff CGA.CGA at (Internal ia) as)"
    proof (simp add: sim_rel_def, intro conjI allI)
      from sc GR TR cga_split_global cga_split_txn cga_split_pre
      show "global_rel (eff CGASplit.CGA at (Internal ic) cs) (eff CGA.CGA at (Internal ia) as)"
        apply -
        apply (cases ia; simp only: sc_simps; (elim disjE conjE)?)
        apply (simp add: CGASplit.cga_simps CGA.cga_simps CGASplit.CGA_def txn_rel_def global_rel_def)
        apply (simp add: CGASplit.cga_simps CGA.cga_simps CGASplit.CGA_def txn_rel_def global_rel_def)
        apply (simp add: global_rel_var global_rel_var_def CGA.cga_simps CGASplit.CGA_def)
        apply (cases "CGA.state.write_set as at = Map.empty")
        apply (simp add: CGASplit.cga_simps)
        apply (simp add: CGASplit.cga_simps)
        apply auto[1]
        apply (simp add: global_rel_var global_rel_var_def CGA.cga_simps CGASplit.CGA_def)
        apply (cases "CGA.state.write_set as at (CGA.state.addr as at)")
        apply (simp add: CGASplit.cga_simps CGA.cga_simps update_partial_def)
        apply (simp add: CGASplit.cga_simps CGA.cga_simps update_partial_def)
        by (simp add: CGASplit.cga_simps CGA.cga_simps CGASplit.CGA_def txn_rel_def global_rel_def)
    next
      fix t a
      {
        assume "t = at"
        from this and sc GR TR cga_split_global cga_split_txn cga_split_pre
        have "txn_rel t a (eff CGASplit.CGA at (Internal ic) cs) (eff CGA.CGA at (Internal ia) as)"
          by (cases ia; simp only: sc_simps; (elim disjE conjE)?; rule_tac b = a in CGASplit.Event_split)     
             (auto simp add: CGASplit.cga_simps CGA.cga_simps CGASplit.CGA_def txn_rel_def global_rel_def split: option.split)
      }
      moreover
      {
        assume two_trans: "t \<noteq> at"

        have "txn_rel t a cs as"
          using SR sim_rel_def by auto

        from this two_trans and sc GR TR cga_split_global cga_split_txn cga_split_pre
        have "txn_rel t a (eff CGASplit.CGA at (Internal ic) cs) (eff CGA.CGA at (Internal ia) as)"
          by (cases ia; simp only: sc_simps; (elim disjE conjE)?; rule_tac b = a in CGASplit.Event_split)     
             (auto simp add: CGASplit.cga_simps CGA.cga_simps CGASplit.CGA_def txn_rel_def global_rel_def split: option.split)
      }
      ultimately
      show "txn_rel t a (eff CGASplit.CGA at (Internal ic) cs) (eff CGA.CGA at (Internal ia) as)"
        by blast
    qed
  qed
qed

lemma "traces (ioa CGA) \<subseteq> traces (ioa CGA)"
  using CGASplit_simulation standard_simulation_trace_inclusion by blast

end