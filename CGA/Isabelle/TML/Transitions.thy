theory Transitions
imports "~~/src/HOL/HOLCF/IOA/IOA" "Interface"
begin

record ('s, 'i) DAut =
  start :: "'s \<Rightarrow> bool"
  pre   :: "('s \<Rightarrow> T \<Rightarrow> 'i Event \<Rightarrow> bool)"
  eff   :: "('s \<Rightarrow> T \<Rightarrow> 'i Event \<Rightarrow> 's)"

definition preserved :: "('s, 'i) DAut \<Rightarrow> ('s \<Rightarrow> bool) \<Rightarrow> 's \<Rightarrow> bool"
  where
  "preserved aut P s \<equiv> \<forall> t a. P s \<and> pre aut s t a \<longrightarrow> P (eff aut s t a)"


definition local_preserved :: "('s, 'i) DAut \<Rightarrow> ('s => T \<Rightarrow> bool) \<Rightarrow> 's \<Rightarrow> bool"
  where
  "local_preserved aut P s \<equiv> preserved aut (\<lambda> s1. \<forall> t. P s1 t) s"

definition noninterference_preserved :: "('s, 'i) DAut \<Rightarrow> ('s => T \<Rightarrow> T \<Rightarrow> bool) \<Rightarrow> 's \<Rightarrow> bool"
  where
  "noninterference_preserved aut P s \<equiv> preserved aut (\<lambda> s1. \<forall> t t'. t \<noteq> t' \<longrightarrow> P s1 t t') s"

lemma preserved_intro:
  "\<lbrakk>\<And> at a. \<lbrakk>P s; pre dt s at a\<rbrakk> \<Longrightarrow>  P (eff dt s at a)\<rbrakk>
   \<Longrightarrow>
   preserved dt P s"
 by (unfold preserved_def, blast)

lemma local_preserved_intro_single:
  "\<lbrakk>\<And> t at a. \<lbrakk>P s t; pre dt s at a; \<forall> t1. P s t1 \<rbrakk> \<Longrightarrow>  P (eff dt s at a) t\<rbrakk>
   \<Longrightarrow>
   local_preserved dt P s"
by (unfold local_preserved_def preserved_def, blast)

lemma local_preserved_intro:
  "\<lbrakk>\<And> at a. \<lbrakk>P s at; pre dt s at a\<rbrakk> \<Longrightarrow> P (eff dt s at a) at;
    \<And> t at a. \<lbrakk>P s t; pre dt s at a; t \<noteq> at; \<forall> t1. P s t1 \<rbrakk> \<Longrightarrow>  P (eff dt s at a) t\<rbrakk>
   \<Longrightarrow>
   local_preserved dt P s"
by (unfold local_preserved_def preserved_def, blast)

lemma noninterference_preserved_intro_single:
  "\<lbrakk>\<And> t t' at a. \<lbrakk>t \<noteq> t'; P s t t'; pre dt s at a; \<forall> t1 t2. t1 \<noteq> t2 \<longrightarrow> P s t1 t2 \<rbrakk>
    \<Longrightarrow> 
    P (eff dt s at a) t t'\<rbrakk>
   \<Longrightarrow>
   noninterference_preserved dt P s"
by (unfold noninterference_preserved_def preserved_def, blast)
  
definition sig :: "('s, 'i) DAut \<Rightarrow> IOA_Event signature"
  where
  "sig aut \<equiv> ({}, {(t, a). a \<noteq> Internal Tau}, {(t, a). a = Internal Tau})"

definition trans :: "('s, 'i) DAut \<Rightarrow> ('s * IOA_Event * 's) set"
  where
  "trans aut \<equiv>
       {(s0, (t, a), s1).
          case a of
              Internal Tau \<Rightarrow> (\<exists> i. pre aut s0 t (Internal i) \<and> s1 = eff aut s0 t (Internal i))
            | External a \<Rightarrow> (pre aut s0 t (External a) \<and> s1 = eff aut s0 t (External a))}"

definition ioa :: "('s, 'i) DAut \<Rightarrow> (IOA_Event, 's) ioa"
  where
  "ioa aut \<equiv>
    (sig aut,
     {s . start aut s},
     (trans aut),
     {},
     {})"

definition reach :: "('s, 'i) DAut \<Rightarrow> 's \<Rightarrow> bool"
  where
  "reach aut s \<equiv> reachable (ioa aut) s"

lemmas unfold_dtrans =
  pre_def eff_def fst_def snd_def

definition local_invariant :: "('s, 'i) DAut \<Rightarrow> ('s \<Rightarrow> T \<Rightarrow> bool) \<Rightarrow> bool"
  where
  "local_invariant aut I \<equiv> \<forall> s. reach aut s \<longrightarrow> (\<forall> t. I s t)"

definition noninterference_invariant :: "('s, 'i) DAut \<Rightarrow> ('s \<Rightarrow> T \<Rightarrow> T \<Rightarrow> bool) \<Rightarrow> bool"
  where
  "noninterference_invariant aut I \<equiv> \<forall> s. reach aut s \<longrightarrow> (\<forall> t t'. t \<noteq> t' \<longrightarrow> I s t t')"

lemma invariant_intro:
  "\<lbrakk>\<And> s. start aut s \<Longrightarrow> P s;
    \<And> s. reach aut s \<Longrightarrow>  preserved aut P s\<rbrakk>
    \<Longrightarrow>
    invariant (ioa aut) P"
  proof (rule invariantI [of "ioa aut" P])
  fix aut P and s
  assume start: "\<And> s :: 's. start aut s \<Longrightarrow> P s" and (*TODO: resolve warning w.out breaking proof *)
         ioa_start: "s \<in> starts_of (ioa aut)"
  from start ioa_start show "P s" by (auto simp add: starts_of_def ioa_def)
  next
  fix s a t
  assume pres: "\<And> s. reach aut s \<Longrightarrow> preserved aut P s" and
         hyp: "P s" and
         ioa_reach: "reachable (ioa aut) s"
  from ioa_reach have "reach aut s" by (simp add: reach_def) 
  from this pres hyp 
  show "s \<midarrow>a\<midarrow>ioa aut \<rightarrow> t \<longrightarrow> P t"
   by (auto simp add: preserved_def trans_of_def ioa_def trans_def Event.exhaust Hidden.exhaust
                  split: Event.split)
      (smt Event.exhaust Hidden.case Hidden.exhaust)
  qed


lemma local_invariant_intro:
  "\<lbrakk>(\<And> s t. start aut s \<Longrightarrow> P s t);
    (\<And> s. reach aut s \<Longrightarrow> local_preserved aut P s)
    \<rbrakk>
  \<Longrightarrow>
  local_invariant aut P"
apply(insert invariant_intro[where aut=aut and P="\<lambda> s. \<forall> t. P s t"])
apply(unfold local_preserved_def invariant_def local_invariant_def reach_def)
apply(blast)
done

lemma noninterference_invariant_intro:
  "\<lbrakk>(\<And> s t t'. start aut s \<and> t \<noteq> t' \<Longrightarrow> P s t t');
    (\<And> s. reach aut s \<Longrightarrow> noninterference_preserved aut P s)
    \<rbrakk>
  \<Longrightarrow>
  noninterference_invariant aut P"
apply(insert invariant_intro[where aut=aut and P="\<lambda> s. \<forall> t1 t2. t1\<noteq>t2 \<longrightarrow> P s t1 t2"])
apply(unfold noninterference_preserved_def invariant_def noninterference_invariant_def reach_def)
apply(blast)
done

lemma invariant_elim:
  "\<lbrakk> invariant (ioa aut) P; reach aut s \<rbrakk> \<Longrightarrow> P s"
by (simp add: invariantE reach_def)

lemma local_invariant_elim:
  "\<lbrakk> local_invariant aut P; reach aut s \<rbrakk> \<Longrightarrow> P s t"
by (simp add: local_invariant_def invariantE reach_def)

lemma noninterference_invariant_elim:
  "\<lbrakk> noninterference_invariant aut P; t \<noteq> t'; reach aut s \<rbrakk> \<Longrightarrow> P s t t'"
by (simp add: noninterference_invariant_def invariantE reach_def)


lemma invariant_strengthen:
  "\<lbrakk>invariant (ioa aut) P;
    (\<forall> s . P s \<longrightarrow> Q s)
    \<rbrakk>
    \<Longrightarrow>
    invariant (ioa aut) Q"
by (simp add: invariant_def)

lemma local_invariant_strengthen:
  "\<lbrakk>local_invariant aut P;
    (\<forall> s t . P s t \<longrightarrow> Q s t)
    \<rbrakk>
    \<Longrightarrow>
    local_invariant aut Q"
by (simp add: local_invariant_def)


definition standard_sim_ext_step :: "('sc, 'ic) DAut \<Rightarrow> ('sa, 'ia) DAut \<Rightarrow> ('sc \<Rightarrow> 'sa \<Rightarrow> bool) \<Rightarrow> bool"
  where
  "standard_sim_ext_step C A R \<equiv>
    \<forall> cs as a at. reach C cs \<and> reach A as \<longrightarrow>
      R cs as \<and> pre C cs at (External a)
       \<longrightarrow>
        pre A as at (External a) \<and> R (eff C cs at (External a)) (eff A as at (External a))"

definition standard_sim_stutter :: "('sc, 'ic) DAut \<Rightarrow> ('sa, 'ia) DAut \<Rightarrow> ('sc \<Rightarrow> T \<Rightarrow> 'ic \<Rightarrow> 'ia option) \<Rightarrow>('sc \<Rightarrow> 'sa \<Rightarrow> bool) \<Rightarrow> bool"
   where
   "standard_sim_stutter C A sc R \<equiv>
    \<forall> cs as ic at. reach C cs \<and> reach A as \<longrightarrow>
       R cs as \<and> pre C cs at (Internal ic) \<and> sc cs at ic = None
         \<longrightarrow>
          R (eff C cs at (Internal ic)) as"

definition standard_sim_int_step :: "('sc, 'ic) DAut \<Rightarrow> ('sa, 'ia) DAut \<Rightarrow> ('sc \<Rightarrow> T \<Rightarrow> 'ic \<Rightarrow> 'ia option) \<Rightarrow>('sc \<Rightarrow> 'sa \<Rightarrow> bool) \<Rightarrow> bool"
   where
   "standard_sim_int_step C A sc R \<equiv>
    \<forall> cs as ic ia at. reach C cs \<and> reach A as \<longrightarrow>
      R cs as \<and> pre C cs at (Internal ic) \<and> sc cs at ic = Some ia
        \<longrightarrow>
          pre A as at (Internal ia) \<and>R (eff C cs at (Internal ic)) (eff A as at (Internal ia))"

definition standard_simulation :: "('sc, 'ic) DAut \<Rightarrow> ('sa, 'ia) DAut \<Rightarrow> ('sc \<Rightarrow> T \<Rightarrow> 'ic \<Rightarrow> 'ia option) \<Rightarrow>('sc \<Rightarrow> 'sa \<Rightarrow> bool) \<Rightarrow> bool"
  where
  "standard_simulation C A sc R \<equiv>
    (\<forall> cs. start C cs \<longrightarrow> (\<exists> as. R cs as \<and> start A as))
    \<and>
    standard_sim_ext_step C A R
    \<and>
    standard_sim_stutter C A sc R
    \<and>
    standard_sim_int_step C A sc R"

(* TODO fix the proofs below *)

lemma standard_simulation_internal_move:
    assumes  sim: "standard_simulation C A sc R" and
           reach_C: "reachable (ioa C) cs" and
           step: "pre C cs (fst p) (Internal i) \<and> cs' = eff C cs (fst p) (Internal i)" and
           rel: "R cs as" and
           reach_A: "reachable (ioa A) as"
    shows "\<exists>as'. R cs' as' \<and> reachable (ioa A) as' \<and> (\<exists>ex. move (ioa A) ex as (fst p, Internal Tau) as')"
proof (cases "sc cs (fst p) i = None")
  assume none: "sc cs (fst p) i = None"
  have stutter_rel: "R cs' as" using sim
    by (simp add: standard_sim_stutter_def standard_simulation_def none local.step reach_A reach_C reach_def rel)
  have stutter_move: "\<exists>ex. move (ioa A) ex as (fst p, Internal Tau) as"
    apply(rule exI[where x="nil"])
    by (simp add: move_def ioa_def externals_def sig_def asig_triple_proj ioa_triple_proj)
  show ?thesis using stutter_move stutter_rel reach_A by blast
next
  assume some: "sc cs (fst p) i \<noteq> None"
  have step_rel: "R cs' (eff A as (fst p) (Internal (the (sc cs (fst p) i))))" using sim
    by (simp add: local.step reach_A reach_C reach_def rel some standard_sim_int_step_def standard_simulation_def)
  have atrans: "as \<midarrow>(fst p, Internal Tau)\<midarrow>ioa A \<rightarrow> (eff A as (fst p) (Internal (the (sc cs (fst p) i))))"
    using some step sim reach_C reach_A rel
    apply(unfold trans_def ioa_def trans_of_def, simp)
    apply(rule exI[where x="the(sc cs (fst p) i)"])
    apply (auto simp add: trans_def ioa_def trans_of_def standard_simulation_def standard_sim_int_step_def)
    by (meson reach_A reach_C reach_def)
  have step_reach: "reachable (ioa A) (eff A as (fst p) (Internal (the (sc cs (fst p) i))))"
    using reach_A atrans by (meson reachable.reachable_n)
  from atrans have step_move: "(\<exists>ex. move (ioa A) ex as (fst p, Internal Tau) (eff A as (fst p) (Internal (the (sc cs (fst p) i)))))"
    apply -
    apply(rule exI[where x="[((fst p, Internal Tau), eff A as (fst p) (Internal (the (sc cs (fst p) i))))!]"])
    by(simp add: move_def ioa_def trans_def)
  from step_rel step_reach step_move show ?thesis by blast
qed

lemma standard_simulation_move:
  assumes  sim: "standard_simulation C A sc R" and
           reach_C: "reachable (ioa C) cs" and
           step: "(cs, p, cs') : trans C" and
           rel: "R cs as" and
           reach_A: "reachable (ioa A) as"
   shows "\<exists>as' ex. (cs', as') \<in> {(x, y). R x y \<and> reachable (ioa A) y} \<and> move (ioa A) ex as p as'"
  proof (cases "snd p")
    case (Internal a)
    have eq: "a = Tau" by (cases a, simp)
    have int_step: "\<exists>i. pre C cs (fst p) (Internal i) \<and> cs' = eff C cs (fst p) (Internal i)"
    using eq Internal step by(auto simp add: trans_def)
    from Internal int_step show ?thesis
    apply -
    apply(drule exE[where Q="\<exists>as'. R cs' as' \<and> reachable (ioa A) as' \<and> (\<exists>ex. move (ioa A) ex as p as')"], simp_all)
    by (metis eq prod.collapse reach_A reach_C rel sim standard_simulation_internal_move)
  next
    case (External a)
    show ?thesis
    proof (rule exI[where x="eff A as (fst p) (External a)"])
    have atrans: "as \<midarrow>p\<midarrow>(ioa A) \<rightarrow> eff A as (fst p) (External a)"
    using step External
    by (auto simp add: ioa_def trans_of_def trans_def eff_def)
       (metis reach_A reach_C reach_def rel sim standard_sim_ext_step_def standard_simulation_def)
    have reach_A_post: "reachable (ioa A) (eff A as (fst p) (External a))"
    using step External reach_A atrans
    by (meson reachable.reachable_n)
    have sim_post: "(cs', eff A as (fst p) (External a)) \<in> {(x, y). R x y}"
    using External sim standard_simulation_def standard_sim_ext_step_def step rel reach_A reach_C
    by (auto simp add: trans_def)
       (metis reach_C reach_def standard_sim_ext_step_def standard_simulation_def)
    from atrans reach_A_post sim_post show "\<exists>ex. (cs', eff A as (fst p) (External a))
         \<in> {(x, y). R x y \<and> reachable (ioa A) y} \<and>
         move (ioa A) ex as p (eff A as (fst p) (External a))"
     by (simp add:  transition_is_ex)
    qed
qed

lemma standard_simulation_trace_inclusion:
  assumes sim: "standard_simulation C A sc R"
    shows      "traces (ioa C) \<subseteq> traces (ioa A)"
proof (rule trace_inclusion_for_simulations[where R="{(cs, as) . R cs as \<and> reachable (ioa A) as}"])
  show "Automata.ext (ioa C) = Automata.ext (ioa A)"
    by (auto simp add: externals_def asig_outputs_def asig_inputs_def asig_of_def fst_def snd_def ioa_def sig_def)
  next
  show "is_simulation {(cs, as) . R cs as \<and> reachable (ioa A) as} (ioa C) (ioa A)"
  proof -
    {from sim have "\<forall>s\<in>(fst \<circ> snd) (ioa C). {(x, y). R x y \<and> reachable (ioa A) y} `` {s} \<inter> (fst \<circ> snd) (ioa A) \<noteq> {}"
      by (insert reachable_0[where C="ioa A"], auto simp add: standard_simulation_def ioa_def starts_of_def)
      }
    moreover
    {from sim have "\<forall> s s' t a.
        reachable (ioa C) s \<and> s \<midarrow>a\<midarrow>(ioa C) \<rightarrow> t \<and> (s, s') \<in> {(x, y). R x y \<and> reachable (ioa A) y} \<longrightarrow> (\<exists>t' ex. (t, t') \<in> {(x, y). R x y \<and> reachable (ioa A) y} \<and> move (ioa A) ex s' a t')"
       by (insert standard_simulation_move[where C=C and A=A and R=R and sc=sc], unfold ioa_def trans_of_def, clarify)
          simp}
    ultimately show ?thesis by (simp add: is_simulation_def starts_of_def)
    qed
qed
    
  
end
