theory TMS2
imports Transitions Interface
begin
  
datatype InternalAction =
    DoCommitReadOnly nat
  | DoCommitWriter
  | do_read: DoRead L nat
  | do_write: DoWrite L V
  
datatype PC =
    CommitPending
  | ReadPending L
  | WritePending L V

type_synonym Status = "PC Interface.Status"
  
record State = StoresState +
  status :: "T \<Rightarrow> Status"
  begin_index :: "T \<Rightarrow> nat"
  read_set :: "T \<Rightarrow> L \<Rightarrow> V option"
  write_set :: "T \<Rightarrow> L \<Rightarrow> V option"
  
  
definition start :: "State \<Rightarrow> bool"
  where
  "start s \<equiv>
      (\<forall> t. status s t = NotStarted)
    \<and> (\<forall> t. read_set s t = empty)
    \<and> (\<forall> t. write_set s t = empty)
    \<and> initial_stores s"

definition default_start :: State
  where
  "default_start =
     \<lparr> stores = (\<lambda> n. if n = 0 then Some (\<lambda> l. v0) else None),
       status = (\<lambda> t. NotStarted),
       begin_index = (\<lambda> t. 0),
       read_set = (\<lambda> t. Map.empty),
       write_set = (\<lambda> t. Map.empty)\<rparr>"

lemma default_is_start:
  "start default_start"
by(auto simp add: start_def default_start_def initial_stores_def mem_initial_def)


definition valid_index :: "State \<Rightarrow> T \<Rightarrow> nat \<Rightarrow> bool"    
    where
    "valid_index s t n \<equiv>
      begin_index s t \<le> n \<and> n \<le> max_index s \<and>
      read_consistent (store_at s n) (read_set s t)"
    
definition update_status :: "T \<Rightarrow> Status \<Rightarrow> State \<Rightarrow> State"
  where
  "update_status t st s \<equiv> s\<lparr> status := ((status s) (t := st))\<rparr>"

definition update_begin_index :: "T \<Rightarrow> nat \<Rightarrow> State \<Rightarrow> State"
  where
  "update_begin_index t n s \<equiv> s\<lparr> begin_index := ((begin_index s) (t := n)) \<rparr>"

definition update_write_set :: "T \<Rightarrow> L \<Rightarrow> V \<Rightarrow> State \<Rightarrow> State"
  where
  "update_write_set t l v s \<equiv> s\<lparr> write_set := update_partial t l v (write_set s)\<rparr>"  

definition update_read_set :: "T \<Rightarrow> L \<Rightarrow> V \<Rightarrow> State \<Rightarrow> State"
  where
  "update_read_set t l v s \<equiv> s\<lparr> read_set := update_partial t l v (read_set s)\<rparr>"  
  

definition status_enabled :: "State \<Rightarrow> T \<Rightarrow> InternalAction Event \<Rightarrow> bool"
  where
  "status_enabled s t e \<equiv>
    case e of
      Internal a \<Rightarrow>
         (case a of
           DoRead l n \<Rightarrow> status s t = Pending(ReadPending l)
           |
           DoCommitReadOnly n \<Rightarrow> status s t = Pending(CommitPending)
           |
           DoCommitWriter \<Rightarrow> status s t = Pending(CommitPending)
           |
           DoWrite l v \<Rightarrow> status s t = Pending(WritePending l v))
      |
      External a \<Rightarrow> ext_enabled (status s t) a"


definition tms_pre :: "State \<Rightarrow> T \<Rightarrow> InternalAction Event \<Rightarrow> bool"
  where
  "tms_pre s t e \<equiv>
      status_enabled s t e
      \<and>
      (case e of
        Internal a \<Rightarrow>
           (case a of
             DoRead l n \<Rightarrow> (l : dom (write_set s t) \<or> valid_index s t n)
             |
             DoCommitReadOnly n \<Rightarrow> (write_set s t = empty \<and> valid_index s t n)
             |
             DoCommitWriter \<Rightarrow> write_set s t \<noteq> Map.empty \<and> read_consistent (latest_store s) (read_set s t)
             |
             DoWrite l v \<Rightarrow> True)
        |
        External a \<Rightarrow> True)"

        
definition ext_eff :: "State \<Rightarrow> T \<Rightarrow> Interface.Action \<Rightarrow> State"
  where
  "ext_eff s0 t a \<equiv>
    case a of
        BeginInv \<Rightarrow>
          (s0 ;; (update_status t BeginResponding)
             ;; (update_begin_index t (Max (dom (stores s0)))))
      | BeginResp \<Rightarrow> (s0 ;; update_status t Ready)
      | CommitInv \<Rightarrow> (s0 ;; update_status t (Pending CommitPending))
      | CommitResp \<Rightarrow> (s0 ;; update_status t CommitResponding)
      | ReadInv l \<Rightarrow> (s0 ;; update_status t (Pending (ReadPending l)))
      | ReadResp v \<Rightarrow> (s0 ;; update_status t Ready)
      | WriteInv l v \<Rightarrow> (s0 ;; update_status t (Pending (WritePending l v)))
      | WriteResp \<Rightarrow> (s0 ;; update_status t Ready)
      | Cancel \<Rightarrow> (s0 ;; update_status t AbortPending)
      | Abort \<Rightarrow> (s0 ;; update_status t Aborted)
    "

definition int_eff :: "State \<Rightarrow> T \<Rightarrow> InternalAction \<Rightarrow> State"
  where
  "int_eff s0 t ia \<equiv>
    case ia of
        DoRead l n \<Rightarrow>
          let v = (value_for s0 n (write_set s0 t) l) in
            (s0 ;; (update_read_set t l v) when (l \<notin> dom (write_set s0 t))
               ;; (update_status t (ReadResponding v)))
                         
      | DoWrite l v \<Rightarrow>
          (s0 ;; (update_write_set t l v)
             ;; (update_status t WriteResponding))
             
      | DoCommitReadOnly n \<Rightarrow>
          (s0 ;; update_status t CommitResponding)
      
      | DoCommitWriter \<Rightarrow>
          (s0 ;; (update_status t CommitResponding)
             ;; (write_back (write_set s0 t)))
      "
      

definition tms_eff :: "State \<Rightarrow> T \<Rightarrow> InternalAction Event \<Rightarrow> State"
  where
  "tms_eff s t e \<equiv> case e of Internal i \<Rightarrow> int_eff s t i | External a \<Rightarrow> ext_eff s t a"

definition TMS2  :: "(State, InternalAction) DAut"
  where
  "TMS2 \<equiv> \<lparr> DAut.start = start,
            DAut.pre = tms_pre,
            DAut.eff = tms_eff\<rparr>"

declare TMS2_def [simp]


lemma Event_split:
  "\<lbrakk>b = External BeginInv \<Longrightarrow> P;
    b = External BeginResp \<Longrightarrow> P;
    \<And> l. b = External (ReadInv l) \<Longrightarrow> P;
    \<And> l n. b = Internal (DoRead l n) \<Longrightarrow> P;
    \<And> v. b = External (ReadResp v) \<Longrightarrow> P;
    \<And> l v. b = External (WriteInv l v) \<Longrightarrow> P;
    \<And> l v. b = Internal (DoWrite l v) \<Longrightarrow> P;
    b = External WriteResp \<Longrightarrow> P;
    b = External CommitInv \<Longrightarrow> P;
    b = Internal DoCommitWriter \<Longrightarrow> P;
    \<And> n. b = Internal (DoCommitReadOnly n) \<Longrightarrow> P;
    b = External CommitResp \<Longrightarrow> P;
    b = External Cancel \<Longrightarrow> P;
    b = External Abort \<Longrightarrow> P \<rbrakk>
   \<Longrightarrow>
    P"
apply(cases rule: Event.exhaust[where y=b])
apply(auto simp add: split: Event.split)
apply (meson Action.exhaust_sel)
by (meson InternalAction.exhaust)

lemmas unfold_updates =
  update_status_def
  update_write_set_def
  update_begin_index_def
  update_read_set_def
  valid_index_def

lemmas unfold_pre =
  tms_pre_def
  status_enabled_def

lemmas unfold_tms2 =
  Transitions.pre_def Transitions.eff_def Transitions.start_def
  start_def ext_eff_def int_eff_def tms_eff_def
  unfold_pre
  unfold_updates

lemmas unfold_all_tms2 =
  unfold_tms2
  initial_stores_def
  RWMemory.all_simps
  Utilities.all_utilities
  ext_enabled_def

lemmas splits = Event.split Action.split InternalAction.split if_splits

lemma begin_index_stable:
  "\<lbrakk>tms_pre s at a; a \<noteq> External BeginInv \<or> t \<noteq> at\<rbrakk>
   \<Longrightarrow>
   begin_index (tms_eff s at a) t = begin_index s t"
by(auto simp add: unfold_all_tms2 split: splits)

lemma max_index_stable:
  "\<lbrakk>tms_pre s at a; a \<noteq> Internal DoCommitWriter\<rbrakk>
   \<Longrightarrow>
   max_index (tms_eff s at a) = max_index s"
by(auto simp add: unfold_all_tms2 split: splits)

lemma stores_stable:
  "\<lbrakk>tms_pre s at a; a \<noteq> Internal DoCommitWriter\<rbrakk>
   \<Longrightarrow>
   stores (tms_eff s at a) = stores s"
by(auto simp add: unfold_all_tms2 split: splits)

lemma stores_update_do_commit_writer:
  "\<lbrakk>tms_pre s at a; a = Internal DoCommitWriter\<rbrakk>
   \<Longrightarrow>
   stores (tms_eff s at a) =
    (stores s) ((max_index s)+1 := Some (apply_partial (store_at s (max_index s)) (write_set s at)))"
by(auto simp add: unfold_all_tms2 split: InternalAction.split)

lemma stores_domain_stable:
  "\<lbrakk>tms_pre s at a; a \<noteq> Internal DoCommitWriter\<rbrakk>
   \<Longrightarrow>
   dom (stores (tms_eff s at a)) = dom(stores s)"
by(auto simp add: unfold_all_tms2 split: splits)

lemma dom_stores_update_do_commit_writer:
  "\<lbrakk> tms_pre s at (Internal DoCommitWriter)\<rbrakk>
    \<Longrightarrow>
    dom (stores (tms_eff s at (Internal DoCommitWriter))) = dom (stores s) \<union> {1 + (max_index s)}"
by (simp add: unfold_all_tms2 max_stores_append)

definition stores_domain :: "State \<Rightarrow> bool"
  where
  "stores_domain s \<equiv>
    finite(dom (stores s)) \<and>
    dom (stores s) \<noteq> {} \<and>
    (\<forall> n. n \<le> max_index s \<longrightarrow> n : dom (stores s))"

lemma stores_domain_max_is_max:
  "stores_domain s \<Longrightarrow> n \<le> max_index s \<longleftrightarrow> n : dom (stores s)"
by(unfold stores_domain_def)
  (metis Max_ge max_index_def)

lemma stores_domain_preserved:
  "\<lbrakk>tms_pre s at a; a \<noteq> Internal DoCommitWriter; stores_domain s\<rbrakk> \<Longrightarrow> stores_domain (tms_eff s at a)"
by(cases rule: Event_split[where b=a],
   simp_all add:  stores_domain_def stores_stable max_index_stable)

lemma stores_domain_preserved_do_commit_writer:
  "\<lbrakk>tms_pre s at (Internal DoCommitWriter); stores_domain s\<rbrakk> \<Longrightarrow> stores_domain (tms_eff s at (Internal DoCommitWriter))"
by(auto simp add:  stores_domain_def stores_update_do_commit_writer unfold_all_tms2
                   max_stores_append domD)

lemma max_index_update:
  "\<lbrakk>tms_pre s at (Internal DoCommitWriter); stores_domain s\<rbrakk>
   \<Longrightarrow>
   max_index (tms_eff s at (Internal DoCommitWriter)) = 1 + max_index s"
by(simp add: unfold_all_tms2 max_stores_append dom_stores_update_do_commit_writer stores_domain_def)


lemma store_at_stable:
  "\<lbrakk>tms_pre s at a;
    a \<noteq> Internal DoCommitWriter \<or> n \<le> max_index s\<rbrakk>
   \<Longrightarrow>
   store_at (tms_eff s at a) n = store_at s n"
by(auto simp add: unfold_all_tms2 split: splits)

lemma read_set_stable:
  "\<lbrakk>tms_pre s at a; (\<forall> l n. a \<noteq> Internal (DoRead l n)) \<or> t \<noteq> at\<rbrakk>
   \<Longrightarrow>
   read_set (tms_eff s at a) t = read_set s t"
by(auto simp add: unfold_all_tms2 split: splits)

lemma read_set_valid:
  "\<lbrakk>l : dom (read_set s t);
    read_consistent (store_at s n) (read_set s t)\<rbrakk>
   \<Longrightarrow>
   read_set s t l = Some(store_at s n l)"
by(auto elim: allE[where x = l] simp add: unfold_all_tms2)

lemma read_set_valid_read_with_valid_index:
  "\<lbrakk> tms_pre s at a;
     l : dom (read_set s at);
     (l \<notin> dom (write_set s at) \<and> a = Internal (DoRead l n))
      \<or>
      a = Internal(DoCommitReadOnly n)\<rbrakk>
    \<Longrightarrow>
   read_set s at l = Some(store_at s n l)"
 using read_set_valid tms_pre_def valid_index_def by fastforce

lemma read_set_valid_read_commit_writer:
  "\<lbrakk>tms_pre s at (Internal DoCommitWriter);
    l : dom (read_set s at)\<rbrakk>
    \<Longrightarrow>
   read_set s at l = Some(store_at s (max_index s) l)"
by (auto simp add: read_set_valid unfold_all_tms2 domD)
  (smt option.simps)

lemma read_set_stable_do_read:
  "\<lbrakk>tms_pre s at (Internal (DoRead l n));
    stores_domain s; 
    l : (dom (write_set s at)) \<or> l : (dom (read_set s at))\<rbrakk>
   \<Longrightarrow>
   read_set (tms_eff s at (Internal (DoRead l n))) at = read_set s at"
by (simp add: stores_domain_def unfold_all_tms2 split: option.split)
   (smt domD domIff fun_upd_triv option.discI option.simps(5))


lemma tau_read_set_update:
  "\<lbrakk>tms_pre s at (Internal (DoRead l n)); l \<notin> dom (write_set s at)\<rbrakk>
    \<Longrightarrow>
   read_set (tms_eff s at (Internal (DoRead l n))) at = (read_set s at)(l := Some(value_at s n l))"
by(auto simp add: unfold_all_tms2 split: splits)

lemma ext_write_set_stable:
  "\<lbrakk>tms_pre s at a; (\<forall> l v. a \<noteq> Internal (DoWrite l v)) \<or> t \<noteq> at\<rbrakk>
   \<Longrightarrow>
   write_set (tms_eff s at a) t = write_set s t"
by(auto simp add: unfold_all_tms2 split: splits)

lemma tau_write_set_update:
  "\<lbrakk>tms_pre s at (Internal (DoWrite l v))\<rbrakk>
    \<Longrightarrow>
   write_set (tms_eff s at (Internal (DoWrite l v))) at = (write_set s at)(l := Some v)"
  by(auto simp add: unfold_all_tms2)

lemma status_stable:
    "\<lbrakk>tms_pre s at a; t \<noteq> at\<rbrakk>
     \<Longrightarrow>
     status (tms_eff s at a) t = status s t"
by(auto simp add: unfold_all_tms2 split: splits)

definition txn_inv :: "InternalAction Event \<Rightarrow> State \<Rightarrow> T \<Rightarrow> bool"
  where
  "txn_inv e s t \<equiv>
     status_enabled s t e \<longrightarrow>
     (case e of
       (External BeginInv) \<Rightarrow> read_set s t = Map.empty \<and> write_set s t = Map.empty
       |
       (External BeginResp) \<Rightarrow>   begin_index s t \<le> max_index s
                               \<and> read_set s t = Map.empty
                               \<and> write_set s t = Map.empty
       |
       (Internal (DoRead l n)) \<Rightarrow> begin_index s t \<le> max_index s
       |
       (External (ReadResp v)) \<Rightarrow> begin_index s t \<le> max_index s
       |
       (Internal (DoWrite l v)) \<Rightarrow> begin_index s t \<le> max_index s
       |
       (External WriteResp) \<Rightarrow> begin_index s t \<le> max_index s \<and> write_set s t \<noteq> Map.empty
       |
       (Internal (DoCommitReadOnly n)) \<Rightarrow> begin_index s t \<le> max_index s
       |
       (Internal DoCommitWriter) \<Rightarrow> begin_index s t \<le> max_index s
       |
       (External CommitResp) \<Rightarrow> True
       |
       (External Abort) \<Rightarrow> True
       |
       _ \<Rightarrow> begin_index s t \<le> max_index s)"

lemma txn_inv_preserved_self:
  "\<lbrakk>reach TML s;
    stores_domain s;
    txn_inv e s at;
    tms_pre s at e\<rbrakk>
    \<Longrightarrow>
    txn_inv b (tms_eff s at e) at"
apply(cases rule: Event_split[where b=b])
apply(simp_all add: txn_inv_def begin_index_stable max_index_stable max_index_update)
apply(simp_all add: unfold_tms2 split: splits)
apply(auto simp add: unfold_all_tms2)
done

lemma txn_inv_preserved_other:
  assumes "stores_domain s" and
          "txn_inv b s t" and
          "t \<noteq> at" and
          "tms_pre s at e"
    shows "txn_inv b (tms_eff s at e) t"
proof (cases e)
 case (External a)
 from this External assms show ?thesis
 by (cases rule: Event_split[where b=b])
    (simp_all add: unfold_all_tms2 txn_inv_def split: Action.split)
 next
 case (Internal a)
   have mi: "max_index s \<le> max_index (tms_eff s at e)"
    using assms Internal by (cases a, auto simp add: max_index_update max_index_stable)
   have bi: "begin_index s t \<le> max_index s \<longrightarrow> begin_index (tms_eff s at e) t \<le> max_index (tms_eff s at e)"
    using mi assms  begin_index_stable tms_eff_def by (cases a, auto simp add: unfold_tms2)
   show ?thesis
   proof (cases a)
     case (DoCommitReadOnly n)
       from this Internal assms show ?thesis
       by (cases rule: Event_split[where b=b])
          (simp_all add: unfold_all_tms2 txn_inv_def)
     next
     case DoCommitWriter
       from this Internal assms bi show ?thesis
      by (cases rule: Event_split[where b=b])
         (auto simp add: unfold_all_tms2 txn_inv_def)
     next
     case (DoRead l n)
       from this Internal assms bi show ?thesis
       by (cases rule: Event_split[where b=b])
          (simp_all add: unfold_all_tms2 txn_inv_def)
     next
     case (DoWrite l v)
       from this Internal assms bi show ?thesis
       by (cases rule: Event_split[where b=b])
          (simp_all add: unfold_all_tms2 txn_inv_def)
   qed
qed

lemma total_inv_initial:
  "start s \<Longrightarrow> stores_domain s \<and> (\<forall> e t. txn_inv e s t)"
by (auto simp add: start_def stores_domain_def txn_inv_def initial_stores_def dom_def
                     max_index_def unfold_pre ext_enabled_def split: if_splits splits)


lemma total_inv:
  "invariant (ioa TMS2) (\<lambda> s. stores_domain s \<and> (\<forall> e t. txn_inv e s t))"
apply(rule invariant_intro)
  apply(rule total_inv_initial, simp)
  apply(unfold preserved_def)
apply(auto simp add: txn_inv_preserved_self txn_inv_preserved_other stores_domain_preserved stores_domain_preserved_do_commit_writer)
using stores_domain_preserved stores_domain_preserved_do_commit_writer apply metis
using txn_inv_preserved_self txn_inv_preserved_other apply metis
done

lemma read_consistent_stable:
  "\<lbrakk>tms_pre s at a;
    stores_domain s;
    txn_inv a s at;
    n \<le> max_index s;
    (\<forall> l n. a \<noteq> Internal (DoRead l n)) \<or> t \<noteq> at\<rbrakk>
   \<Longrightarrow>
   read_consistent (store_at (tms_eff s at a) n) (read_set (tms_eff s at a) t) =
     read_consistent (store_at s n) (read_set s t)"
by(simp add: store_at_stable read_set_stable)

end