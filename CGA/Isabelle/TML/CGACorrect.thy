theory CGACorrect
imports CGA TMS2
begin

definition write_count :: "nat \<Rightarrow> nat"
  where
  "write_count n \<equiv> n div 2"

definition step_correspondence :: "CGA.state \<Rightarrow> T \<Rightarrow> CGA.pc \<Rightarrow> TMS2.InternalAction option"
  where
  "step_correspondence cs t a \<equiv>
    case a of
        Read \<Rightarrow> if loc cs t = glb cs
                then Some (DoRead (addr cs t) (write_count (loc cs t)))
                else None
      | Write \<Rightarrow> if loc cs t = glb cs then Some (DoWrite (addr cs t) (val cs t)) else None
      | Commit \<Rightarrow> if even (loc cs t)
                  then Some (DoCommitReadOnly (write_count (loc cs t)))
                  else Some DoCommitWriter
      | _ \<Rightarrow> None"

definition writes :: "CGA.state \<Rightarrow> TMS2.State \<Rightarrow> L \<Rightarrow> V option"
  where
  "writes cs0 as0 \<equiv>
    case (writer cs0) of Some w \<Rightarrow> write_set as0 w | None \<Rightarrow> Map.empty"

definition global_rel :: "CGA.state \<Rightarrow> TMS2.State \<Rightarrow> bool"
  where
  "global_rel cs0 as0 \<equiv>
     store cs0 = apply_partial (latest_store as0) (writes cs0 as0)
     \<and>
     write_count (glb cs0) = max_index as0"

definition validity_prop :: "CGA.state \<Rightarrow> TMS2.State \<Rightarrow> T \<Rightarrow> bool"
  where
  "validity_prop cs as t \<equiv>
           begin_index as t \<le> write_count (loc cs t)
        \<and> (read_consistent (store_at as (write_count (loc cs t))) (read_set as t))"

definition in_flight :: "CGA.state \<Rightarrow> TMS2.State \<Rightarrow> T \<Rightarrow> bool"
  where
  "in_flight cs as t \<equiv>
      (even (loc cs t) \<longleftrightarrow> write_set as t = Map.empty)
      \<and> validity_prop cs as t
      \<and> (odd (loc cs t) \<longrightarrow> writer cs = Some t \<and> glb cs = loc cs t)" (* Had to add this *)

definition txn_rel :: "CGA.pc Event \<Rightarrow> CGA.state \<Rightarrow> TMS2.State \<Rightarrow> T \<Rightarrow> bool"
  where
  "txn_rel e cs0 as0 t \<equiv>
    cga_pre cs0 t e \<longrightarrow>
   (case e of
      External BeginInv \<Rightarrow> status as0 t = NotStarted
      |
      Internal Begin \<Rightarrow>   status as0 t = BeginResponding
      |
      External BeginResp \<Rightarrow> status as0 t = BeginResponding
                         \<and> (begin_index as0 t \<le> write_count (loc cs0 t))
      |
      Internal Read \<Rightarrow>   status as0 t = Pending (ReadPending (addr cs0 t))
                        \<and> in_flight cs0 as0 t
      |
      External (ReadResp v) \<Rightarrow>   status as0 t = ReadResponding (val cs0 t)
                        \<and> in_flight cs0 as0 t
      |
      Internal Write \<Rightarrow>   status as0 t = Pending (WritePending (addr cs0 t) (val cs0 t))
                        \<and> in_flight cs0 as0 t
      |
      External WriteResp \<Rightarrow> status as0 t = WriteResponding
                        \<and> validity_prop cs0 as0 t
      |
      Internal Commit \<Rightarrow>  status as0 t = Pending CommitPending
                        \<and> in_flight cs0 as0 t
      |
      External CommitResp \<Rightarrow> status as0 t = CommitResponding
      |
      External Abort \<Rightarrow> status as0 t \<notin> {NotStarted, Ready, CommitResponding, Committed, Aborted}
      |
      External Cancel \<Rightarrow> False
      |
      _ \<Rightarrow> (  status as0 t = Ready
            \<and> in_flight cs0 as0 t))"

lemmas cga_tms2_simps =
  cga_simps TMS2.unfold_tms2 validity_prop_def RWMemory.all_simps txn_rel_def

lemma validity_prop_stable_stutter:
  "\<lbrakk>cga_pre cs at a;
    t \<noteq> at;
    write_count (loc cs t) : dom (stores as);
    validity_prop cs as t\<rbrakk>
    \<Longrightarrow>
    validity_prop (cga_eff cs at a) as t"
    by (cases rule: CGA.Event_split[of a]) (simp_all add: cga_simps validity_prop_def)

lemma validity_prop_stable_external:
  "\<lbrakk>cga_pre cs at (External ac);
    ac \<noteq> BeginInv \<or> t \<noteq> at;
    validity_prop cs as t\<rbrakk>
    \<Longrightarrow>
    validity_prop (cga_eff cs at (External ac)) (tms_eff as at (External ac)) t"
    by (cases ac) (simp_all add: cga_tms2_simps)


lemma write_count_in_domain:
  assumes "b \<notin> {External BeginInv, Internal Begin, External CommitResp, External Abort}"
  and "cga_pre cs t b"
  and "txn_rel b cs as t"
  and "global_rel cs as"
  and "CGA.txn_inv b cs t"
  shows "write_count (state.loc cs t) \<le> max_index as"
  using assms
  apply (cases rule: CGA.Event_split[of b])
  apply (simp_all add: cga_tms2_simps global_rel_def write_count_def)
  apply linarith+
  done

lemma index_is_valid:
  assumes "global_rel cs as"
  and "CGA.txn_inv (Internal pc) cs t"
  and "(pc = Commit \<and> even (loc cs t))
       \<or> (pc = Read \<and> glb cs = loc cs t)"
  and "cga_pre cs t (Internal pc)"
  and "validity_prop cs as t"
  shows "valid_index as t (write_count (loc cs t))"
  using assms
  apply (simp add: cga_tms2_simps)
  by (metis div_le_mono eq_imp_le global_rel_def is_ready_def max_index_def pc.simps(14) write_count_def)

lemma precondition_external:
  assumes "CGA.txn_inv (External ac) cs0 at"
  and "txn_rel (External ac) cs0 as0 at"
  and "cga_pre cs0 at (External ac)"
  shows "tms_pre as0 at (External ac)"
  using assms
  by (cases ac) (simp_all add: cga_tms2_simps)

lemma precondition_internal_step:
  assumes "CGA.txn_inv (Internal pc) cs0 at"
  and "global_rel cs0 as0"
  and "txn_rel (Internal pc) cs0 as0 at"
  and "cga_pre cs0 at (Internal pc)"
  and "step_correspondence cs0 at pc = Some ai"
  shows "tms_pre as0 at (Internal ai)"
  using assms
  apply -
  apply (erule rev_mp)+
  apply (cases pc)
  apply (erule ssubst) apply (intro impI) defer
  apply (erule ssubst) apply (intro impI) defer
  apply (erule ssubst) apply (intro impI) defer
  apply (erule ssubst) apply (intro impI) defer

  apply (simp add: step_correspondence_def)
  
  apply (simp add: step_correspondence_def)
  apply (cases "even (state.loc cs0 at)")
  apply simp
  apply (drule HOL.sym)
  apply simp
  apply (simp add: cga_tms2_simps)
  apply auto[1]
  using in_flight_def apply blast
  apply (simp add: in_flight_def validity_prop_def)
  apply (metis div_le_mono global_rel_def max_index_def write_count_def)
  apply (simp add: in_flight_def read_consistent_def store_at_def validity_prop_def)
  apply (simp add: cga_tms2_simps)
  apply auto[1]
  using in_flight_def apply auto[1]
  apply (simp add: global_rel_def in_flight_def max_index_def read_consistent_def store_at_def validity_prop_def)

  apply (simp add: step_correspondence_def)
  apply (cases "state.loc cs0 at = glb cs0")
  apply (simp add: cga_tms2_simps)
  apply (drule HOL.sym)
  apply simp
  apply (simp add: global_rel_def in_flight_def unfold_tms2(14) validity_prop_def)
  apply simp

  apply (simp add: step_correspondence_def)
  apply (cases "state.loc cs0 at = glb cs0")
  apply (simp add: cga_tms2_simps split: if_splits)
  apply (drule HOL.sym)
  apply simp+
  done

lemma global_rel_preserved_external:
  assumes "global_rel cs0 as0"
  and "txn_rel (External ac) cs0 as0 at"
  and "cga_pre cs0 at (External ac)"
  and "tms_pre as0 at (External ac)"
  shows "global_rel (cga_eff cs0 at (External ac)) (tms_eff as0 at (External ac))"
  using assms
  by (cases ac) (simp_all add: cga_tms2_simps global_rel_def writes_def split: option.split)

lemma txn_rel_preserved_external_self:
  assumes "CGA.txn_inv (External ac) cs0 at"
  and "TMS2.txn_inv (External ac) as0 at"
  and "global_rel cs0 as0"
  and "txn_rel (External ac) cs0 as0 at"
  and "cga_pre cs0 at (External ac)"
  shows "txn_rel b (cga_eff cs0 at (External ac)) (tms_eff as0 at (External ac)) at"
proof -
  {
    assume "ac = BeginResp"
    from this and assms have ?thesis
      by (cases rule: CGA.Event_split[of b]) (simp_all add: cga_tms2_simps TMS2.txn_inv_def in_flight_def)
  }
  moreover
  {
    assume "ac = WriteResp"
    from this assms
    and validity_prop_stable_external[where at = at and t = at and ac = ac and cs = cs0 and as = as0]
    have ?thesis
      by (cases rule: CGA.Event_split[of b]) (auto simp add: cga_tms2_simps TMS2.txn_inv_def in_flight_def global_rel_def)
  }
  moreover
  {
    assume ac: "ac \<noteq> BeginResp \<and> ac \<noteq> WriteResp"
    from this assms
    and validity_prop_stable_external[where at = at and t = at and ac = ac and cs = cs0 and as = as0]
    have ?thesis
      by (cases ac, cases rule: CGA.Event_split[of b]) (simp_all add: cga_tms2_simps global_rel_def in_flight_def split: TMS2.splits)
  }
  ultimately show ?thesis by blast
qed

lemma txn_rel_preserved_external_other:
  assumes "t \<noteq> at"
  and "CGA.txn_inv b cs0 t"
  and "txn_rel b cs0 as0 t"
  and "cga_pre cs0 at (External ac)"
  shows "txn_rel b (cga_eff cs0 at (External ac)) (tms_eff as0 at (External ac)) t"
  using assms validity_prop_stable_external[where at = at and t = t and ac = ac and cs = cs0 and as = as0]
  by (cases ac) (cases rule: CGA.Event_split[of b], simp_all add: cga_tms2_simps in_flight_def)+

lemma global_rel_preserved_internal_stutter:
  assumes "CGA.global_inv cs0"
  and "CGA.txn_inv a cs0 at"
  and "global_rel cs0 as0"
  and "txn_rel a cs0 as0 at"
  and a: "a = Internal pc"
  and "cga_pre cs0 at a"
  and sc: "step_correspondence cs0 at pc = None"
  shows "global_rel (cga_eff cs0 at a) as0"
  using assms
  apply (cases pc)
  apply (simp add: cga_tms2_simps global_rel_def writes_def)
  apply (auto simp add: cga_tms2_simps global_rel_def writes_def in_flight_def step_correspondence_def)[1]
  apply (simp_all add: cga_tms2_simps global_rel_def writes_def in_flight_def step_correspondence_def)
  apply metis
  by (simp add: cga_tms2_simps global_rel_def writes_def in_flight_def step_correspondence_def split: if_splits)

lemma remove_impl: "B \<Longrightarrow> A \<longrightarrow> B"
  by simp

lemma global_rel_preserved_internal_step:
  assumes "CGA.global_inv cs0"
  and "CGA.txn_inv (Internal pc) cs0 at"
  and "stores_domain as0"
  and "global_rel cs0 as0"
  and "txn_rel (Internal pc) cs0 as0 at"
  and "cga_pre cs0 at (Internal pc)"
  and "tms_pre as0 at (Internal ai)"
  and "step_correspondence cs0 at pc = Some ai"
  shows "global_rel (cga_eff cs0 at (Internal pc)) (tms_eff as0 at (Internal ai))"
proof -
  {
    assume pc_def: "pc = Read" and ai_def: "ai = DoRead (addr cs0 at) (write_count (loc cs0 at))"

    from `CGA.txn_inv (Internal pc) cs0 at` and `cga_pre cs0 at (Internal pc)` and pc_def
    have "is_ready cs0 at"
      by (simp add: CGA.txn_inv_def)

    from `txn_rel (Internal pc) cs0 as0 at` and `cga_pre cs0 at (Internal pc)` and pc_def
    have [simp]: "CGA.status cs0 at = Pending Read" and [simp]: "TMS2.status as0 at = Pending (ReadPending (addr cs0 at))" and "in_flight cs0 as0 at"
      by (auto simp add: txn_rel_def cga_pre_def)

    from `step_correspondence cs0 at pc = Some ai` 
    have loc_glb: "loc cs0 at = glb cs0"
      apply -
      apply (erule rev_mp)
      apply (insert pc_def ai_def)
      apply (erule ssubst)+
      by (simp add: step_correspondence_def)

    have ?thesis
      apply (insert pc_def ai_def)
      apply (erule ssubst)+
      apply (simp add: global_rel_def, intro conjI)
      prefer 2
      apply (simp add: loc_glb)
      apply (rule remove_impl)
      apply (simp add: all_utilities CGA.update_status_def update_val_def writes_def)
      apply (cases "writer cs0")
      apply simp_all
      apply (intro conjI)
      apply (metis Event.inject(2) InternalAction.simps(11) ai_def assms(4) assms(7) global_rel_def latest_store_def max_index_stable option.simps(4) store_at_stable writes_def)
      using ai_def assms(4) assms(7) global_rel_def max_index_stable apply auto[1]
      using \<open>in_flight cs0 as0 at\<close> assms(1) assms(4) global_inv_def global_rel_def in_flight_def loc_glb apply auto[1]
      apply (simp add: ai_def assms(7) ext_write_set_stable latest_store_def max_index_stable store_at_stable writes_def)
      using ai_def assms(7) ext_write_set_stable max_index_stable store_at_stable apply auto[1]
      using ai_def assms(7) max_index_stable by auto[1]
  }
  moreover
  {
    assume pc_def [simp]: "pc = Write" and ai_def [simp]: "ai = DoWrite (addr cs0 at) (val cs0 at)"

    from `step_correspondence cs0 at pc = Some ai` 
    have loc_glb [simp]: "loc cs0 at = glb cs0"
      by (auto simp add: step_correspondence_def split: if_splits)

    from `global_rel cs0 as0`
    have "store cs0 = apply_partial (latest_store as0) (writes cs0 as0)"
    and "glb cs0 div 2 = max_index as0"
    and "write_count (glb cs0) = max_index as0"
      by (auto simp add: global_rel_def write_count_def)

    have [simp]: "cga_pre cs0 at (Internal Write)"
      using assms(6) pc_def by blast

    from `txn_rel (Internal pc) cs0 as0 at`
    have "TMS2.status as0 at = Pending (WritePending (addr cs0 at) (state.val cs0 at))"
    and "in_flight cs0 as0 at"
      by (simp add: txn_rel_def)+

    from `stores_domain as0`
    have [simp]: "stores as0 (max_index as0) = Some (latest_store as0)"
      by (auto simp add: stores_domain_def max_index_def latest_store_def store_at_def)

    have ?thesis
    proof (simp only: global_rel_def cga_eff.simps pc_def ai_def tms_eff_def Event.case, intro conjI)
      show "store (CGA.int_eff cs0 at Write) =
            apply_partial
             (latest_store
               (TMS2.int_eff as0 at
                 (DoWrite (addr cs0 at)
                   (state.val cs0 at))))
             (writes (CGA.int_eff cs0 at Write)
               (TMS2.int_eff as0 at
                 (DoWrite (addr cs0 at)
                   (state.val cs0 at))))"
        apply (simp add: cga_tms2_simps writes_def apply_partial_simp)
        apply (simp add: `store cs0 = apply_partial (latest_store as0) (writes cs0 as0)`)
        apply (simp add: max_index_def[symmetric] writes_def)
        apply (cases "writer cs0")
        apply simp
        using \<open>in_flight cs0 as0 at\<close> assms(1) cga_tms2_simps(12) in_flight_def loc_glb apply auto[1]
        apply simp
        apply (simp add: latest_store_def)
        apply (rule ext)
        apply (simp add: apply_partial_def)
        using \<open>in_flight cs0 as0 at\<close> assms(1) assms(4) cga_tms2_simps(12)
        using in_flight_def loc_glb option.simps(3) option.simps(5) by auto
    next
      show "write_count (glb (CGA.int_eff cs0 at Write)) = max_index (TMS2.int_eff as0 at (DoWrite (addr cs0 at) (state.val cs0 at)))"
        by (simp add: cga_tms2_simps \<open>glb cs0 div 2 = max_index as0\<close> write_count_def)
    qed
  }
  moreover
  {
    assume pc_def: "pc = Commit" and "even (loc cs0 at)" and ai_def: "ai = DoCommitReadOnly (write_count (loc cs0 at))"
    from assms and this(2) have ?thesis
      apply (simp add: pc_def ai_def cga_tms2_simps step_correspondence_def global_rel_def writes_def)
      apply safe
      by simp+
  }
  moreover
  {
    assume pc_def [simp]: "pc = Commit" and ai_def [simp]: "ai = DoCommitWriter"

    from `CGA.txn_inv (Internal pc) cs0 at` and `cga_pre cs0 at (Internal pc)` and pc_def
    have "is_ready cs0 at"
      by (simp add: CGA.txn_inv_def)

    have [simp]: "cga_pre cs0 at (Internal Commit)"
      using assms(6) pc_def by blast

    from `global_rel cs0 as0`
    have "store cs0 = apply_partial (latest_store as0) (writes cs0 as0)"
    and "glb cs0 div 2 = max_index as0"
    and "write_count (glb cs0) = max_index as0"
      by (auto simp add: global_rel_def write_count_def)

    from `step_correspondence cs0 at pc = Some ai` 
    have loc_odd [simp]: "odd (loc cs0 at)"
      by (auto simp add: step_correspondence_def split: if_splits)

    from `txn_rel (Internal pc) cs0 as0 at`
    have "TMS2.status as0 at = Pending CommitPending"
    and "in_flight cs0 as0 at"
      by (simp add: txn_rel_def)+

    from `stores_domain as0`
    have [simp]: "stores as0 (max_index as0) = Some (latest_store as0)"
      by (auto simp add: stores_domain_def max_index_def latest_store_def store_at_def)

    have [simp]: "write_count (Suc (glb cs0)) = Suc (max_index as0)"
      apply (simp add: write_count_def)
      using \<open>glb cs0 div 2 = max_index as0\<close> \<open>in_flight cs0 as0 at\<close> in_flight_def loc_odd by auto

    have writer_unique: "\<And>t. writer cs0 = Some t \<Longrightarrow> t = at" 
      using \<open>in_flight cs0 as0 at\<close> in_flight_def by auto

    have ?thesis
    proof (simp only: global_rel_def cga_eff.simps pc_def ai_def tms_eff_def Event.case, intro conjI)
      show "write_count (glb (CGA.int_eff cs0 at Commit)) = max_index (TMS2.int_eff as0 at DoCommitWriter)"
        apply (simp add: cga_tms2_simps) using assms(3) stores_domain_def by auto
    next
      show "store (CGA.int_eff cs0 at Commit) =
            apply_partial
             (latest_store
               (TMS2.int_eff as0 at DoCommitWriter))
             (writes (CGA.int_eff cs0 at Commit)
               (TMS2.int_eff as0 at DoCommitWriter))"
        apply (simp add: cga_tms2_simps writes_def apply_partial_simp)
        apply (simp add: `store cs0 = apply_partial (latest_store as0) (writes cs0 as0)`)
        apply (simp add: max_index_def[symmetric] writes_def)
        apply (cases "writer cs0")
        apply simp
        using \<open>in_flight cs0 as0 at\<close> in_flight_def apply auto[1]
        apply simp
        apply auto
        apply (drule writer_unique)
        apply (rule ext)
        apply (simp add: apply_partial_def)
        apply (drule writer_unique)
        apply (rule ext)
        apply (simp add: apply_partial_def)
        by (meson Max_insert2 assms(3) le_SucI stores_domain_def stores_domain_max_is_max)
    qed
  }
  ultimately show ?thesis using assms
    by (cases rule: CGA.Event_split[where b = "Internal pc"]) (simp_all add: step_correspondence_def split: if_splits)
qed

lemma txn_rel_self_preserved_internal_step:
  assumes "CGA.global_inv cs0"
  and "CGA.txn_inv (Internal pc) cs0 at"
  and "global_rel cs0 as0"
  and "txn_rel (Internal pc) cs0 as0 at"
  and "cga_pre cs0 at (Internal pc)"
  and "step_correspondence cs0 at pc = Some ai"
  shows "txn_rel b (cga_eff cs0 at (Internal pc)) (tms_eff as0 at (Internal ai)) at"
proof -
  {
    assume [simp]: "pc = Read" and "loc cs0 at = glb cs0" and [simp]: "ai = DoRead (addr cs0 at) (write_count (loc cs0 at))"
    and "addr cs0 at \<in> dom (write_set as0 at)"
    from this and assms have ?thesis
      apply (simp add: cga_tms2_simps in_flight_def step_correspondence_def writes_def split: TMS2.splits option.split)
      apply safe
      apply (simp_all add: global_rel_def apply_partial_def writes_def when_fn_def)
      by (metis (mono_tags) option.case_eq_if option.distinct(1) option.sel)+
  }
  moreover
  {
    assume [simp]: "pc = Read" and "loc cs0 at = glb cs0" and [simp]: "ai = DoRead (addr cs0 at) (write_count (loc cs0 at))"
    and "(addr cs0 at) \<notin> dom (write_set as0 at)"
    from this and assms have ?thesis
      apply (simp add: cga_tms2_simps in_flight_def step_correspondence_def writes_def split: TMS2.splits option.split)
      apply safe
      apply (simp_all add: global_rel_def apply_partial_def writes_def when_fn_def store_at_def update_partial_def update_read_set_def latest_store_def)
      by (metis (mono_tags) option.case_eq_if option.distinct(1) option.sel)+
  }
  moreover
  {
    assume [simp]: "pc = Write" and [simp]: "ai = DoWrite (addr cs0 at) (val cs0 at)"
    from this and assms have ?thesis
      apply simp
      apply (simp add: cga_tms2_simps split: TMS2.splits option.split)
      apply (simp add: in_flight_def step_correspondence_def writes_def)
      apply safe
      apply (simp add: read_consistent_def validity_prop_def write_count_def global_rel_def apply_partial_def writes_def when_fn_def store_at_def update_partial_def update_read_set_def latest_store_def)+
      apply (metis (mono_tags) option.case_eq_if option.distinct(1) option.sel)
      apply (simp add: read_consistent_def validity_prop_def write_count_def global_rel_def apply_partial_def writes_def when_fn_def store_at_def update_partial_def update_read_set_def latest_store_def)+
      apply (metis (mono_tags) option.case_eq_if option.distinct(1) option.sel)
      apply (simp add: read_consistent_def validity_prop_def write_count_def global_rel_def apply_partial_def writes_def when_fn_def store_at_def update_partial_def update_read_set_def latest_store_def)
      apply (simp add: read_consistent_def validity_prop_def write_count_def global_rel_def apply_partial_def writes_def when_fn_def store_at_def update_partial_def update_read_set_def latest_store_def)
      apply (simp add: read_consistent_def validity_prop_def write_count_def global_rel_def apply_partial_def writes_def when_fn_def store_at_def update_partial_def update_read_set_def latest_store_def)
      apply (simp add: read_consistent_def validity_prop_def write_count_def global_rel_def apply_partial_def writes_def when_fn_def store_at_def update_partial_def update_read_set_def latest_store_def)
      apply (simp add: read_consistent_def validity_prop_def write_count_def global_rel_def apply_partial_def writes_def when_fn_def store_at_def update_partial_def update_read_set_def latest_store_def)
      apply (simp add: read_consistent_def validity_prop_def write_count_def global_rel_def apply_partial_def writes_def when_fn_def store_at_def update_partial_def update_read_set_def latest_store_def)
      apply (simp add: read_consistent_def validity_prop_def write_count_def global_rel_def apply_partial_def writes_def when_fn_def store_at_def update_partial_def update_read_set_def latest_store_def)
      apply (simp add: read_consistent_def validity_prop_def write_count_def global_rel_def apply_partial_def writes_def when_fn_def store_at_def update_partial_def update_read_set_def latest_store_def)
      apply (metis option.distinct(1))+
      apply (simp_all add: read_consistent_def validity_prop_def write_count_def global_rel_def apply_partial_def writes_def when_fn_def store_at_def update_partial_def update_read_set_def latest_store_def)
      apply (metis (mono_tags) option.simps(4) option.simps(5))
      apply (metis (mono_tags) option.simps(5))
      apply (metis (mono_tags) option.simps(4) option.simps(5))
      apply (metis (mono_tags) option.simps(5))
      done
  }
  moreover
  {
    assume [simp]: "pc = Commit" and "even (loc cs0 at)" and [simp]: "ai = DoCommitReadOnly (write_count (loc cs0 at))"
    from this and assms have ?thesis
      by (simp add: cga_tms2_simps in_flight_def step_correspondence_def writes_def split: TMS2.splits option.split)
  }
  moreover
  {
    assume [simp]: "pc = Commit" and "ai = DoCommitWriter"
    from this and assms have ?thesis
      by (simp add: cga_tms2_simps in_flight_def step_correspondence_def writes_def split: TMS2.splits option.split)
  }
  ultimately show ?thesis using assms
    apply (cases rule: CGA.Event_split[where b = "Internal pc"])
    apply (auto simp add: step_correspondence_def)
    by blast
qed 

lemma txn_rel_other_preserved_internal_step:
  assumes "CGA.global_inv cs0"
  and "CGA.txn_inv (Internal pc) cs0 at"
  and "global_rel cs0 as0"
  and "txn_rel (Internal pc) cs0 as0 at"
  and "cga_pre cs0 at (Internal pc)"
  and "t \<noteq> at"
  and "CGA.txn_inv b cs0 t"
  and "txn_rel b cs0 as0 t"
  and "step_correspondence cs0 at pc = Some ai"
  shows "txn_rel b (cga_eff cs0 at (Internal pc)) (tms_eff as0 at (Internal ai)) t"
proof -
  {
    assume [simp]: "pc = Read" and "loc cs0 at = glb cs0" and [simp]: "ai = DoRead (addr cs0 at) (write_count (loc cs0 at))"
    and "addr cs0 at \<in> dom (write_set as0 at)"
    from this and assms have ?thesis
      by (cases rule: CGA.Event_split[of b]) (simp_all add: cga_tms2_simps step_correspondence_def all_utilities in_flight_def)
  }
  moreover
  {
    assume [simp]: "pc = Read" and "loc cs0 at = glb cs0" and [simp]: "ai = DoRead (addr cs0 at) (write_count (loc cs0 at))"
    and "(addr cs0 at) \<notin> dom (write_set as0 at)"
    from this and assms have ?thesis
      by (cases rule: CGA.Event_split[of b]) (simp_all add: cga_tms2_simps step_correspondence_def all_utilities in_flight_def)
  }
  moreover
  {
    assume [simp]: "pc = Write" and [simp]: "ai = DoWrite (addr cs0 at) (val cs0 at)"
    from this and assms have ?thesis
      apply (cases rule: CGA.Event_split[of b])
      apply (simp_all add: cga_tms2_simps step_correspondence_def all_utilities in_flight_def split: TMS2.splits option.split)
      apply (simp_all add: read_consistent_def validity_prop_def global_rel_def apply_partial_def writes_def when_fn_def store_at_def update_partial_def update_read_set_def latest_store_def)
      apply safe
      apply auto
      by (metis (mono_tags) option.case_eq_if option.distinct(1) option.sel)+
  }
  moreover
  {
    assume [simp]: "pc = Commit" and "even (loc cs0 at)" and [simp]: "ai = DoCommitReadOnly (write_count (loc cs0 at))"
    from this and assms have ?thesis
      by (cases rule: CGA.Event_split[of b]) (simp_all add: cga_tms2_simps step_correspondence_def all_utilities in_flight_def)
  }
  moreover
  {
    assume [simp]: "pc = Commit" and "ai = DoCommitWriter"
    from this and assms have ?thesis
      apply (cases rule: CGA.Event_split[of b])
      apply (simp_all add: cga_tms2_simps step_correspondence_def all_utilities in_flight_def split: TMS2.splits option.split)
      apply (simp_all add: read_consistent_def validity_prop_def global_rel_def apply_partial_def writes_def when_fn_def store_at_def update_partial_def update_read_set_def latest_store_def)
      apply safe
      apply (simp_all add: max_index_def write_count_def)
      by (metis (mono_tags) option.simps(4) option.simps(5) write_count_def)+
  }
  ultimately show ?thesis using assms
    apply (cases rule: CGA.Event_split[where b = "Internal pc"])
    apply (auto simp add: step_correspondence_def)
    by blast
qed

lemma txn_rel_self_preserved_stutter:
  assumes "CGA.global_inv cs0"
  and "CGA.txn_inv (Internal pc) cs0 at"
  and "\<forall>c. TMS2.txn_inv c as0 at"
  and "global_rel cs0 as0"
  and "txn_rel (Internal pc) cs0 as0 at"
  and "cga_pre cs0 at (Internal pc)"
  and "step_correspondence cs0 at pc = None"
  shows "txn_rel b (cga_eff cs0 at (Internal pc)) as0 at"
proof -
  {
    assume [simp]: "pc = Begin"
    from assms spec[where P="\<lambda> c. TMS2.txn_inv c as0 at" and x="External BeginResp"]
    have "begin_index as0 at \<le> max_index as0"
      by(simp add: cga_tms2_simps TMS2.txn_inv_def)
    hence ?thesis using assms
      by (simp_all add: global_rel_def cga_tms2_simps all_utilities split: TMS2.splits)
  }
  moreover
  {
    assume [simp]: "pc = Read"
    have ?thesis using assms
      apply (simp add: global_rel_def cga_tms2_simps all_utilities split: TMS2.splits)
      apply safe
      apply (simp add: step_correspondence_def)
      apply (simp add: step_correspondence_def)
      apply (metis option.distinct(1))
      apply (subst (asm) step_correspondence_def)
      by (metis (mono_tags, lifting) option.simps(3) pc.simps(15))
  }
  moreover
  {
    assume [simp]: "pc = Write"
    have ?thesis using assms
      apply (simp_all add: global_rel_def cga_tms2_simps all_utilities split: TMS2.splits)
      apply safe
      apply (simp add: step_correspondence_def)
      apply (simp add: step_correspondence_def)
      apply (metis option.distinct(1))
      apply (simp add: step_correspondence_def)
      apply (subst (asm) step_correspondence_def)
      apply presburger
      by (simp add: step_correspondence_def)
  }
  moreover
  {
    assume [simp]: "pc = Commit"
    have ?thesis using assms
      apply (simp_all add: global_rel_def cga_tms2_simps all_utilities split: TMS2.splits)
      apply safe
      by (simp_all add: step_correspondence_def)
  }
  ultimately show ?thesis
    using pc.exhaust by blast
qed

lemma txn_rel_other_preserved_stutter:
  assumes "CGA.global_inv cs0"
  and "CGA.txn_inv (Internal pc) cs0 at"
  and "stores_domain as0"
  and "global_rel cs0 as0"
  and "txn_rel (Internal pc) cs0 as0 at"
  and "cga_pre cs0 at (Internal pc)"
  and "t \<noteq> at"
  and "txn_rel b cs0 as0 t"
  and "CGA.txn_inv b cs0 t"
  and "step_correspondence cs0 at pc = None"
  shows "txn_rel b (cga_eff cs0 at (Internal pc)) as0 t"
proof -
  {
    assume [simp]: "pc = Begin"
    have ?thesis using assms
      apply (simp_all add: global_rel_def cga_tms2_simps all_utilities split: TMS2.splits)
      apply safe
      apply (simp_all add: step_correspondence_def cga_tms2_simps)
      apply (simp_all add: in_flight_def validity_prop_def split: pc.split)
      apply safe
      apply (simp_all add: in_flight_def validity_prop_def)
      by (simp_all add: cga_simps)
  }
  moreover
  {
    assume [simp]: "pc = Read"
    have ?thesis using assms
      apply (simp_all add: global_rel_def cga_tms2_simps all_utilities split: TMS2.splits)
      apply safe
      apply (simp (no_asm_use) add: step_correspondence_def cga_tms2_simps, blast | linarith)+
      apply simp+
      apply (simp_all (no_asm_use) add: ext_enabled_def)
      apply blast+
      apply (simp_all (no_asm_use) add: in_flight_def validity_prop_def is_writer_def split: pc.split)
      apply blast+
      apply simp
      apply (simp_all (no_asm_use) add: step_correspondence_def split: option.split)
      apply simp+
      apply safe
      apply (simp_all (no_asm_use) add: cga_tms2_simps in_flight_def)
      apply blast+
      by (metis (mono_tags) option.case_eq_if option.distinct(1) option.sel)+
  }
  moreover
  {
    assume [simp]: "pc = Write"
    have ?thesis using assms
      apply (cases rule: CGA.Event_split[of b])
      apply (simp_all add: global_rel_def cga_tms2_simps all_utilities split: TMS2.splits)
      apply safe
      apply (simp_all (no_asm_use) add: step_correspondence_def split: option.split)
      apply (simp_all (no_asm_use) add: cga_tms2_simps in_flight_def)
      apply blast+
      apply simp
      apply simp
      apply blast+
      by (metis (mono_tags) option.case_eq_if option.distinct(1) option.sel)+
  }
  moreover
  {
    assume [simp]: "pc = Commit"
    have ?thesis using assms
      apply (cases rule: CGA.Event_split[of b])
      apply (simp_all add: global_rel_def cga_tms2_simps all_utilities split: TMS2.splits)
      apply safe      
      apply (simp_all (no_asm_use) add: step_correspondence_def split: option.split)
      apply (simp_all (no_asm_use) add: cga_tms2_simps in_flight_def)
      by (metis (mono_tags) option.case_eq_if option.distinct(1) option.sel)+
  }
  ultimately show ?thesis
    using pc.exhaust by blast
qed

definition sim_rel :: "CGA.state \<Rightarrow> TMS2.State \<Rightarrow> bool"
  where
  "sim_rel cs as \<equiv> global_rel cs as \<and> (\<forall> a t. txn_rel a cs as t)"

lemma sim_rel_start:
  "CGA.start cs \<Longrightarrow> \<exists> as. TMS2.start as \<and> sim_rel cs as"
  apply (rule exI[where x= TMS2.default_start], insert default_is_start, simp)
  apply (auto simp add: sim_rel_def global_rel_def cga_tms2_simps write_count_def writes_def
                        default_start_def mem_initial_def domD initial_stores_def CGA.start_def
              split: option.split Event.split Action.split InternalAction.split PC.split)
  apply (auto simp add: dom_def)
  apply (rule ext)
  by (simp add: apply_partial_def)

lemma sim_rel_external:
  "standard_sim_ext_step CGA TMS2 sim_rel"
  apply (unfold standard_sim_ext_step_def sim_rel_def)
  apply (insert invariant_elim[OF CGA.total_inv], simp)
  apply (insert invariant_elim[OF TMS2.total_inv], simp)
  apply (insert precondition_external global_rel_preserved_external txn_rel_preserved_external_other txn_rel_preserved_external_self)
  by (metis CGA_def DAut.select_convs(2) DAut.select_convs(3))

lemma sim_rel_stutter:
  "standard_sim_stutter CGA TMS2 step_correspondence sim_rel"
  apply(unfold standard_sim_stutter_def sim_rel_def)
  apply(insert invariant_elim[OF CGA.total_inv], simp)
  apply(insert invariant_elim[OF TMS2.total_inv], simp)
  apply(insert global_rel_preserved_internal_stutter txn_rel_self_preserved_stutter txn_rel_other_preserved_stutter)
  by (metis CGA_def DAut.select_convs(2) DAut.select_convs(3))

lemma sim_rel_internal_step:
  "standard_sim_int_step CGA TMS2 step_correspondence sim_rel"
  apply(unfold standard_sim_int_step_def sim_rel_def)
  apply(insert invariant_elim[OF CGA.total_inv], simp)
  apply(insert invariant_elim[OF TMS2.total_inv], simp)
  apply(insert precondition_internal_step global_rel_preserved_internal_step txn_rel_self_preserved_internal_step txn_rel_other_preserved_internal_step)
  by (metis CGA_def DAut.select_convs(2) DAut.select_convs(3))

lemma CGA_simulation:
  "standard_simulation CGA TMS2 step_correspondence sim_rel"
  apply (insert sim_rel_start sim_rel_external sim_rel_stutter sim_rel_internal_step)
  by (metis (full_types) CGA_def DAut.select_convs(1) TMS2_def standard_simulation_def)

lemma TML_correct:
  "traces (ioa CGA) \<subseteq> traces (ioa TMS2)"
  using standard_simulation_trace_inclusion CGA_simulation by metis

end