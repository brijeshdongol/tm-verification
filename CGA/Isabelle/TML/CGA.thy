theory CGA
imports Transitions Interface Utilities
begin

datatype pc =
  Begin
| Commit
| Read
| Write

type_synonym status = "pc Interface.Status"

record state =
  glb :: nat
  store :: "L \<Rightarrow> V"
  status :: "T \<Rightarrow> status"
  val :: "T \<Rightarrow> V"
  loc :: "T \<Rightarrow> nat"
  addr :: "T \<Rightarrow> L"
  (* Auxillary var *)
  writer :: "T option"

find_consts name:glb

definition update_status :: "T \<Rightarrow> status \<Rightarrow> state \<Rightarrow> state" where
  "update_status t st s \<equiv> s \<lparr> status := ((status s) (t := st))\<rparr>"

definition update_val :: "T \<Rightarrow> V \<Rightarrow> state \<Rightarrow> state" where
  "update_val t v s \<equiv> s \<lparr> val := ((val s) (t := v))\<rparr>"

definition update_loc :: "T \<Rightarrow> nat \<Rightarrow> state \<Rightarrow> state" where
  "update_loc t n s \<equiv> s \<lparr> loc := ((loc s) (t := n))\<rparr>"

definition update_addr :: "T \<Rightarrow> L \<Rightarrow> state \<Rightarrow> state" where
  "update_addr t l s \<equiv> s \<lparr> addr := ((addr s) (t := l))\<rparr>"

definition update_store :: "L \<Rightarrow> V \<Rightarrow> state \<Rightarrow> state" where
  "update_store l v s \<equiv> s \<lparr> store := ((store s) (l := v)) \<rparr>"

definition start :: "state \<Rightarrow> bool" where
  "start s \<equiv>
      (\<forall>t. status s t = NotStarted)
    \<and> store s : mem_initial
    \<and> glb s = 0
    \<and> writer s = None"

definition cga_pre :: "state \<Rightarrow> T \<Rightarrow> pc Event \<Rightarrow> bool" where
  "cga_pre s t e \<equiv>
      e \<noteq> External Cancel
    \<and> (e = External Abort \<longrightarrow> status s t = AbortPending)
    \<and> (case e of
           External a \<Rightarrow> ext_enabled (status s t) a
         | Internal a \<Rightarrow> status s t = Pending a)"

fun ext_eff :: "state \<Rightarrow> T \<Rightarrow> Action \<Rightarrow> state" where
  "ext_eff s0 t BeginInv = (s0 ;; update_status t (Pending Begin))"
| "ext_eff s0 t BeginResp = (s0 ;; update_status t Ready)"
| "ext_eff s0 t CommitInv = (s0 ;; update_status t (Pending Commit))"
| "ext_eff s0 t CommitResp = (s0 ;; update_status t Committed)"
| "ext_eff s0 t Abort = (s0 ;; update_status t Aborted)"
| "ext_eff s0 t (ReadInv l) = (s0 ;; update_addr t l ;; update_status t (Pending Read))"
| "ext_eff s0 t (ReadResp v) = (s0 ;; update_status t Ready)"
| "ext_eff s0 t (WriteInv l v) =
  (s0 ;; update_addr t l ;; update_val t v ;; update_status t (Pending Write))"
| "ext_eff s0 t WriteResp = (s0 ;; update_status t Ready)"

notation fcomp (infixl "\<circ>>" 60)

fun int_eff :: "state \<Rightarrow> T \<Rightarrow> pc \<Rightarrow> state" where
  "int_eff s0 t Begin = (s0 ;;
    if even (glb s0)
    then update_status t BeginResponding \<circ> update_loc t (glb s0)
    else update_status t (Pending Begin))"

| "int_eff s0 t Commit = (s0 ;;
    if odd (loc s0 t) then (glb_update (op + 1) \<circ>> writer_update Map.empty) else id ;;
    update_status t CommitResponding)"

| "int_eff s0 t Read = (s0 ;;
    (if (glb s0 = loc s0 t)
    then (update_val t (store s0 (addr s0 t)) \<circ>> update_status t (ReadResponding (store s0 (addr s0 t))))
    else update_status t AbortPending))"

| "int_eff s0 t Write = (s0 ;;
    if (glb s0 \<noteq> loc s0 t) then update_status t AbortPending
    else
      ((if even (loc s0 t) then glb_update (op + 1) \<circ> update_loc t (loc s0 t + 1) else id) \<circ>>
      writer_update (\<lambda>_. (Some t)) \<circ>>
      update_store (addr s0 t) (val s0 t) \<circ>>
      update_status t WriteResponding))"

fun cga_eff :: "state \<Rightarrow> T \<Rightarrow> pc Event \<Rightarrow> state"
  where
  "cga_eff s t (Internal a) = int_eff s t a"
| "cga_eff s t (External a) = ext_eff s t a"

definition CGA :: "(state, pc) DAut" where
  "CGA \<equiv> \<lparr> DAut.start = start, DAut.pre = cga_pre, DAut.eff = cga_eff \<rparr>"

definition global_inv :: "state \<Rightarrow> bool" where
  "global_inv s \<equiv> even (glb s) \<longleftrightarrow> writer s = None"

definition is_writer :: "state \<Rightarrow> T \<Rightarrow> bool" where
  "is_writer s t \<equiv> writer s = Some t \<and> loc s t = glb s"

definition is_ready :: "state \<Rightarrow> T \<Rightarrow> bool" where
  "is_ready s t \<equiv> 
      loc s t \<le> glb s
    \<and> (odd (loc s t) \<longrightarrow> is_writer s t)"

definition txn_inv :: "pc Event \<Rightarrow> state \<Rightarrow> T \<Rightarrow> bool" where
  "txn_inv e s t \<equiv>
   cga_pre s t e \<longrightarrow>
   (case e of
      External BeginInv \<Rightarrow> True
      |
      Internal Begin \<Rightarrow> True
      |
      External BeginResp \<Rightarrow> (  loc s t \<le> glb s
                             \<and> even (loc s t))
      |
      Internal Read \<Rightarrow> is_ready s t
      |
      External (ReadResp v) \<Rightarrow>  v = val s t
                              \<and> is_ready s t
      |
      Internal Write \<Rightarrow> is_ready s t
      |
      External WriteResp \<Rightarrow> odd(loc s t) \<and> is_writer s t
      |
      Internal Commit \<Rightarrow> is_ready s t
      |
      External CommitResp \<Rightarrow> True
      |
      External Abort \<Rightarrow> True
      |
      External Cancel \<Rightarrow> False
      |
      _ \<Rightarrow> is_ready s t)"

lemma Event_split:
  "\<lbrakk>b = External BeginInv \<Longrightarrow> P;
    b = Internal Begin \<Longrightarrow> P;
    b = External BeginResp \<Longrightarrow> P;
    \<And> l. b = External (ReadInv l) \<Longrightarrow> P;
    b = Internal Read \<Longrightarrow> P;
    \<And> v. b = External (ReadResp v) \<Longrightarrow> P;
    \<And> l v. b = External (WriteInv l v) \<Longrightarrow> P;
    b = Internal Write \<Longrightarrow> P;
    b = External WriteResp \<Longrightarrow> P;
    b = External CommitInv \<Longrightarrow> P;
    b = Internal Commit \<Longrightarrow> P;
    b = External CommitResp \<Longrightarrow> P;
    b = External Cancel \<Longrightarrow> P;
    b = External Abort \<Longrightarrow> P \<rbrakk>
   \<Longrightarrow>
    P"
  apply (cases rule: Event.exhaust[where y = b])
  using Action.exhaust_sel apply blast
  using pc.exhaust apply blast
  done

lemmas unfold_updates =
  update_status_def
  update_loc_def
  update_addr_def
  update_val_def
  update_store_def
  rev_app_def

lemmas cga_simps =
  txn_inv_def
  cga_pre_def
  ext_enabled_def
  unfold_updates
  is_ready_def
  is_writer_def
  global_inv_def

(* Begin *)

lemma txn_inv_pres_begin_inv_self:
  "\<lbrakk>global_inv s;
    txn_inv a s at;
    a = (External BeginInv);
    cga_pre s at a\<rbrakk>
   \<Longrightarrow>
   txn_inv b (cga_eff s at a) at"
   apply (cases rule: Event_split[of b])
   by (auto simp add: cga_simps)

lemma txn_inv_pres_begin_inv_other:
  "\<lbrakk>global_inv s;
    txn_inv b s t;
    txn_inv a s at;
    t \<noteq> at;
    a = (External BeginInv);
    cga_pre s at a\<rbrakk>
   \<Longrightarrow>
   txn_inv b (cga_eff s at a) t"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_begin_self:
  "\<lbrakk>global_inv s;
    txn_inv a s at;
    a = (Internal Begin);
    cga_pre s at a\<rbrakk>
   \<Longrightarrow>
   txn_inv b (cga_eff s at a) at"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_begin_other:
  "\<lbrakk>global_inv s;
    txn_inv b s t;
    txn_inv a s at;
    t \<noteq> at;
    a = (Internal Begin);
    cga_pre s at a\<rbrakk>
   \<Longrightarrow>
   txn_inv b (cga_eff s at a) t"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps)

lemma txn_inv_pres_begin_resp_self:
  "\<lbrakk>global_inv s;
    txn_inv a s at;
    a = (External BeginResp);
    cga_pre s at a\<rbrakk>
   \<Longrightarrow>
   txn_inv b (cga_eff s at a) at"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps)

lemma txn_inv_pres_begin_resp_other:
  "\<lbrakk>global_inv s;
    txn_inv b s t;
    txn_inv a s at;
    t \<noteq> at;
    a = (External BeginResp);
    cga_pre s at a\<rbrakk>
   \<Longrightarrow>
   txn_inv b (cga_eff s at a) t"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps)

(* Read *)

lemma txn_inv_pres_read_inv_self:
  "\<lbrakk>global_inv s;
    txn_inv a s at;
    a = (External (ReadInv l));
    cga_pre s at a\<rbrakk>
   \<Longrightarrow>
   txn_inv b (cga_eff s at a) at"
   apply (cases rule: Event_split[of b])
   by (auto simp add: cga_simps)

lemma txn_inv_pres_read_inv_other:
  "\<lbrakk>global_inv s;
    txn_inv b s t;
    txn_inv a s at;
    t \<noteq> at;
    a = External (ReadInv l);
    cga_pre s at a\<rbrakk>
   \<Longrightarrow>
   txn_inv b (cga_eff s at a) t"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps)

lemma txn_inv_pres_read_self:
  "\<lbrakk>global_inv s;
    txn_inv a s at;
    a = (Internal Read);
    cga_pre s at a\<rbrakk>
   \<Longrightarrow>
   txn_inv b (cga_eff s at a) at"
   apply (cases rule: Event_split[of b])
   by (simp_all add: cga_simps)

lemma txn_inv_pres_read_other:
  "\<lbrakk>global_inv s;
    txn_inv b s t;
    txn_inv a s at;
    t \<noteq> at;
    a = (Internal Read);
    cga_pre s at a\<rbrakk>
   \<Longrightarrow>
   txn_inv b (cga_eff s at a) t"
   apply (cases rule: Event_split[of b])
   apply (simp_all add: cga_simps)
   apply linarith+
   by force+

lemma txn_inv_pres_read_resp_self:
  "\<lbrakk>global_inv s;
    txn_inv a s at;
    a = (External (ReadResp v));
    cga_pre s at a\<rbrakk>
   \<Longrightarrow>
   txn_inv b (cga_eff s at a) at"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_read_resp_other:
  "\<lbrakk>global_inv s;
    txn_inv b s t;
    txn_inv a s at;
    t \<noteq> at;
    a = External (ReadResp l);
    cga_pre s at a\<rbrakk>
   \<Longrightarrow>
   txn_inv b (cga_eff s at a) t"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

(* Write *)

lemma txn_inv_pres_write_inv_self:
  "\<lbrakk>global_inv s;
    txn_inv a s at;
    a = (External (WriteInv l v));
    cga_pre s at a\<rbrakk>
   \<Longrightarrow>
   txn_inv b (cga_eff s at a) at"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_write_inv_other:
  "\<lbrakk>global_inv s;
    txn_inv b s t;
    txn_inv a s at;
    t \<noteq> at;
    a = External (WriteInv l v);
    cga_pre s at a\<rbrakk>
   \<Longrightarrow>
   txn_inv b (cga_eff s at a) t"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_write_self:
  "\<lbrakk>global_inv s;
    txn_inv a s at;
    a = (Internal Write);
    cga_pre s at a\<rbrakk>
   \<Longrightarrow>
   txn_inv b (cga_eff s at a) at"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_write_other:
  "\<lbrakk>global_inv s;
    txn_inv b s t;
    txn_inv a s at;
    t \<noteq> at;
    a = (Internal Write);
    cga_pre s at a\<rbrakk>
   \<Longrightarrow>
   txn_inv b (cga_eff s at a) t"
   apply (cases rule: Event_split[of b])
   apply (simp add: cga_simps)+
   apply linarith
   apply (simp add: cga_simps)
   apply (metis le_Suc_eq option.inject)
   apply (simp add: cga_simps)
   apply (metis le_Suc_eq option.inject)
   apply (simp add: cga_simps)
   apply (intro conjI impI)
   apply (erule impE)
   apply assumption
   apply simp
   apply (simp add: cga_simps)+
   apply (metis le_Suc_eq option.inject)
   apply (simp add: cga_simps)
   apply (metis le_Suc_eq option.inject)
   apply (simp add: cga_simps)
   apply (metis option.inject)
   apply (simp add: cga_simps)
   apply (metis le_Suc_eq option.inject)
   apply (simp add: cga_simps)
   apply (metis le_Suc_eq option.inject)
   by (simp add: cga_simps)+

lemma txn_inv_pres_write_resp_self:
  "\<lbrakk>global_inv s;
    txn_inv a s at;
    a = (External WriteResp);
    cga_pre s at a\<rbrakk>
   \<Longrightarrow>
   txn_inv b (cga_eff s at a) at"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_write_resp_other:
  "\<lbrakk>global_inv s;
    txn_inv b s t;
    txn_inv a s at;
    t \<noteq> at;
    a = External WriteResp;
    cga_pre s at a\<rbrakk>
   \<Longrightarrow>
   txn_inv b (cga_eff s at a) t"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

(* Commit *)

lemma txn_inv_pres_commit_inv_self:
  "\<lbrakk>global_inv s;
    txn_inv a s at;
    a = (External CommitInv);
    cga_pre s at a\<rbrakk>
   \<Longrightarrow>
   txn_inv b (cga_eff s at a) at"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_commit_inv_other:
  "\<lbrakk>global_inv s;
    txn_inv b s t;
    txn_inv a s at;
    t \<noteq> at;
    a = (External CommitInv);
    cga_pre s at a\<rbrakk>
   \<Longrightarrow>
   txn_inv b (cga_eff s at a) t"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_commit_self:
  "\<lbrakk>global_inv s;
    txn_inv a s at;
    a = (Internal Commit);
    cga_pre s at a\<rbrakk>
   \<Longrightarrow>
   txn_inv b (cga_eff s at a) at"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_commit_other:
  "\<lbrakk>global_inv s;
    txn_inv b s t;
    txn_inv a s at;
    t \<noteq> at;
    a = (Internal Commit);
    cga_pre s at a\<rbrakk>
   \<Longrightarrow>
   txn_inv b (cga_eff s at a) t"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps)

lemma txn_inv_pres_commit_resp_self:
  "\<lbrakk>global_inv s;
    txn_inv a s at;
    a = (External CommitResp);
    cga_pre s at a\<rbrakk>
   \<Longrightarrow>
   txn_inv b (cga_eff s at a) at"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_commit_resp_other:
  "\<lbrakk>global_inv s;
    txn_inv b s t;
    txn_inv a s at;
    t \<noteq> at;
    a = (External CommitResp);
    cga_pre s at a\<rbrakk>
   \<Longrightarrow>
   txn_inv b (cga_eff s at a) t"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_cancel_self:
  "\<lbrakk>global_inv s;
    txn_inv a s at;
    a = (External Cancel);
    cga_pre s at a\<rbrakk>
   \<Longrightarrow>
   txn_inv b (cga_eff s at a) at"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_cancel_other:
  "\<lbrakk>global_inv s;
    txn_inv b s t;
    txn_inv a s at;
    t \<noteq> at;
    a = (External Cancel);
    cga_pre s at a\<rbrakk>
   \<Longrightarrow>
   txn_inv b (cga_eff s at a) t"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_abort_self:
  "\<lbrakk>global_inv s;
    txn_inv a s at;
    a = (External Abort);
    cga_pre s at a\<rbrakk>
   \<Longrightarrow>
   txn_inv b (cga_eff s at a) at"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_abort_other:
  "\<lbrakk>global_inv s;
    txn_inv b s t;
    txn_inv a s at;
    t \<noteq> at;
    a = (External Abort);
    cga_pre s at a\<rbrakk>
   \<Longrightarrow>
   txn_inv b (cga_eff s at a) t"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma allE2: "\<forall>x. P x \<Longrightarrow> (P x \<Longrightarrow> P y \<Longrightarrow> Q) \<Longrightarrow> Q"
  by auto

lemma total_inv: "invariant (ioa CGA) (\<lambda>s. global_inv s \<and> (\<forall>e t. txn_inv e s t))"
proof (intro invariant_intro conjI allI)
  fix s
  assume "DAut.start CGA s"
  thus "global_inv s"
    by (simp add: CGA_def start_def global_inv_def)
next
  fix s e t
  assume "DAut.start CGA s"
  thus "txn_inv e s t"
    by (cases rule: Event_split[of e]) (simp add: txn_inv_def cga_pre_def CGA_def start_def ext_enabled_def)+
next
  fix s
  assume "reach CGA s"
  show "preserved  CGA (\<lambda>s. global_inv s \<and> (\<forall>e t. txn_inv e s t)) s"
    apply (simp only: preserved_def)
    apply (intro allI impI)
    apply (erule conjE)+
    apply (intro conjI allI)
  proof -
    fix t a
    assume "pre CGA s t a" and "global_inv s" and "\<forall>e. All (txn_inv e s)"
    thus "global_inv (eff CGA s t a)"
      apply (cases rule: Event_split[of a])
      apply (simp add: global_inv_def CGA_def cga_pre_def unfold_updates)
      apply (simp add: global_inv_def CGA_def cga_pre_def unfold_updates)
      apply (simp add: global_inv_def CGA_def cga_pre_def unfold_updates)
      apply (simp add: global_inv_def CGA_def cga_pre_def unfold_updates)
      apply (simp add: global_inv_def CGA_def cga_pre_def unfold_updates)
      apply (erule_tac x = "Internal Read" in allE)
      apply (erule_tac x = t in allE)
      apply (simp add: txn_inv_def)
      apply (erule impE)
      apply (simp add: cga_pre_def)
      apply (simp add: is_ready_def)
      apply auto[1]
      apply (simp add: global_inv_def CGA_def cga_pre_def unfold_updates)
      apply (simp add: global_inv_def CGA_def cga_pre_def unfold_updates)
      apply (simp add: global_inv_def CGA_def cga_pre_def unfold_updates)
      apply (simp add: global_inv_def CGA_def cga_pre_def unfold_updates)
      apply (simp add: global_inv_def CGA_def cga_pre_def unfold_updates)
      apply (simp add: global_inv_def CGA_def cga_pre_def unfold_updates)
      apply (erule_tac x = "Internal Commit" in allE)
      apply (erule_tac x = t in allE)
      apply (simp add: txn_inv_def)
      apply (erule impE)
      apply (simp add: cga_pre_def)
      apply (simp add: is_ready_def is_writer_def)
      apply (simp add: global_inv_def CGA_def cga_pre_def unfold_updates)
      apply (simp add: global_inv_def CGA_def cga_pre_def unfold_updates)
      apply (simp add: global_inv_def CGA_def cga_pre_def unfold_updates)
      done
  next
    fix t a e t'
    assume "pre CGA s t a" and "global_inv s" and "\<forall>e. All (txn_inv e s)"
    thus "txn_inv e (eff CGA s t a) t'"
      apply (cases "t' = t")
      apply (erule ssubst)
      apply (erule_tac x = e and y = a in allE2)
      apply (erule_tac x = t in allE)
      apply (erule_tac x = t in allE)
      apply (cases rule: Event_split[of a])
      apply (simp_all add: CGA_def del: cga_eff.simps)
      apply (rule txn_inv_pres_begin_inv_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_begin_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_begin_resp_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_inv_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_resp_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_inv_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_resp_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_inv_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_resp_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_cancel_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_abort_self, simp, simp, simp, simp)
      apply (erule_tac x = e and y = a in allE2)
      apply (erule_tac x = t' in allE)
      apply (erule_tac x = t in allE)
      apply (cases rule: Event_split[of a])
      apply (rule txn_inv_pres_begin_inv_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_begin_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_begin_resp_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_inv_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_resp_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_inv_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_resp_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_inv_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_resp_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_cancel_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_abort_other, simp, simp, simp, simp, simp, simp)
      done
  qed
qed

end