theory CGACorrect
imports CGA TMS2Var "~~/src/HOL/Eisbach/Eisbach_Tools"
begin

definition step_correspondence :: "CGA.state \<Rightarrow> T \<Rightarrow> CGA.pc \<Rightarrow> TMS2Var.InternalAction option"
  where
  "step_correspondence cs t a \<equiv>
    case a of
        Read \<Rightarrow> (case CGA.write_set cs t (addr cs t) of
          Some v \<Rightarrow> Some (DoRead (addr cs t) (commits cs))
        | None \<Rightarrow> if validate t cs
                  then Some (DoRead (addr cs t) (commits cs))
                  else None)
      | Write \<Rightarrow> Some (DoWrite (addr cs t) (val cs t))
      | Commit \<Rightarrow> if (\<forall>l. CGA.write_set cs t l = None) then Some DoCommitReadOnly
                  else (if validate t cs then Some DoCommitWriter else None)
      | _ \<Rightarrow> None"

lemma scf1: "step_correspondence cs t a = Some DoCommitReadOnly \<longleftrightarrow>
  ((\<forall>l. CGA.write_set cs t l = None) \<and> a = Commit)"
  by (cases a) (auto simp add: step_correspondence_def split: option.split)

lemma scf2: "step_correspondence cs t a = Some DoCommitWriter \<longleftrightarrow>
  ((\<exists>l v. CGA.write_set cs t l = Some v) \<and> validate t cs \<and> a = Commit)"
  by (cases a) (auto simp add: step_correspondence_def split: option.split)

lemma scf3: "step_correspondence cs t a = Some (DoWrite x v) \<longleftrightarrow>
  (x = addr cs t \<and> v = val cs t \<and> a = Write)"
  by (cases a) (auto simp add: step_correspondence_def split: option.split)

lemma scf4: "step_correspondence cs t a = Some (DoRead x n) \<longleftrightarrow>
  ( (n = commits cs \<and> x = addr cs t \<and> a = Read)
  \<and> ((\<exists>v. CGA.write_set cs t x = Some v) \<or> (CGA.write_set cs t (addr cs t) = None \<and> validate t cs)))"
  by (cases a) (auto simp add: step_correspondence_def split: option.split)

lemma scf1': "step_correspondence cs t Commit = Some DoCommitReadOnly \<longleftrightarrow>
  ((\<forall>l. CGA.write_set cs t l = None))"
  by (auto simp add: step_correspondence_def split: option.split)

lemma scf2': "step_correspondence cs t Commit = Some DoCommitWriter \<longleftrightarrow>
  ((\<exists>l v. CGA.write_set cs t l = Some v) \<and> validate t cs)"
  by (auto simp add: step_correspondence_def split: option.split)

lemma scf3': "step_correspondence cs t Write = Some (DoWrite x v) \<longleftrightarrow>
  (x = addr cs t \<and> v = val cs t)"
  by (auto simp add: step_correspondence_def split: option.split)

lemma scf4': "step_correspondence cs t Read = Some (DoRead x n) \<longleftrightarrow>
  ( (n = commits cs \<and> x = addr cs t)
  \<and> ((\<exists>v. CGA.write_set cs t x = Some v) \<or> (CGA.write_set cs t (addr cs t) = None \<and> validate t cs)))"
  by (auto simp add: step_correspondence_def split: option.split)

lemmas sc_simps = scf1 scf2 scf3 scf4

lemmas sc_simps' = scf1' scf2' scf3' scf4'

lemma step_correspondence_None_cases:
  assumes "step_correspondence cs t a = None"
  shows "(a = Read \<and> \<not> validate t cs \<and> CGA.write_set cs t (addr cs t) = None)
       \<or> (a = Commit \<and> \<not> validate t cs \<and> (\<exists>l v. CGA.write_set cs t l = Some v))
       \<or> (a = Begin)"
  using assms
  apply (cases a)
  apply (auto simp add: step_correspondence_def split: option.splits)
  apply (metis option.distinct(1))
  by (meson not_None_eq)

lemma step_correspondence_None:
  assumes "step_correspondence cs t a = None"
  and "a = Read \<Longrightarrow> \<not> validate t cs \<Longrightarrow> CGA.write_set cs t (addr cs t) = None \<Longrightarrow> P"
  and "\<And>l v. a = Commit \<Longrightarrow> \<not> validate t cs \<Longrightarrow> CGA.write_set cs t l = Some v \<Longrightarrow> P"
  and "a = Begin \<Longrightarrow> P"
  shows "P"
  by (meson assms(1) assms(2) assms(3) assms(4) step_correspondence_None_cases)

lemma step_correspondence_None_var:
  assumes "step_correspondence cs t a = None"
  and "\<not> validate t cs \<Longrightarrow> CGA.write_set cs t (addr cs t) = None \<Longrightarrow> P Read"
  and "\<And>l v. \<not> validate t cs \<Longrightarrow> CGA.write_set cs t l = Some v \<Longrightarrow> P Commit"
  and "P Begin"
  shows "P a"
  by (metis assms(1) assms(2) assms(3) assms(4) step_correspondence_None)

definition global_rel :: "CGA.state \<Rightarrow> State \<Rightarrow> bool" where
  "global_rel cs as \<equiv> (commits cs = max_index as) \<and> latest_store as = store cs"

definition in_flight :: "T \<Rightarrow> CGA.state \<Rightarrow> Interface.State \<Rightarrow> bool" where
  "in_flight t cs as \<equiv>
     (state.write_set cs t = State.write_set as t) \<and>
     (State.read_set as t = state.read_set cs t)"

definition txn_rel :: "T \<Rightarrow> CGA.pc Event \<Rightarrow> CGA.state \<Rightarrow> State \<Rightarrow> bool"
  where
  "txn_rel t e cs0 as0 \<equiv> in_flight t cs0 as0 \<and>
     (cga_pre t e cs0 \<longrightarrow>
       (case e of
          External BeginInv \<Rightarrow> State.status as0 t = NotStarted
        | Internal Begin \<Rightarrow> State.status as0 t = BeginResponding
        | External BeginResp \<Rightarrow> State.status as0 t = BeginResponding
        | Internal Read \<Rightarrow> State.status as0 t = Pending (ReadPending (addr cs0 t))
        | External (ReadResp v) \<Rightarrow> State.status as0 t = ReadResponding (val cs0 t)
        | Internal Write \<Rightarrow> State.status as0 t = Pending (WritePending (addr cs0 t) (val cs0 t))
        | External WriteResp \<Rightarrow> State.status as0 t = WriteResponding
        | Internal Commit \<Rightarrow> State.status as0 t = Pending CommitPending
        | External CommitResp \<Rightarrow> State.status as0 t = CommitResponding
        | External Abort \<Rightarrow> State.status as0 t \<notin> {NotStarted, Ready, CommitResponding, Committed, Aborted}
        | External Cancel \<Rightarrow> False
        | _ \<Rightarrow> State.status as0 t = Ready))"

definition sim_rel :: "CGA.state \<Rightarrow> State \<Rightarrow> bool"
  where
  "sim_rel cs as \<equiv> global_rel cs as \<and> (\<forall>t a. txn_rel t a cs as)"

named_theorems simps

method double_case_simp for a and b :: "pc Event" declares simps =
  (cases a; (rule CGA.Event_split[where b = b]; simp add: simps))

lemma tms2_reachable_invariant: "reach TMS2Var s \<Longrightarrow> txn_inv t e s"
  using invariant_elim total_inv by fastforce

lemma ws_empty: "(\<forall>l. state.write_set cs t l = None) \<longleftrightarrow> state.write_set cs t = Map.empty"
  by auto

lemmas cga_tms2_simps =
  cga_simps TMS2Var.unfold_tms2 RWMemory.all_simps txn_rel_def

lemma txn_rel_self_preserved_stutter_Begin:
  assumes "CGA.global_inv cs0"
  and "CGA.txn_inv at (Internal pc) cs0"
  and "\<forall>c. TMS2Var.txn_inv at c as0"
  and "global_rel cs0 as0"
  and "txn_rel at (Internal pc) cs0 as0"
  and "cga_pre at (Internal pc) cs0"
  and "step_correspondence cs0 at pc = None"
  and [simp]: "pc = Begin"
  shows "txn_rel at b (cga_eff at (Internal pc) cs0) as0"
proof -
  from assms spec[where P="\<lambda> c. TMS2Var.txn_inv at c as0" and x="External BeginResp"]
  have "begin_index as0 at \<le> max_index as0"
    by (simp add: cga_tms2_simps TMS2Var.txn_inv_def)
  thus "txn_rel at b (cga_eff at (Internal pc) cs0) as0" using assms
    by (simp_all add: global_rel_def cga_tms2_simps all_utilities in_flight_def split: TMS2Var.splits)
qed

lemma txn_rel_self_preserved_stutter_Commit:
  assumes "CGA.global_inv cs0"
  and "CGA.txn_inv at (Internal pc) cs0"
  and "\<forall>c. TMS2Var.txn_inv at c as0"
  and "global_rel cs0 as0"
  and "txn_rel at (Internal pc) cs0 as0"
  and "cga_pre at (Internal pc) cs0"
  and "step_correspondence cs0 at pc = None"
  and [simp]: "pc = Commit"
  shows "txn_rel at b (cga_eff at (Internal pc) cs0) as0"
proof -
  show ?thesis using assms
    apply (simp_all add: global_rel_def cga_tms2_simps all_utilities split: TMS2Var.splits)
    apply safe
    apply (simp_all add: step_correspondence_def in_flight_def)
    by (metis option.distinct(1))
qed

lemma txn_rel_self_preserved_stutter_Read:
  assumes "CGA.global_inv cs0"
  and "CGA.txn_inv at (Internal pc) cs0"
  and "\<forall>c. TMS2Var.txn_inv at c as0"
  and "global_rel cs0 as0"
  and "txn_rel at (Internal pc) cs0 as0"
  and "cga_pre at (Internal pc) cs0"
  and "step_correspondence cs0 at pc = None"
  and [simp]: "state.write_set cs0 at (addr cs0 at) = None"
  and [simp]: "pc = Read"
  shows "txn_rel at b (cga_eff at (Internal pc) cs0) as0"
proof -
  show ?thesis using assms
    apply (simp_all add: global_rel_def cga_tms2_simps all_utilities split: TMS2Var.splits)
    apply safe
    by (simp_all add: step_correspondence_def in_flight_def)
qed

lemma CGA_simulation:
  "standard_simulation CGA TMS2Var step_correspondence sim_rel"
proof (unfold standard_simulation_def, intro conjI allI impI exI)
  fix cs
  assume "daut.start CGA cs"
  thus "sim_rel cs default_start"
  proof (auto simp add: sim_rel_def CGA_def CGA.start_def cga_pre_def)
    fix t e
    assume "\<forall>t. state.status cs t = NotStarted"
    and "store cs \<in> mem_initial"
    and "commits cs = 0"
    and "\<forall>t l. state.read_set cs t l = None"
    and "\<forall>t l. state.write_set cs t l = None"
    note assms = this
    thus "txn_rel t e cs default_start"
      apply -
      apply (rule CGA.Event_split[where b = e])
      by (auto simp add: txn_rel_def cga_pre_def ext_enabled_def default_start_def ws_empty store_at_def max_index_def dom_def in_flight_def)
    from assms show "global_rel cs default_start"
      by (simp add: global_rel_def default_start_def max_index_def latest_store_def store_at_def dom_def mem_initial_def)
   qed
next
  fix cs
  assume "daut.start CGA cs"
  thus "daut.start TMS2Var default_start"
    by (simp add: CGA_def CGA.start_def Interface.start_def default_start_def initial_stores_def mem_initial_def)
next
  show "standard_sim_ext_step CGA TMS2Var sim_rel"
    unfolding standard_sim_ext_step_def
  proof (intro allI impI, (erule conjE)+, rule conjI)
    fix cs as a t
    assume "sim_rel cs as" and "reach CGA cs"
    and "pre CGA t (External a) cs"
    thus "pre TMS2Var t (External a) as"
      apply (simp add: sim_rel_def)
      apply (erule conjE)
      apply (erule_tac x = t in allE)
      apply (erule_tac x = "External a" in allE)
      apply -
      apply (drule CGA.reachable_invariant[where t = t and e = "External a"])
      apply (cases a)
      apply (simp_all add: CGA_def cga_pre_def tms_pre_def ext_enabled_def sim_rel_def status_enabled_def txn_rel_def CGA.txn_inv_def)
      done
  next
    fix cs as a t
    assume "sim_rel cs as" and "reach CGA cs"
    and "pre CGA t (External a) cs"
    thus "sim_rel (eff CGA t (External a) cs) (eff TMS2Var t (External a) as)"
    proof (simp add: sim_rel_def, intro conjI allI)
      fix t' b
      assume "global_rel cs as \<and> (\<forall>t a. txn_rel t a cs as)"
      hence "txn_rel t' b cs as" and "txn_rel t (External a) cs as"
        by auto
      from this and `pre CGA t (External a) cs`
      show "txn_rel t' b (eff CGA t (External a) cs) (tms_eff t (External a) as)"
        apply -
        apply (cases a; rule CGA.Event_split[where b = b])
        apply (simp_all add: txn_rel_def cga_pre_def ext_enabled_def CGA_def tms_eff_def TMS2Var.ext_eff_def unfold_updates CGA.update_status_def in_flight_def update_addr_def update_val_def)
        done
    next
      assume "global_rel cs as \<and> (\<forall>t a. txn_rel t a cs as)"
      and "pre CGA t (External a) cs"
      thus "global_rel (eff CGA t (External a) cs) (tms_eff t (External a) as)"
      apply -
      apply (erule conjE)
      apply (cases a)
      by (simp_all add: global_rel_def max_index_def latest_store_def store_at_def cga_pre_def ext_enabled_def CGA_def tms_eff_def TMS2Var.ext_eff_def unfold_updates CGA.update_status_def in_flight_def update_addr_def update_val_def)
    qed
  qed
next
  show "standard_sim_stutter CGA TMS2Var step_correspondence sim_rel"
    unfolding standard_sim_stutter_def
  proof (intro impI allI, (erule conjE)+)
    fix cs as a t
    assume "reach CGA cs"
    and "reach TMS2Var as"
    and "sim_rel cs as"
    and "pre CGA t (Internal a) cs"
    and sc_None: "step_correspondence cs t a = None"

    note assms = this

    from `step_correspondence cs t a = None`
    show "sim_rel (eff CGA t (Internal a) cs) as"

    proof (rule step_correspondence_None)
      assume [simp]: "a = Read"
      and "\<not> validate t cs"
      and ws_None: "state.write_set cs t (addr cs t) = None"
      have "global_rel cs as" and "\<forall>t' e. txn_rel t' e cs as"
        using \<open>sim_rel cs as\<close> sim_rel_def apply auto[1]      
        using \<open>sim_rel cs as\<close> sim_rel_def by auto

      show "sim_rel (eff CGA t (Internal a) cs) as"
      proof (auto simp add: sim_rel_def)
        from `global_rel cs as` and `\<not> validate t cs` and ws_None
        show "global_rel (eff CGA t (Internal Read) cs) as"
          by (auto simp add: global_rel_def CGA_def CGA.unfold_updates)
      next
        fix t' e

        have "txn_rel t' e cs as"
          by (simp add: \<open>\<forall>t' e. txn_rel t' e cs as\<close>)

        from this `pre CGA t (Internal a) cs` `\<not> validate t cs` and ws_None
        show "txn_rel t' e (eff CGA t (Internal Read) cs) as"
          apply -
          apply (cases "t = t'")
          apply (metis CGA.reachable_invariant CGA_def \<open>\<forall>t' e. txn_rel t' e cs as\<close> \<open>a = Read\<close> \<open>global_rel cs as\<close> assms(1) assms(2) daut.select_convs(2) daut.select_convs(3) global_inv_def sc_None tms2_reachable_invariant txn_rel_self_preserved_stutter_Read)
          apply (rule CGA.Event_split[where b = e])
          by (auto simp add: validate_def stores_domain_def CGA.txn_inv_def txn_rel_def CGA_def CGA.unfold_updates in_flight_def update_partial_def cga_pre_def ext_enabled_def)
      qed
    next
      fix l v
      assume [simp]: "a = Commit"
      and "\<not> validate t cs"
      and ws_Some: "state.write_set cs t l = Some v"
      have "global_rel cs as" and "\<forall>t' e. txn_rel t' e cs as"
        using \<open>sim_rel cs as\<close> sim_rel_def apply auto[1]      
        using \<open>sim_rel cs as\<close> sim_rel_def by auto

      show "sim_rel (eff CGA t (Internal a) cs) as"
      proof (auto simp add: sim_rel_def)
        from `global_rel cs as` and `\<not> validate t cs` and ws_Some
        show "global_rel (eff CGA t (Internal Commit) cs) as"
          by (auto simp add: global_rel_def CGA_def CGA.unfold_updates)        
      next
        fix t' e

        have "txn_rel t' e cs as"
          by (simp add: \<open>\<forall>t' e. txn_rel t' e cs as\<close>)

        from this `pre CGA t (Internal a) cs` `\<not> validate t cs` and ws_Some
        show "txn_rel t' e (eff CGA t (Internal Commit) cs) as"
          apply -
          apply (cases "t = t'")
          apply (metis CGA.reachable_invariant CGA_def \<open>\<forall>t' e. txn_rel t' e cs as\<close> \<open>a = Commit\<close> \<open>global_rel cs as\<close> assms(1) assms(2) daut.select_convs(2) daut.select_convs(3) global_inv_def sc_None tms2_reachable_invariant txn_rel_self_preserved_stutter_Commit)
          apply (rule CGA.Event_split[where b = e])
          by (auto simp add: validate_def stores_domain_def CGA.txn_inv_def txn_rel_def CGA_def CGA.unfold_updates in_flight_def update_partial_def cga_pre_def ext_enabled_def)
      qed
    next
      assume [simp]: "a = Begin"
      have "global_rel cs as" and "\<forall>t' e. txn_rel t' e cs as"
        using \<open>sim_rel cs as\<close> sim_rel_def apply auto[1]      
        using \<open>sim_rel cs as\<close> sim_rel_def by auto

      show "sim_rel (eff CGA t (Internal a) cs) as"
      proof (auto simp add: sim_rel_def)
        from `global_rel cs as`
        show "global_rel (eff CGA t (Internal Begin) cs) as"
          by (auto simp add: global_rel_def CGA_def CGA.unfold_updates)
      next
        fix t' e

        have "txn_rel t' e cs as"
          by (simp add: \<open>\<forall>t' e. txn_rel t' e cs as\<close>)

        from this `pre CGA t (Internal a) cs`
        show "txn_rel t' e (eff CGA t (Internal Begin) cs) as"
          apply (cases "t = t'")
          apply (metis CGA.reachable_invariant CGA_def \<open>\<forall>t' e. txn_rel t' e cs as\<close> \<open>a = Begin\<close> \<open>global_rel cs as\<close> assms(1) assms(2) assms(4) daut.select_convs(2) daut.select_convs(3) global_inv_def sc_None tms2_reachable_invariant txn_rel_self_preserved_stutter_Begin)
          apply (rule CGA.Event_split[where b = e])
          by (auto simp add: validate_def stores_domain_def CGA.txn_inv_def txn_rel_def CGA_def CGA.unfold_updates in_flight_def update_partial_def cga_pre_def ext_enabled_def)
      qed
    qed
  qed
next
  show "standard_sim_int_step CGA TMS2Var step_correspondence sim_rel"
    unfolding standard_sim_int_step_def
  proof (intro impI allI, (erule conjE)+, intro conjI)
    fix cs as ci ai t
    assume "reach CGA cs" and "reach TMS2Var as"
    and "sim_rel cs as"
    and "pre CGA t (Internal ci) cs"
    and "step_correspondence cs t ci = Some ai"

    from this(3) and this(4) and this (5)
    and CGA.reachable_invariant[OF `reach CGA cs`, where t = t and e = "Internal ci"]
    and tms2_reachable_invariant[OF `reach TMS2Var as`, where t = t and e = "Internal ai"]
    show "pre TMS2Var t (Internal ai) as"
      apply (simp add: CGA_def tms_pre_def)
      apply (intro conjI)
      apply (simp add: status_enabled_def CGA.txn_inv_def TMS2Var.txn_inv_def)
      apply (cases ai)
      apply (simp add: sc_simps sim_rel_def txn_rel_def global_rel_def)
      apply (erule conjE)+
      apply (erule_tac x = t in allE)
      apply (erule conjE)
      apply (erule_tac x = "Internal ci" in allE)
      apply simp

      apply simp
      apply (simp add: sc_simps sim_rel_def txn_rel_def global_rel_def)
      apply (erule conjE)+
      apply (erule_tac x = t in allE)
      apply (erule conjE)
      apply (erule_tac x = "Internal ci" in allE)
      apply simp

      apply simp
      apply (simp add: sc_simps sim_rel_def txn_rel_def global_rel_def)
      apply (erule conjE)+
      apply (erule_tac x = t in allE)
      apply (erule conjE)
      apply (erule_tac x = "Internal ci" in allE)
      apply simp

      apply simp
      apply (simp add: sc_simps sim_rel_def txn_rel_def global_rel_def)
      apply (erule conjE)+
      apply (erule_tac x = t in allE)
      apply (erule conjE)
      apply (erule_tac x = "Internal ci" in allE)
      apply simp

      apply (simp add: CGA.txn_inv_def TMS2Var.txn_inv_def)
      apply (cases ai)
      apply simp_all

      apply (simp add: sc_simps sim_rel_def global_rel_def)
      apply (erule conjE)+
      apply (erule_tac x = t in allE)
      apply (erule_tac x = "Internal ci" in allE)
      apply (simp add: txn_rel_def)
      apply (simp add: in_flight_def ws_empty)

      apply (simp add: sc_simps sim_rel_def global_rel_def)
      apply (erule conjE)+
      apply (erule_tac x = t in allE)
      apply (erule_tac x = "Internal ci" in allE)
      apply (intro conjI)
      apply (simp add: txn_rel_def status_enabled_def in_flight_def)
      apply auto[1]

      apply (simp add: txn_rel_def status_enabled_def in_flight_def)
      apply (simp add: latest_store_def)
      apply (simp add: RWMemory.all_simps(6) validate_def_var)

      apply (simp add: sc_simps sim_rel_def txn_rel_def global_rel_def)
      apply (erule conjE)+
      apply (erule_tac x = t in allE)
      apply (erule conjE)
      apply (erule_tac x = "Internal ci" in allE)
      apply (simp add: txn_rel_def)
      
      apply (simp add: in_flight_def)
      apply (simp add: latest_store_def status_enabled_def)
      by (metis (full_types) RWMemory.all_simps(6) domIff option.simps(3) order_refl valid_index_def validate_def_var)
  next
    fix cs as ci ai t
    assume "reach CGA cs" and "reach TMS2Var as"
    and "sim_rel cs as"
    and "pre CGA t (Internal ci) cs"
    and sc: "step_correspondence cs t ci = Some ai"

    note case_hyps = this

    hence grel: "global_rel cs as" and trel: "\<forall>t e. txn_rel t e cs as"
      by (auto simp add: sim_rel_def)

    note cga_inv = CGA.reachable_invariant[OF `reach CGA cs`, where t = t and e = "Internal ci"]
    note tms2_inv = TMS2Var.reachable_invariant[OF `reach TMS2Var as`, where t = t and e = "Internal ai"]    

    show "sim_rel (eff CGA t (Internal ci) cs) (eff TMS2Var t (Internal ai) as)"
    proof (simp only: sim_rel_def, intro conjI allI)
      from sc grel trel cga_inv tms2_inv and case_hyps(4)
      show "global_rel (eff CGA t (Internal ci) cs) (eff TMS2Var t (Internal ai) as)"
        apply -
        apply (cases ai; (erule rev_mp)+; rule impI; erule_tac t = ai in ssubst; intro impI)
        apply (simp_all only: sc_simps)
        apply (simp add: global_rel_def CGA_def tms_eff_def TMS2Var.int_eff_def Interface.update_status_def max_index_def latest_store_def store_at_def CGA.update_status_def)
        apply (simp only: global_rel_def)
        apply (intro conjI)
        apply (simp add: tms_eff_def CGA_def CGA.update_status_def TMS2Var.int_eff_def write_back_def Interface.update_status_def max_index_def stores_domain_def update_store_set_def)
        apply (simp add: Suc_n_not_le_n max_def)
        apply (simp add: tms_eff_def CGA_def CGA.update_status_def TMS2Var.int_eff_def write_back_def Interface.update_status_def latest_store_def store_at_def max_index_def apply_partial_def stores_domain_def update_store_set_def)
        apply (intro conjI; intro impI; intro conjI)
        apply auto[1]
        apply (rule ext)
        apply (simp add: apply_partial_def)
        apply (intro conjI)
        apply (simp add: in_flight_def txn_rel_def)
        using in_flight_def txn_rel_def apply auto[1]
        using in_flight_def txn_rel_def apply auto[1]
        using in_flight_def txn_rel_def apply auto[1]
        apply (simp add: CGA_def)
        apply (intro conjI; intro impI)
        apply (simp add: CGA.unfold_updates tms_eff_def TMS2Var.int_eff_def global_rel_def)
        apply (intro conjI)
        apply (simp add: max_index_def)
        apply (erule conjE)+
        apply simp
        apply (erule disjE)
        apply (erule exE)
        apply (simp add: CGA.unfold_updates Interface.update_status_def when_fn_def Interface.update_read_set_def)
        apply (simp add: CGA.unfold_updates Interface.update_status_def when_fn_def Interface.update_read_set_def)
        apply (erule conjE)+
        apply simp
        apply (erule disjE)
        apply (erule exE)
        apply (simp add: CGA.unfold_updates latest_store_def store_at_def max_index_def)
        apply (rule ext)
        apply (simp add: latest_store_def store_at_def CGA.unfold_updates Interface.update_status_def when_fn_def Interface.update_read_set_def)
        apply (simp add: CGA.unfold_updates latest_store_def store_at_def max_index_def)
        apply (rule ext)
        apply (simp add: latest_store_def store_at_def CGA.unfold_updates Interface.update_status_def when_fn_def Interface.update_read_set_def)
        apply (erule conjE)+
        apply (erule disjE)
        apply (erule exE)
        apply (simp add: CGA.unfold_updates tms_eff_def TMS2Var.int_eff_def global_rel_def)
        apply (intro conjI)
        apply (simp add: max_index_def latest_store_def store_at_def CGA.unfold_updates Interface.update_status_def when_fn_def Interface.update_read_set_def)
        apply (simp add: max_index_def latest_store_def store_at_def CGA.unfold_updates Interface.update_status_def when_fn_def Interface.update_read_set_def)
        apply (simp add: CGA.unfold_updates tms_eff_def TMS2Var.int_eff_def global_rel_def)
        apply (simp add: global_rel_def CGA_def tms_eff_def TMS2Var.int_eff_def Interface.update_status_def max_index_def latest_store_def store_at_def CGA.update_status_def)
        apply (intro conjI)
        apply (simp add: Interface.update_write_set_def CGA.update_write_set_def)
        by (simp add: Interface.update_write_set_def CGA.update_write_set_def)
    next
      fix t' e

      note cga2_inv = CGA.reachable_invariant[OF `reach CGA cs`, where t = t' and e = e]

      from grel trel sc cga_inv cga2_inv tms2_inv and case_hyps(4)
      show "txn_rel t' e (eff CGA t (Internal ci) cs) (eff TMS2Var t (Internal ai) as)"
        apply -
        apply (erule_tac x = t' in allE)
        apply (erule_tac x = e in allE)
        apply (cases ai; (erule rev_mp)+; rule impI; erule_tac t = ai in ssubst; intro impI; simp only: sc_simps)
        apply (rule CGA.Event_split[where b = e];
               simp add: txn_rel_def CGA_def cga_pre_def ext_enabled_def in_flight_def CGA.unfold_updates tms_eff_def
               TMS2Var.int_eff_def Interface.update_status_def TMS2Var.txn_inv_def status_enabled_def stores_domain_def
               CGA.txn_inv_def max_index_def global_rel_def latest_store_def store_at_def)
        apply (rule CGA.Event_split[where b = e];
               simp add: txn_rel_def CGA_def cga_pre_def ext_enabled_def in_flight_def CGA.unfold_updates tms_eff_def
               TMS2Var.int_eff_def Interface.update_status_def TMS2Var.txn_inv_def status_enabled_def stores_domain_def
               CGA.txn_inv_def max_index_def global_rel_def latest_store_def store_at_def write_back_def)
        apply (rule CGA.Event_split[where b = e];
               simp add: txn_rel_def CGA_def cga_pre_def ext_enabled_def in_flight_def CGA.unfold_updates tms_eff_def
               TMS2Var.int_eff_def Interface.update_status_def TMS2Var.txn_inv_def status_enabled_def stores_domain_def
               CGA.txn_inv_def max_index_def global_rel_def latest_store_def store_at_def validate_def when_fn_def
               Interface.update_read_set_def update_partial_def value_for_def value_at_def
               split: splits option.split
               ; intro impI conjI allI; simp)
        apply (rule ext; simp; blast)+
        apply (rule CGA.Event_split[where b = e];
               simp add: txn_rel_def CGA_def cga_pre_def ext_enabled_def in_flight_def CGA.unfold_updates tms_eff_def
               TMS2Var.int_eff_def Interface.update_status_def TMS2Var.txn_inv_def status_enabled_def stores_domain_def
               CGA.txn_inv_def max_index_def global_rel_def latest_store_def store_at_def
               Interface.update_write_set_def update_partial_def; intro impI conjI allI; simp)
        done
    qed
  qed
qed

lemma "traces (ioa CGA) \<subseteq> traces (ioa TMS2Var)"
  using CGA_simulation standard_simulation_trace_inclusion by blast

end