theory CGACorrect2
  imports CGACorrect TMS2VarCorrect
begin

theorem "traces (ioa CGA) \<subseteq> traces (ioa TMS2)"
  by (meson CGA_simulation dual_order.trans sim standard_simulation_trace_inclusion)

end