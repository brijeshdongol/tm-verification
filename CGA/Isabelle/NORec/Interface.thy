theory Interface
imports RWMemory
begin

(* Transactions *)

typedecl T

consts TWriter :: "T set"

definition TReader :: "T set" where "TReader = {t . t \<notin> TWriter}"

(* The actions of TM automata *)

datatype Action = 
    begin_inv: BeginInv
  | begin_resp: BeginResp
  | commit_inv: CommitInv
  | commit_resp: CommitResp
  | cancel: Cancel
  | abort: Abort
  | read_inv: ReadInv (loc: L)
  | read_resp: ReadResp (val : V)
  | write_inv: WriteInv (loc: L)(val : V)
  | write_resp: WriteResp

datatype 'i Event =
    External Action
  | Internal 'i

type_synonym 'i TM_Event = "T * 'i Event"

datatype Hidden = Tau

type_synonym IOA_Event = "T * Hidden Event"

datatype 'pc Status =
    NotStarted
  | BeginResponding
  | WriteResponding
  | ReadResponding V
  | CommitResponding
  | AbortPending
  | Ready
  | Committed
  | Aborted
  | Pending 'pc

definition ext_enabled :: "'pc Status \<Rightarrow> Action \<Rightarrow> bool"
  where
  "ext_enabled s a \<equiv>
    (s = NotStarted \<and> a = BeginInv)
    \<or>
    (s = Ready \<and> (\<exists> l v. a = WriteInv l v))
    \<or>
    (s = Ready \<and> (\<exists> l. a = ReadInv l))
    \<or>
    (s = Ready \<and> a = CommitInv)
    \<or>
    (s = Ready \<and> a = Cancel)
    \<or>
    (s = BeginResponding \<and> a = BeginResp)
    \<or>
    (s = WriteResponding \<and> a = WriteResp)
    \<or>
    (s = CommitResponding \<and> a = CommitResp)
    \<or>
    (\<exists> v. s = ReadResponding v \<and> a = ReadResp v)
    \<or>
    (s \<notin> {NotStarted, Ready, CommitResponding, Committed, Aborted} \<and> a = Abort)"


datatype PC =
    CommitPending
  | ReadPending L
  | WritePending L V

type_synonym status = "PC Status"

record State = StoresState +
  status :: "T \<Rightarrow> status"
  begin_index :: "T \<Rightarrow> nat"
  read_set :: "T \<Rightarrow> L \<Rightarrow> V option"
  write_set :: "T \<Rightarrow> L \<Rightarrow> V option"

definition start :: "State \<Rightarrow> bool"
  where
  "start s \<equiv>
      (\<forall> t. status s t = NotStarted)
    \<and> (\<forall> t. read_set s t = empty)
    \<and> (\<forall> t. write_set s t = empty)
    \<and> initial_stores s"

definition default_start :: State
  where
  "default_start =
     \<lparr> stores = (\<lambda> n. if n = 0 then Some (\<lambda> l. v0) else None),
       status = (\<lambda> t. NotStarted),
       begin_index = (\<lambda> t. 0),
       read_set = (\<lambda> t. Map.empty),
       write_set = (\<lambda> t. Map.empty)\<rparr>"

lemma default_is_start:
  "start default_start"
by(auto simp add: start_def default_start_def initial_stores_def mem_initial_def)

definition valid_index :: "State \<Rightarrow> T \<Rightarrow> nat \<Rightarrow> bool"    
    where
    "valid_index s t n \<equiv>
      begin_index s t \<le> n \<and> n \<le> max_index s \<and>
      read_consistent (store_at s n) (read_set s t)"
    
definition update_status :: "T \<Rightarrow> status \<Rightarrow> State \<Rightarrow> State"
  where
  "update_status t st s \<equiv> s\<lparr> status := ((status s) (t := st))\<rparr>"

definition update_begin_index :: "T \<Rightarrow> nat \<Rightarrow> State \<Rightarrow> State"
  where
  "update_begin_index t n s \<equiv> s\<lparr> begin_index := ((begin_index s) (t := n)) \<rparr>"

definition update_write_set :: "T \<Rightarrow> L \<Rightarrow> V \<Rightarrow> State \<Rightarrow> State"
  where
  "update_write_set t l v s \<equiv> s\<lparr> write_set := update_partial t l v (write_set s)\<rparr>"  

definition update_read_set :: "T \<Rightarrow> L \<Rightarrow> V \<Rightarrow> State \<Rightarrow> State"
  where
  "update_read_set t l v s \<equiv> s\<lparr> read_set := update_partial t l v (read_set s)\<rparr>"  

end