theory TMS2Var         
imports Transitions Interface
begin
  
datatype InternalAction =
    DoCommitReadOnly
  | DoCommitWriter
  | do_read: DoRead L nat
  | do_write: DoWrite L V

definition status_enabled :: "T \<Rightarrow> InternalAction Event \<Rightarrow> State \<Rightarrow> bool"
  where
  "status_enabled t e s \<equiv>
    case e of
      Internal a \<Rightarrow>
         (case a of
           DoRead l n \<Rightarrow> status s t = Pending(ReadPending l)
           |
           DoCommitReadOnly \<Rightarrow> status s t = Pending(CommitPending)
           |
           DoCommitWriter \<Rightarrow> status s t = Pending(CommitPending)
           |
           DoWrite l v \<Rightarrow> status s t = Pending(WritePending l v))
      |
      External a \<Rightarrow> ext_enabled (status s t) a"

definition tms_pre :: "T \<Rightarrow> InternalAction Event \<Rightarrow> State \<Rightarrow> bool"
  where
  "tms_pre t e s \<equiv>
      status_enabled t e s
      \<and>
      (case e of
        Internal a \<Rightarrow>
           (case a of
             DoRead l n \<Rightarrow> (l : dom (write_set s t) \<or> valid_index s t n)
             |
             DoCommitReadOnly \<Rightarrow> (write_set s t = Map.empty)
             |
             DoCommitWriter \<Rightarrow> write_set s t \<noteq> Map.empty \<and> read_consistent (latest_store s) (read_set s t)
             |
             DoWrite l v \<Rightarrow> True)
        |
        External a \<Rightarrow> True)"

        
definition ext_eff :: "T \<Rightarrow> Action \<Rightarrow> State \<Rightarrow> State"
  where
  "ext_eff t a \<equiv>
    (case a of
        BeginInv \<Rightarrow> (\<lambda>s. (update_status t BeginResponding \<circ>> update_begin_index t (Max (dom (stores s)))) s)
      | BeginResp \<Rightarrow> update_status t Ready
      | CommitInv \<Rightarrow> update_status t (Pending CommitPending)
      | CommitResp \<Rightarrow> update_status t CommitResponding
      | ReadInv l \<Rightarrow> update_status t (Pending (ReadPending l))
      | ReadResp v \<Rightarrow> update_status t Ready
      | WriteInv l v \<Rightarrow> update_status t (Pending (WritePending l v))
      | WriteResp \<Rightarrow> update_status t Ready
      | Cancel \<Rightarrow> update_status t AbortPending
      | Abort \<Rightarrow> update_status t Aborted)"

definition int_eff :: "T \<Rightarrow> InternalAction \<Rightarrow> State \<Rightarrow> State"
  where
  "int_eff t ia \<equiv>
    case ia of
        DoRead l n \<Rightarrow> (\<lambda>s.
          let v = (value_for s n (write_set s t) l)
          in ((update_read_set t l v) when (l \<notin> dom (write_set s t)) \<circ>> update_status t (ReadResponding v)) s)
                         
      | DoWrite l v \<Rightarrow> update_write_set t l v \<circ>> update_status t WriteResponding
             
      | DoCommitReadOnly \<Rightarrow> update_status t CommitResponding
      
      | DoCommitWriter \<Rightarrow> (\<lambda>s. (update_status t CommitResponding \<circ>> write_back (write_set s t)) s)"
      

definition tms_eff :: "T \<Rightarrow> InternalAction Event \<Rightarrow> State \<Rightarrow> State"
  where
  "tms_eff t e s \<equiv> case e of Internal i \<Rightarrow> int_eff t i s | External a \<Rightarrow> ext_eff t a s"

definition TMS2Var  :: "(State, InternalAction) daut"
  where
  "TMS2Var \<equiv> \<lparr> daut.start = Interface.start, daut.pre = tms_pre, daut.eff = tms_eff\<rparr>"
                                             
declare TMS2Var_def [simp]

lemma Event_split:
  "\<lbrakk>b = External BeginInv \<Longrightarrow> P;
    b = External BeginResp \<Longrightarrow> P;
    \<And> l. b = External (ReadInv l) \<Longrightarrow> P;
    \<And> l n. b = Internal (DoRead l n) \<Longrightarrow> P;
    \<And> v. b = External (ReadResp v) \<Longrightarrow> P;
    \<And> l v. b = External (WriteInv l v) \<Longrightarrow> P;
    \<And> l v. b = Internal (DoWrite l v) \<Longrightarrow> P;
    b = External WriteResp \<Longrightarrow> P;
    b = External CommitInv \<Longrightarrow> P;
    b = Internal DoCommitWriter \<Longrightarrow> P;
    \<And> n. b = Internal DoCommitReadOnly \<Longrightarrow> P;
    b = External CommitResp \<Longrightarrow> P;
    b = External Cancel \<Longrightarrow> P;
    b = External Abort \<Longrightarrow> P \<rbrakk>
   \<Longrightarrow>
    P"
apply(cases rule: Event.exhaust[where y=b])
apply(auto simp add: split: Event.split)
apply (meson Action.exhaust_sel)
by (meson InternalAction.exhaust)

lemmas unfold_updates =
  update_status_def
  update_write_set_def
  update_begin_index_def
  update_read_set_def
  valid_index_def

lemmas unfold_pre =
  tms_pre_def
  status_enabled_def

lemmas unfold_tms2 =
  Interface.start_def ext_eff_def int_eff_def tms_eff_def
  unfold_pre
  unfold_updates

lemmas unfold_all_tms2 =
  unfold_tms2
  initial_stores_def
  RWMemory.all_simps
  Utilities.all_utilities
  ext_enabled_def

lemmas splits = Event.split Action.split InternalAction.split if_splits

definition stores_domain :: "State \<Rightarrow> bool"
  where
  "stores_domain s \<equiv>
    finite(dom (stores s)) \<and>
    dom (stores s) \<noteq> {} \<and>
    (\<forall> n. n \<le> max_index s \<longrightarrow> n : dom (stores s))"

lemma stores_domain_defined: "stores_domain s \<Longrightarrow> stores_defined s"
  by (simp add: stores_domain_def stores_defined_def) blast

lemma begin_index_stable:
  "\<lbrakk>tms_pre at a s; a \<noteq> External BeginInv \<or> t \<noteq> at\<rbrakk> \<Longrightarrow> begin_index (tms_eff at a s) t = begin_index s t"
  by(auto simp add: unfold_all_tms2 split: splits)

lemma max_index_stable:
  "\<lbrakk>tms_pre at a s; a \<noteq> Internal DoCommitWriter\<rbrakk> \<Longrightarrow> max_index (tms_eff at a s) = max_index s"
  by(auto simp add: unfold_all_tms2 split: splits)

lemma valid_index_stable:
  "\<lbrakk>tms_pre t' a (s :: State); t \<noteq> t'; stores_domain s; valid_index s t n\<rbrakk> \<Longrightarrow> (\<exists>n. valid_index (tms_eff t' a s) t n)"
  apply (rule Event_split[where b = a])
  apply (simp add: unfold_all_tms2; rule_tac x = n in exI; simp)
  apply (simp add: unfold_all_tms2; rule_tac x = n in exI; simp)
  apply (simp add: unfold_all_tms2; rule_tac x = n in exI; simp)
  defer
  apply (simp add: unfold_all_tms2; rule_tac x = n in exI; simp)
  apply (simp add: unfold_all_tms2; rule_tac x = n in exI; simp)
  apply (simp add: unfold_all_tms2; rule_tac x = n in exI; simp)
  apply (simp add: unfold_all_tms2; rule_tac x = n in exI; simp)
  apply (simp add: unfold_all_tms2; rule_tac x = n in exI; simp)
  defer
  apply (simp add: unfold_all_tms2; rule_tac x = n in exI; simp)
  apply (simp add: unfold_all_tms2; rule_tac x = n in exI; simp)
  apply (simp add: unfold_all_tms2; rule_tac x = n in exI; simp)
  apply (simp add: unfold_all_tms2; rule_tac x = n in exI; simp)


  apply (rename_tac l m)
  apply (rule_tac x = n in exI)
  apply (simp add: unfold_all_tms2 split: splits)
  apply (simp add: option.case_eq_if update_read_set_def)
  
  apply (rule_tac x = n in exI)
  apply (simp add: unfold_all_tms2 split: splits)
  apply (erule conjE)+
  apply (rule_tac b = "Max (dom (stores s))" in order.trans)
  apply simp
  apply (rule Max_mono)
  apply blast
  by (simp_all add: stores_domain_def)

lemma stores_stable:
  "\<lbrakk>tms_pre at a s; a \<noteq> Internal DoCommitWriter\<rbrakk> \<Longrightarrow> stores (tms_eff at a s) = stores s"
  by(auto simp add: unfold_all_tms2 split: splits)

lemma stores_update_do_commit_writer:
  "\<lbrakk>tms_pre at a s; a = Internal DoCommitWriter\<rbrakk> \<Longrightarrow>
   stores (tms_eff at a s) =
    (stores s) ((max_index s)+1 := Some (apply_partial (store_at s (max_index s)) (write_set s at)))"
  by(auto simp add: unfold_all_tms2 split: InternalAction.split)

lemma stores_domain_stable:
  "\<lbrakk>tms_pre at a s; a \<noteq> Internal DoCommitWriter\<rbrakk> \<Longrightarrow>
   dom (stores (tms_eff at a s)) = dom (stores s)"
  by(auto simp add: unfold_all_tms2 split: splits)

lemma dom_stores_update_do_commit_writer:
  "tms_pre at (Internal DoCommitWriter) s \<Longrightarrow>
   dom (stores (tms_eff at (Internal DoCommitWriter) s)) = dom (stores s) \<union> {1 + (max_index s)}"
  by (simp add: unfold_all_tms2 max_stores_append)

lemma stores_domain_max_is_max:
  "stores_domain s \<Longrightarrow> n \<le> max_index s \<longleftrightarrow> n : dom (stores s)"
  by (unfold stores_domain_def) (metis Max_ge max_index_def)

lemma stores_domain_preserved:
  "\<lbrakk>tms_pre at a s; a \<noteq> Internal DoCommitWriter; stores_domain s\<rbrakk> \<Longrightarrow> stores_domain (tms_eff at a s)"
  by(cases rule: Event_split[where b=a], simp_all add:  stores_domain_def stores_stable max_index_stable)

lemma stores_domain_preserved_do_commit_writer:
  "\<lbrakk>tms_pre at (Internal DoCommitWriter) s; stores_domain s\<rbrakk> \<Longrightarrow> stores_domain (tms_eff at (Internal DoCommitWriter) s)"
  by(auto simp add: stores_domain_def stores_update_do_commit_writer unfold_all_tms2 max_stores_append domD)

lemma max_index_update:
  "\<lbrakk>tms_pre at (Internal DoCommitWriter) s; stores_domain s\<rbrakk>
   \<Longrightarrow>
   max_index (tms_eff at (Internal DoCommitWriter) s) = 1 + max_index s"
  by(simp add: unfold_all_tms2 max_stores_append dom_stores_update_do_commit_writer stores_domain_def)

lemma store_at_stable:
  "\<lbrakk>tms_pre at a s; a \<noteq> Internal DoCommitWriter \<or> n \<le> max_index s\<rbrakk> \<Longrightarrow>
   store_at (tms_eff at a s) n = store_at s n"
  by (auto simp add: unfold_all_tms2 split: splits)

lemma read_set_stable:
  "\<lbrakk>tms_pre at a s; (\<forall> l n. a \<noteq> Internal (DoRead l n)) \<or> t \<noteq> at\<rbrakk> \<Longrightarrow>
   read_set (tms_eff at a s) t = read_set s t"
  by(auto simp add: unfold_all_tms2 split: splits)

lemma read_set_valid:
  "\<lbrakk>l \<in> dom (read_set s t); read_consistent (store_at s n) (read_set s t)\<rbrakk> \<Longrightarrow>
   read_set s t l = Some (store_at s n l)"
  by(auto elim: allE[where x = l] simp add: unfold_all_tms2)

(*
lemma read_set_valid_read_with_valid_index:
  "\<lbrakk>tms_pre at a s; l \<in> dom (read_set s at);
    (l \<notin> dom (write_set s at) \<and> a = Internal (DoRead l n)) \<or> a = Internal DoCommitReadOnly\<rbrakk> \<Longrightarrow>
   read_set s at l = Some (store_at s n l)"
 using read_set_valid tms_pre_def valid_index_def by fastforce
*)

lemma read_set_valid_read_with_valid_index:
  "\<lbrakk>tms_pre at a s; l \<in> dom (read_set s at);
    (l \<notin> dom (write_set s at) \<and> a = Internal (DoRead l n))\<rbrakk> \<Longrightarrow>
   read_set s at l = Some (store_at s n l)"
 using read_set_valid tms_pre_def valid_index_def by fastforce

lemma read_set_valid_read_commit_writer:
  "\<lbrakk>tms_pre at (Internal DoCommitWriter) s; l \<in> dom (read_set s at)\<rbrakk> \<Longrightarrow>
   read_set s at l = Some(store_at s (max_index s) l)"
  by (auto simp add: read_set_valid unfold_all_tms2 domD) (smt option.simps) (* FIXME: SMT *)

lemma read_set_stable_do_read:
  "\<lbrakk>tms_pre at (Internal (DoRead l n)) s;
    stores_domain s; l \<in> (dom (write_set s at)) \<or> l : (dom (read_set s at))\<rbrakk> \<Longrightarrow>
   read_set (tms_eff at (Internal (DoRead l n)) s) at = read_set s at"
  by (simp add: stores_domain_def unfold_all_tms2 split: option.split) (smt domD domIff fun_upd_triv option.discI option.simps(5))

lemma tau_read_set_update:
  "\<lbrakk>tms_pre at (Internal (DoRead l n)) s; l \<notin> dom (write_set s at)\<rbrakk> \<Longrightarrow>
   read_set (tms_eff at (Internal (DoRead l n)) s) at = (read_set s at) (l := Some (value_at s n l))"
  by (auto simp add: unfold_all_tms2 split: splits)

lemma ext_write_set_stable:
  "\<lbrakk>tms_pre at a s; (\<forall> l v. a \<noteq> Internal (DoWrite l v)) \<or> t \<noteq> at\<rbrakk>
   \<Longrightarrow>
   write_set (tms_eff at a s) t = write_set s t"
  by (auto simp add: unfold_all_tms2 split: splits)

lemma tau_write_set_update:
  "\<lbrakk>tms_pre at (Internal (DoWrite l v)) s\<rbrakk> \<Longrightarrow>
   write_set (tms_eff at (Internal (DoWrite l v)) s) at = (write_set s at)(l := Some v)"
  by (auto simp add: unfold_all_tms2)

lemma status_stable:
    "\<lbrakk>tms_pre at a s; t \<noteq> at\<rbrakk> \<Longrightarrow>
     status (tms_eff at a s) t = status s t"
  by(auto simp add: unfold_all_tms2 split: splits)

definition txn_inv :: "T \<Rightarrow> InternalAction Event \<Rightarrow> State \<Rightarrow> bool"
  where
  "txn_inv t e s \<equiv>
     status_enabled t e s \<longrightarrow>
     (case e of
       (External BeginInv) \<Rightarrow> begin_index s t \<le> max_index s
                              \<and> read_set s t = Map.empty \<and> write_set s t = Map.empty
       |
       (External BeginResp) \<Rightarrow>   begin_index s t \<le> max_index s
                               \<and> read_set s t = Map.empty
                               \<and> write_set s t = Map.empty
       |
       (Internal (DoRead l n)) \<Rightarrow> begin_index s t \<le> max_index s \<and> (\<exists>n. valid_index s t n)
       |
       (External (ReadResp v)) \<Rightarrow> begin_index s t \<le> max_index s \<and> (\<exists>n. valid_index s t n)
       |
       (Internal (DoWrite l v)) \<Rightarrow> begin_index s t \<le> max_index s \<and> (\<exists>n. valid_index s t n)
       |
       (External WriteResp) \<Rightarrow> begin_index s t \<le> max_index s \<and> write_set s t \<noteq> Map.empty \<and> (\<exists>n. valid_index s t n)
       |
       (Internal DoCommitReadOnly) \<Rightarrow> begin_index s t \<le> max_index s \<and> (\<exists>n. valid_index s t n)
       |
       (Internal DoCommitWriter) \<Rightarrow> begin_index s t \<le> max_index s
       |
       (External CommitResp) \<Rightarrow> True
       |
       (External Abort) \<Rightarrow> True
       |
       _ \<Rightarrow> begin_index s t \<le> max_index s \<and> (\<exists>n. valid_index s t n))"

lemma txn_inv_preserved_self:
  "\<lbrakk>stores_domain s;
    txn_inv at e s;
    tms_pre at e s\<rbrakk>
    \<Longrightarrow>
    txn_inv at b (tms_eff at e s)"
apply(cases rule: Event_split[where b=b])
apply(simp_all add: txn_inv_def begin_index_stable max_index_stable max_index_update)
apply(simp_all add: unfold_tms2 split: splits)
apply(auto simp add: unfold_all_tms2)
apply(rule_tac x = x in exI)
apply(simp add: valid_index_def read_consistent_def max_index_def store_at_def)
apply(rule_tac x = x in exI)
apply(simp add: valid_index_def read_consistent_def max_index_def store_at_def)
apply(rule_tac x = x in exI)
apply(simp add: valid_index_def read_consistent_def max_index_def store_at_def)
apply(rename_tac z y x)
apply(rule_tac x = y in exI)
apply(simp add: valid_index_def read_consistent_def max_index_def store_at_def)
apply(rule_tac x = x in exI; simp add: valid_index_def read_consistent_def max_index_def store_at_def)+
done

lemma txn_inv_preserved_other:
  assumes "stores_domain s" and
          "txn_inv t b s" and
          "t \<noteq> at" and
          "tms_pre at e s"
    shows "txn_inv t b (tms_eff at e s)"
proof (cases e)
 case (External a)
 from this and assms show ?thesis
   by (cases rule: Event_split[where b=b];
       simp add: txn_inv_def;
       intro impI conjI;
       auto intro: valid_index_stable simp add: read_set_stable status_enabled_def status_stable begin_index_stable max_index_stable ext_write_set_stable)
 next
 case (Internal a)
   have mi: "max_index s \<le> max_index (tms_eff at e s)"
    using assms Internal by (cases a, auto simp add: max_index_update max_index_stable)
   have bi: "begin_index s t \<le> max_index s \<longrightarrow> begin_index (tms_eff at e s) t \<le> max_index (tms_eff at e s)"
    using mi assms  begin_index_stable tms_eff_def by (cases a, auto simp add: unfold_tms2)
   show ?thesis
   proof (cases a)
     case DoCommitReadOnly
     from this Internal assms show ?thesis
       by (cases rule: Event_split[where b=b];
           simp add: txn_inv_def;
           intro impI conjI;
           auto intro: valid_index_stable simp add: read_set_stable status_enabled_def status_stable begin_index_stable max_index_stable ext_write_set_stable)
   next
     case DoCommitWriter
     from this Internal assms bi show ?thesis
       by (cases rule: Event_split[where b=b];
           simp add: txn_inv_def;
           intro impI conjI;
           auto intro: valid_index_stable simp add: read_set_stable status_enabled_def status_stable begin_index_stable max_index_stable ext_write_set_stable)
   next
     case (DoRead l n)
     from this Internal assms bi show ?thesis
       by (cases rule: Event_split[where b=b];
           simp add: txn_inv_def;
           intro impI conjI;
           auto intro: valid_index_stable simp add: read_set_stable status_enabled_def status_stable begin_index_stable max_index_stable ext_write_set_stable)
     next
     case (DoWrite l v)
       from this Internal assms bi show ?thesis
       by (cases rule: Event_split[where b=b];
           simp add: txn_inv_def;
           intro impI conjI;
           auto intro: valid_index_stable simp add: read_set_stable status_enabled_def status_stable begin_index_stable max_index_stable ext_write_set_stable)
   qed
qed

lemma total_inv_initial:
  "Interface.start s \<Longrightarrow> stores_domain s \<and> (\<forall> e t. txn_inv t e s)"
by (auto simp add: Interface.start_def stores_domain_def txn_inv_def initial_stores_def dom_def
                     max_index_def unfold_pre ext_enabled_def split: if_splits splits)

lemma total_inv:
  "invariant (ioa TMS2Var) (\<lambda> s. stores_domain s \<and> (\<forall> e t. txn_inv t e s))"
apply(rule invariant_intro)
  apply(rule total_inv_initial, simp)
  apply(unfold preserved_def)
apply(auto simp add: txn_inv_preserved_self txn_inv_preserved_other stores_domain_preserved stores_domain_preserved_do_commit_writer)
using stores_domain_preserved stores_domain_preserved_do_commit_writer apply metis
using txn_inv_preserved_self txn_inv_preserved_other apply metis
done

lemma read_consistent_stable:
  "\<lbrakk>tms_pre at a s;
    stores_domain s;
    txn_inv at a s;
    n \<le> max_index s;
    (\<forall> l n. a \<noteq> Internal (DoRead l n)) \<or> t \<noteq> at\<rbrakk>
   \<Longrightarrow>
   read_consistent (store_at (tms_eff at a s) n) (read_set (tms_eff at a s) t) =
     read_consistent (store_at s n) (read_set s t)"
by(simp add: store_at_stable read_set_stable)

lemma reachable_invariant: "reach TMS2Var s \<Longrightarrow> stores_domain s \<and> txn_inv t e s"
  using invariant_elim total_inv by fastforce

end