theory CGA
imports Transitions Interface Utilities
begin

datatype pc =
  Begin
| Commit
| Read
| Write

type_synonym status = "pc Interface.Status"

record state =
  store :: "L \<Rightarrow> V"
  status :: "T \<Rightarrow> status"
  val :: "T \<Rightarrow> V"
  addr :: "T \<Rightarrow> L"

  (* Read and write set for lazy TM *)
  write_set :: "T \<Rightarrow> L \<Rightarrow> V option"
  read_set :: "T \<Rightarrow> L \<Rightarrow> V option"

  (* Auxillary variables *)
  commits :: nat
  last_read_validation :: "T \<Rightarrow> nat"

definition validate :: "T \<Rightarrow> state \<Rightarrow> bool" where
  "validate t s \<equiv> (\<forall>l v. read_set s t l = Some v \<longrightarrow> v = store s l)"

lemma validate_def_var: "validate t s = (\<forall>l. case read_set s t l of (Some v) \<Rightarrow> v = store s l | None \<Rightarrow> True)"
  apply (auto simp add: validate_def)
  apply (simp add: option.case_eq_if)
  by (metis (full_types) option.case_eq_if option.distinct(1) option.sel)

definition update_status :: "T \<Rightarrow> status \<Rightarrow> state \<Rightarrow> state" where
  "update_status t st \<equiv> \<lambda>s. s \<lparr> status := ((status s) (t := st))\<rparr>"

definition update_val :: "T \<Rightarrow> V \<Rightarrow> state \<Rightarrow> state" where
  "update_val t v \<equiv> \<lambda>s. s \<lparr> val := ((val s) (t := v))\<rparr>"

definition update_addr :: "T \<Rightarrow> L \<Rightarrow> state \<Rightarrow> state" where
  "update_addr t l \<equiv> \<lambda>s. s \<lparr> addr := ((addr s) (t := l))\<rparr>"

definition update_store :: "L \<Rightarrow> V \<Rightarrow> state \<Rightarrow> state" where
  "update_store l v \<equiv> \<lambda>s. s \<lparr> store := ((store s) (l := v)) \<rparr>"

definition update_write_set :: "T \<Rightarrow> L \<Rightarrow> V \<Rightarrow> state \<Rightarrow> state"
  where
  "update_write_set t l v \<equiv> \<lambda>s. s \<lparr> write_set := update_partial t l v (write_set s)\<rparr>"  

definition update_read_set :: "T \<Rightarrow> L \<Rightarrow> V \<Rightarrow> state \<Rightarrow> state"
  where
  "update_read_set t l v \<equiv> \<lambda>s. s \<lparr> read_set := update_partial t l v (read_set s)\<rparr>"  

definition update_store_set :: "(L \<Rightarrow> V option) \<Rightarrow> state \<Rightarrow> state" where
  "update_store_set ws \<equiv> \<lambda>s. s \<lparr> store := (\<lambda>l. if ws l = None then store s l else the (ws l)) \<rparr>"

definition update_last_read_validation :: "T \<Rightarrow> nat \<Rightarrow> state \<Rightarrow> state"
  where
  "update_last_read_validation t n s \<equiv> s\<lparr> last_read_validation := ((last_read_validation s) (t := n)) \<rparr>"


definition start :: "state \<Rightarrow> bool" where
  "start s \<equiv>
      (\<forall>t. status s t = NotStarted)
    \<and> store s \<in> mem_initial
    \<and> commits s = 0
    \<and> (\<forall>t. last_read_validation s t = 0)
    \<and> (\<forall>t l. read_set s t l = None)
    \<and> (\<forall>t l. write_set s t l = None)"

definition cga_pre :: "T \<Rightarrow> pc Event \<Rightarrow> state \<Rightarrow> bool" where
  "cga_pre t e s \<equiv>
      e \<noteq> External Cancel
    \<and> (e = External Abort \<longrightarrow> status s t = AbortPending)
    \<and> (case e of
           External a \<Rightarrow> ext_enabled (status s t) a
         | Internal a \<Rightarrow> status s t = Pending a)"

fun ext_eff :: "T \<Rightarrow> Action \<Rightarrow> state \<Rightarrow> state" where
  "ext_eff t BeginInv = update_status t (Pending Begin)"
| "ext_eff t BeginResp = update_status t Ready"
| "ext_eff t CommitInv = update_status t (Pending Commit)"
| "ext_eff t CommitResp = update_status t Committed"
| "ext_eff t Abort = update_status t Aborted"
| "ext_eff t (ReadInv l) = update_addr t l \<circ>> update_status t (Pending Read)"
| "ext_eff t (ReadResp v) = update_status t Ready"
| "ext_eff t (WriteInv l v) = update_addr t l \<circ>> update_val t v \<circ>> update_status t (Pending Write)"
| "ext_eff t WriteResp = update_status t Ready"

fun int_eff :: "T \<Rightarrow> pc \<Rightarrow> state \<Rightarrow> state" where
  "int_eff t Begin = update_status t BeginResponding"

| "int_eff t Commit = (\<lambda>s.
    if (\<forall>l. write_set s t l = None)
    then (update_status t CommitResponding) s
    else (if validate t s
      then (update_store_set (write_set s t) \<circ>> commits_update (\<lambda>n. n + 1) \<circ>> update_status t CommitResponding) s
      else update_status t AbortPending s))"

| "int_eff t Read = (\<lambda>s.
    case write_set s t (addr s t) of
      Some v \<Rightarrow> update_val t v (update_status t (ReadResponding v) s)
    | None \<Rightarrow> case read_set s t (addr s t) of
                Some v \<Rightarrow> update_val t v (update_status t (ReadResponding v) s)
              | None \<Rightarrow> if validate t s
                        then let v  = store s (addr s t) in (update_val t v \<circ>> update_last_read_validation t (commits s) \<circ>> update_read_set t (addr s t) v \<circ>> update_status t (ReadResponding v)) s
                        else update_status t AbortPending s)"

| "int_eff t Write = (\<lambda>s. update_write_set t (addr s t) (val s t) s) \<circ>> update_status t WriteResponding"

fun cga_eff :: "T \<Rightarrow> pc Event \<Rightarrow> state \<Rightarrow> state" where
  "cga_eff t (Internal a) = int_eff t a"
| "cga_eff t (External a) = ext_eff t a"

definition CGA :: "(state, pc) daut" where
  "CGA \<equiv> \<lparr> daut.start = start, daut.pre = cga_pre, daut.eff = cga_eff \<rparr>"

definition global_inv :: "state \<Rightarrow> bool" where
  "global_inv s \<equiv> True"

definition is_writer :: "state \<Rightarrow> T \<Rightarrow> bool" where
  "is_writer s t \<equiv> (\<exists>l. write_set s t l \<noteq> None)"

definition txn_inv :: "T \<Rightarrow> pc Event \<Rightarrow> state \<Rightarrow> bool" where
  "txn_inv t e s \<equiv> cga_pre t e s \<longrightarrow>
   (case e of
     External BeginInv \<Rightarrow> read_set s t = Map.empty \<and> write_set s t = Map.empty
   | Internal Begin \<Rightarrow> read_set s t = Map.empty \<and> write_set s t = Map.empty
   | External BeginResp \<Rightarrow> read_set s t = Map.empty \<and> write_set s t = Map.empty
   | External (ReadResp v) \<Rightarrow> v = val s t
   | External WriteResp \<Rightarrow> write_set s t \<noteq> Map.empty
   | _ \<Rightarrow> True)"

lemma Event_split:
  "\<lbrakk>b = External BeginInv \<Longrightarrow> P;
    b = Internal Begin \<Longrightarrow> P;
    b = External BeginResp \<Longrightarrow> P;
    \<And> l. b = External (ReadInv l) \<Longrightarrow> P;
    b = Internal Read \<Longrightarrow> P;
    \<And> v. b = External (ReadResp v) \<Longrightarrow> P;
    \<And> l v. b = External (WriteInv l v) \<Longrightarrow> P;
    b = Internal Write \<Longrightarrow> P;
    b = External WriteResp \<Longrightarrow> P;
    b = External CommitInv \<Longrightarrow> P;
    b = Internal Commit \<Longrightarrow> P;
    b = External CommitResp \<Longrightarrow> P;
    b = External Cancel \<Longrightarrow> P;
    b = External Abort \<Longrightarrow> P \<rbrakk>
   \<Longrightarrow>
    P"
  apply (cases rule: Event.exhaust[where y = b])
  using Action.exhaust_sel apply blast
  using pc.exhaust apply blast
  done

lemmas unfold_updates =
  update_status_def
  update_addr_def
  update_val_def
  update_store_def
  update_read_set_def
  update_write_set_def
  update_store_set_def
  update_last_read_validation_def

lemmas cga_simps =
  txn_inv_def
  cga_pre_def
  ext_enabled_def
  unfold_updates
  is_writer_def
  global_inv_def

(* Begin *)

lemma txn_inv_pres_begin_inv_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External BeginInv);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (cga_eff at a s)"
   apply (cases rule: Event_split[of b])
   by (auto simp add: cga_simps)

lemma txn_inv_pres_begin_inv_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (External BeginInv);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_begin_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (Internal Begin);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_begin_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (Internal Begin);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps)

lemma txn_inv_pres_begin_resp_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External BeginResp);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps)

lemma txn_inv_pres_begin_resp_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (External BeginResp);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps)

(* Read *)

lemma txn_inv_pres_read_inv_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External (ReadInv l));
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (cga_eff at a s)"
   apply (cases rule: Event_split[of b])
   by (auto simp add: cga_simps)

lemma txn_inv_pres_read_inv_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = External (ReadInv l);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps)

lemma txn_inv_pres_read_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (Internal Read);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps split: option.split)

lemma txn_inv_pres_read_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (Internal Read);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps update_partial_def split: option.split)

lemma txn_inv_pres_read_resp_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External (ReadResp v));
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_read_resp_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = External (ReadResp l);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

(* Write *)

lemma txn_inv_pres_write_inv_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External (WriteInv l v));
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_write_inv_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = External (WriteInv l v);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_write_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (Internal Write);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps update_partial_def)

lemma txn_inv_pres_write_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (Internal Write);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (cga_eff at a s)"
   apply (cases rule: Event_split[of b])
   by (simp add: cga_simps update_partial_def)+

lemma txn_inv_pres_write_resp_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External WriteResp);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_write_resp_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = External WriteResp;
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

(* Commit *)

lemma txn_inv_pres_commit_inv_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External CommitInv);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_commit_inv_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (External CommitInv);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_commit_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (Internal Commit);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (cga_eff at a s)"
   apply (cases rule: Event_split[of b])
   apply (simp_all add: cga_simps)
   done

lemma txn_inv_pres_commit_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (Internal Commit);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps)

lemma txn_inv_pres_commit_resp_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External CommitResp);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_commit_resp_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (External CommitResp);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_cancel_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External Cancel);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_cancel_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (External Cancel);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_abort_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External Abort);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma txn_inv_pres_abort_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (External Abort);
    cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps)

lemma allE2: "\<forall>x. P x \<Longrightarrow> (P x \<Longrightarrow> P y \<Longrightarrow> Q) \<Longrightarrow> Q"
  by auto

lemma start_invariant: "daut.start CGA s \<Longrightarrow> txn_inv t e s"
  apply (cases rule: Event_split[of e])
  apply (simp_all add: txn_inv_def cga_pre_def CGA_def start_def ext_enabled_def)
  apply (erule conjE)+
  by blast

lemma total_inv: "invariant (ioa CGA) (\<lambda>s. global_inv s \<and> (\<forall>e t. txn_inv t e s))"
proof (intro invariant_intro conjI allI)
  fix s
  assume "daut.start CGA s"
  thus "global_inv s"
    by (simp add: CGA_def start_def global_inv_def)
next
  fix s e t
  assume "daut.start CGA s"
  thus "txn_inv t e s"
    by (rule start_invariant)
next
  fix s
  show "preserved  CGA (\<lambda>s. global_inv s \<and> (\<forall>e t. txn_inv t e s)) s"
    apply (simp only: preserved_def)
    apply (intro allI impI)
    apply (erule conjE)+
    apply (intro conjI allI)
  proof -
    fix t a
    assume "pre CGA t a s" and "global_inv s" and "\<forall>e t. (txn_inv t e s)"
    thus "global_inv (eff CGA t a s)"
      apply (cases rule: Event_split[of a])
      by (simp add: global_inv_def CGA_def cga_pre_def unfold_updates)+
  next
    fix t a e t'
    assume "pre CGA t a s" and "global_inv s" and "\<forall>e t. txn_inv t e s"
    thus "txn_inv t' e (eff CGA t a s)"
      apply (cases "t' = t")
      apply (erule ssubst)
      apply (erule_tac x = e and y = a in allE2)
      apply (erule_tac x = t in allE)
      apply (erule_tac x = t in allE)
      apply (cases rule: Event_split[of a])
      apply (simp_all add: CGA_def del: cga_eff.simps)
      apply (rule txn_inv_pres_begin_inv_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_begin_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_begin_resp_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_inv_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_resp_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_inv_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_resp_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_inv_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_resp_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_cancel_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_abort_self, simp, simp, simp, simp)
      apply (erule_tac x = e and y = a in allE2)
      apply (erule_tac x = t' in allE)
      apply (erule_tac x = t in allE)   
      apply (cases rule: Event_split[of a])
      apply (rule txn_inv_pres_begin_inv_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_begin_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_begin_resp_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_inv_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_resp_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_inv_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_resp_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_inv_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_resp_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_cancel_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_abort_other, simp, simp, simp, simp, simp, simp)
      done
  qed
qed

lemma reachable_invariant: "reach CGA s \<Longrightarrow> txn_inv t e s"
  using invariant_elim total_inv by fastforce

end