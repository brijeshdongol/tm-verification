theory TMS2VarCorrect
imports TMS2Var TMS2
begin

definition step_correspondence :: "State \<Rightarrow> T \<Rightarrow> TMS2Var.InternalAction \<Rightarrow> TMS2.InternalAction option"
  where
  "step_correspondence cs t a \<equiv>
    case a of
      TMS2Var.DoCommitReadOnly \<Rightarrow> Some (DoCommitReadOnly (SOME n. valid_index cs t n))
    | TMS2Var.DoCommitWriter \<Rightarrow> Some DoCommitWriter
    | (TMS2Var.DoRead l n) \<Rightarrow> Some (DoRead l n)
    | (TMS2Var.DoWrite l v) \<Rightarrow> Some (DoWrite l v)"
      
lemmas sim_simps = TMS2.unfold_all_tms2 TMS2Var.unfold_all_tms2

lemma sim:
  "standard_simulation TMS2Var TMS2 step_correspondence op ="
proof (unfold standard_simulation_def, intro conjI allI impI, rule_tac x = cs in exI, simp)
  show "standard_sim_ext_step TMS2Var TMS2 op ="
  proof (unfold standard_sim_ext_step_def, intro impI allI, (erule conjE)+)
    fix cs as e t
    assume "reach TMS2Var cs" and "reach TMS2 as"
    and "cs = as"
    and "pre TMS2Var t (External e) cs"
    thus "pre TMS2 t (External e) as \<and> eff TMS2Var t (External e) cs = eff TMS2 t (External e) as"
      by (cases e) (simp_all add: sim_simps)
  qed
next
  show "standard_sim_stutter TMS2Var TMS2 step_correspondence op ="
  proof (unfold standard_sim_stutter_def, intro impI allI, (erule conjE)+)
    fix cs as i t
    assume "reach TMS2Var cs" and "reach TMS2 as"
    and "cs = as"
    and "pre TMS2Var t (Internal i) cs"
    and "step_correspondence cs t i = None"
    thus "eff TMS2Var t (Internal i) cs = as"
      by (cases i) (simp_all add: step_correspondence_def)
  qed
next
  show "standard_sim_int_step TMS2Var TMS2 step_correspondence op ="
  proof (unfold standard_sim_int_step_def, intro impI allI, (erule conjE)+)
    fix cs as ic ia t
    assume "reach TMS2Var cs" and "reach TMS2 as"
    and "cs = as"
    and "pre TMS2Var t (Internal ic) cs"
    and "step_correspondence cs t ic = Some ia"
    from this(3) and this(4) and this(5) and reachable_invariant[OF `reach TMS2Var cs`, where t = t and e = "Internal ic"]
    show "pre TMS2 t (Internal ia) as \<and> eff TMS2Var t (Internal ic) cs = eff TMS2 t (Internal ia) as"
      apply (cases ic)
      apply (simp_all add: step_correspondence_def)
      apply (simp_all add: sim_simps TMS2Var.txn_inv_def)
      apply (drule_tac t = ia in sym)
      apply simp
      apply (simp add: someI_ex update_status_def)
      defer
      apply (drule_tac t = ia in sym)
      apply simp
      apply (simp add: update_partial_def update_status_def update_write_set_def)
      apply (drule_tac t = ia in sym)
      apply simp
      apply (intro conjI)
      apply (intro impI)
      apply (intro conjI)
      apply (simp add: max_index_def read_consistent_def store_at_def valid_index_def)
      apply (simp add: TMS2Var.unfold_updates TMS2.unfold_updates when_fn_def update_partial_def value_for_def)
      by (simp add: TMS2Var.unfold_updates TMS2.unfold_updates when_fn_def update_partial_def value_for_def)
  qed
qed

end