theory TMLCorrect
imports TML TMS2
begin

definition 
step_correspondence :: "(T, lstate, gstate) state \<Rightarrow> T \<Rightarrow> TML.pc \<Rightarrow> internal_action option"
  where
  "step_correspondence cs t a \<equiv>
    case a of
        Read1 \<Rightarrow> if lvar loc cs t = glb (global cs)
                then Some (DoRead (lvar addr cs t) (write_count (lvar loc cs t)))
                else None
      | Write5 \<Rightarrow> if lvar loc cs t = glb (global cs) then Some (DoWrite (lvar addr cs t) (lvar val cs t)) else None
      | Commit1 \<Rightarrow> if even (lvar loc cs t)
                  then Some DoCommitReadOnly
                  else None
      | Commit2 \<Rightarrow> Some DoCommitWriter
      | _ \<Rightarrow> None"

lemma scf1: "step_correspondence cs t ia = Some (DoRead l n) \<longleftrightarrow>
  (ia = Read1 \<and> l = lvar addr cs t \<and> n = write_count (lvar loc cs t) \<and> lvar loc cs t = glb (global cs))"
  by (cases ia) (auto simp add: step_correspondence_def)

lemma scf2: "step_correspondence cs t ia = Some (DoWrite l v) \<longleftrightarrow>
  (ia = Write5 \<and> l = lvar addr cs t \<and> v = lvar val cs t \<and> lvar loc cs t = glb (global cs))"
  by (cases ia) (auto simp add: step_correspondence_def)

lemma scf3: "step_correspondence cs t ia = Some DoCommitReadOnly \<longleftrightarrow> (ia = Commit1 \<and> even (lvar loc cs t))"
  by (cases ia) (auto simp add: step_correspondence_def)

lemma scf4: "step_correspondence cs t ia = Some DoCommitWriter \<longleftrightarrow> ia = Commit2"
  by (cases ia) (auto simp add: step_correspondence_def)

lemmas scf_simps = scf1 scf2 scf3 scf4

lemma scf_None: "step_correspondence cs t ia = None \<longleftrightarrow>
  ((lvar loc cs t \<noteq> glb (global cs) \<and> ia = Read1) \<or> (lvar loc cs t \<noteq> glb (global cs) \<and> ia = Write5)
  \<or> ia = Read2 \<or> ia = Write1 \<or> ia = Write2 \<or> ia = Write4 \<or> (ia = Commit1 \<and> (odd (lvar loc cs t))) \<or> ia = Begin1 \<or> ia = Begin2)"
  by (cases ia) (auto simp add: step_correspondence_def)

type_synonym tml_state = "(T, lstate, gstate) state"

type_synonym tms2_state = "(T, Local, stores_state) state"


definition writes :: "tml_state \<Rightarrow> tms2_state \<Rightarrow> L \<Rightarrow> V option"
  where
  "writes cs0 as0 \<equiv>
    case (the_writer cs0) of Some w \<Rightarrow> lvar write_set as0 w | None \<Rightarrow> Map.empty"

lemma the_writer_Some: "the_writer cs = Some t \<Longrightarrow> writes cs as = lvar write_set as t"
  by (auto simp add: writes_def)

lemma the_writer_None: "the_writer cs = None \<Longrightarrow> writes cs as = Map.empty"
  by (auto simp add: writes_def)

definition global_rel :: "tml_state \<Rightarrow> tms2_state \<Rightarrow> bool"
  where
  "global_rel cs0 as0 \<equiv>
     gvar store cs0 = apply_partial (gvar latest_store as0) (writes cs0 as0)
     \<and>
     write_count (gvar glb cs0) = gvar max_index as0"

definition validity_prop :: "tml_state \<Rightarrow> tms2_state \<Rightarrow> T \<Rightarrow> bool"
  where
  "validity_prop cs as t \<equiv>
          lvar begin_index as t \<le> write_count (lvar loc cs t)
        \<and> write_count (lvar loc cs t) \<le> gvar max_index as (* Added for hybrid *)
        \<and> (read_consistent (gvar store_at as (write_count (lvar loc cs t))) (lvar read_set as t))"

definition in_flight :: "tml_state \<Rightarrow> tms2_state \<Rightarrow> T \<Rightarrow> bool"
  where
  "in_flight cs as t \<equiv>
      (even (lvar loc cs t) \<longleftrightarrow> lvar write_set as t = Map.empty)
      \<and> validity_prop cs as t
      \<and> (odd (lvar loc cs t) \<longrightarrow> the_writer cs = Some t \<and> gvar glb cs = lvar loc cs t)" (* Had to add this *)

definition txn_rel :: "TML.pc event \<Rightarrow> tml_state \<Rightarrow> tms2_state \<Rightarrow> T \<Rightarrow> bool"
  where
  "txn_rel e cs0 as0 t \<equiv>
    tml_pre e (view t cs0) \<longrightarrow>
   (case e of
      External BeginInv \<Rightarrow> lvar status as0 t = NotStarted
      |
      Internal Begin1 \<Rightarrow>   lvar status as0 t = BeginResponding
      |
      Internal Begin2 \<Rightarrow>   lvar status as0 t = BeginResponding
                         \<and> (lvar begin_index as0 t \<le> write_count (lvar loc cs0 t))
      |
      External BeginResp \<Rightarrow> lvar status as0 t = BeginResponding
                         \<and> (lvar begin_index as0 t \<le> write_count (lvar loc cs0 t))
      |
      Internal Read1 \<Rightarrow>   lvar status as0 t = Pending (ReadPending (lvar addr cs0 t))
                        \<and> in_flight cs0 as0 t
      |
      Internal Read2 \<Rightarrow> ((lvar status as0 t = ReadResponding (lvar val cs0 t))
                         \<or>
                         (lvar status as0 t = Pending(ReadPending (lvar addr cs0 t))
                                             \<and> lvar loc cs0 t \<noteq> gvar glb cs0))
                        \<and> in_flight cs0 as0 t
      |
      External (ReadResp v) \<Rightarrow>   lvar status as0 t = ReadResponding (lvar val cs0 t)
                        \<and> in_flight cs0 as0 t
      |
      Internal Write1 \<Rightarrow>   lvar status as0 t = Pending (WritePending (lvar addr cs0 t) (lvar val cs0 t))
                        \<and> in_flight cs0 as0 t
      |
      Internal Write2 \<Rightarrow>   lvar status as0 t = Pending (WritePending (lvar addr cs0 t) (lvar val cs0 t))
                         \<and> lvar write_set as0 t = Map.empty
                         \<and> validity_prop cs0 as0 t
      |
      Internal Write4 \<Rightarrow> lvar status as0 t = Pending (WritePending (lvar addr cs0 t) (lvar val cs0 t))
                         \<and> validity_prop cs0 as0 t
      |
      Internal Write5 \<Rightarrow> lvar status as0 t = Pending (WritePending (lvar addr cs0 t) (lvar val cs0 t))
                        \<and> validity_prop cs0 as0 t
      |
      External WriteResp \<Rightarrow> lvar status as0 t = WriteResponding
                        \<and> validity_prop cs0 as0 t
      |
      Internal Commit1 \<Rightarrow>  lvar status as0 t = Pending CommitPending
                        \<and> in_flight cs0 as0 t
      |
      Internal Commit2 \<Rightarrow> lvar status as0 t = Pending CommitPending
                        \<and> in_flight cs0 as0 t
      |
      External CommitResp \<Rightarrow> lvar status as0 t = CommitResponding
      |
      External Abort \<Rightarrow> lvar status as0 t \<notin> {NotStarted, Ready, CommitResponding, Committed, Aborted}
      |
      External Cancel \<Rightarrow> False
      |
      _ \<Rightarrow> (  lvar status as0 t = Ready
            \<and> in_flight cs0 as0 t))"

lemma latest_store1[simp]: "latest_store (fst ag, snd ag @ [X]) = X"
  by (simp add: latest_store_def store_at_def max_index_def)

lemma store_wb: "n \<le> max_index g \<Longrightarrow> store_at (write_back ws g) n = store_at g n"
  by (cases n) (simp_all add: write_back_def store_at_def max_index_def nth_append)

definition sim_rel :: "tml_state \<Rightarrow> tms2_state \<Rightarrow> bool"
  where
  "sim_rel cs as \<equiv> global_rel cs as \<and> (\<forall> a t. txn_rel a cs as t)"

lemma tml_lpre: "local_pre (interference \<rhd> TML) = local_tml_pre"
  by (rule ext)+ (simp add: local_tml_pre_def local_pre_def TML_def Rely_def)

lemma tml_leff: "local_eff (interference \<rhd> TML) = local_tml_eff"
  by (rule ext)+ (simp add: local_tml_eff_def local_eff_def TML_def Rely_def)

lemma txn_rel_self_preserved_internal_step:
  assumes "TML.global_inv cs0"
  and "TML.txn_inv at (Internal pc) cs0"
  and "global_rel cs0 as0"
  and "txn_rel (Internal pc) cs0 as0 at"
  and "local_tml_pre at (Internal pc) cs0"
  and "step_correspondence cs0 at pc = Some ai"
  shows "txn_rel b (local_tml_eff at (Internal pc) cs0) (local_tms_eff at (Internal ai) as0) at"
  using assms
  apply (cases ai; simp add: scf_simps; (elim conjE)?; rule_tac b = b in TML.Event_split; simp add: txn_rel_def)

  apply (simp_all add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def cong del: if_weak_cong)
  apply (simp_all add: when_fn_def value_for_def value_at_def validity_prop_def update_partial_def apply_partial_def locality_tml)

  apply (cases "even (glb (snd cs0))")
    apply (subgoal_tac "write_set (fst as0 at) (addr (fst cs0 at)) = None")
    apply (simp add: read_consistent_def value_at_def)
    apply (subst the_writer_None)
    apply (simp add: writes_def the_writer_def locality_tml)
    apply blast
    apply (simp add: latest_store_def)
    apply simp

    apply simp
    apply (intro conjI impI)
    apply (subgoal_tac "write_set (fst as0 at) (addr (fst cs0 at)) = None")
    apply (subst the_writer_Some[where t = at])
    apply blast
    apply (simp add: locality_tml value_at_def latest_store_def)
    apply blast
    apply (subgoal_tac "write_set (fst as0 at) (addr (fst cs0 at)) = None")
    apply (simp add: read_consistent_def value_at_def)
    apply blast
    apply (simp add: the_writer_def locality_tml)
    apply (simp add: dom_def)
    apply auto[1]
   apply (subst the_writer_Some[where t = at])
    apply blast
    apply (simp add: the_writer_def locality_tml)
    apply auto[1]
   apply (simp add: the_writer_def locality_tml)
    by auto[1]

(* Simp rules for Txn Rel Other Preserved Internal Step (TROPIS for short) *)
lemmas TROPIS_simps =
  global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def le_Suc_eq store_wb validity_prop_def the_writer_def max_index_wb write_count_def read_consistent_def

lemma txn_rel_other_preserved_internal_step:
  assumes "TML.global_inv cs0"
  and "TML.txn_inv at (Internal pc) cs0"
  and "global_rel cs0 as0"
  and "txn_rel (Internal pc) cs0 as0 at"
  and "local_tml_pre at (Internal pc) cs0"
  and "t \<noteq> at"
  and "TML.txn_inv t b cs0"
  and "txn_rel b cs0 as0 t"
  and "step_correspondence cs0 at pc = Some ai"
  shows "txn_rel b (local_tml_eff at (Internal pc) cs0) (local_tms_eff at (Internal ai) as0) t"
  using assms
  apply (cases ai; simp add: scf_simps; (elim conjE)?; rule_tac b = b in TML.Event_split; simp add: txn_rel_def)
  apply ((simp add: TROPIS_simps cong del: if_weak_cong; blast)+)[4]
  apply (meson option.inject)
  apply ((simp add: TROPIS_simps cong del: if_weak_cong; blast)+)[7]
  apply (simp add: TROPIS_simps cong del: if_weak_cong; meson option.inject)
  apply (simp add: TROPIS_simps cong del: if_weak_cong; meson option.inject)
  apply ((simp add: TROPIS_simps cong del: if_weak_cong; metis (no_types, lifting) option.inject)+)[6]
  apply (simp add: TROPIS_simps cong del: if_weak_cong; metis)
  apply (simp add: TROPIS_simps cong del: if_weak_cong; metis)
  apply (simp add: TROPIS_simps cong del: if_weak_cong; metis (no_types, lifting) option.inject)
  apply (simp add: TROPIS_simps cong del: if_weak_cong; metis (no_types, lifting) option.inject)
  apply (simp add: TROPIS_simps cong del: if_weak_cong)
  apply (smt case_optionE option.simps(4) option.simps(5))
  apply (simp add: TROPIS_simps cong del: if_weak_cong; metis)
  apply ((simp add: TROPIS_simps cong del: if_weak_cong; metis)+)[8]
  apply ((simp add: TROPIS_simps cong del: if_weak_cong; metis (no_types, lifting) less_SucI nat_less_le)+)[14]
  apply (simp add: local_tml_eff_def local_tms_eff_def locality_tms2(4) localize_def lvar_def tml_pre_def view_def)
  apply (simp add: local_tml_eff_def local_tms_eff_def locality_tms2(4) localize_def lvar_def tml_pre_def view_def)
  apply (simp add: local_tml_eff_def local_tms_eff_def locality_tms2(4) localize_def lvar_def tml_pre_def view_def)
  apply (simp add: local_tml_eff_def local_tms_eff_def locality_tms2(4) localize_def lvar_def tml_pre_def view_def)
  apply (simp add: local_tml_eff_def local_tms_eff_def locality_tms2(4) localize_def lvar_def tml_pre_def view_def)
  apply (simp add: TROPIS_simps cong del: if_weak_cong)
  apply (smt case_optionE option.simps(4) option.simps(5) snd_apfst snd_conv)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_tml; blast?)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_tml; blast?)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_tml; blast?)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_tml; blast?)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_tml; blast?)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_tml; blast?)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_tml; blast?)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_tml; blast?)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_tml; blast?)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_tml; blast?)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_tml; blast?)
  apply (intro impI conjI; simp add: the_writer_def locality_tml; blast?)
  apply (simp add: tml_simps locality_tms2 local_tml_eff_def)+
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_tml; blast?)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_tml; blast?)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_tml; blast?)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_tml; blast?)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_tml; blast?)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_tml; blast?)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_tml; blast?)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_tml; blast?)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_tml; blast?)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_tml; blast?)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_tml; blast?)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply auto[1]
  apply (intro impI conjI; simp add: the_writer_def locality_tml; blast?)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (simp add: global_rel_def tml_simps locality_tml unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_tml; blast?)
  apply (simp add: tml_simps locality_tms2 local_tml_eff_def)+
  done


lemma same_writer_ap: "the_writer cs = the_writer cs' \<Longrightarrow> apply_partial ls (writes cs as) = apply_partial ls (writes cs' as)"
  by (rule ext) (simp add: writes_def)    
    

lemma become_writer: "the_writer cs = None \<Longrightarrow> the_writer cs' = Some t \<Longrightarrow> lvar write_set as t = Map.empty \<Longrightarrow> writes cs as = writes cs' as"
  by (rule ext) (simp add: writes_def)

lemma become_writer_ap: "the_writer cs = None \<Longrightarrow> the_writer cs' = Some t \<Longrightarrow> lvar write_set as t = Map.empty \<Longrightarrow> apply_partial ls (writes cs as) = apply_partial ls (writes cs' as)"
  by (rule ext) (simp add: apply_partial_def writes_def)    
    
lemma stutter_global_rel:
  assumes "global_inv cs"
  and "TML.txn_inv at (Internal ic) cs"
  and "global_rel cs as"
  and "txn_rel (Internal ic) cs as at"
  and "local_tml_pre at (Internal ic) cs"
  and "step_correspondence cs at ic = None"
  shows "global_rel (local_tml_eff at (Internal ic) cs) as"
  using assms
  apply -
  apply (simp add: scf_None; elim disjE; simp add: global_rel_def)
  apply (simp add: tml_simps locality_tml txn_rel_def cong del: if_weak_cong)
  apply (rule same_writer_ap)
  apply (simp add: the_writer_def locality_tml)
  apply (simp add: tml_simps locality_tml txn_rel_def cong del: if_weak_cong)
  apply (simp add: tml_simps locality_tml txn_rel_def cong del: if_weak_cong; intro conjI impI)
  apply (rule same_writer_ap)
  apply (simp add: the_writer_def locality_tml)
  apply (simp add: the_writer_def locality_tml)
  apply (rule same_writer_ap)
  apply (simp add: the_writer_def locality_tml)
  apply (simp add: the_writer_def locality_tml)
  apply (simp add: tml_simps locality_tml txn_rel_def cong del: if_weak_cong; intro conjI impI)
  apply (rule same_writer_ap)
  apply (simp add: the_writer_def locality_tml)
  apply (rule same_writer_ap)
  apply (simp add: the_writer_def locality_tml)
  apply (simp add: tml_simps locality_tml txn_rel_def cong del: if_weak_cong; intro conjI impI)
  apply (simp add: tml_simps locality_tml txn_rel_def cong del: if_weak_cong; intro conjI impI)
  apply (rule become_writer_ap[where t = at])
  apply (simp add: the_writer_def locality_tml)
  apply blast
  apply (simp add: the_writer_def locality_tml)
  apply (simp add: the_writer_def locality_tml)
  apply (simp add: write_count_def)
  apply (rule same_writer_ap)
  apply (simp add: the_writer_def locality_tml)
  apply (simp add: tml_simps locality_tml txn_rel_def cong del: if_weak_cong)
  apply (rule same_writer_ap)
  apply (simp add: the_writer_def locality_tml)
  apply (simp add: tml_simps locality_tml txn_rel_def cong del: if_weak_cong)
  apply (rule same_writer_ap)
  apply (simp add: the_writer_def locality_tml)
  apply (simp add: tml_simps locality_tml txn_rel_def cong del: if_weak_cong)
  apply (rule same_writer_ap)
  apply (simp add: the_writer_def locality_tml)
  apply (simp add: tml_simps locality_tml txn_rel_def cong del: if_weak_cong; intro conjI impI)
  apply (rule same_writer_ap)
  apply (simp add: the_writer_def locality_tml)
  apply (rule same_writer_ap)
  by (simp add: the_writer_def locality_tml)
    
lemma txn_rel_self_preserved_stutter:
  assumes "TML.global_inv cs0"
  and "TML.txn_inv at (Internal pc) cs0"
  and "\<forall>c. TMS2.txn_inv at c as0"
  and "global_rel cs0 as0"
  and "txn_rel (Internal pc) cs0 as0 at"
  and "local_tml_pre at (Internal pc) cs0"
  and "step_correspondence cs0 at pc = None"
  shows "txn_rel b (local_tml_eff at (Internal pc) cs0) as0 at"
  using assms
  apply (simp only: scf_None; elim disjE; rule_tac b = b in TML.Event_split; simp add: txn_rel_def)
  apply (simp_all add: locality_tml tml_simps in_flight_def validity_prop_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_tml)
  apply force
  apply auto[1]
  apply (metis even_Suc_div_two write_count_def)
  apply (metis (full_types) even_Suc_div_two write_count_def)
  apply (simp add: the_writer_def locality_tml) 
  apply (metis theI')
  apply (simp add: write_count_def global_rel_def gvar_def global_def)
  apply (erule_tac x = "External BeginResp" in allE)
  by (simp add: TMS2.txn_inv_def unfold_tms2 locality_tms2 ext_enabled_def)
  
lemma txn_rel_other_preserved_stutter:
  assumes "TML.global_inv cs0"
  and "TML.txn_inv at (Internal pc) cs0"
  and "global_rel cs0 as0"
  and "txn_rel (Internal pc) cs0 as0 at"
  and "local_tml_pre at (Internal pc) cs0"
  and "t \<noteq> at"
  and "txn_rel b cs0 as0 t"
  and "TML.txn_inv t b cs0"
  and "step_correspondence cs0 at pc = None"
  shows "txn_rel b (local_tml_eff at (Internal pc) cs0) as0 t" 
  using assms
  apply (simp only: scf_None; elim disjE; rule_tac b = b in TML.Event_split; simp add: txn_rel_def)
  apply (simp_all add: locality_tml tml_simps in_flight_def validity_prop_def cong del: if_weak_cong)
  apply ((intro impI conjI; simp add: the_writer_def locality_tml; auto)+)[10]
  apply ((intro impI conjI; simp add: the_writer_def locality_tml; auto)+)[10]
  apply ((intro impI conjI; simp add: the_writer_def locality_tml; auto)+)[10]
  apply ((intro impI conjI; simp add: the_writer_def locality_tml; auto)+)[10]
  apply ((intro impI conjI; simp add: the_writer_def locality_tml; auto)+)[10]
  apply ((intro impI conjI; simp add: the_writer_def locality_tml; auto)+)[10]
  apply ((intro impI conjI; simp add: the_writer_def locality_tml; auto)+)[10]
  by ((intro impI conjI; simp add: the_writer_def locality_tml; auto)+)[6]

lemma internal_tms_pre:
  assumes "global_inv cs"
  and "TML.txn_inv at (Internal ic) cs"
  and "TMS2.txn_inv at (Internal ia) as"
  and "global_rel cs as"
  and "txn_rel (Internal ic) cs as at"
  and "local_tml_pre at (Internal ic) cs"
  and "step_correspondence cs at ic = Some ia"
  shows "local_tms_pre at (Internal ia) as"
  using assms
  apply -
  apply (cases ia; simp add: scf_simps; (elim conjE)?)
  apply (simp add: tml_simps unfold_tms2 locality_tml local_tms_pre_def)
  apply (simp add: latest_store_def write_count_def tml_simps unfold_tms2 locality_tml txn_rel_def global_rel_def TMS2.txn_inv_def in_flight_def validity_prop_def local_tms_pre_def)
  apply (simp add: local_tms_pre_def unfold_tms2)
  apply (simp add: latest_store_def write_count_def tml_simps unfold_tms2 locality_tml txn_rel_def global_rel_def TMS2.txn_inv_def in_flight_def validity_prop_def local_tms_pre_def)
  apply blast
  apply (simp add: latest_store_def write_count_def tml_simps unfold_tms2 locality_tml txn_rel_def global_rel_def TMS2.txn_inv_def in_flight_def validity_prop_def local_tms_pre_def)
  by (simp add: latest_store_def write_count_def tml_simps unfold_tms2 locality_tml txn_rel_def global_rel_def TMS2.txn_inv_def in_flight_def validity_prop_def local_tms_pre_def)

lemma writers_ap: "(the_writer cs = None \<and> the_writer cs' = None) \<or> (\<exists>t. the_writer cs = Some t \<and> the_writer cs' = Some t \<and> lvar write_set as t = lvar write_set as' t) \<Longrightarrow> apply_partial ls (writes cs as) = apply_partial ls (writes cs' as')"
  using writes_def by auto
    
lemma writes_update: "the_writer cs = Some t \<Longrightarrow> the_writer cs' = Some t \<Longrightarrow> (lvar write_set as t)(l \<mapsto> v) = lvar write_set as' t \<Longrightarrow> (apply_partial ls (writes cs as)) (l := v) = apply_partial ls (writes cs' as')"
  by (simp add: writes_def apply_partial_simp[symmetric])
    
lemma internal_global_rel:
  assumes "global_inv cs"
  and "TML.txn_inv at (Internal ic) cs"
  and "TMS2.txn_inv at (Internal ia) as" 
  and "global_rel cs as"
  and "txn_rel (Internal ic) cs as at"
  and "local_tml_pre at (Internal ic) cs"
  and "step_correspondence cs at ic = Some ia"
  shows "global_rel (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as)"
  using assms
  apply -
  apply (cases ia; simp add: scf_simps; (elim conjE)?)
  apply (simp_all add: global_rel_def)
  apply (simp_all add: locality_tml tml_simps unfold_tms2 txn_rel_def local_tms_eff_def TMS2.txn_inv_def write_count_def max_index_wb dom_def cong del: if_weak_cong)
  apply (rule writers_ap)
  apply (cases "\<exists>t. t \<noteq> at \<and> writer (fst cs t)")
  apply (rule disjI2)
  apply (simp add: the_writer_def locality_tml)
  apply blast
  apply (rule disjI1)
  apply (simp add: the_writer_def locality_tml)
  apply auto[1]
  apply (subst the_writer_Some[where t = at])
  apply (simp add: the_writer_def locality_tml)
  apply (meson the_equality)
  apply (subst the_writer_None)
  apply (simp add: the_writer_def locality_tml)
  apply blast
  apply (simp add: locality_tml write_back_def)
  apply auto[1]
  apply (intro impI conjI)
  apply (rule writers_ap)
  apply (simp add: the_writer_def locality_tml)
  apply (rule writers_ap)
  apply (simp add: the_writer_def locality_tml)
  apply (rule writes_update[where t = at])
  apply (simp add: the_writer_def locality_tml)
  apply (meson the_equality)
  apply (simp add: the_writer_def locality_tml)
  apply (meson the_equality)
  by (simp add: the_writer_def locality_tml update_partial_def)
    
lemma tml_sim_int_step:
  "standard_sim_int_step (interference \<rhd> TML) (tms2_interference \<rhd> TMS2) step_correspondence sim_rel"
  apply (simp only: standard_sim_int_step_def sim_rel_def tml_leff tml_lpre abs_leff abs_lpre)
  apply (intro conjI impI allI; elim conjE)
  using reachable_invariant_tms2_interference tml_reach_inv
  apply -
  prefer 3
  apply (metis txn_rel_other_preserved_internal_step txn_rel_self_preserved_internal_step)
  using internal_tms_pre apply blast
  by (simp add: internal_global_rel)
    
lemma stutter_txn_rel:
  assumes "global_inv cs"
  and "TML.txn_inv at (Internal ic) cs"
  and "TML.txn_inv t a cs"
  and "global_rel cs as"
  and "txn_rel (Internal ic) cs as at"
  and "txn_rel a cs as t"
  and "\<forall>c. TMS2.txn_inv at c as"
  and "local_tml_pre at (Internal ic) cs"
  and "step_correspondence cs at ic = None"
  shows "txn_rel a (local_tml_eff at (Internal ic) cs) as t"
  using assms
  apply (cases "at = t")
  apply simp
  apply (rule txn_rel_self_preserved_stutter; blast)
  apply (rule txn_rel_other_preserved_stutter; blast)
  done

lemma tml_sim_stutter:
  "standard_sim_stutter (interference \<rhd> TML) (tms2_interference \<rhd> TMS2) step_correspondence sim_rel"
  apply (simp only: standard_sim_stutter_def sim_rel_def tml_leff tml_lpre abs_leff abs_lpre)
  apply (intro conjI impI allI; elim conjE)
  using reachable_invariant_tms2_interference tml_reach_inv
  apply -
  apply (simp add: stutter_global_rel)
  using stutter_txn_rel apply auto[1]
  done
lemma external_validity_prop_other:
  assumes "global_inv cs"
  and "TML.txn_inv at (External e) cs"
  and "TMS2.txn_inv at (External e) as"
  and "global_rel cs as"
  and "validity_prop cs as t"
  and "local_tml_pre at (External e) cs"
  and "local_tms_pre at (External e) as"
  and "at \<noteq> t"
  shows "validity_prop (local_tml_eff at (External e) cs) (local_tms_eff at (External e) as) t"
  using assms
  by (cases e) (simp_all add: validity_prop_def max_index_wb tml_simps locality_tml local_tms_eff_def unfold_tms2)
    
lemma if_Some: "(if B then Some a else None) = Some c \<longleftrightarrow> B \<and> a = c"
  by simp
    
lemma external_in_flight_other:
  assumes "global_inv cs"
  and "TML.txn_inv at (External e) cs"
  and "TMS2.txn_inv at (External e) as"
  and "global_rel cs as"
  and "in_flight cs as t"
  and "local_tml_pre at (External e) cs"
  and "local_tms_pre at (External e) as"
  and "at \<noteq> t"
  shows "in_flight (local_tml_eff at (External e) cs) (local_tms_eff at (External e) as) t"
  using assms
  apply (cases e; simp add: in_flight_def; intro impI conjI external_validity_prop_other; blast?)
  by (simp_all add: in_flight_def max_index_wb tml_simps locality_tml local_tms_eff_def unfold_tms2 the_writer_def if_Some)

lemma external_txn_rel_other:
  assumes "global_inv cs"
  and "TML.txn_inv at (External e) cs"
  and "TML.txn_inv t a cs"
  and "TMS2.txn_inv at (External e) as"
  and "global_rel cs as"
  and "txn_rel (External e) cs as at"
  and "txn_rel a cs as t"
  and "local_tml_pre at (External e) cs"
  and "local_tms_pre at (External e) as"
  and "at \<noteq> t"
  shows "txn_rel a (local_tml_eff at (External e) cs) (local_tms_eff at (External e) as) t"
  using assms
  apply (rule_tac b = a in TML.Event_split; cases e; simp add: txn_rel_def;
         (intro impI conjI external_in_flight_other external_validity_prop_other)?; blast?)
  by (simp_all add: tml_simps locality_tml local_tms_eff_def unfold_tms2)
    
definition even_ws_empty :: "tml_state \<Rightarrow> tms2_state \<Rightarrow> T \<Rightarrow> bool" where
  "even_ws_empty cs as t \<equiv> even (lvar loc cs t) \<longleftrightarrow> lvar write_set as t = Map.empty"

definition odd_is_writer :: "tml_state \<Rightarrow> tms2_state \<Rightarrow> T \<Rightarrow> bool" where
  "odd_is_writer cs as t \<equiv> odd (lvar loc cs t) \<longrightarrow> the_writer cs = Some t \<and> lvar loc cs t = gvar glb cs"
  
lemma external_even_ws_empty_self:
  assumes "global_inv cs"
  and "TML.txn_inv at (External e) cs"
  and "TMS2.txn_inv at (External e) as"
  and "global_rel cs as"
  and "even_ws_empty cs as at"
  and "local_tml_pre at (External e) cs"
  and "local_tms_pre at (External e) as"
  shows "even_ws_empty (local_tml_eff at (External e) cs) (local_tms_eff at (External e) as) at"
  using assms
  by (cases e) (simp_all add: even_ws_empty_def max_index_wb tml_simps locality_tml local_tms_eff_def unfold_tms2)

lemma external_odd_is_writer_self:
  assumes "global_inv cs"
  and "TML.txn_inv at (External e) cs"
  and "TMS2.txn_inv at (External e) as"
  and "global_rel cs as"
  and "odd_is_writer cs as at"
  and "local_tml_pre at (External e) cs"
  and "local_tms_pre at (External e) as"
  shows "odd_is_writer (local_tml_eff at (External e) cs) (local_tms_eff at (External e) as) at"
  using assms
  apply (cases e)
  by (simp_all add: odd_is_writer_def max_index_wb tml_simps locality_tml local_tms_eff_def unfold_tms2 the_writer_def locality_tml if_Some)

lemma external_validity_prop_self:
  assumes "global_inv cs"
  and "TML.txn_inv at (External e) cs"
  and "TMS2.txn_inv at (External e) as"
  and "global_rel cs as"
  and "validity_prop cs as at"
  and "local_tml_pre at (External e) cs"
  and "local_tms_pre at (External e) as"
  and "e \<noteq> BeginInv"
  shows "validity_prop (local_tml_eff at (External e) cs) (local_tms_eff at (External e) as) at"
  using assms
  by (cases e) (simp_all add: validity_prop_def max_index_wb tml_simps locality_tml local_tms_eff_def unfold_tms2)
    
lemma in_flight_var: "in_flight cs as t \<longleftrightarrow> even_ws_empty cs as t \<and> validity_prop cs as t \<and> odd_is_writer cs as t"
  apply (simp add: in_flight_def even_ws_empty_def odd_is_writer_def)
  by linarith

lemma external_in_flight_self:
  assumes "global_inv cs"
  and "TML.txn_inv at (External e) cs"
  and "TMS2.txn_inv at (External e) as"
  and "global_rel cs as"
  and "in_flight cs as at"
  and "local_tml_pre at (External e) cs"
  and "local_tms_pre at (External e) as"
  and "e \<noteq> BeginInv"
  shows "in_flight (local_tml_eff at (External e) cs) (local_tms_eff at (External e) as) at"
  using assms
  apply -
  by (cases e; simp add: in_flight_var; intro conjI external_validity_prop_self external_odd_is_writer_self external_even_ws_empty_self; blast?)

lemma external_txn_rel_self:
  assumes "global_inv cs"
  and "TML.txn_inv at (External e) cs"
  and inv2: "TML.txn_inv at a cs"
  and "TMS2.txn_inv at (External e) as"
  and "global_rel cs as"
  and "txn_rel (External e) cs as at"
  and "local_tml_pre at (External e) cs"
  and "local_tms_pre at (External e) as"
  shows "txn_rel a (local_tml_eff at (External e) cs) (local_tms_eff at (External e) as) at"
  using assms(1) assms(2) assms(4) assms(5) assms(6) assms(7) assms(8)
  apply -
  apply (cases e; rule_tac b = a in TML.Event_split; simp add: txn_rel_def;
         (intro impI conjI external_in_flight_self)?; blast?)
  apply (simp_all add: tml_simps locality_tml local_tms_eff_def unfold_tms2 in_flight_def local_tms_pre_def global_rel_def TMS2.txn_inv_def)
  apply (simp add: global_rel_def locality_tml write_count_def)
  apply (simp_all add: validity_prop_def locality_tml write_count_def read_consistent_def)
  using div_le_mono apply linarith+
  using inv2
  apply (simp add: tml_simps locality_tml the_writer_def)
  apply (meson the_equality)
  apply (simp add: tml_simps locality_tml the_writer_def)
  apply (meson the_equality)
  apply (simp add: tml_simps locality_tml the_writer_def)
  by (meson the_equality)

lemma external_txn_rel:
  assumes "global_inv cs"
  and "TML.txn_inv at (External e) cs"
  and "TML.txn_inv t a cs"
  and "TMS2.txn_inv at (External e) as"
  and "global_rel cs as"
  and "txn_rel (External e) cs as at"
  and "txn_rel a cs as t"
  and "local_tml_pre at (External e) cs"
  and "local_tms_pre at (External e) as"
  shows "txn_rel a (local_tml_eff at (External e) cs) (local_tms_eff at (External e) as) t"
  using assms external_txn_rel_other external_txn_rel_self by blast

lemma external_tms_pre:
  assumes "global_inv cs"
  and "TML.txn_inv at (External e) cs"
  and "TMS2.txn_inv at (External e) as"
  and "global_rel cs as"
  and "txn_rel (External e) cs as at"
  and "local_tml_pre at (External e) cs"
  shows "local_tms_pre at (External e) as"
  using assms
  apply -
  apply (cases e)
  by (simp_all add: tml_simps locality_tml local_tms_pre_def unfold_tms2 txn_rel_def) 

lemma external_global_rel:
  assumes "global_inv cs"
  and "TML.txn_inv at (External e) cs"
  and "TMS2.txn_inv at (External e) as"
  and "global_rel cs as"
  and "txn_rel (External e) cs as at"
  and "local_tml_pre at (External e) cs"
  and "local_tms_pre at (External e) as"
  shows "global_rel (local_tml_eff at (External e) cs) (local_tms_eff at (External e) as)"
  using assms
  apply -
  apply (cases e)
  apply (simp_all add: tml_simps locality_tml local_tms_eff_def unfold_tms2 global_rel_def)
  apply (rule writers_ap, simp add: the_writer_def locality_tml unfold_tms2)+
  done
    
lemma tml_sim_ext_step:
  "standard_sim_ext_step (interference \<rhd> TML) (tms2_interference \<rhd> TMS2) sim_rel"
  apply (simp only: standard_sim_ext_step_def sim_rel_def tml_leff tml_lpre abs_leff abs_lpre)
  apply (intro conjI impI allI; elim conjE)
  using reachable_invariant_tms2_interference tml_reach_inv
  apply -
  using external_tms_pre apply blast
  using external_global_rel external_tms_pre apply blast
  apply (rule external_txn_rel; blast?)
  using external_tms_pre by blast

lemma TML_weak_simulation:
  "weak_standard_simulation (interference \<rhd> TML) (tms2_interference \<rhd> TMS2) step_correspondence sim_rel"
  unfolding weak_standard_simulation_def
  apply (simp only: tml_sim_int_step tml_sim_stutter tml_sim_ext_step)
  apply safe
  apply (rule_tac x = default_start in exI)
  apply (simp add: starts_def Rely_def image_def TML_def lstarts_def tms_starts_def default_start_def sim_rel_def
                   global_rel_def gstart_def locality_tml mem_initial_def initial_stores_def latest_store_def max_index_def
                   write_count_def writes_def the_writer_def)
  apply (intro allI)
  apply (rename_tac cs cg a t)
  apply (rule_tac b = a in TML.Event_split)
  by (simp_all add: txn_rel_def locality_tml tml_pre_def ext_enabled_def)

end