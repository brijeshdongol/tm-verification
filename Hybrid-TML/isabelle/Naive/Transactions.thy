theory Transactions
  imports Main RWMemoryList
begin

(* ================================================================================== *)
(* The type of transactions, T.                                                       *)
(* ================================================================================== *)

typedecl T

(*
consts is_writer :: "T \<Rightarrow> bool"

definition is_reader :: "T \<Rightarrow> bool" where
  "is_reader t \<equiv> \<not> (is_writer t)"
*)

(* ================================================================================== *)
(* The actions of a TM automata.                                                      *)
(* ================================================================================== *)

datatype action =
    BeginInv
  | BeginResp
  | CommitInv
  | CommitResp
  | Cancel
  | Abort
  | ReadInv L
  | ReadResp V
  | WriteInv L V
  | WriteResp

datatype 'i event =
    External action
  | Internal 'i

type_synonym 'i tm_event = "(T \<times> 'i event) + unit"

datatype hidden = Tau

type_synonym 't ioa_event = "('t \<times> hidden event) + unit"

datatype 'pc status =
    NotStarted
  | BeginResponding
  | WriteResponding
  | ReadResponding V
  | CommitResponding
  | AbortPending
  | Ready
  | Committed
  | Aborted
  | Pending 'pc

definition ext_enabled :: "'pc status \<Rightarrow> action \<Rightarrow> bool"
  where
  "ext_enabled s a \<equiv>
    (s = NotStarted \<and> a = BeginInv)
    \<or>
    (s = Ready \<and> (\<exists> l v. a = WriteInv l v))
    \<or>
    (s = Ready \<and> (\<exists> l. a = ReadInv l))
    \<or>
    (s = Ready \<and> a = CommitInv)
    \<or>
    (s = Ready \<and> a = Cancel)
    \<or>
    (s = BeginResponding \<and> a = BeginResp)
    \<or>
    (s = WriteResponding \<and> a = WriteResp)
    \<or>
    (s = CommitResponding \<and> a = CommitResp)
    \<or>
    (\<exists> v. s = ReadResponding v \<and> a = ReadResp v)
    \<or>
    (s \<notin> {NotStarted, Ready, CommitResponding, Committed, Aborted} \<and> a = Abort)"

end