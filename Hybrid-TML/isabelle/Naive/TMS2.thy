theory TMS2             
imports RGA Transactions "~~/src/HOL/Eisbach/Eisbach_Tools"
begin

datatype PC =
    CommitPending
  | ReadPending L
  | WritePending L V

record Local =
  status :: "PC status"
  begin_index :: "nat"
  read_set :: "L \<Rightarrow> V option"
  write_set :: "L \<Rightarrow> V option"

type_synonym tms2_state = "(T \<Rightarrow> Local) \<times> stores_state"

datatype internal_action =
    DoCommitReadOnly
  | DoCommitWriter
  | DoRead L nat
  | DoWrite L V

definition status_enabled :: "internal_action event \<Rightarrow> Local \<times> stores_state \<Rightarrow> bool"
  where
  "status_enabled e s \<equiv>
    case e of
      Internal a \<Rightarrow>
         (case a of
           DoRead l n \<Rightarrow> status (fst s) = Pending (ReadPending l)
           |
           DoCommitReadOnly \<Rightarrow> status (fst s) = Pending CommitPending
           |
           DoCommitWriter \<Rightarrow> status (fst s) = Pending CommitPending
           |
           DoWrite l v \<Rightarrow> status (fst s) = Pending (WritePending l v))
      |
      External a \<Rightarrow> ext_enabled (status (fst s)) a"

definition valid_index :: "Local \<times> stores_state \<Rightarrow> nat \<Rightarrow> bool"    
    where
    "valid_index s n \<equiv>
      begin_index (fst s) \<le> n \<and> n \<le> max_index (snd s) \<and>
      read_consistent (store_at (snd s) n) (read_set (fst s))"

definition tms_pre :: "internal_action event \<Rightarrow> Local \<times> stores_state \<Rightarrow> bool"
  where
  "tms_pre e s \<equiv>
      status_enabled e s
      \<and>
      (case e of
        Internal a \<Rightarrow>
           (case a of
             DoRead l n \<Rightarrow> (l : dom (write_set (fst s)) \<or> valid_index s n)
             |
             DoCommitReadOnly \<Rightarrow> write_set (fst s) = empty
             |
             DoCommitWriter \<Rightarrow> write_set (fst s) \<noteq> Map.empty \<and> read_consistent (latest_store (snd s)) (read_set (fst s))
             |
             DoWrite l v \<Rightarrow> True)
        |
        External a \<Rightarrow> True)"

definition update_status :: "PC status \<Rightarrow> Local \<times> stores_state \<Rightarrow> Local \<times> stores_state" where
  "update_status st \<equiv> apfst (\<lambda>l. l \<lparr> status := st\<rparr>)"

definition update_begin_index :: "nat \<Rightarrow> Local \<times> stores_state \<Rightarrow> Local \<times> stores_state" where
  "update_begin_index n \<equiv> apfst (\<lambda>l. l \<lparr> begin_index := n \<rparr> )"

definition update_write_set :: "L \<Rightarrow> V \<Rightarrow> Local \<times> stores_state \<Rightarrow> Local \<times> stores_state" where
  "update_write_set l v \<equiv> apfst (\<lambda>s . s \<lparr> write_set := update_partial l v (write_set s) \<rparr>)" 

definition update_read_set :: "L \<Rightarrow> V \<Rightarrow> Local \<times> stores_state \<Rightarrow> Local \<times> stores_state" where
  "update_read_set l v \<equiv> apfst (\<lambda>s . s \<lparr> read_set := update_partial l v (read_set s) \<rparr>)"  

definition ext_eff :: "action \<Rightarrow> Local \<times> stores_state \<Rightarrow> Local \<times> stores_state" where
  "ext_eff a \<equiv>
    case a of
        BeginInv \<Rightarrow> (\<lambda>s. (update_status BeginResponding \<circ>> update_begin_index (max_index (snd s))) s)
      | BeginResp \<Rightarrow> update_status Ready
      | CommitInv \<Rightarrow> update_status (Pending CommitPending)
      | CommitResp \<Rightarrow> update_status CommitResponding
      | ReadInv l \<Rightarrow> update_status (Pending (ReadPending l))
      | ReadResp v \<Rightarrow> update_status Ready
      | WriteInv l v \<Rightarrow> update_status (Pending (WritePending l v))
      | WriteResp \<Rightarrow> update_status Ready
      | Cancel \<Rightarrow> update_status AbortPending
      | Abort \<Rightarrow> update_status Aborted"

definition int_eff :: "internal_action \<Rightarrow> Local \<times> stores_state \<Rightarrow> Local \<times> stores_state"
  where
  "int_eff ia \<equiv>
    case ia of
        DoRead l n \<Rightarrow> (\<lambda>s.
          let v = (value_for (snd s) n (write_set (fst s)) l)
          in ((update_read_set l v) when (l \<notin> dom (write_set (fst s))) \<circ>> update_status (ReadResponding v)) s)
                         
      | DoWrite l v \<Rightarrow> update_write_set l v \<circ>> update_status WriteResponding
             
      | DoCommitReadOnly \<Rightarrow> update_status CommitResponding
      
      | DoCommitWriter \<Rightarrow> (\<lambda>s. (update_status CommitResponding \<circ>> apsnd (write_back (write_set (fst s)))) s)"
      
definition tms_eff :: "internal_action event \<Rightarrow> Local \<times> stores_state \<Rightarrow> Local \<times> stores_state"
  where
  "tms_eff e s \<equiv> case e of Internal i \<Rightarrow> int_eff i s | External a \<Rightarrow> ext_eff a s"

primrec tms_eff_var :: "internal_action event \<Rightarrow> Local \<times> stores_state \<Rightarrow> Local \<times> stores_state"
  where
  "tms_eff_var (Internal i) = int_eff i"
| "tms_eff_var (External a) = ext_eff a"

lemma tms_eff_var: "tms_eff = tms_eff_var"
proof (rule ext)+
  fix e s
  show "tms_eff e s = tms_eff_var e s"
    by (cases e) (simp add: tms_eff_def)+
qed

definition tms_starts :: "(T \<Rightarrow> Local) set"
  where
  "tms_starts \<equiv> {f. (\<forall>t. status (f t) = NotStarted \<and> read_set (f t) = empty \<and> write_set (f t) = empty \<and> begin_index (f t) = 0)}"

definition default_start :: "(T, Local, stores_state) state" where
  "default_start \<equiv> (\<lambda>t. \<lparr> status = NotStarted, begin_index = 0, read_set = empty, write_set = empty \<rparr>, initial_stores)"

definition TMS2  :: "(T, Local, stores_state, internal_action) RGA"
  where
  "TMS2 \<equiv> \<lparr> RGA.local_starts = tms_starts, RGA.global_start = initial_stores, RGA.pre = tms_pre, RGA.eff = tms_eff, RGA.environment = Id\<rparr>"
           
definition local_tms_eff :: "T \<Rightarrow> internal_action event \<Rightarrow> (T, Local, stores_state) state \<Rightarrow> (T, Local, stores_state) state" where
  "local_tms_eff t a s = localize t (tms_eff a) s"

definition local_tms_pre :: "T \<Rightarrow> internal_action event \<Rightarrow> (T, Local, stores_state) state \<Rightarrow> bool" where
  "local_tms_pre t a s = tms_pre a (view t s)"
                                  
declare TMS2_def [simp]

lemma Event_split:
  "\<lbrakk>b = External BeginInv \<Longrightarrow> P;
    b = External BeginResp \<Longrightarrow> P;
    \<And> l. b = External (ReadInv l) \<Longrightarrow> P;
    \<And> l n. b = Internal (DoRead l n) \<Longrightarrow> P;
    \<And> v. b = External (ReadResp v) \<Longrightarrow> P;
    \<And> l v. b = External (WriteInv l v) \<Longrightarrow> P;
    \<And> l v. b = Internal (DoWrite l v) \<Longrightarrow> P;
    b = External WriteResp \<Longrightarrow> P;
    b = External CommitInv \<Longrightarrow> P;
    b = Internal DoCommitWriter \<Longrightarrow> P;
    b = Internal DoCommitReadOnly \<Longrightarrow> P;
    b = External CommitResp \<Longrightarrow> P;
    b = External Cancel \<Longrightarrow> P;
    b = External Abort \<Longrightarrow> P \<rbrakk>
   \<Longrightarrow>
    P"
apply(cases rule: event.exhaust[where y=b])
apply(auto simp add: split: event.split)
using action.exhaust apply blast
using internal_action.exhaust by blast

lemmas unfold_updates =
  update_status_def
  update_write_set_def
  update_begin_index_def
  update_read_set_def
  valid_index_def

lemmas unfold_pre =
  tms_pre_def
  status_enabled_def

lemmas unfold_tms2 =
  tms_starts_def ext_eff_def int_eff_def tms_eff_def
  unfold_pre
  unfold_updates

lemmas unfold_all_tms2 =
  unfold_tms2
  initial_stores_def
  RWMemoryList.all_simps
  Utilities.all_utilities
  ext_enabled_def

lemmas locality_tms2 =
  local_tms_eff_def
  local_tms_pre_def
  view_def
  local_def
  localize_def
  global_def
  lvar_def

lemmas splits = event.split action.split internal_action.split if_splits

lemma begin_index_stable:
  "\<lbrakk>local_tms_pre at a s; a \<noteq> External BeginInv \<or> t \<noteq> at\<rbrakk> \<Longrightarrow> begin_index (local t (local_tms_eff at a s)) = begin_index (local t s)"
  by (auto simp add: unfold_all_tms2 locality_tms2 split: splits)

lemma max_index_stable:
  "\<lbrakk>local_tms_pre at a s; a \<noteq> Internal DoCommitWriter\<rbrakk> \<Longrightarrow> max_index (global (local_tms_eff at a s)) = max_index (global s)"
  by (auto simp add: unfold_all_tms2 locality_tms2 split: splits)

lemma stores_stable:
  "\<lbrakk>local_tms_pre at a s; a \<noteq> Internal DoCommitWriter\<rbrakk> \<Longrightarrow> global (local_tms_eff at a s) = global s"
  by (auto simp add: unfold_all_tms2 locality_tms2 split: splits)

definition vin_ex where
  "vin_ex t s \<equiv> (\<exists>I. valid_index (view t s) I)" 

lemma valid_index_stable:
  fixes s :: "(T, Local, stores_state) state"
  assumes "local_tms_pre t' a s"
  and "t \<noteq> t'"
  and "vin_ex t s"
  shows "vin_ex t (local_tms_eff t' a s)"
  using assms
  apply (simp add: vin_ex_def)
  apply (erule exE)
  apply (rule_tac x = I in exI)
  apply (rule_tac b = a in Event_split)
  apply (simp_all add: unfold_all_tms2 locality_tms2 option.case_eq_if)
  apply (rule_tac y = I in nat.exhaust)
  by (simp_all add: nth_append)

definition VIN_EX_PRE where
  "VIN_EX_PRE t t' a s \<equiv> local_tms_pre t' a s \<and> t \<noteq> t' \<and> vin_ex t s"

definition VIN_EX_NS where
  "VIN_EX_NS t t' a s \<equiv> vin_ex t (local_tms_eff t' a s)"

lemma vin_ex_nsI: "VIN_EX_NS t t' a s \<Longrightarrow> vin_ex t (local_tms_eff t' a s)"
  by (simp add: VIN_EX_NS_def)

lemma max_index_update:
  "\<lbrakk>local_tms_pre at (Internal DoCommitWriter) s\<rbrakk>
   \<Longrightarrow>
   max_index (global (local_tms_eff at (Internal DoCommitWriter) s)) = 1 + max_index (global s)"
  by (simp add: unfold_all_tms2 locality_tms2)

lemma store_at_stable:
  "\<lbrakk>local_tms_pre at a s; a \<noteq> Internal DoCommitWriter \<or> n \<le> max_index (global s)\<rbrakk> \<Longrightarrow>
   store_at (global (local_tms_eff at a s)) n = store_at (global s) n"
  by (auto simp add: unfold_all_tms2 locality_tms2 split: splits) (simp add: nth_append)

lemma read_set_stable:
  "\<lbrakk>local_tms_pre at a s; (\<forall> l n. a \<noteq> Internal (DoRead l n)) \<or> t \<noteq> at\<rbrakk> \<Longrightarrow>
   lvar read_set (local_tms_eff at a s) t = lvar read_set s t"
  by(auto simp add: unfold_all_tms2 locality_tms2 split: splits)

lemma read_set_valid:
  "\<lbrakk>l \<in> dom (lvar read_set s t); read_consistent (store_at (global s) n) (lvar read_set s t)\<rbrakk> \<Longrightarrow>
   lvar read_set s t l = Some (store_at (global s) n l)"
  by (auto elim: allE[where x = l] simp add: unfold_all_tms2)

(*
lemma read_set_valid_read_with_valid_index:
  "\<lbrakk>local_tms_pre at a s; l \<in> dom (lvar read_set s at);
    (l \<notin> dom (lvar write_set s at) \<and> a = Internal (DoRead l n)) \<or> a = Internal DoCommitReadOnly\<rbrakk> \<Longrightarrow>
   lvar read_set s at l = Some (store_at (global s) n l)"
  apply (auto simp add: unfold_all_tms2 locality_tms2 split: splits)
  apply (smt option.simps(5))
  by (smt option.simps(5))
*)

lemma read_set_valid_read_commit_writer:
  "\<lbrakk>local_tms_pre at (Internal DoCommitWriter) s; l \<in> dom (lvar read_set s at)\<rbrakk> \<Longrightarrow>
   lvar read_set s at l = Some (store_at (global s) (max_index (global s)) l)"
  by (auto simp add: read_set_valid unfold_all_tms2 locality_tms2 domD) (smt option.simps) (* FIXME: SMT *)

lemma read_set_stable_do_read:
  "\<lbrakk>local_tms_pre at (Internal (DoRead l n)) s;
    l \<in> (dom (lvar write_set s at)) \<or> l : (dom (lvar read_set s at))\<rbrakk> \<Longrightarrow>
   lvar read_set (local_tms_eff at (Internal (DoRead l n)) s) at = lvar read_set s at"
  by (simp add: unfold_all_tms2 locality_tms2 split: option.split) (smt domD domIff fun_upd_triv option.discI option.simps(5))

lemma tau_read_set_update:
  "\<lbrakk>local_tms_pre at (Internal (DoRead l n)) s; l \<notin> dom (lvar write_set s at)\<rbrakk> \<Longrightarrow>
   lvar read_set (local_tms_eff at (Internal (DoRead l n)) s) at = (lvar read_set s at) (l := Some (value_at (global s) n l))"
  by (auto simp add: unfold_all_tms2 locality_tms2 split: splits)

lemma ext_write_set_stable:
  "\<lbrakk>local_tms_pre at a s; (\<forall> l v. a \<noteq> Internal (DoWrite l v)) \<or> t \<noteq> at\<rbrakk>
   \<Longrightarrow>
   lvar write_set (local_tms_eff at a s) t = lvar write_set s t"
  by (auto simp add: unfold_all_tms2 locality_tms2 split: splits)

lemma tau_write_set_update:
  "\<lbrakk>local_tms_pre at (Internal (DoWrite l v)) s\<rbrakk> \<Longrightarrow>
   lvar write_set (local_tms_eff at (Internal (DoWrite l v)) s) at = (lvar write_set s at)(l := Some v)"
  by (auto simp add: unfold_all_tms2 locality_tms2)

lemma status_stable:
    "\<lbrakk>local_tms_pre at a s; t \<noteq> at\<rbrakk> \<Longrightarrow>
     lvar status (local_tms_eff at a s) t = lvar status s t"
  by(auto simp add: unfold_all_tms2 locality_tms2 split: splits)

definition txn_inv :: "T \<Rightarrow> internal_action event \<Rightarrow> (T, Local, stores_state) state \<Rightarrow> bool"
  where
  "txn_inv t e s \<equiv>
     status_enabled e (view t s) \<longrightarrow>
     (case e of
       (External BeginInv) \<Rightarrow> lvar read_set s t = Map.empty \<and> lvar write_set s t = Map.empty
       |
       (External BeginResp) \<Rightarrow> lvar begin_index s t \<le> max_index (global s)
                             \<and> lvar read_set s t = Map.empty
                             \<and> lvar write_set s t = Map.empty
       |
       (Internal (DoRead l n)) \<Rightarrow> lvar begin_index s t \<le> max_index (global s) \<and> vin_ex t s
       |
       (External (ReadResp v)) \<Rightarrow> lvar begin_index s t \<le> max_index (global s) \<and> vin_ex t s
       |
       (Internal (DoWrite l v)) \<Rightarrow> lvar begin_index s t \<le> max_index (global s) \<and> vin_ex t s
       |
       (External WriteResp) \<Rightarrow> lvar begin_index s t \<le> max_index (global s) \<and> lvar write_set s t \<noteq> Map.empty \<and> vin_ex t s
       |
       (Internal DoCommitReadOnly) \<Rightarrow> lvar begin_index s t \<le> max_index (global s) \<and> vin_ex t s
       |
       (Internal DoCommitWriter) \<Rightarrow> lvar begin_index s t \<le> max_index (global s)
       |
       (External CommitResp) \<Rightarrow> True
       |
       (External Abort) \<Rightarrow> True
       |
       _ \<Rightarrow> lvar begin_index s t \<le> max_index (global s) \<and> vin_ex t s)"

lemma txn_inv_preserved_ext_self:
  "\<lbrakk>txn_inv at (External e) s;
    local_tms_pre at (External e) s\<rbrakk>
    \<Longrightarrow>
    txn_inv at b (local_tms_eff at (External e) s)"
    apply (cases e; cases rule: Event_split[of b]; simp add: txn_inv_def; intro impI conjI vin_ex_nsI)
    apply (auto simp add: unfold_tms2 txn_inv_def ext_enabled_def locality_tms2)
    apply (simp add: VIN_EX_NS_def vin_ex_def, rule_tac x = "max_index (snd s)" in exI, simp add: unfold_tms2 locality_tms2 read_consistent_def)
    apply (simp add: VIN_EX_NS_def vin_ex_def, rule_tac x = "max_index (snd s)" in exI, simp add: unfold_tms2 locality_tms2 read_consistent_def)
    apply (simp add: VIN_EX_NS_def vin_ex_def, rule_tac x = "max_index (snd s)" in exI, simp add: unfold_tms2 locality_tms2 read_consistent_def)
    apply (simp add: VIN_EX_NS_def vin_ex_def, rule_tac x = "max_index (snd s)" in exI, simp add: unfold_tms2 locality_tms2 read_consistent_def)
    by (simp add: VIN_EX_NS_def vin_ex_def, erule exE, rule_tac x = I in exI, simp add: unfold_tms2 locality_tms2 read_consistent_def)+

lemma txn_inv_preserved_int_self:
  "\<lbrakk>txn_inv at (Internal i) s;
    local_tms_pre at (Internal i) s\<rbrakk>
    \<Longrightarrow>
    txn_inv at b (local_tms_eff at (Internal i) s)"
    apply (cases i; cases rule: Event_split[of b]; simp add: txn_inv_def; intro impI conjI vin_ex_nsI)
    apply (auto simp add: unfold_tms2 txn_inv_def ext_enabled_def locality_tms2 value_for_def update_partial_def when_fn_def)
    apply (simp add: VIN_EX_NS_def vin_ex_def)
    apply (erule exE)
    apply (rule_tac x = I in exI)
    apply (rule_tac y = I in nat.exhaust)
    apply (simp add: unfold_tms2 locality_tms2 read_consistent_def value_for_def update_partial_def when_fn_def domI option.case_eq_if)
    apply (simp add: unfold_tms2 locality_tms2 read_consistent_def value_for_def update_partial_def when_fn_def domI option.case_eq_if)
    apply (simp add: VIN_EX_NS_def vin_ex_def)
    apply (erule exE)
    apply (rename_tac x y I)
    apply (rule_tac x = y in exI)
    apply (simp add: unfold_tms2 locality_tms2 RWMemoryList.all_simps when_fn_def domI option.case_eq_if)
    apply blast
    apply (simp add: VIN_EX_NS_def vin_ex_def)
    apply (erule exE)
    apply (simp add: unfold_tms2 locality_tms2 RWMemoryList.all_simps when_fn_def domI option.case_eq_if)
    by blast

lemma txn_inv_preserved_self:
  "\<lbrakk>txn_inv at e s;
    local_tms_pre at e s\<rbrakk>
    \<Longrightarrow>
    txn_inv at b (local_tms_eff at e s)"
  by (metis event.exhaust txn_inv_preserved_ext_self txn_inv_preserved_int_self)

lemma miwb: "max_index (write_back ws s) = Suc (max_index s)"
  by (simp add: write_back_def max_index_def)

lemma txn_inv_preserved_int_other1:
  assumes "txn_inv t b s"
  and "t \<noteq> at"
  and "local_tms_pre at (Internal DoCommitReadOnly) s"
  shows "txn_inv t b (local_tms_eff at (Internal DoCommitReadOnly) s)"
  using assms
  apply (rule_tac b = b in TMS2.Event_split; simp add: txn_inv_def; intro impI conjI vin_ex_nsI)
  apply (simp_all add: txn_inv_def unfold_tms2 locality_tms2 ext_enabled_def vin_ex_def miwb option.case_eq_if)
  by (elim conjE exE, simp add: VIN_EX_NS_def vin_ex_def, rule_tac x = "I" in exI, simp add: unfold_tms2 locality_tms2 read_consistent_def)+

lemma txn_inv_preserved_int_other2:
  assumes "txn_inv t b s"
  and "t \<noteq> at"
  and "local_tms_pre at (Internal (DoRead l v)) s"
  shows "txn_inv t b (local_tms_eff at (Internal (DoRead l v)) s)"
  using assms
  apply (rule_tac b = b in TMS2.Event_split; simp add: txn_inv_def; intro impI conjI)
  by (simp_all add: txn_inv_def unfold_tms2 locality_tms2 ext_enabled_def vin_ex_def miwb option.case_eq_if when_fn_def)

lemma txn_inv_preserved_int_other3:
  assumes "txn_inv t b s"
  and "t \<noteq> at"
  and "local_tms_pre at (Internal (DoWrite l v)) s"
  shows "txn_inv t b (local_tms_eff at (Internal (DoWrite l v)) s)"
  using assms
  apply (rule_tac b = b in TMS2.Event_split; simp add: txn_inv_def; intro impI conjI)
  by (simp_all add: txn_inv_def unfold_tms2 locality_tms2 ext_enabled_def vin_ex_def miwb option.case_eq_if when_fn_def)

lemma mi_store_wb: "I \<le> max_index s \<Longrightarrow> store_at (write_back ws s) I = store_at s I"
  by (cases I) (simp_all add: max_index_def store_at_def write_back_def nth_append)

lemma mi_rc_wb: "I \<le> max_index s \<Longrightarrow> read_consistent (store_at (write_back ws s) I) rs \<longleftrightarrow> read_consistent (store_at s I) rs"
  by (simp add: mi_store_wb)

lemma txn_inv_preserved_int_other4:
  assumes "txn_inv t b s"
  and "t \<noteq> at"
  and "local_tms_pre at (Internal DoCommitWriter) s"
  shows "txn_inv t b (local_tms_eff at (Internal DoCommitWriter) s)"
  using assms
  apply (rule_tac b = b in TMS2.Event_split; simp add: txn_inv_def; intro impI conjI)
  apply (simp_all add: txn_inv_def unfold_tms2 locality_tms2 ext_enabled_def vin_ex_def miwb option.case_eq_if when_fn_def)
  by (elim conjE exE, rule_tac x = I in exI, simp add: mi_store_wb)+

lemma txn_inv_preserved_int_other:
  assumes "txn_inv t b s"
  and "t \<noteq> at"
  and "local_tms_pre at (Internal ic) s"
  shows "txn_inv t b (local_tms_eff at (Internal ic) s)"
  using assms
  apply (cases ic)
  using txn_inv_preserved_int_other1 apply blast
  using txn_inv_preserved_int_other4 apply blast
  apply (simp add: txn_inv_preserved_int_other2)
  by (simp add: txn_inv_preserved_int_other3)

lemma txn_inv_preserved_ext_other:
  assumes "txn_inv t b s"
  and "t \<noteq> at"
  and "local_tms_pre at (External e) s"
  shows "txn_inv t b (local_tms_eff at (External e) s)"
  using assms
  apply (cases e; rule_tac b = b in TMS2.Event_split)
  by (simp_all add: txn_inv_def unfold_tms2 locality_tms2 ext_enabled_def vin_ex_def miwb option.case_eq_if when_fn_def)

lemma txn_inv_preserved_other:
  assumes "txn_inv t b s"
  and "t \<noteq> at"
  and "local_tms_pre at e s"
  shows "txn_inv t b (local_tms_eff at e s)"
  using assms
  by (metis event.exhaust txn_inv_preserved_ext_other txn_inv_preserved_int_other)

lemma total_inv_initial:
  "fst s \<in> tms_starts \<Longrightarrow> snd s = initial_stores \<Longrightarrow> \<forall>e t. txn_inv t e s"
  by (auto simp add: tms_starts_def txn_inv_def initial_stores_def dom_def
                     max_index_def unfold_pre ext_enabled_def locality_tms2 split: if_splits splits)

lemma total_inv:
  "invariant (ioa TMS2) (\<lambda>s. (\<forall>e t. txn_inv t e s))"
  apply(rule invariant_intro)
  apply(rule total_inv_initial, simp)
  apply(unfold preserved_def)
  apply(auto simp add: txn_inv_preserved_self txn_inv_preserved_other starts_def local_preserved_def global_preserved_def)
  apply (metis (full_types) local_tms_eff_def local_tms_pre_def txn_inv_preserved_other txn_inv_preserved_self)
  by (simp add: locality_tms2)

definition tms2_interference :: "stores_state rel" where
  "tms2_interference \<equiv> {(g, g'). \<exists>S. g' = g \<bowtie> S}"

definition tms2_interference2 :: "stores_state rel" where
  "tms2_interference2 \<equiv> {(g, g'). \<exists>s. g' = g \<bowtie> [s]}"

definition mk_write :: "(L \<times> V) \<Rightarrow> internal_action event list" where
  "mk_write write \<equiv> let (l, v) = write in [External (WriteInv l v), Internal (DoWrite l v), External WriteResp]" 

definition mk_writes :: "(L \<times> V) list \<Rightarrow> (internal_action event) list" where
  "mk_writes writes = [External BeginInv, External BeginResp] @ (List.maps mk_write writes) @ [External CommitInv, Internal DoCommitWriter, External CommitResp]"

definition not_started :: "nat \<Rightarrow> tms2_state \<Rightarrow> T list" where
  "not_started n s \<equiv> (SOME ts. distinct ts \<and> (\<forall>t\<in>set ts. lvar status s t = NotStarted))"

lemma rtrancl_ka_induct2: "Id \<union> (R O S) \<subseteq> S \<Longrightarrow> R\<^sup>* \<subseteq> S"
  apply safe
  apply (rule_tac r = R and a = a in converse_rtrancl_induct)
  by blast+

lemma tms2_interference_pow_inv: "tms2_interference\<^sup>* = tms2_interference"
  apply (simp add: tms2_interference_def)
  apply (rule antisym)
  apply (rule rtrancl_ka_induct2)
  defer
  apply auto[1]
  apply auto
  apply (rule_tac x = "[]" in exI)
  apply (simp add: stores_append_def)
  apply (rule_tac x = "(S @ Sa)" in exI)
  by (simp add: stores_append_assoc)

lemma vidx_pres: "valid_index (x, y, z) I \<Longrightarrow> valid_index (x, y, z @ S) I"
  apply (simp add: max_index_def valid_index_def read_consistent_def store_at_def)
  apply (erule conjE)+
  apply (intro allI)
  apply (erule_tac x = l in allE)
  apply (subgoal_tac "(read_set x l = None) \<or> (\<exists>v. read_set x l = Some v)")
  apply (erule disjE)
  apply simp
  apply (erule exE)
  apply simp
  apply (cases I)
  apply simp
  apply (simp add: nth_append)
  using not_None_eq by blast

lemma total_inv_interference:
  "invariant (ioa (tms2_interference \<rhd> TMS2)) (\<lambda>s. (\<forall>e t. txn_inv t e s))"
  apply (rule invariant_intro)
  apply (rule total_inv_initial, simp add: Rely_def)
  apply (unfold preserved_def)
  apply (auto simp add: Rely_def txn_inv_preserved_self txn_inv_preserved_other starts_def local_preserved_def global_preserved_def)
  apply (metis (full_types) local_tms_eff_def local_tms_pre_def txn_inv_preserved_other txn_inv_preserved_self)
  apply (erule_tac x = e in allE)
  apply (erule_tac x = t in allE)
  apply (simp add: tms2_interference_def)
  apply (erule exE)
  apply (simp add: global_def stores_append_def)
  apply (erule conjE)
  apply (rule_tac b = e in Event_split)
  apply (simp_all add: txn_inv_def locality_tms2 status_enabled_def ext_enabled_def max_index_def trans_le_add1 vin_ex_def)
  apply (intro impI, erule impE, assumption, erule conjE, erule exE, rule_tac x = "I" in exI, rule vidx_pres, assumption)
  apply (intro impI, erule impE, assumption, erule conjE, erule exE)
  apply (rule_tac x = "x" in exI, rule vidx_pres, assumption)
  apply (intro impI, erule impE, assumption, (erule conjE)+, erule exE, rule_tac x = "I" in exI, rule vidx_pres, assumption)
  apply (intro impI, erule impE, assumption, erule conjE, erule exE)
  apply (rule_tac x = "x" in exI, rule vidx_pres, assumption)
  apply (intro impI, erule impE, assumption, erule conjE, erule exE)
  apply (rule_tac x = "x" in exI, rule vidx_pres, assumption)
  by (intro impI, erule impE, assumption, (erule conjE)+, erule exE, rule_tac x = "I" in exI, rule vidx_pres, assumption)+

lemma read_consistent_stable:
  "\<lbrakk>local_tms_pre at a s;
    stores_domain s;
    txn_inv at a s;
    n \<le> max_index (global s);
    (\<forall> l n. a \<noteq> Internal (DoRead l n)) \<or> t \<noteq> at\<rbrakk>
   \<Longrightarrow>
   read_consistent (store_at (global (local_tms_eff at a s)) n) (lvar read_set (local_tms_eff at a s) t) =
   read_consistent (store_at (global s) n) (lvar read_set s t)"
  by (simp add: store_at_stable read_set_stable)

lemma reachable_invariant_tms2: "reach TMS2 s \<Longrightarrow> txn_inv t e s"
  using invariant_elim total_inv by fastforce

lemma reachable_invariant_tms2_interference: "reach (tms2_interference \<rhd> TMS2) s \<Longrightarrow> txn_inv t e s"
  using invariant_elim total_inv_interference by fastforce

lemma abs_leff [simp]: "local_eff (tms2_interference \<rhd> TMS2) = local_tms_eff"
  by (rule ext)+ (simp add: local_eff_def local_tms_eff_def)

lemma abs_lpre [simp]: "local_pre (tms2_interference \<rhd> TMS2) = local_tms_pre"
  by (rule ext)+ (simp add: local_pre_def local_tms_pre_def Rely_def)

end