theory CGA
imports Shared
begin

datatype pc =
  Begin
| Commit
| Read
| Write

type_synonym status = "pc status"

record lstate =
  status :: status
  val :: V
  loc :: nat
  addr :: L
  writer :: bool

definition update_status :: "status \<Rightarrow> lstate \<times> gstate \<Rightarrow> lstate \<times> gstate" where
  "update_status st \<equiv> apfst (\<lambda>l. l \<lparr> status := st \<rparr>)"

definition update_val :: "V \<Rightarrow> lstate \<times> gstate \<Rightarrow> lstate \<times> gstate" where
  "update_val v \<equiv> apfst (\<lambda>l. l \<lparr> val := v \<rparr>)"

definition update_loc :: "nat \<Rightarrow> lstate \<times> gstate \<Rightarrow> lstate \<times> gstate" where
  "update_loc n \<equiv> apfst (\<lambda>l. l \<lparr> loc := n \<rparr>)"

definition update_addr :: "L \<Rightarrow> lstate \<times> gstate \<Rightarrow> lstate \<times> gstate" where
  "update_addr l \<equiv> apfst (\<lambda>s. s \<lparr> addr := l\<rparr>)"

definition update_writer :: "bool \<Rightarrow> lstate \<times> gstate \<Rightarrow> lstate \<times> gstate" where
  "update_writer b \<equiv> apfst (\<lambda>l. l \<lparr> writer := b \<rparr>)"

definition lstarts :: "(T \<Rightarrow> lstate) set" where
  "lstarts \<equiv> {s. (\<forall>t. status (s t) = NotStarted \<and> writer (s t) = False)}"

definition cga_pre :: "pc event \<Rightarrow> lstate \<times> gstate \<Rightarrow> bool" where
  "cga_pre e s \<equiv>
      e \<noteq> External Cancel
    \<and> (e = External Abort \<longrightarrow> status (fst s) = AbortPending)
    \<and> (case e of
           External a \<Rightarrow> ext_enabled (status (fst s)) a
         | Internal a \<Rightarrow> status (fst s) = Pending a)"

notation fcomp (infixl "\<circ>>" 60)

fun ext_eff :: "action \<Rightarrow> lstate \<times> gstate \<Rightarrow> lstate \<times> gstate" where
  "ext_eff BeginInv = update_status (Pending Begin)"
| "ext_eff BeginResp = update_status Ready"
| "ext_eff CommitInv = update_status (Pending Commit)"
| "ext_eff CommitResp = update_status Committed"
| "ext_eff Abort = update_status Aborted"
| "ext_eff (ReadInv l) = update_addr l \<circ>> update_status (Pending Read)"
| "ext_eff (ReadResp v) = update_status Ready"
| "ext_eff (WriteInv l v) = (update_addr l \<circ>> update_val v \<circ>> update_status (Pending Write))"
| "ext_eff WriteResp = update_status Ready"
| "ext_eff Cancel = undefined"

fun int_eff :: "pc \<Rightarrow> lstate \<times> gstate \<Rightarrow> lstate \<times> gstate" where
  "int_eff Begin = (\<lambda>s0.
    if even (glb (snd s0))
    then (update_status BeginResponding \<circ>> update_loc (glb (snd s0))) s0
    else update_status (Pending Begin) s0)"

| "int_eff Commit = (\<lambda>s0.
    (if odd (loc (fst s0)) then apsnd (glb_update (op + 1)) \<circ> update_writer False else id) (update_status CommitResponding s0))"

| "int_eff Read = (\<lambda>s0.
    (if (glb (snd s0) = loc (fst s0))
    then (update_val (store (snd s0) (addr (fst s0))) \<circ>> update_status (ReadResponding (store (snd s0) (addr (fst s0)))))
    else update_status AbortPending) s0)"

| "int_eff Write = (\<lambda>s0.
    (if (glb (snd s0) \<noteq> loc (fst s0)) then update_status AbortPending
     else
       ((if even (loc (fst s0)) then apsnd (glb_update (op + 1)) \<circ> update_loc (loc (fst s0) + 1) else id) \<circ>>
       update_writer True \<circ>>
       apsnd (update_store (addr (fst s0)) (val (fst s0))) \<circ>>
       update_status WriteResponding)) s0)"

fun cga_eff :: "pc event \<Rightarrow> lstate \<times> gstate \<Rightarrow> lstate \<times> gstate"
  where
  "cga_eff (Internal a) = int_eff a"
| "cga_eff (External a) = ext_eff a"

definition CGA :: "(T, lstate, gstate, pc) RGA" where
  "CGA \<equiv> \<lparr> RGA.local_starts = lstarts, RGA.global_start = gstart, RGA.pre = cga_pre, RGA.eff = cga_eff, RGA.environment = Id\<rparr>"

definition is_writer :: "T \<Rightarrow> (T, lstate, gstate) state \<Rightarrow> bool" where
  "is_writer t s \<equiv> writer (local t s) = True \<and> lvar loc s t = glb (global s)"

definition writer_unique :: "(T, lstate, gstate) state \<Rightarrow> bool" where
  "writer_unique s \<equiv> ((\<exists>t. writer (local t s) = True) \<longrightarrow> (\<exists>!t. writer (local t s) = True))"

definition global_inv :: "(T, lstate, gstate) state \<Rightarrow> bool" where
  "global_inv s \<equiv> (even (glb (global s)) \<longleftrightarrow> (\<forall>t. writer (local t s) = False)) \<and> writer_unique s"

definition is_ready :: "T \<Rightarrow> (T, lstate, gstate) state \<Rightarrow> bool" where
  "is_ready t s \<equiv> 
      lvar loc s t \<le> glb (global s)
    \<and> (odd (lvar loc s t) \<longrightarrow> is_writer t s)"

definition txn_inv :: "T \<Rightarrow> pc event \<Rightarrow> (T, lstate, gstate) state \<Rightarrow> bool" where
  "txn_inv t e s \<equiv>
   cga_pre e (view t s) \<longrightarrow>
   (case e of
      External BeginInv \<Rightarrow> True
      |
      Internal Begin \<Rightarrow> True
      |
      External BeginResp \<Rightarrow> (  lvar loc s t \<le> glb (global s)
                             \<and> even (lvar loc s t))
      |
      Internal Read \<Rightarrow> is_ready t s
      |
      External (ReadResp v) \<Rightarrow>  v = lvar val s t
                              \<and> is_ready t s
      |
      Internal Write \<Rightarrow> is_ready t s
      |
      External WriteResp \<Rightarrow> odd (lvar loc s t) \<and> is_writer t s \<and> odd (lvar loc s t)
      |
      Internal Commit \<Rightarrow> is_ready t s
      |
      External CommitResp \<Rightarrow> True
      |
      External Abort \<Rightarrow> True
      |
      External Cancel \<Rightarrow> False
      |
      _ \<Rightarrow> is_ready t s)"

lemma Event_split:
  "\<lbrakk>b = External BeginInv \<Longrightarrow> P;
    b = Internal Begin \<Longrightarrow> P;
    b = External BeginResp \<Longrightarrow> P;
    \<And> l. b = External (ReadInv l) \<Longrightarrow> P;
    b = Internal Read \<Longrightarrow> P;
    \<And> v. b = External (ReadResp v) \<Longrightarrow> P;
    \<And> l v. b = External (WriteInv l v) \<Longrightarrow> P;
    b = Internal Write \<Longrightarrow> P;
    b = External WriteResp \<Longrightarrow> P;
    b = External CommitInv \<Longrightarrow> P;
    b = Internal Commit \<Longrightarrow> P;
    b = External CommitResp \<Longrightarrow> P;
    b = External Cancel \<Longrightarrow> P;
    b = External Abort \<Longrightarrow> P \<rbrakk>
   \<Longrightarrow>
    P"
  apply (cases rule: event.exhaust[where y = b])
  apply simp_all
  using action.exhaust apply blast
  using pc.exhaust by blast

lemmas unfold_updates =
  update_status_def
  update_loc_def
  update_addr_def
  update_val_def
  update_store_def
  update_writer_def

lemmas cga_simps =
  txn_inv_def
  cga_pre_def
  ext_enabled_def
  unfold_updates
  is_ready_def
  is_writer_def
  global_inv_def
  writer_unique_def

(* Begin *)

definition local_cga_eff :: "T \<Rightarrow> pc event \<Rightarrow> (T \<Rightarrow> lstate) \<times> gstate \<Rightarrow> (T \<Rightarrow> lstate) \<times> gstate" where
  "local_cga_eff t a = localize t (cga_eff a)" 

definition local_cga_pre :: "T \<Rightarrow> pc event \<Rightarrow> (T, lstate, gstate) state \<Rightarrow> bool" where
  "local_cga_pre t a s = cga_pre a (view t s)"

lemmas locality_cga =
  local_cga_eff_def
  local_cga_pre_def
  view_def
  local_def
  localize_def
  global_def
  lvar_def
  gvar_def

lemma txn_inv_pres_begin_inv_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External BeginInv);
    local_cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_cga_eff at a s)"
   apply (cases rule: Event_split[of b])
   by (auto simp add: cga_simps locality_cga)

lemma txn_inv_pres_begin_inv_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (External BeginInv);
    local_cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps locality_cga)

lemma txn_inv_pres_begin_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (Internal Begin);
    local_cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps locality_cga)

lemma txn_inv_pres_begin_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (Internal Begin);
    local_cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps locality_cga)

lemma txn_inv_pres_begin_resp_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External BeginResp);
    local_cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps locality_cga)

lemma txn_inv_pres_begin_resp_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (External BeginResp);
    local_cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps locality_cga)

(* Read *)

lemma txn_inv_pres_read_inv_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External (ReadInv l));
    local_cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps locality_cga)

lemma txn_inv_pres_read_inv_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = External (ReadInv l);
    local_cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps locality_cga)

lemma txn_inv_pres_read_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (Internal Read);
    local_cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps locality_cga)

lemma txn_inv_pres_read_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (Internal Read);
    local_cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_cga_eff at a s)"
   apply (cases rule: Event_split[of b])
   apply (simp_all add: cga_simps locality_cga)
   apply linarith+
   by force+

lemma txn_inv_pres_read_resp_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External (ReadResp v));
    local_cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps locality_cga)

lemma txn_inv_pres_read_resp_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = External (ReadResp l);
    local_cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps locality_cga)

(* Write *)

lemma txn_inv_pres_write_inv_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External (WriteInv l v));
    local_cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps locality_cga)

lemma txn_inv_pres_write_inv_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = External (WriteInv l v);
    local_cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps locality_cga)

lemma txn_inv_pres_write_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (Internal Write);
    local_cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps locality_cga)

lemma txn_inv_pres_write_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (Internal Write);
    local_cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_cga_eff at a s)"
   apply (cases rule: Event_split[of b])
   apply (simp_all add: cga_simps locality_cga)
   by (linarith | metis)+

lemma txn_inv_pres_write_resp_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External WriteResp);
    local_cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps locality_cga)

lemma txn_inv_pres_write_resp_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = External WriteResp;
    local_cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps locality_cga)

(* Commit *)

lemma txn_inv_pres_commit_inv_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External CommitInv);
    local_cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps locality_cga)

lemma txn_inv_pres_commit_inv_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (External CommitInv);
    local_cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps locality_cga)

lemma txn_inv_pres_commit_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (Internal Commit);
    local_cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps locality_cga)

lemma txn_inv_pres_commit_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (Internal Commit);
    local_cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps locality_cga)

lemma txn_inv_pres_commit_resp_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External CommitResp);
    local_cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps locality_cga)

lemma txn_inv_pres_commit_resp_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (External CommitResp);
    local_cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps locality_cga)

lemma txn_inv_pres_cancel_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External Cancel);
    local_cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps locality_cga)

lemma txn_inv_pres_cancel_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (External Cancel);
    local_cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps locality_cga)

lemma txn_inv_pres_abort_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    a = (External Abort);
    local_cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_cga_eff at a s)"
   by (cases rule: Event_split[of b]) (simp_all add: cga_simps locality_cga)

lemma txn_inv_pres_abort_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    a = (External Abort);
    local_cga_pre at a s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_cga_eff at a s)"
   by (cases rule: Event_split[of b]) (auto simp add: cga_simps locality_cga)

lemma allE2: "\<forall>x. P x \<Longrightarrow> (P x \<Longrightarrow> P y \<Longrightarrow> Q) \<Longrightarrow> Q"
  by auto

lemma eff_CGA_env [simp]: "eff (CGA \<lparr> environment := R \<rparr>) a = eff CGA a"
  by (simp add: CGA_def)

lemma pre_CGA_env [simp]: "pre (CGA \<lparr> environment := R \<rparr>) a = pre CGA a"
  by (simp add: CGA_def)

lemma loc_CGA: "localize t (eff CGA a) s = local_cga_eff t a s"
  by (simp add: CGA_def local_cga_eff_def)

lemma pre_CGA: "pre CGA a (view t s) = local_cga_pre t a s"
  by (simp add: CGA_def local_cga_pre_def)

lemma total_inv: "invariant (ioa CGA) (\<lambda>s. global_inv s \<and> (\<forall>e t. txn_inv t e s))"
proof (intro invariant_intro conjI allI)
  fix s
  assume "s \<in> starts CGA"
  thus "global_inv s"
    by (auto simp add: CGA_def starts_def lstarts_def gstart_def global_inv_def locality_cga writer_unique_def is_writer_def)
next
  fix s e t
  assume "s \<in> starts CGA"
  thus "txn_inv t e s"
    apply (cases rule: Event_split[of e])
    by (auto simp add: txn_inv_def cga_pre_def CGA_def starts_def gstart_def lstarts_def locality_cga ext_enabled_def)
next
  fix s
  assume "reach CGA s"
  show "preserved  CGA (\<lambda>s. global_inv s \<and> (\<forall>e t. txn_inv t e s)) s"
    apply (simp only: preserved_def local_preserved_def global_preserved_def loc_CGA pre_CGA)
    apply (intro conjI)
    apply (intro allI impI)
    apply (erule conjE)+
    apply (intro conjI allI)
  proof -
    fix t a
    assume "local_cga_pre t a s" and "global_inv s" and "\<forall>e t. txn_inv t e s"
    thus "global_inv (local_cga_eff t a s)"
      apply (cases rule: Event_split[of a])
      apply (simp_all add: cga_simps locality_cga)
      apply metis
      apply (smt \<open>\<forall>e t. txn_inv t e s\<close> \<open>local_cga_pre t a s\<close> event.simps(6) is_ready_def is_writer_def local_cga_pre_def local_def lvar_def pc.simps(16) txn_inv_def)
      apply safe
      apply auto
      apply (smt \<open>\<forall>e t. txn_inv t e s\<close> \<open>local_cga_pre t a s\<close> event.simps(6) is_ready_def is_writer_def local_cga_pre_def local_def lvar_def pc.simps(14) txn_inv_def)
      by (smt \<open>\<forall>e t. txn_inv t e s\<close> \<open>local_cga_pre t a s\<close> event.simps(6) global_def is_ready_def is_writer_def local_cga_pre_def local_def lvar_def pc.simps(14) txn_inv_def)
  next
    fix t a e t'
    assume "local_cga_pre t a s" and "global_inv s" and "\<forall>e t. txn_inv t e s"
    thus "txn_inv t' e (local_cga_eff t a s)"
      apply (cases "t' = t")
      apply (erule ssubst)
      apply (erule_tac x = e and y = a in allE2)
      apply (erule_tac x = t in allE)
      apply (erule_tac x = t in allE)
      apply (cases rule: Event_split[of a])
      apply (simp_all add: CGA_def del: cga_eff.simps)
      apply (rule txn_inv_pres_begin_inv_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_begin_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_begin_resp_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_inv_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_resp_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_inv_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_resp_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_inv_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_resp_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_cancel_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_abort_self, simp, simp, simp, simp)
      apply (erule_tac x = e and y = a in allE2)
      apply (erule_tac x = t' in allE)
      apply (erule_tac x = t in allE)
      apply (cases rule: Event_split[of a])
      apply (rule txn_inv_pres_begin_inv_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_begin_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_begin_resp_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_inv_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_resp_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_inv_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_resp_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_inv_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_resp_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_cancel_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_abort_other, simp, simp, simp, simp, simp, simp)
      done
  next
    show "\<forall>g. (global s, g) \<in> environment CGA \<and> global_inv s \<and> (\<forall>e t. txn_inv t e s) \<longrightarrow>
              global_inv (fst s, g) \<and> (\<forall>e t. txn_inv t e (fst s, g))"
      apply (intro allI impI)
      apply (intro conjI)
      apply (simp_all add: CGA_def)
      apply (metis global_def prod.collapse)
      by (metis global_def prod.collapse)
  qed
qed

lemma total_inv_interference: "invariant (ioa (interference \<rhd> CGA)) (\<lambda>s. global_inv s \<and> (\<forall>e t. txn_inv t e s))"
proof (intro invariant_intro conjI allI)
  fix s
  assume "s \<in> starts (Rely interference CGA)"
  thus "global_inv s"
    by (auto simp add: CGA_def starts_def lstarts_def Rely_def gstart_def global_inv_def locality_cga writer_unique_def is_writer_def)
next
  fix s e t
  assume "s \<in> starts (Rely interference CGA)"
  thus "txn_inv t e s"
    apply (cases rule: Event_split[of e])
    by (auto simp add: txn_inv_def Rely_def cga_pre_def CGA_def starts_def gstart_def lstarts_def locality_cga ext_enabled_def)
next
  fix s
  assume "reach (Rely interference CGA) s"
  show "preserved  (Rely interference CGA) (\<lambda>s. global_inv s \<and> (\<forall>e t. txn_inv t e s)) s"
    apply (simp only: preserved_def Rely_def local_preserved_def global_preserved_def loc_CGA pre_CGA eff_CGA_env pre_CGA_env)
    apply (intro conjI)
    apply (intro allI impI)
    apply (erule conjE)+
    apply (intro conjI allI)
  proof -
    fix t a
    assume "local_cga_pre t a s" and "global_inv s" and "\<forall>e t. txn_inv t e s"
    thus "global_inv (local_cga_eff t a s)"
      apply (cases rule: Event_split[of a])
      apply (simp_all add: cga_simps locality_cga)
      apply metis
      apply (smt \<open>\<forall>e t. txn_inv t e s\<close> \<open>local_cga_pre t a s\<close> event.simps(6) is_ready_def is_writer_def local_cga_pre_def local_def lvar_def pc.simps(16) txn_inv_def)
      apply safe
      apply auto
      apply (smt \<open>\<forall>e t. txn_inv t e s\<close> \<open>local_cga_pre t a s\<close> event.simps(6) is_ready_def is_writer_def local_cga_pre_def local_def lvar_def pc.simps(14) txn_inv_def)
      by (smt \<open>\<forall>e t. txn_inv t e s\<close> \<open>local_cga_pre t a s\<close> event.simps(6) global_def is_ready_def is_writer_def local_cga_pre_def local_def lvar_def pc.simps(14) txn_inv_def)
  next
    fix t a e t'
    assume "local_cga_pre t a s" and "global_inv s" and "\<forall>e t. txn_inv t e s"
    thus "txn_inv t' e (local_cga_eff t a s)"
      apply (cases "t' = t")
      apply (erule ssubst)
      apply (erule_tac x = e and y = a in allE2)
      apply (erule_tac x = t in allE)
      apply (erule_tac x = t in allE)
      apply (cases rule: Event_split[of a])
      apply (simp_all add: CGA_def del: cga_eff.simps)
      apply (rule txn_inv_pres_begin_inv_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_begin_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_begin_resp_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_inv_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_resp_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_inv_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_resp_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_inv_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_resp_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_cancel_self, simp, simp, simp, simp)
      apply (rule txn_inv_pres_abort_self, simp, simp, simp, simp)
      apply (erule_tac x = e and y = a in allE2)
      apply (erule_tac x = t' in allE)
      apply (erule_tac x = t in allE)
      apply (cases rule: Event_split[of a])
      apply (rule txn_inv_pres_begin_inv_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_begin_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_begin_resp_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_inv_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_read_resp_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_inv_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_write_resp_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_inv_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_commit_resp_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_cancel_other, simp, simp, simp, simp, simp, simp)
      apply (rule txn_inv_pres_abort_other, simp, simp, simp, simp, simp, simp)
      done
  next
    show "\<forall>g. (global s, g) \<in> environment (CGA \<lparr> environment := interference \<rparr>) \<and> global_inv s \<and> (\<forall>e t. txn_inv t e s) \<longrightarrow>
              global_inv (fst s, g) \<and> (\<forall>e t. txn_inv t e (fst s, g))"
      apply (simp add: CGA_def)
      apply (simp add: interference_simple)
      apply (intro allI impI)
      apply (erule conjE)+
      apply (erule disjE)
      apply (metis global_def prod.collapse)
      apply (intro conjI)
      apply (simp add: global_inv_def global_def)
      apply (simp add: local_def writer_unique_def)
      apply (simp add: global_def)
      apply (intro allI)
      apply (rule_tac b = e in Event_split)
      apply (simp_all add: txn_inv_def)
      apply (erule_tac x = "External BeginResp" in allE)
      apply (simp add: cga_pre_def locality_cga ext_enabled_def)
      apply auto[1]
      apply (erule_tac x = "External (ReadInv l)" in allE)
      apply (simp add: cga_pre_def locality_cga ext_enabled_def global_inv_def is_ready_def is_writer_def)
      using dual_order.trans apply blast
      apply (erule_tac x = "Internal Read" in allE)
      apply (simp add: cga_pre_def locality_cga ext_enabled_def is_ready_def is_writer_def)
      using dual_order.trans apply blast
      apply (erule_tac x = "External (ReadResp v)" in allE)
      apply (simp add: cga_pre_def locality_cga ext_enabled_def is_ready_def is_writer_def)
      using dual_order.trans apply blast
      apply (erule_tac x = "External (WriteInv l v)" in allE)
      apply (simp add: cga_pre_def locality_cga ext_enabled_def is_ready_def is_writer_def)
      using dual_order.trans apply blast
      apply (erule_tac x = "Internal Write" in allE)
      apply (simp add: cga_pre_def locality_cga ext_enabled_def is_ready_def is_writer_def)
      using dual_order.trans apply blast
      apply (erule_tac x = "External WriteResp" in allE)
      apply (simp add: cga_pre_def locality_cga ext_enabled_def is_ready_def is_writer_def)
      apply auto[1]
      apply (erule_tac x = "External CommitInv" in allE)
      apply (simp add: cga_pre_def locality_cga ext_enabled_def is_ready_def is_writer_def)
      using dual_order.trans apply blast
      apply (erule_tac x = "Internal Commit" in allE)
      apply (simp add: cga_pre_def locality_cga ext_enabled_def is_ready_def is_writer_def)
      using dual_order.trans apply blast
      apply (erule_tac x = "External CommitResp" in allE)
      apply (simp add: cga_pre_def locality_cga ext_enabled_def is_ready_def is_writer_def)
      done
  qed
qed

lemma reachable_invariant_cga: "reach CGA s \<Longrightarrow> global_inv s \<and> txn_inv t e s"
  using invariant_elim total_inv by fastforce

lemma reachable_invariant_cga_interference: "reach (interference \<rhd> CGA) s \<Longrightarrow> global_inv s \<and> txn_inv t e s"
  using invariant_elim total_inv_interference by fastforce

definition the_writer :: "(T, lstate, gstate) state \<Rightarrow> T option" where
  "the_writer s \<equiv> if (\<exists>!t. lvar writer s t = True) then Some (THE t. lvar writer s t = True) else None" 

end