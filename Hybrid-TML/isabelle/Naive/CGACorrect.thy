theory CGACorrect
imports CGA TMS2
begin

definition step_correspondence :: "(T, lstate, gstate) state \<Rightarrow> T \<Rightarrow> CGA.pc \<Rightarrow> internal_action option"
  where
  "step_correspondence cs t a \<equiv>
    case a of
        Read \<Rightarrow> if lvar loc cs t = glb (global cs)
                then Some (DoRead (lvar addr cs t) (write_count (lvar loc cs t)))
                else None
      | Write \<Rightarrow> if lvar loc cs t = glb (global cs) then Some (DoWrite (lvar addr cs t) (lvar val cs t)) else None
      | Commit \<Rightarrow> if even (lvar loc cs t)
                  then Some DoCommitReadOnly
                  else Some DoCommitWriter
      | _ \<Rightarrow> None"

definition local_step_correspondence :: "lstate \<times> gstate \<Rightarrow> CGA.pc \<Rightarrow> internal_action option"
  where
  "local_step_correspondence cs a \<equiv>
    case a of
        Read \<Rightarrow> if loc (fst cs) = glb (snd cs)
                then Some (DoRead (addr (fst cs)) (write_count (loc (fst cs))))
                else None
      | Write \<Rightarrow> if loc (fst cs) = glb (snd cs) then Some (DoWrite (addr (fst cs)) (val (fst cs))) else None
      | Commit \<Rightarrow> if even (loc (fst cs))
                  then Some DoCommitReadOnly
                  else Some DoCommitWriter
      | _ \<Rightarrow> None"

lemma scf1: "step_correspondence cs t ia = Some (DoRead l n) \<longleftrightarrow>
  (ia = Read \<and> l = lvar addr cs t \<and> n = write_count (lvar loc cs t) \<and> lvar loc cs t = glb (global cs))"
  by (cases ia) (auto simp add: step_correspondence_def)

lemma scf2: "step_correspondence cs t ia = Some (DoWrite l v) \<longleftrightarrow>
  (ia = Write \<and> l = lvar addr cs t \<and> v = lvar val cs t \<and> lvar loc cs t = glb (global cs))"
  by (cases ia) (auto simp add: step_correspondence_def)

lemma scf3: "step_correspondence cs t ia = Some DoCommitReadOnly \<longleftrightarrow> (ia = Commit \<and> even (lvar loc cs t))"
  by (cases ia) (auto simp add: step_correspondence_def)

lemma scf4: "step_correspondence cs t ia = Some DoCommitWriter \<longleftrightarrow> (ia = Commit \<and> odd (lvar loc cs t))"
  by (cases ia) (auto simp add: step_correspondence_def)

lemmas scf_simps = scf1 scf2 scf3 scf4

lemma scf_None: "step_correspondence cs t ia = None \<longleftrightarrow>
  ((lvar loc cs t \<noteq> glb (global cs) \<and> ia = Read) \<or> (lvar loc cs t \<noteq> glb (global cs) \<and> ia = Write) \<or> ia = Begin)"
  by (cases ia) (auto simp add: step_correspondence_def)

lemma "step_correspondence cs t a = local_step_correspondence (view t cs) a"
  apply (cases a)
  by (auto simp add: step_correspondence_def local_step_correspondence_def lvar_def global_def local_def view_def)

type_synonym cga_state = "(T, lstate, gstate) state"

type_synonym tms2_state = "(T, Local, stores_state) state"

definition writes :: "cga_state \<Rightarrow> tms2_state \<Rightarrow> L \<Rightarrow> V option"
  where
  "writes cs0 as0 \<equiv>
    case (the_writer cs0) of Some w \<Rightarrow> lvar write_set as0 w | None \<Rightarrow> Map.empty"

lemma the_writer_Some: "the_writer cs = Some t \<Longrightarrow> writes cs as = lvar write_set as t"
  by (auto simp add: writes_def)

lemma the_writer_None: "the_writer cs = None \<Longrightarrow> writes cs as = Map.empty"
  by (auto simp add: writes_def)

definition global_rel :: "cga_state \<Rightarrow> tms2_state \<Rightarrow> bool"
  where
  "global_rel cs0 as0 \<equiv>
     gvar store cs0 = apply_partial (gvar latest_store as0) (writes cs0 as0)
     \<and>
     write_count (gvar glb cs0) = gvar max_index as0"

definition validity_prop :: "cga_state \<Rightarrow> tms2_state \<Rightarrow> T \<Rightarrow> bool"
  where
  "validity_prop cs as t \<equiv>
          lvar begin_index as t \<le> write_count (lvar loc cs t)
        \<and> write_count (lvar loc cs t) \<le> gvar max_index as (* Added for hybrid *)
        \<and> (read_consistent (gvar store_at as (write_count (lvar loc cs t))) (lvar read_set as t))"

definition in_flight :: "cga_state \<Rightarrow> tms2_state \<Rightarrow> T \<Rightarrow> bool"
  where
  "in_flight cs as t \<equiv>
      (even (lvar loc cs t) \<longleftrightarrow> lvar write_set as t = Map.empty)
      \<and> validity_prop cs as t
      \<and> (odd (lvar loc cs t) \<longrightarrow> the_writer cs = Some t \<and> gvar glb cs = lvar loc cs t)" (* Had to add this *)

definition txn_rel :: "CGA.pc event \<Rightarrow> cga_state \<Rightarrow> tms2_state \<Rightarrow> T \<Rightarrow> bool"
  where
  "txn_rel e cs0 as0 t \<equiv>
    cga_pre e (view t cs0) \<longrightarrow>
   (case e of
      External BeginInv \<Rightarrow> lvar status as0 t = NotStarted
      |
      Internal Begin \<Rightarrow>   lvar status as0 t = BeginResponding
      |
      External BeginResp \<Rightarrow> lvar status as0 t = BeginResponding
                         \<and> (lvar begin_index as0 t \<le> write_count (lvar loc cs0 t))
      |
      Internal Read \<Rightarrow> lvar status as0 t = Pending (ReadPending (lvar addr cs0 t))
                        \<and> in_flight cs0 as0 t
      |
      External (ReadResp v) \<Rightarrow> lvar status as0 t = ReadResponding (lvar val cs0 t)
                        \<and> in_flight cs0 as0 t
      |
      Internal Write \<Rightarrow>   lvar status as0 t = Pending (WritePending (lvar addr cs0 t) (lvar val cs0 t))
                        \<and> in_flight cs0 as0 t
      |
      External WriteResp \<Rightarrow> lvar status as0 t = WriteResponding
                        \<and> validity_prop cs0 as0 t
      |
      Internal Commit \<Rightarrow>  lvar status as0 t = Pending CommitPending
                        \<and> in_flight cs0 as0 t
      |
      External CommitResp \<Rightarrow> lvar status as0 t = CommitResponding
      |
      External Abort \<Rightarrow> lvar status as0 t \<notin> {NotStarted, Ready, CommitResponding, Committed, Aborted}
      |
      External Cancel \<Rightarrow> False
      |
      _ \<Rightarrow> (  lvar status as0 t = Ready
            \<and> in_flight cs0 as0 t))"

definition sim_rel :: "cga_state \<Rightarrow> tms2_state \<Rightarrow> bool"
  where
  "sim_rel cs as \<equiv> global_rel cs as \<and> (\<forall> a t. txn_rel a cs as t)"

lemma Max_insert: "finite X \<Longrightarrow> X \<noteq> {} \<Longrightarrow> Max (insert (Suc (Max X)) X) = Suc (Max X)"
  by simp

lemma gstores_wb_max: "max_index g \<le> max_index (write_back Map.empty g)"
  by (simp add: max_index_def write_back_def)

lemmas locality_simps =
  view_def lvar_def local_def global_def localize_def gvar_def

lemma empty_wb_latest: "latest_store g t = latest_store (write_back Map.empty g) t"
  by (simp add: latest_store_def store_at_def write_back_def max_index_def)

lemma max_index_wb: "max_index (write_back ws g) = Suc (max_index g)"
  by (simp add: write_back_def max_index_def)

lemma store_wb: "n \<le> max_index g \<Longrightarrow> store_at (write_back ws g) n = store_at g n"
  by (cases n) (simp_all add: write_back_def store_at_def max_index_def nth_append)

lemma rc_sa_wc: "write_count n \<le> max_index (snd as) \<Longrightarrow> read_consistent (store_at (snd as) (write_count n)) (read_set (fst as t)) = read_consistent (store_at (write_back ws (snd as)) (write_count n)) (read_set (fst as t))"
  by (subst store_wb) simp_all

lemma "txn_rel (External BeginInv) cs as t \<Longrightarrow> txn_rel (External BeginInv) (fst cs, snd cs\<lparr>glb := Suc (Suc (glb (snd cs)))\<rparr>) (fst as, write_back Map.empty (snd as)) t"
  by (simp add: txn_rel_def cga_pre_def ext_enabled_def locality_simps)

lemma cga_txn_pres1: "the_writer cs = None \<Longrightarrow> global_inv cs \<Longrightarrow> CGA.txn_inv t e cs \<Longrightarrow> CGA.txn_inv t e (fst cs, snd cs\<lparr>glb := Suc (Suc (glb (snd cs)))\<rparr>)"
  apply (rule_tac b = e in CGA.Event_split)
  apply (simp_all add: CGA.txn_inv_def locality_simps cga_pre_def ext_enabled_def le_Suc_eq)
  apply (simp_all add: is_ready_def locality_simps CGA.is_writer_def le_Suc_eq global_inv_def writer_unique_def the_writer_def)
  by (metis option.distinct(1))+

lemma glb_inv_pres1: "global_inv cs \<Longrightarrow> global_inv (fst cs, snd cs\<lparr>glb := Suc (Suc (glb (snd cs)))\<rparr>)"
  by (simp add: global_inv_def locality_simps writer_unique_def)

lemma txn_inv_fw: "(\<And>t e. CGA.txn_inv t e cs) \<Longrightarrow> (\<forall>t e. CGA.txn_inv t e cs)"
  by auto

lemma wc_cases: "0 = write_count N \<Longrightarrow> N = 0 \<or> N = 1 \<or> N = 2"
  by (simp add: div_eq_0_iff less_2_cases write_count_def)

lemma wc_two: "write_count 2 = 1 \<or> write_count 2 = 0"
  by (simp add: write_count_def)

lemma N3_ge: "3 < N \<Longrightarrow> N div 2 - Suc 0 = n \<Longrightarrow> (\<exists>m. n = Suc m)"
  by (induct n) simp_all

lemma replicate_Suc_rev: "replicate (Suc n) x = replicate n x @ [x]"
  by (induct n) simp_all

lemma latest_store_helper: "latest_store (fst x, snd x @ replicate M (latest_store x)) = latest_store x"
  apply (simp add: latest_store_def store_at_def max_index_def)
  apply (cases "length (snd x) + M")
  apply simp_all
  apply (cases "length (snd x)")
  apply simp_all
  apply (metis lessI nth_replicate replicate_Suc)
  by (metis Suc_eq_plus1 Suc_pred add.right_neutral add_diff_cancel_left' diff_diff_add less_Suc_eq neq0_conv not_add_less1 nth_append nth_replicate)


lemmas cga_tms2_simps =
  cga_simps locality_cga local_tms_eff_def local_tms_pre_def TMS2.unfold_tms2 validity_prop_def RWMemoryList.all_simps txn_rel_def
  option.case_eq_if

lemma validity_prop_stable_stutter:
  "\<lbrakk>local_cga_pre at a cs;
    t \<noteq> at;
    write_count (lvar loc cs t) \<le> max_index (global as);
    validity_prop cs as t\<rbrakk>
    \<Longrightarrow>
    validity_prop (local_cga_eff at a cs) as t"
    by (cases rule: CGA.Event_split[of a]) (simp_all add: cga_simps validity_prop_def locality_cga)

lemma validity_prop_stable_external:
  "\<lbrakk>local_cga_pre at (External ac) cs;
    ac \<noteq> BeginInv \<or> t \<noteq> at;
    validity_prop cs as t\<rbrakk>
    \<Longrightarrow>
    validity_prop (local_cga_eff at (External ac) cs) (local_tms_eff at (External ac) as) t"
    by (cases ac) (simp_all add: cga_tms2_simps)

lemma write_count_in_domain:
  assumes "b \<notin> {External BeginInv, Internal Begin, External CommitResp, External Abort}"
  and "local_cga_pre t b cs"
  and "txn_rel b cs as t"
  and "global_rel cs as"
  and "CGA.txn_inv t b cs"
  shows "write_count (lvar loc cs t) \<le> max_index (global as)"
  using assms
  apply (cases rule: CGA.Event_split[of b])
  apply (simp_all add: cga_tms2_simps global_rel_def write_count_def)
  apply linarith+
  done

lemma index_is_valid:
  assumes "global_rel cs as"
  and "CGA.txn_inv t (Internal pc) cs"
  and "(pc = Commit \<and> even (lvar loc cs t))
       \<or> (pc = Read \<and> gvar glb cs = lvar loc cs t)"
  and "local_cga_pre t (Internal pc) cs"
  and "validity_prop cs as t"
  shows "valid_index (view t as) (write_count (lvar loc cs t))"
  using assms by (simp add: cga_tms2_simps)

lemma precondition_external:
  assumes "CGA.txn_inv at (External ac) cs0"
  and "txn_rel (External ac) cs0 as0 at"
  and "local_cga_pre at (External ac) cs0"
  shows "local_tms_pre at (External ac) as0"
  using assms
  by (cases ac) (simp_all add: cga_tms2_simps)

lemma precondition_internal_step:
  assumes "CGA.txn_inv at (Internal pc) cs0"
  and "global_rel cs0 as0"
  and "txn_rel (Internal pc) cs0 as0 at"
  and "local_cga_pre at (Internal pc) cs0"
  and "step_correspondence cs0 at pc = Some ai"
  shows "local_tms_pre at (Internal ai) as0"
  using assms by (cases ai; simp add: scf_simps cga_tms2_simps in_flight_def global_rel_def)

lemma global_rel_preserved_external:
  assumes "global_rel cs0 as0"
  and "txn_rel (External ac) cs0 as0 at"
  and "local_cga_pre at (External ac) cs0"
  and "local_tms_pre at (External ac) as0"
  shows "global_rel (local_cga_eff at (External ac) cs0) (local_tms_eff at (External ac) as0)"
  using assms
  by (cases ac) (simp_all add: cga_tms2_simps global_rel_def writes_def the_writer_def)

lemma txn_rel_preserved_external_self:
  assumes "CGA.txn_inv at (External ac) cs0"
  and "TMS2.txn_inv at (External ac) as0"
  and "global_rel cs0 as0"
  and "txn_rel (External ac) cs0 as0 at"
  and "local_cga_pre at (External ac) cs0"
  and "global_inv cs0" (* Added for hybrid *)
  shows "txn_rel b (local_cga_eff at (External ac) cs0) (local_tms_eff at (External ac) as0) at"
proof -
  {
    assume "ac = BeginResp"
    from this and assms have ?thesis
      apply (cases rule: CGA.Event_split[of b])
      apply (simp_all add: cga_tms2_simps TMS2.txn_inv_def in_flight_def global_rel_def write_count_def)
      by linarith+
  }
  moreover
  {
    assume "ac = WriteResp"
    from this assms
    and validity_prop_stable_external[where at = at and t = at and ac = ac and cs = cs0 and as = as0]
    have ?thesis
      by (cases rule: CGA.Event_split[of b]) (auto simp add: cga_tms2_simps TMS2.txn_inv_def in_flight_def global_rel_def the_writer_def)
  }
  moreover
  {
    assume ac: "ac \<noteq> BeginResp \<and> ac \<noteq> WriteResp"
    from this assms
    and validity_prop_stable_external[where at = at and t = at and ac = ac and cs = cs0 and as = as0]
    have ?thesis
      by (cases ac, cases rule: CGA.Event_split[of b]) (simp_all add: cga_tms2_simps global_rel_def in_flight_def the_writer_def split: TMS2.splits)
  }
  ultimately show ?thesis by blast
qed

lemma txn_rel_preserved_external_other:
  assumes "t \<noteq> at"
  and "CGA.txn_inv t b cs0"
  and "txn_rel b cs0 as0 t"
  and "local_cga_pre at (External ac) cs0"
  and "global_inv cs0"
  shows "txn_rel b (local_cga_eff at (External ac) cs0) (local_tms_eff at (External ac) as0) t"
  using assms validity_prop_stable_external[where at = at and t = t and ac = ac and cs = cs0 and as = as0]
  by (cases ac) (cases rule: CGA.Event_split[of b]; simp add: cga_tms2_simps in_flight_def the_writer_def; metis (no_types, lifting) the1I2)+

lemma global_rel_preserved_internal_stutter:
  assumes "CGA.global_inv cs0"
  and "CGA.txn_inv at a cs0"
  and "global_rel cs0 as0"
  and "txn_rel a cs0 as0 at"
  and a: "a = Internal pc"
  and "local_cga_pre at a cs0"
  and sc: "step_correspondence cs0 at pc = None"
  shows "global_rel (local_cga_eff at a cs0) as0"
  using assms
  apply (simp add: scf_None)
  apply (elim conjE disjE)
  by (simp_all add: cga_tms2_simps global_rel_def writes_def the_writer_def)

lemma remove_impl: "B \<Longrightarrow> A \<longrightarrow> B"
  by simp

lemma not_Ex1: "\<not> Ex1 P \<longleftrightarrow> (\<forall>x. \<not> P x \<or> (\<exists>y. P y \<and> y \<noteq> x))"
  by auto

lemma the_writer_no_change: "the_writer cs0 = Some at \<Longrightarrow> the_writer
     (\<lambda>t'. if at = t' then fst cs0 at\<lparr>lstate.status := S\<rparr>
            else fst cs0 t',
      snd cs0) = Some at"
  by (simp add: the_writer_def lvar_def local_def split: if_splits cong del: if_weak_cong)

lemma same_update [intro!]: "g = g' \<Longrightarrow> apply_partial f g = apply_partial f g'"
  by auto

lemma latest_store1[simp]: "latest_store (fst ag, snd ag @ [X]) = X"
  by (simp add: latest_store_def store_at_def max_index_def)

lemma latest_store2[simp]: "apply_partial (latest_store ag) (write_set as) = latest_store (write_back (write_set as) ag)"
  by (simp add: write_back_def)

lemma apply_partial_upd: "(apply_partial f g) (l := v) = apply_partial f (g (l \<mapsto> v))"
     by (rule ext) (simp add: apply_partial_def)

lemma if_SN: "(if x then Some y else None) = Some y' \<Longrightarrow> x \<and> y = y'"
  by (cases x) auto

lemma no_unique_ex: "\<not> (\<exists>!t. P t) \<longleftrightarrow> (\<forall>t. \<not> P t \<or> (\<exists>t'. t \<noteq> t' \<and> P t'))"
  by auto metis

lemma global_rel_preserved_internal_step:
  assumes "CGA.global_inv cs0"
  and "CGA.txn_inv at (Internal pc) cs0"
  and "global_rel cs0 as0"
  and "txn_rel (Internal pc) cs0 as0 at"
  and "local_cga_pre at (Internal pc) cs0"
  and "local_tms_pre at (Internal ai) as0"
  and "step_correspondence cs0 at pc = Some ai"
  shows "global_rel (local_cga_eff at (Internal pc) cs0) (local_tms_eff at (Internal ai) as0)"
  using assms
  apply (cases ai; simp add: scf_simps; elim conjE)
  apply (simp add: global_rel_def cga_simps locality_cga unfold_tms2 locality_tms2 txn_rel_def in_flight_def cong del: if_weak_cong)
  apply (rule same_update)
  apply (simp add: writes_def the_writer_def locality_cga)
  apply blast
  apply (simp add: global_rel_def cga_simps locality_cga unfold_tms2 locality_tms2 txn_rel_def in_flight_def max_index_wb cong del: if_weak_cong)
  apply (subst the_writer_None) back
  apply (simp add: writes_def the_writer_def locality_cga)
  apply blast
  apply (intro conjI)
  apply simp
  apply (subst the_writer_Some[where t = at])
  apply (simp add: writes_def the_writer_def locality_cga)
  apply (simp add: locality_cga)
  apply (simp add: write_count_def)
  apply (simp add: global_rel_def)
  apply (simp add: unfold_tms2 locality_tms2 cga_simps locality_cga when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI)
  apply (rule same_update)
  apply (simp add: writes_def the_writer_def locality_cga)
  apply (rule same_update)
  apply (simp add: writes_def the_writer_def locality_cga)
  apply (simp add: global_rel_def)
  apply (simp add: unfold_tms2 locality_tms2 cga_simps locality_cga apply_partial_upd cong del: if_weak_cong)
  apply (intro impI conjI)
  apply (rule same_update)
  apply (simp add: writes_def the_writer_def locality_cga)
  apply (intro conjI)
  apply blast
  apply (simp add: update_partial_def)
  apply (subgoal_tac "write_set (fst as0 at) = Map.empty")
  apply simp
  apply (simp add: txn_rel_def cga_pre_def in_flight_def locality_cga)
  apply (simp add: write_count_def)
  apply (rule same_update)
  apply (simp add: writes_def the_writer_def locality_cga update_partial_def)
  apply (intro conjI)
  apply (metis (mono_tags, lifting) the1I2)
  apply (simp add: txn_rel_def cga_pre_def in_flight_def locality_cga)
  apply (subgoal_tac "(THE t. writer (fst cs0 t)) = at")
  apply simp
  apply (subgoal_tac "(\<exists>!t. at \<noteq> t \<longrightarrow> writer (fst cs0 t))")
  apply simp
  apply (subgoal_tac "(THE t. at \<noteq> t \<longrightarrow> writer (fst cs0 t)) = at")
  apply simp
  apply (rule the_equality)
  apply simp
  apply blast
  apply (rule_tac a = at in ex1I)
  apply simp
  apply blast
  by (metis the_equality)

lemma txn_rel_self_preserved_internal_step:
  assumes "CGA.global_inv cs0"
  and "CGA.txn_inv at (Internal pc) cs0"
  and "global_rel cs0 as0"
  and "txn_rel (Internal pc) cs0 as0 at"
  and "local_cga_pre at (Internal pc) cs0"
  and "step_correspondence cs0 at pc = Some ai"
  shows "txn_rel b (local_cga_eff at (Internal pc) cs0) (local_tms_eff at (Internal ai) as0) at"
  using assms
  apply (cases ai; simp add: scf_simps; elim conjE; rule_tac b = b in CGA.Event_split; simp add: txn_rel_def)
  
  apply (simp_all add: global_rel_def cga_simps locality_cga unfold_tms2 locality_tms2 in_flight_def cong del: if_weak_cong)

  apply (simp_all add: when_fn_def value_for_def value_at_def validity_prop_def update_partial_def apply_partial_def locality_cga)
  apply (cases "even (glb (snd cs0))")

    apply (subgoal_tac "write_set (fst as0 at) (addr (fst cs0 at)) = None")
    apply (simp add: read_consistent_def value_at_def)
    apply (subst the_writer_None) back
    apply (simp add: writes_def the_writer_def locality_cga)
    apply blast
    apply (simp add: latest_store_def)
    apply simp

    apply simp
    apply (intro conjI impI)
    apply (subgoal_tac "write_set (fst as0 at) (addr (fst cs0 at)) = None")
    apply (subst the_writer_Some[where t = at])
    apply blast
    apply (simp add: locality_cga value_at_def latest_store_def)
    apply simp
    apply blast
    apply (subgoal_tac "write_set (fst as0 at) (addr (fst cs0 at)) = None")
    apply (simp add: read_consistent_def value_at_def)
    apply blast
    apply (simp add: the_writer_def locality_cga)
    apply (simp add: dom_def)
    apply (erule exE)
    apply simp
    apply (subst the_writer_Some[where t = at])
    apply (simp add: the_writer_def locality_cga)
    apply (simp add: locality_cga)
    apply (simp add: the_writer_def locality_cga)

  apply (intro conjI impI)
  by (simp_all add: write_count_def)

lemmas TROPIS_simps =
  global_rel_def cga_simps locality_cga unfold_tms2 locality_tms2 in_flight_def le_Suc_eq store_wb validity_prop_def the_writer_def max_index_wb write_count_def read_consistent_def

lemma txn_rel_other_preserved_internal_step:
  assumes "CGA.global_inv cs0"
  and "CGA.txn_inv at (Internal pc) cs0"
  and "global_rel cs0 as0"
  and "txn_rel (Internal pc) cs0 as0 at"
  and "local_cga_pre at (Internal pc) cs0"
  and "t \<noteq> at"
  and "CGA.txn_inv t b cs0"
  and "txn_rel b cs0 as0 t"
  and "step_correspondence cs0 at pc = Some ai"
  shows "txn_rel b (local_cga_eff at (Internal pc) cs0) (local_tms_eff at (Internal ai) as0) t"
  using assms
  apply (cases ai; simp add: scf_simps; elim conjE; rule_tac b = b in CGA.Event_split; simp add: txn_rel_def)
  apply (simp add: global_rel_def cga_simps locality_cga unfold_tms2 locality_tms2 in_flight_def validity_prop_def the_writer_def cong del: if_weak_cong; (metis (no_types, lifting) option.sel)?)+
  apply (simp add: TROPIS_simps cong del: if_weak_cong; blast)
  apply (simp add: TROPIS_simps cong del: if_weak_cong; blast)
  apply (simp add: TROPIS_simps cong del: if_weak_cong; blast)
  apply (simp add: TROPIS_simps cong del: if_weak_cong; blast)
  apply (simp add: TROPIS_simps cong del: if_weak_cong; blast)
  apply (simp add: TROPIS_simps cong del: if_weak_cong; blast)
  apply (simp add: TROPIS_simps cong del: if_weak_cong; blast)
  apply (simp add: TROPIS_simps cong del: if_weak_cong; blast)
  apply (simp add: TROPIS_simps cong del: if_weak_cong; blast)
  apply (simp add: TROPIS_simps cong del: if_weak_cong; blast)
  apply (simp add: TROPIS_simps cong del: if_weak_cong; blast)
  apply (simp add: cga_simps locality_tms2 local_cga_eff_def)+
  apply (simp add: global_rel_def cga_simps locality_cga unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_cga; blast?)
  apply (simp add: global_rel_def cga_simps locality_cga unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_cga; blast?)
  apply (simp add: global_rel_def cga_simps locality_cga unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_cga; blast?)
  apply (simp add: global_rel_def cga_simps locality_cga unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_cga; blast?)
  apply (simp add: global_rel_def cga_simps locality_cga unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_cga; blast?)
  apply (simp add: global_rel_def cga_simps locality_cga unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_cga; blast?)
  apply (simp add: global_rel_def cga_simps locality_cga unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)+
  apply (intro impI conjI; simp add: the_writer_def locality_cga; blast?)
  apply (simp add: global_rel_def cga_simps locality_cga unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_cga; blast?)
  apply (simp add: cga_simps locality_tms2 local_cga_eff_def)+
  apply (simp add: global_rel_def cga_simps locality_cga unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_cga; blast?)
  apply (simp add: global_rel_def cga_simps locality_cga unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_cga; blast?)
  apply (simp add: global_rel_def cga_simps locality_cga unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_cga; blast?)
  apply (simp add: global_rel_def cga_simps locality_cga unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_cga; blast?)
  apply (simp add: global_rel_def cga_simps locality_cga unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_cga; blast?)
  apply (simp add: global_rel_def cga_simps locality_cga unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_cga; blast?)
  apply (simp add: global_rel_def cga_simps locality_cga unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_cga; blast?)
  apply (simp add: global_rel_def cga_simps locality_cga unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (simp add: global_rel_def cga_simps locality_cga unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_cga; blast?)
  apply (simp add: global_rel_def cga_simps locality_cga unfold_tms2 locality_tms2 in_flight_def validity_prop_def when_fn_def cong del: if_weak_cong)
  apply (intro impI conjI; simp add: the_writer_def locality_cga; blast?)
  by (simp add: cga_simps locality_tms2 local_cga_eff_def)+

lemma txn_rel_self_preserved_stutter:
  assumes "CGA.global_inv cs0"
  and "CGA.txn_inv at (Internal pc) cs0"
  and "\<forall>c. TMS2.txn_inv at c as0"
  and "global_rel cs0 as0"
  and "txn_rel (Internal pc) cs0 as0 at"
  and "local_cga_pre at (Internal pc) cs0"
  and "step_correspondence cs0 at pc = None"
  shows "txn_rel b (local_cga_eff at (Internal pc) cs0) as0 at"
  using assms
  apply (simp add: scf_None)
  apply (elim disjE conjE; rule_tac b = b in CGA.Event_split)
  apply (simp_all add: txn_rel_def TROPIS_simps)
  apply (erule_tac x = "External BeginResp" in allE)
  by (simp add: TMS2.txn_inv_def locality_tms2 status_enabled_def ext_enabled_def)
  
lemma txn_rel_other_preserved_stutter:
  assumes "CGA.global_inv cs0"
  and "CGA.txn_inv at (Internal pc) cs0"
  and "global_rel cs0 as0"
  and "txn_rel (Internal pc) cs0 as0 at"
  and "local_cga_pre at (Internal pc) cs0"
  and "t \<noteq> at"
  and "txn_rel b cs0 as0 t"
  and "CGA.txn_inv t b cs0"
  and "step_correspondence cs0 at pc = None"
  shows "txn_rel b (local_cga_eff at (Internal pc) cs0) as0 t"
  using assms
  apply (simp add: scf_None)
  apply (elim disjE conjE; rule_tac b = b in CGA.Event_split)
  apply (simp_all add: txn_rel_def TMS2.txn_inv_def TROPIS_simps option.case_eq_if)
  apply (intro conjI impI, rule the_equality, blast, force, blast)+
  by (metis the_equality)+

lemma cga_lpre: "local_pre (interference \<rhd> CGA) = local_cga_pre"
  by (rule ext)+ (simp add: local_cga_pre_def local_pre_def CGA_def Rely_def)

lemma cga_leff: "local_eff (interference \<rhd> CGA) = local_cga_eff"
  by (rule ext)+ (simp add: local_cga_eff_def local_eff_def CGA_def Rely_def)

lemma sim_rel_external:
  "standard_sim_ext_step (interference \<rhd> CGA) (tms2_interference \<rhd> TMS2) sim_rel"
  apply (simp only: standard_sim_ext_step_def sim_rel_def cga_lpre cga_leff abs_leff abs_lpre)
  apply (intro allI impI)
  apply (elim conjE)
  apply (rename_tac cs as a t)
  apply (frule_tac t = t and e = "External a" in reachable_invariant_cga_interference)
  apply (drule_tac t = t and e = "External a" in reachable_invariant_tms2_interference)
  apply (intro conjI)
  apply (rule precondition_external; blast)
  apply (rule global_rel_preserved_external; blast?)
  using precondition_external apply blast
  apply (intro allI)
  apply (rename_tac cs as a t b t')
  apply (subgoal_tac "t = t' \<or> t \<noteq> t'")
  apply (elim disjE)
  apply simp
  apply (rule txn_rel_preserved_external_self; blast?)
  apply (rule txn_rel_preserved_external_other; blast?)
  using reachable_invariant_cga_interference apply blast
  by blast

lemma sim_rel_stutter:
  "standard_sim_stutter (interference \<rhd> CGA) (tms2_interference \<rhd> TMS2) step_correspondence sim_rel"
  apply (simp only: standard_sim_stutter_def sim_rel_def cga_lpre cga_leff abs_leff abs_lpre)
  apply (intro allI impI)
  apply (elim conjE)
  apply (rename_tac cs as a t)
  apply (frule_tac t = t and e = "Internal a" in reachable_invariant_cga_interference)
  apply (intro conjI)
  apply (rule global_rel_preserved_internal_stutter; blast?)
  apply (intro allI)
  apply (rename_tac cs as a t b t')
  apply (subgoal_tac "t = t' \<or> t \<noteq> t'")
  apply (elim disjE)
  apply simp
  apply (rule txn_rel_self_preserved_stutter; blast?)
  apply (simp add: reachable_invariant_tms2_interference)
  apply (rule txn_rel_other_preserved_stutter; blast?)
  apply (simp add: reachable_invariant_cga_interference)
  by blast

lemma sim_rel_internal_step:
  "standard_sim_int_step (interference \<rhd> CGA) (tms2_interference \<rhd> TMS2) step_correspondence sim_rel"
  apply (simp only: standard_sim_int_step_def sim_rel_def cga_lpre cga_leff abs_leff abs_lpre)
  apply (intro allI impI)
  apply (elim conjE)
  apply (rename_tac cs as ic ia t)
  apply (frule_tac t = t and e = "Internal ic" in reachable_invariant_cga_interference)
  apply (frule_tac t = t and e = "Internal ia" in reachable_invariant_tms2_interference)
  apply (intro conjI)
  using precondition_internal_step apply blast
  apply (rule global_rel_preserved_internal_step; blast?)
  using precondition_internal_step apply blast
  apply (intro allI)
  apply (rename_tac cs as ic ia t ic' t')
  apply (subgoal_tac "t = t' \<or> t \<noteq> t'")
  apply (elim disjE)
  apply simp
  apply (rule txn_rel_self_preserved_internal_step; blast?)
  apply (simp add: reachable_invariant_tms2_interference)
  apply (rule txn_rel_other_preserved_internal_step; blast?)
  apply (simp add: reachable_invariant_cga_interference)
  by blast

lemma cga_start_sim: "(a, b) \<in> starts (interference \<rhd> CGA) \<Longrightarrow> (\<exists>s. sim_rel (a, b) s \<and> s \<in> starts (tms2_interference \<rhd> TMS2))"
  apply (rule_tac x = default_start in exI)
  apply (simp add: starts_def initial_stores_def max_index_def write_count_def image_def CGA_def Rely_def default_start_def sim_rel_def gstart_def global_rel_def locality_cga mem_initial_def)
  apply (intro conjI)
  apply (rule ext)
  apply (simp add: apply_partial_def writes_def the_writer_def option.case_eq_if locality_cga latest_store_def max_index_def)
  apply (intro allI)
  apply (rename_tac e l)
  apply (rule_tac b = e in CGA.Event_split; simp add: lstarts_def txn_rel_def cga_simps locality_cga)
  by (simp add: tms_starts_def)

lemma CGA_weak_simulation:
  "weak_standard_simulation (interference \<rhd> CGA) (tms2_interference \<rhd> TMS2) step_correspondence sim_rel"
  apply (simp add: weak_standard_simulation_def del: TMS2_def)
  apply (intro conjI)
  using cga_start_sim apply auto[1]
  using sim_rel_external apply auto[1]
  using sim_rel_stutter apply auto[1]
  using sim_rel_internal_step by auto

end