theory HWTMCorrect
imports HWTM TMS2
begin

type_synonym hw_state = "(T, hw_lstate, gstate) state"

type_synonym tms2_state = "(T, Local, stores_state) state"

definition step_correspondence :: "hw_state \<Rightarrow> T \<Rightarrow> hwpc \<Rightarrow> internal_action option"
  where
  "step_correspondence cs t a \<equiv>
    case a of
        Read1 \<Rightarrow> (case lvar hw_write_set cs t (lvar hw_addr cs t) of
          Some v \<Rightarrow> Some (DoRead (lvar hw_addr cs t) (write_count (gvar glb cs)))
        | None \<Rightarrow> if validate (view t cs)
                  then Some (DoRead (lvar hw_addr cs t) (write_count (gvar glb cs)))
                  else None)
      | Write1 \<Rightarrow> Some (DoWrite (lvar hw_addr cs t) (lvar hw_val cs t))
      | Commit3 \<Rightarrow> if validate (view t cs) then Some DoCommitWriter else None
      | Commit4 \<Rightarrow> if validate (view t cs) then Some DoCommitReadOnly else None
      | _ \<Rightarrow> None"

lemma scf1: "step_correspondence cs t a = Some DoCommitReadOnly \<longleftrightarrow>
  (validate (view t cs) \<and> a = Commit4)"
  by (cases a) (auto simp add: step_correspondence_def split: option.split)

lemma scf2: "step_correspondence cs t a = Some DoCommitWriter \<longleftrightarrow>
  (validate (view t cs) \<and> a = Commit3)"
  by (cases a) (auto simp add: step_correspondence_def split: option.split)

lemma scf3: "step_correspondence cs t a = Some (DoWrite x v) \<longleftrightarrow>
  (x = lvar hw_addr cs t \<and> v = lvar hw_val cs t \<and> a = Write1)"
  by (cases a) (auto simp add: step_correspondence_def split: option.split)

lemma scf4: "step_correspondence cs t a = Some (DoRead x n) \<longleftrightarrow>
  ( (n = write_count (gvar glb cs) \<and> x = lvar hw_addr cs t \<and> a = Read1)
  \<and> ((\<exists>v. lvar hw_write_set cs t x = Some v) \<or> (lvar hw_write_set cs t (lvar hw_addr cs t) = None \<and> validate (view t cs))))"
  by (cases a) (auto simp add: step_correspondence_def split: option.split)

lemmas scf_simps = scf1 scf2 scf3 scf4

lemma scf_None: "step_correspondence cs t ic = None \<longleftrightarrow>
  ( ic = Begin1
  \<or> ic = Begin2
  \<or> ic = Begin3
  \<or> ic = Begin4
  \<or> (ic = Read1 \<and> lvar hw_write_set cs t (lvar hw_addr cs t) = None \<and> \<not> validate (view t cs))
  \<or> ic = Write2
  \<or> ic = Commit1
  \<or> ic = Commit2
  \<or> (ic = Commit3 \<and> \<not> validate (view t cs))
  \<or> (ic = Commit4 \<and> \<not> validate (view t cs)))"
  by (cases ic) (simp_all add: step_correspondence_def option.case_eq_if)

definition global_rel :: "hw_state \<Rightarrow> tms2_state \<Rightarrow> bool" where
  "global_rel cs as \<equiv> (write_count (gvar glb cs) = max_index (global as)) \<and>
                      (even (gvar glb cs) \<longrightarrow> latest_store (global as) = store (global cs))"

definition in_flight :: "T \<Rightarrow> hw_state \<Rightarrow> tms2_state \<Rightarrow> bool" where
  "in_flight t cs as \<equiv>
     (lvar hw_write_set cs t = lvar write_set as t) \<and>
     (lvar hw_read_set cs t = lvar read_set as t)"

(*
     (validate (view t cs) \<longrightarrow> (\<forall>l. lvar hw_read_set cs t l \<noteq> None \<longrightarrow> latest_store (global as) l = store (global cs) l))"
*)

definition txn_rel :: "T \<Rightarrow> hwpc event \<Rightarrow> hw_state \<Rightarrow> tms2_state \<Rightarrow> bool"
  where
  "txn_rel t e cs as \<equiv> in_flight t cs as \<and>
     (local_hw_pre t e cs \<longrightarrow>
       (case e of
          External BeginInv \<Rightarrow> lvar status as t = NotStarted
        | Internal Begin1 \<Rightarrow> lvar status as t = BeginResponding
        | Internal Begin2 \<Rightarrow> lvar status as t = BeginResponding
        | Internal Begin3 \<Rightarrow> lvar status as t = BeginResponding
        | Internal Begin4 \<Rightarrow> lvar status as t = BeginResponding
        | External BeginResp \<Rightarrow> lvar status as t = BeginResponding
        | Internal Read1 \<Rightarrow> lvar status as t = Pending (ReadPending (lvar hw_addr cs t))
        | External (ReadResp v) \<Rightarrow> lvar status as t = ReadResponding (lvar hw_val cs t)
        | Internal Write1 \<Rightarrow> lvar status as t = Pending (WritePending (lvar hw_addr cs t) (lvar hw_val cs t))
        | Internal Write2 \<Rightarrow> lvar status as t = WriteResponding
        | External WriteResp \<Rightarrow> lvar status as t = WriteResponding
        | Internal Commit1 \<Rightarrow> lvar status as t = Pending CommitPending
        | Internal Commit2 \<Rightarrow> lvar status as t = Pending CommitPending
        | Internal Commit3 \<Rightarrow> lvar status as t = Pending CommitPending
        | Internal Commit4 \<Rightarrow> lvar status as t = Pending CommitPending
        | External CommitResp \<Rightarrow> lvar status as t = CommitResponding
        | External Abort \<Rightarrow> lvar status as t \<notin> {NotStarted, Ready, CommitResponding, Committed, Aborted}
        | External Cancel \<Rightarrow> False
        | _ \<Rightarrow> lvar status as t = Ready))"

definition sim_rel :: "hw_state \<Rightarrow> tms2_state \<Rightarrow> bool"
  where
  "sim_rel cs as \<equiv> global_rel cs as \<and> (\<forall>t a. txn_rel t a cs as)"

lemma card_P_0: "(\<And>t. \<not> P t) \<Longrightarrow> card {t. P t} = 0"
  by auto

lemma cnc_leff [simp]: "local_eff (glb_change \<rhd> HWTM) = local_hw_eff"
  by (rule ext)+ (simp add: local_eff_def HWTM_def local_hw_eff_def)

lemma cnc_lpre [simp]: "local_pre (glb_change \<rhd> HWTM) = local_hw_pre"
  by (rule ext)+ (simp add: local_pre_def HWTM_def local_hw_pre_def Rely_def)

lemmas step_simps = cnc_leff abs_leff cnc_lpre abs_lpre

lemma sim_rel_global: "sim_rel cs as \<Longrightarrow> global_rel cs as"
  by (simp add: sim_rel_def)

lemma wc_Suc_Suc: "write_count (Suc (Suc n)) = max_index (write_back ws as) \<longleftrightarrow> write_count n = max_index as"
  by (simp add: write_count_def max_index_def write_back_def)

lemma store_at_wb: "store_at as (max_index as) = store cs \<Longrightarrow> store_at (write_back ws as) (max_index (write_back ws as)) = apply_partial (store cs) ws"
  by (auto simp add: write_back_def max_index_def latest_store_def)

lemma store_at_wb_point: "store_at as (max_index as) l = store cs l \<Longrightarrow> store_at (write_back ws as) (max_index (write_back ws as)) l = apply_partial (store cs) ws l"
  by (auto simp add: write_back_def max_index_def latest_store_def) (metis apply_partial_def)

lemma store_at_wb_point': "ws = ws' \<Longrightarrow> store_at as (max_index as) l = store cs l \<Longrightarrow> store_at (write_back ws as) (max_index (write_back ws as)) l = apply_partial (store cs) ws' l"
  by (auto simp add: write_back_def max_index_def latest_store_def) (metis apply_partial_def)

lemmas sim_simps =
  hw_simps locality_hw unfold_tms2 TMS2.txn_inv_def locality_tms2
  txn_rel_def in_flight_def read_consistent_def global_rel_def latest_store_def wc_Suc_Suc store_at_wb
  option.case_eq_if domIff when_fn_def value_for_def value_at_def

lemma ap_emptyness: "g = Map.empty \<Longrightarrow> f = apply_partial f g"
  by (rule ext) (simp add: apply_partial_def)

method sim_solver =
  (simp add: sim_simps; ((smt status.distinct) | (force intro!: ap_emptyness))?)

lemma txn_rel_dupD: "\<forall>t a. txn_rel t a cs as \<Longrightarrow> (\<forall>t t' a b. txn_rel t a cs as \<and> txn_rel t' b cs as)"
  by auto

lemma HWTM_weak_simulation:
  "weak_standard_simulation (glb_change \<rhd> HWTM) (tms2_interference \<rhd> TMS2) step_correspondence sim_rel"
proof (unfold weak_standard_simulation_def, intro conjI allI impI exI)
  fix cs
  assume cs_starts: "cs \<in> starts (glb_change \<rhd> HWTM)"

  hence "global_rel cs default_start"
    apply (simp add: starts_def HWTM_def image_def Rely_def unfold_tms2 global_rel_def RWMemoryList.all_simps default_start_def global_def initial_stores_def)
    apply (erule bexE)
    apply simp
    (* apply (intro conjI) *)
    by (simp_all add: write_count_def gstart_def gvar_def global_def)
  moreover
  {
    fix t e
    from cs_starts
    have "txn_rel t e cs default_start"
      apply (simp add: starts_def HWTM_def Rely_def image_def)
      apply (erule bexE)
      apply (rule_tac b = e in HWTM.Event_split)
      by (simp_all add: txn_rel_def hw_simps locality_hw in_flight_def default_start_def hw_lstarts_def latest_store_def gstart_def store_at_def initial_stores_def max_index_def)
  }
  ultimately show "sim_rel cs default_start"
    by (simp add: sim_rel_def)

  show "default_start \<in> starts (tms2_interference \<rhd> TMS2)"
    by (simp add: default_start_def Rely_def starts_def image_def tms_starts_def)
next
  show "standard_sim_int_step (glb_change \<rhd> HWTM) (tms2_interference \<rhd> TMS2) step_correspondence sim_rel"
  proof (simp only: standard_sim_int_step_def step_simps, intro allI conjI impI; (erule conjE)+)
    fix cs as ic ia t
    assume reach_hw: "reach (glb_change \<rhd> HWTM) cs"
    and reach_tms2: "reach (tms2_interference \<rhd> TMS2) as"
    and SR: "sim_rel cs as"
    and lpre: "local_hw_pre t (Internal ic) cs"
    and sc: "step_correspondence cs t ic = Some ia"

    have hw_inv: "(\<forall>t e. HWTM.txn_inv t e cs)"
      using hw_reachable_invariant reach_hw by auto
    have tms2_inv: "(\<forall>t e. TMS2.txn_inv t e as)"
      using reach_tms2 reachable_invariant_tms2_interference by auto

    from SR have flying: "(\<forall>t. in_flight t cs as)"
      by (simp add: sim_rel_def txn_rel_def)

    from sc lpre SR and hw_inv tms2_inv
    show "local_tms_pre t (Internal ia) as"
      apply (simp add: sim_rel_def)
      apply (erule conjE)
      apply (erule_tac x = t in allE)
      apply (erule_tac x = "Internal ic" in allE)
      apply (erule_tac x = t in allE)
      apply (erule_tac x = "Internal ia" in allE)
      apply (erule_tac x = t in allE)
      apply (erule_tac x = "Internal ic" in allE)
      apply (cases ia, simp_all only: scf_simps, safe)
      by sim_solver+

    from sc lpre SR and hw_inv tms2_inv
    show "sim_rel (local_hw_eff t (Internal ic) cs) (local_tms_eff t (Internal ia) as)"
      apply (simp add: sim_rel_def)
      apply (erule conjE)
      apply (rule conjI)
      apply (erule_tac x = t in allE)
      apply (erule_tac x = "Internal ic" in allE)
      apply (erule_tac x = t in allE)
      apply (erule_tac x = "Internal ia" in allE)
      apply (erule_tac x = t in allE)
      apply (erule_tac x = "Internal ic" in allE)
      apply (cases ia, simp_all only: scf_simps, safe)
      apply sim_solver
      apply sim_solver
      apply sim_solver
      apply sim_solver
      apply sim_solver
      apply (rename_tac t' ic')
      apply (subst txn_rel_def)
      apply (intro conjI)
      apply (erule_tac x = t' in allE, erule_tac x = "Internal ic" in allE) back
      apply (simp add: txn_rel_def)
      apply (drule HOL.conjunct1)
      apply (erule_tac x = t in allE)
      apply (erule_tac x = "Internal ic" in allE)
      apply (erule_tac x = t in allE)
      apply (erule_tac x = "Internal ia" in allE)
      apply (cases ia, simp_all only: scf_simps)
      apply sim_solver
      apply sim_solver
      apply sim_solver
      apply sim_solver
      apply (erule_tac x = t' in allE, erule_tac x = ic' in allE) back
      apply (erule_tac x = t in allE)
      apply (erule_tac x = "Internal ic" in allE)
      apply (erule_tac x = t in allE)
      apply (erule_tac x = "Internal ia" in allE)
      apply (cases ia; simp only: scf_simps; rule_tac b = ic' in HWTM.Event_split; simp)
      by sim_solver+
  qed
next
  show "standard_sim_stutter (glb_change \<rhd> HWTM) (tms2_interference \<rhd> TMS2) step_correspondence sim_rel"
  proof (simp only: standard_sim_stutter_def step_simps, intro allI conjI impI; (erule conjE)+)
    fix cs as ic t
    assume reach_hw: "reach (glb_change \<rhd> HWTM) cs"
    and reach_tms2: "reach (tms2_interference \<rhd> TMS2) as"
    and SR: "sim_rel cs as"
    and lpre: "local_hw_pre t (Internal ic) cs"
    and sc: "step_correspondence cs t ic = None"

    have hw_inv: "(\<forall>t e. HWTM.txn_inv t e cs)"
      using hw_reachable_invariant reach_hw by auto
    have tms2_inv: "(\<forall>t e. TMS2.txn_inv t e as)"
      using reach_tms2 reachable_invariant_tms2_interference by auto

    from sc lpre SR and hw_inv tms2_inv
    show "sim_rel (local_hw_eff t (Internal ic) cs) as"
      apply (simp add: sim_rel_def)
      apply (erule conjE)
      apply (rule conjI)
      apply (erule_tac x = t in allE, erule_tac x = "Internal ic" in allE)
      apply (erule_tac x = t in allE, erule_tac x = "Internal ic" in allE)
      apply (simp add: global_rel_def)
      apply (simp add: scf_None disj_assoc[symmetric] del: disj_assoc)
      apply ((erule disjE)+; sim_solver)
      apply (intro allI)
      apply (rename_tac t' a)
      apply (erule_tac x = t in allE, erule_tac x = "Internal ic" in allE)
      apply (drule txn_rel_dupD)
      apply (erule_tac x = t' in allE, erule_tac x = t in allE, erule_tac x = a in allE, erule_tac x = "Internal ic" in allE)
      apply (simp add: scf_None disj_assoc[symmetric] del: disj_assoc, (erule disjE)+;
             rule_tac b = a in HWTM.Event_split; simp)
      by sim_solver+
  qed
next
  show "standard_sim_ext_step (glb_change \<rhd> HWTM) (tms2_interference \<rhd> TMS2) sim_rel"
  proof (simp only: standard_sim_ext_step_def step_simps, intro allI conjI impI; (erule conjE)+)
    fix cs as e t
    assume reach_hw: "reach (glb_change \<rhd> HWTM) cs"
    and reach_tms2: "reach (tms2_interference \<rhd> TMS2) as"
    and SR: "sim_rel cs as"
    and lpre: "local_hw_pre t (External e) cs"

    have hw_inv: "(\<forall>t e. HWTM.txn_inv t e cs)"
      using hw_reachable_invariant reach_hw by auto
    have tms2_inv: "(\<forall>t e. TMS2.txn_inv t e as)"
      using reach_tms2 reachable_invariant_tms2_interference by auto

    from lpre SR and hw_inv tms2_inv
    show "local_tms_pre t (External e) as"
      apply (simp add: sim_rel_def)
      apply (erule conjE)
      apply (erule_tac x = t in allE, erule_tac x = "External e" in allE)+
      apply (cases e)
      by (simp_all add: hw_simps locality_hw unfold_tms2 locality_tms2 global_rel_def txn_rel_def)

    from lpre SR and hw_inv tms2_inv
    show "sim_rel (local_hw_eff t (External e) cs) (local_tms_eff t (External e) as)"
      apply (simp add: sim_rel_def)
      apply (erule conjE)
      apply (rule conjI)
      apply (erule_tac x = t in allE, erule_tac x = "External e" in allE)+
      apply (cases e; simp add: hw_simps locality_hw unfold_tms2 locality_tms2 global_rel_def txn_rel_def)
      apply (drule txn_rel_dupD)
      apply (erule_tac x = t in allE, erule_tac x = "External e" in allE)+
      apply (intro allI)
      apply (rename_tac t' a)
      apply (erule_tac x = t' in allE, erule_tac x = t in allE, erule_tac x = a in allE, erule_tac x = "External e" in allE)
      apply (cases e; rule_tac b = a in HWTM.Event_split)
      by (simp_all add: hw_simps locality_hw unfold_tms2 locality_tms2 global_rel_def txn_rel_def in_flight_def)
  qed
qed

end