theory HWTM
imports Shared "~~/src/HOL/Eisbach/Eisbach_Tools"
begin

datatype hwpc =
  Begin1 | Begin2 | Begin3 | Begin4
| Read1
| Write1 | Write2
| Commit1 | Commit2 | Commit3 | Commit4

type_synonym hw_status = "hwpc status"

record hw_lstate =
  hw_status :: hw_status
  hw_val :: V
  hw_loc :: nat
  hw_addr :: L
  hw_writer :: bool
  hw_write_set :: "L \<Rightarrow> V option"
  hw_read_set :: "L \<Rightarrow> V option"

definition validate :: "hw_lstate \<times> gstate \<Rightarrow> bool" where
  "validate s \<equiv> (\<forall>l v. hw_read_set (fst s) l = Some v \<longrightarrow> store (snd s) l = v) \<and> hw_loc (fst s) = glb (snd s)"

definition update_hw_status :: "hw_status \<Rightarrow> hw_lstate \<times> gstate \<Rightarrow> hw_lstate \<times> gstate" where
  "update_hw_status st \<equiv> apfst (\<lambda>l. l \<lparr> hw_status := st \<rparr>)"

definition update_hw_val :: "V \<Rightarrow> hw_lstate \<times> gstate \<Rightarrow> hw_lstate \<times> gstate" where
  "update_hw_val v \<equiv> apfst (\<lambda>l. l \<lparr> hw_val := v \<rparr>)"

definition update_hw_loc :: "nat \<Rightarrow> hw_lstate \<times> gstate \<Rightarrow> hw_lstate \<times> gstate" where
  "update_hw_loc n \<equiv> apfst (\<lambda>l. l \<lparr> hw_loc := n \<rparr>)"

definition update_hw_addr :: "L \<Rightarrow> hw_lstate \<times> gstate \<Rightarrow> hw_lstate \<times> gstate" where
  "update_hw_addr l \<equiv> apfst (\<lambda>s. s \<lparr> hw_addr := l\<rparr>)"

definition update_hw_writer :: "bool \<Rightarrow> hw_lstate \<times> gstate \<Rightarrow> hw_lstate \<times> gstate" where
  "update_hw_writer b \<equiv> apfst (\<lambda>l. l \<lparr> hw_writer := b \<rparr>)"

definition update_hw_write_set :: "L \<Rightarrow> V \<Rightarrow> hw_lstate \<times> gstate \<Rightarrow> hw_lstate \<times> gstate" where
  "update_hw_write_set l v \<equiv> apfst (\<lambda>s. s \<lparr> hw_write_set := update_partial l v (hw_write_set s)\<rparr>)"  

definition update_hw_read_set :: "L \<Rightarrow> V \<Rightarrow> hw_lstate \<times> gstate \<Rightarrow> hw_lstate \<times> gstate" where
  "update_hw_read_set l v \<equiv> apfst (\<lambda>s. s \<lparr> hw_read_set := update_partial l v (hw_read_set s)\<rparr>)"  

definition HW_FLUSH :: "hw_lstate \<times> gstate \<Rightarrow> hw_lstate \<times> gstate" where
  "HW_FLUSH \<equiv> (\<lambda>s. (fst s, snd s \<lparr> store := apply_partial (store (snd s)) (hw_write_set (fst s)) \<rparr>))"

definition hw_lstarts :: "(T \<Rightarrow> hw_lstate) set" where
  "hw_lstarts \<equiv> {s. (\<forall>t. hw_status (s t) = NotStarted \<and> hw_writer (s t) = False \<and> hw_write_set (s t) = Map.empty \<and> hw_read_set (s t) = Map.empty)}"

definition hw_pre :: "hwpc event \<Rightarrow> hw_lstate \<times> gstate \<Rightarrow> bool" where
  "hw_pre e s \<equiv>
      e \<noteq> External Cancel
    \<and> (e = External Abort \<longrightarrow> hw_status (fst s) = AbortPending)
    \<and> (case e of
           External a \<Rightarrow> ext_enabled (hw_status (fst s)) a
         | Internal a \<Rightarrow> hw_status (fst s) = Pending a)"

notation fcomp (infixl "\<circ>>" 60)

definition XABORT :: "hw_lstate \<times> gstate \<Rightarrow> hw_lstate \<times> gstate" where
  "XABORT = update_hw_status AbortPending"

definition XEND :: "(nat \<Rightarrow> nat) \<Rightarrow> hw_lstate \<times> gstate \<Rightarrow> hw_lstate \<times> gstate" where
  "XEND counterU \<equiv> \<lambda>s. if validate s then update_hw_status CommitResponding (apsnd (counter_update counterU) (HW_FLUSH s)) else XABORT s"

definition XBEGIN :: "hw_lstate \<times> gstate \<Rightarrow> hw_lstate \<times> gstate" where
  "XBEGIN \<equiv> id"

fun ext_eff :: "action \<Rightarrow> hw_lstate \<times> gstate \<Rightarrow> hw_lstate \<times> gstate" where
  "ext_eff BeginInv = update_hw_status (Pending Begin1)"
| "ext_eff BeginResp = update_hw_status Ready"
| "ext_eff CommitInv = update_hw_status (Pending Commit1)"
| "ext_eff CommitResp = update_hw_status Committed"
| "ext_eff Abort = update_hw_status Aborted"
| "ext_eff (ReadInv l) = update_hw_addr l \<circ>> update_hw_status (Pending Read1)"
| "ext_eff (ReadResp v) = update_hw_status Ready"
| "ext_eff (WriteInv l v) = update_hw_addr l \<circ>> update_hw_val v \<circ>> update_hw_status (Pending Write1)"
| "ext_eff WriteResp = update_hw_status Ready"
| "ext_eff Cancel = undefined"

definition HW_READ :: "hw_lstate \<times> gstate \<Rightarrow> hw_lstate \<times> gstate" where
  "HW_READ s \<equiv>
    case hw_write_set (fst s) (hw_addr (fst s)) of
      Some v \<Rightarrow> update_hw_val v (update_hw_status (ReadResponding v) s)
    | None \<Rightarrow> if validate s
              then let v = store (snd s) (hw_addr (fst s)) in (update_hw_val v \<circ>> update_hw_read_set (hw_addr (fst s)) v \<circ>> update_hw_status (ReadResponding v)) s
              else update_hw_status AbortPending s"

definition HW_WRITE :: "hw_lstate \<times> gstate \<Rightarrow> hw_lstate \<times> gstate" where
  "HW_WRITE s \<equiv> update_hw_write_set (hw_addr (fst s)) (hw_val (fst s)) s"

fun int_eff :: "hwpc \<Rightarrow> hw_lstate \<times> gstate \<Rightarrow> hw_lstate \<times> gstate" where
  "int_eff Begin1 = XBEGIN \<circ>> update_hw_status (Pending Begin2)"
| "int_eff Begin2 = (\<lambda>s. update_hw_loc (glb (snd s)) s) \<circ>> update_hw_status (Pending Begin3)"
| "int_eff Begin3 = (\<lambda>s. if odd (hw_loc (fst s)) then update_hw_status (Pending Begin4) s else update_hw_status BeginResponding s)"
| "int_eff Begin4 = XABORT"

| "int_eff Read1 = HW_READ"

| "int_eff Write1 = HW_WRITE \<circ>> update_hw_status (Pending Write2)"
| "int_eff Write2 = update_hw_writer True \<circ>> update_hw_status WriteResponding"

| "int_eff Commit1 = (\<lambda>s. if hw_writer (fst s) then update_hw_status (Pending Commit2) s else update_hw_status (Pending Commit4) s)"
| "int_eff Commit2 = update_hw_status (Pending Commit3)"
| "int_eff Commit3 = XEND (op + 1)"
| "int_eff Commit4 = XEND id"

definition commits :: "(T, hw_lstate, gstate) state \<Rightarrow> nat" where
  "commits s = card {t. lvar hw_status s t = CommitResponding \<or> lvar hw_status s t = Committed}" 

fun hw_eff :: "hwpc event \<Rightarrow> hw_lstate \<times> gstate \<Rightarrow> hw_lstate \<times> gstate" where
  "hw_eff (Internal a) = int_eff a"
| "hw_eff (External a) = ext_eff a"

definition HWTM :: "(T, hw_lstate, gstate, hwpc) RGA" where
  "HWTM \<equiv> \<lparr> local_starts = hw_lstarts, global_start = gstart, pre = hw_pre, eff = hw_eff, environment = Id \<rparr>"

definition global_inv :: "(T, hw_lstate, gstate) state \<Rightarrow> bool" where
  "global_inv s \<equiv> True"

definition is_writer :: "(T, hw_lstate, gstate) state \<Rightarrow> T \<Rightarrow> bool" where
  "is_writer s t \<equiv> (\<exists>l. lvar hw_write_set s t l \<noteq> None)"

definition local_hw_eff :: "T \<Rightarrow> hwpc event \<Rightarrow> (T \<Rightarrow> hw_lstate) \<times> gstate \<Rightarrow> (T \<Rightarrow> hw_lstate) \<times> gstate" where
  "local_hw_eff t a = localize t (hw_eff a)" 

definition local_hw_pre :: "T \<Rightarrow> hwpc event \<Rightarrow> (T, hw_lstate, gstate) state \<Rightarrow> bool" where
  "local_hw_pre t a s = hw_pre a (view t s)"

definition reads_writes_empty :: "T \<Rightarrow> (T, hw_lstate, gstate) state \<Rightarrow> bool" where
  "reads_writes_empty t s \<equiv> lvar hw_read_set s t = Map.empty \<and> lvar hw_write_set s t = Map.empty"

definition is_writer_ws :: "T \<Rightarrow> (T, hw_lstate, gstate) state \<Rightarrow> bool" where
  "is_writer_ws t s \<equiv> lvar hw_writer s t \<longleftrightarrow> (lvar hw_write_set s t \<noteq> Map.empty)"

definition txn_inv :: "T \<Rightarrow> hwpc event \<Rightarrow> (T, hw_lstate, gstate) state \<Rightarrow> bool" where
  "txn_inv t e s \<equiv> local_hw_pre t e s \<longrightarrow>
   (case e of
     External BeginInv \<Rightarrow> reads_writes_empty t s \<and> is_writer_ws t s
   | Internal Begin1 \<Rightarrow> reads_writes_empty t s \<and> is_writer_ws t s
   | Internal Begin2 \<Rightarrow> reads_writes_empty t s \<and> is_writer_ws t s
   | Internal Begin3 \<Rightarrow> reads_writes_empty t s \<and> is_writer_ws t s
   | Internal Begin4 \<Rightarrow> reads_writes_empty t s \<and> is_writer_ws t s \<and> odd (lvar hw_loc s t)
   | External BeginResp \<Rightarrow> reads_writes_empty t s \<and> is_writer_ws t s \<and> even (lvar hw_loc s t)
   | External (ReadResp v) \<Rightarrow> v = lvar hw_val s t \<and> is_writer_ws t s \<and> even (lvar hw_loc s t)
   | Internal Write1 \<Rightarrow> is_writer_ws t s \<and> even (lvar hw_loc s t)
   | Internal Write2 \<Rightarrow> lvar hw_write_set s t \<noteq> Map.empty \<and> (lvar hw_writer s t \<longrightarrow> (lvar hw_write_set s t \<noteq> Map.empty)) \<and> even (lvar hw_loc s t)
   | External WriteResp \<Rightarrow> lvar hw_write_set s t \<noteq> Map.empty \<and> is_writer_ws t s \<and> even (lvar hw_loc s t)
   | Internal Commit2 \<Rightarrow> lvar hw_writer s t \<and> lvar hw_write_set s t \<noteq> Map.empty \<and> even (lvar hw_loc s t)
   | Internal Commit3 \<Rightarrow> lvar hw_writer s t \<and> lvar hw_write_set s t \<noteq> Map.empty \<and> even (lvar hw_loc s t)
   | Internal Commit4 \<Rightarrow> \<not> lvar hw_writer s t \<and> lvar hw_write_set s t = Map.empty \<and> even (lvar hw_loc s t)
   | External Abort \<Rightarrow> is_writer_ws t s
   | _ \<Rightarrow> is_writer_ws t s \<and> even (lvar hw_loc s t))"

lemma Event_split:
  "\<lbrakk>b = External BeginInv \<Longrightarrow> P;
    b = Internal Begin1 \<Longrightarrow> P;
    b = Internal Begin2 \<Longrightarrow> P;
    b = Internal Begin3 \<Longrightarrow> P;
    b = Internal Begin4 \<Longrightarrow> P;
    b = External BeginResp \<Longrightarrow> P;
    \<And> l. b = External (ReadInv l) \<Longrightarrow> P;
    b = Internal Read1 \<Longrightarrow> P;
    \<And> v. b = External (ReadResp v) \<Longrightarrow> P;
    \<And> l v. b = External (WriteInv l v) \<Longrightarrow> P;
    b = Internal Write1 \<Longrightarrow> P;
    b = Internal Write2 \<Longrightarrow> P;
    b = External WriteResp \<Longrightarrow> P;
    b = External CommitInv \<Longrightarrow> P;
    b = Internal Commit1 \<Longrightarrow> P;
    b = Internal Commit2 \<Longrightarrow> P;
    b = Internal Commit3 \<Longrightarrow> P;
    b = Internal Commit4 \<Longrightarrow> P;
    b = External CommitResp \<Longrightarrow> P;
    b = External Cancel \<Longrightarrow> P;
    b = External Abort \<Longrightarrow> P \<rbrakk>
   \<Longrightarrow>
    P"
  apply (cases rule: event.exhaust[where y = b])
  apply simp_all
  using action.exhaust apply blast
  using hwpc.exhaust by blast

lemmas locality_hw =
  local_hw_eff_def
  local_hw_pre_def
  view_def
  local_def
  localize_def
  global_def
  lvar_def
  gvar_def

lemmas hw_updates =
  update_hw_status_def
  update_hw_val_def
  update_hw_loc_def
  update_hw_addr_def
  update_hw_writer_def
  update_hw_write_set_def
  update_hw_read_set_def
  HW_FLUSH_def
  HW_READ_def
  HW_WRITE_def
  XEND_def
  XABORT_def
  XBEGIN_def

lemmas hw_simps =
  txn_inv_def
  hw_pre_def
  ext_enabled_def
  hw_updates
  global_inv_def
  reads_writes_empty_def
  is_writer_ws_def
  validate_def
  update_partial_def

lemma txn_inv_pres_ext_other:
  "\<lbrakk>txn_inv t b s;
    txn_inv at (External e) s;
    t \<noteq> at;
    local_hw_pre at (External e) s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_hw_eff at (External e) s)"
   by (cases rule: Event_split[of b]) (auto simp add: hw_simps locality_hw)

lemma txn_inv_pres_int_other:
  "\<lbrakk>txn_inv t b s;
    txn_inv at (Internal i) s;
    t \<noteq> at;
    local_hw_pre at (Internal i) s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_hw_eff at (Internal i) s)"
   by (cases i; cases rule: Event_split[of b]) (simp_all add: hw_simps locality_hw split: option.split)

lemma txn_inv_pres_other:
  "\<lbrakk>txn_inv t b s; txn_inv at a s; t \<noteq> at; local_hw_pre at a s\<rbrakk> \<Longrightarrow> txn_inv t b (local_hw_eff at a s)"
  by (metis hw_eff.elims txn_inv_pres_ext_other txn_inv_pres_int_other)

lemma txn_inv_pres_ext_self:
  "\<lbrakk>txn_inv t (External e) s;
    local_hw_pre t (External e) s\<rbrakk>
   \<Longrightarrow> txn_inv t b (local_hw_eff t (External e) s)"
   by (cases e; cases rule: Event_split[of b]) (auto simp add: hw_simps locality_hw)

lemma txn_inv_pres_int_self:
  "\<lbrakk>txn_inv t (Internal i) s;
    local_hw_pre t (Internal i) s\<rbrakk>
   \<Longrightarrow> txn_inv t b (local_hw_eff t (Internal i) s)"
   by (cases i; cases rule: Event_split[of b]) (auto simp add: hw_simps locality_hw split: option.split)

lemma txn_inv_pres_self:
  "\<lbrakk>txn_inv t e s; local_hw_pre t e s\<rbrakk> \<Longrightarrow> txn_inv t b (local_hw_eff t e s)"
  by (metis event.exhaust txn_inv_pres_ext_self txn_inv_pres_int_self)

lemma txn_inv_pres:
  "\<lbrakk>txn_inv t b s; txn_inv at a s; local_hw_pre at a s\<rbrakk> \<Longrightarrow> txn_inv t b (local_hw_eff at a s)"
  using txn_inv_pres_other txn_inv_pres_self by blast

lemma loc_HWTM: "localize t (eff HWTM a) s = local_hw_eff t a s"
  by (simp add: HWTM_def local_hw_eff_def)

lemma pre_HWTM: "pre HWTM a (view t s) = local_hw_pre t a s"
  by (simp add: HWTM_def local_hw_pre_def)

method starts_solver =
  (simp add: locality_hw starts_of_def HWTM_def ioa_def starts_def image_def hw_lstarts_def;
   erule exE;
   simp add: reads_writes_empty_def is_writer_ws_def locality_hw)

lemma total_inv: "invariant (ioa (glb_change \<rhd> HWTM)) (\<lambda>s. global_inv s \<and> (\<forall>e t. txn_inv t e s))"
proof (intro invariant_intro impI conjI allI)
  fix s
  assume "s \<in> starts (glb_change \<rhd> HWTM)"
  thus "global_inv s"
    by (simp add: global_inv_def)
next
  fix s e t
  assume "s \<in> starts (glb_change \<rhd> HWTM)"
  thus "txn_inv t e s"
    by - (rule_tac b = e in Event_split; simp_all add: Rely_def txn_inv_def locality_hw hw_pre_def ext_enabled_def; starts_solver)
next
  fix s
  assume "reach (glb_change \<rhd> HWTM) s"
  show "preserved (glb_change \<rhd> HWTM) (\<lambda>s. global_inv s \<and> (\<forall>e t. txn_inv t e s)) s"
  proof (simp add: preserved_def global_preserved_def global_inv_def local_preserved_def Rely_def loc_HWTM pre_HWTM, safe)
    fix at a b t
    assume "\<forall>e t. txn_inv t e s"
    and "local_hw_pre at a s"
    thus "txn_inv t b (local_hw_eff at a s)"
      by (simp add: txn_inv_pres)
  next
    fix g e t
    assume "(global s, g) \<in> glb_change"
    and "\<forall>e t. txn_inv t e s"
    thus "txn_inv t e (fst s, g)"
      apply -
      apply (erule_tac x = e in allE)
      apply (erule_tac x = t in allE)
      apply (rule_tac b = e in Event_split)
      by (simp_all add: glb_change_def hw_simps locality_hw)
  qed
qed

lemma hw_reach_inv: "reach (glb_change \<rhd> HWTM) s \<Longrightarrow> txn_inv t e s"
  using invariant_elim total_inv by blast

end