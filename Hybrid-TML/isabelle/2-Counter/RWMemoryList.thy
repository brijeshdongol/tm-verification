theory RWMemoryList
  imports Main Utilities
begin

typedecl L

typedecl V
consts v0 :: "V"

type_synonym write_set = "L \<Rightarrow> V option"

definition mem_initial :: "L \<Rightarrow> V" where
  "mem_initial \<equiv> \<lambda>l. v0"

type_synonym stores_state = "(L \<Rightarrow> V) \<times> (L \<Rightarrow> V) list"

definition update_partial :: "L \<Rightarrow> V \<Rightarrow> (L \<Rightarrow> V option) \<Rightarrow> L \<Rightarrow> V option"
  where
  "update_partial l v p \<equiv> p (l := (Some v))"

definition make_write_set :: "(L \<times> V) list \<Rightarrow> write_set" where
  "make_write_set writes = foldr (case_prod update_partial) writes Map.empty"

lemma mws_map: "make_write_set ws = map_of ws"
  apply (induct ws)
  apply (simp_all add: make_write_set_def)
  apply (rule ext)
  by (simp add: case_prod_beta' update_partial_def)

definition writes_of :: "(L \<Rightarrow> V option) \<Rightarrow> (L \<times> V) set" where
  "writes_of ws \<equiv> {(l, v). l \<in> dom ws \<and> ws l = Some v}"

definition write_list :: "write_set \<Rightarrow> (L \<times> V) list" where
  "write_list ws = (SOME writes. writes_of ws = set writes)"

lemma writes_of_image_def: "writes_of ws = (\<lambda>l. (l, the (ws l))) ` dom ws"
  by (auto simp add: writes_of_def image_def)

lemma mws_Cons [simp]: "make_write_set ((l, v) # writes) = update_partial l v (make_write_set writes)"
  by (simp add: make_write_set_def)
  
lemma insert_h1: "X = insert y Y \<Longrightarrow> y \<notin> Y \<Longrightarrow> X - {y} = Y"
  by simp

lemma insert_h2: "X = insert y Y \<Longrightarrow> y \<in> Y \<Longrightarrow> X = Y"
  by (simp add: insert_absorb)

definition delete_partial :: "L \<Rightarrow> (L \<Rightarrow> V option) \<Rightarrow> L \<Rightarrow> V option" where
  "delete_partial l p \<equiv> p (l := None)"

lemma writes_of_delete: "ws l = Some v \<Longrightarrow> writes_of ws - {(l, v)} = writes_of (delete_partial l ws)"
  by (auto simp add: writes_of_def delete_partial_def)

lemma delete_update: "ws l = Some v \<Longrightarrow> delete_partial l ws = ws' \<Longrightarrow> ws = update_partial l  v ws'"
  by (auto simp add: delete_partial_def update_partial_def)

lemma writes_of_set: "writes_of ws = set writes \<Longrightarrow> ws = make_write_set writes"
proof (induct writes arbitrary: ws)
  case Nil thus ?case
    by (simp add: writes_of_def make_write_set_def) auto[1]
next
  case (Cons w writes)

  obtain l v where w_def: "w = (l, v)"
    by (cases w) simp

  from Cons
  show ?case
    apply (simp add: w_def)
    apply (cases "(l, v) \<in> set writes")
    apply (drule_tac X = "writes_of ws" in insert_h2)
    apply assumption
    using fun_upd_triv update_partial_def writes_of_def apply fastforce
    apply (drule_tac X = "writes_of ws" in insert_h1)
    apply assumption
    apply (erule rev_mp)+
    apply (subst writes_of_delete)
    using Cons.prems w_def writes_of_def apply auto[1]
    apply (intro impI)
    apply (rule delete_update)
    apply (metis (mono_tags, lifting) Cons.prems case_prod_beta' list.set_intros(1) mem_Collect_eq prod.sel(1) prod.sel(2) w_def writes_of_def)
    by blast
qed

definition max_index :: "stores_state \<Rightarrow> nat" where
  "max_index s = length (snd s)"

definition store_at :: "stores_state \<Rightarrow> nat \<Rightarrow> L \<Rightarrow> V" where
  "store_at s n \<equiv> case n of 0 \<Rightarrow> fst s | Suc n \<Rightarrow> snd s ! n"

lemma [simp]: "store_at s 0 = fst s"
  by (simp add: store_at_def)

lemma [simp]: "store_at s (Suc n) = snd s ! n"
  by (simp add: store_at_def)

lemma store_at_eq: "store_at s n = store_at t n \<longleftrightarrow> ((n = 0 \<and> fst s = fst t) \<or> (\<exists>m. n = Suc m \<and> snd s ! m = snd t ! m))"
  by (cases n) simp_all

definition value_at :: "stores_state \<Rightarrow> nat \<Rightarrow> L \<Rightarrow> V" where
  "value_at s n l = store_at s n l"

definition value_for :: "stores_state \<Rightarrow> nat \<Rightarrow> write_set \<Rightarrow> L \<Rightarrow> V" where
  "value_for s n ws l \<equiv>
      case (ws l) of
        Some v \<Rightarrow> v
      | None \<Rightarrow> value_at s n l"

definition latest_store :: "stores_state \<Rightarrow> L \<Rightarrow> V" where
  "latest_store s \<equiv> store_at s (max_index s)"

definition write_back :: "write_set \<Rightarrow> stores_state \<Rightarrow> stores_state" where
  "write_back ws s \<equiv> (fst s, snd s @ [apply_partial (latest_store s) ws])"

definition initial_stores :: "stores_state" where
  "initial_stores \<equiv> (mem_initial, [])"

definition read_consistent :: "(L \<Rightarrow> V) \<Rightarrow> (L \<Rightarrow> V option) \<Rightarrow> bool"              
  where
  "read_consistent st rs \<equiv>
    \<forall> l. case (rs l) of Some v \<Rightarrow> v = st l | None \<Rightarrow> True"

lemmas all_lookups =
  value_at_def latest_store_def max_index_def value_for_def read_consistent_def

lemmas all_updates =
  update_partial_def write_back_def

lemma [simp]: "(\<forall>m. n \<noteq> Suc m) \<longleftrightarrow> n = 0"
  using not0_implies_Suc by blast

lemmas all_simps =
  store_at_eq
  all_lookups
  all_updates

definition stores_append :: "stores_state \<Rightarrow> (L \<Rightarrow> V) list \<Rightarrow> stores_state" (infixl "\<bowtie>" 64) where
  "stores_append s s' \<equiv> (fst s, snd s @ s')"

lemma stores_append_assoc: "(s \<bowtie> t) \<bowtie> u = s \<bowtie> (t @ u)"
  by (simp add: stores_append_def)  

lemma max_index_wb: "max_index (write_back ws g) = Suc (max_index g)"
  by (simp add: write_back_def max_index_def)

lemma ap_empty [simp]: "apply_partial x Map.empty = x"
  apply (rule ext)
  by (simp add: apply_partial_def)

lemma apply_partial_upd: "(apply_partial f g) (l := v) = apply_partial f (g (l \<mapsto> v))"
     by (rule ext) (simp add: apply_partial_def)

end