theory TML
imports Shared
begin

datatype pc =
    Begin1
  | Begin2
  | Begin3
  | Commit1
  | Commit2
  | Read1
  | Read2
  | Read3
  | Write1
  | Write2
  | Write4
  | Write5

type_synonym status = "pc status"

record lstate =
  status :: status
  val :: V
  loc :: nat
  lcounter :: nat
  addr :: L
  writer :: bool

definition update_status :: "status \<Rightarrow> lstate \<times> gstate \<Rightarrow> lstate \<times> gstate" where
  "update_status st \<equiv> apfst (\<lambda>l. l \<lparr> status := st \<rparr>)"

definition update_val :: "V \<Rightarrow> lstate \<times> gstate \<Rightarrow> lstate \<times> gstate" where
  "update_val v \<equiv> apfst (\<lambda>l. l \<lparr> val := v \<rparr>)"

definition update_loc :: "nat \<Rightarrow> lstate \<times> gstate \<Rightarrow> lstate \<times> gstate" where
  "update_loc n \<equiv> apfst (\<lambda>l. l \<lparr> loc := n \<rparr>)"

definition update_lcounter :: "nat \<Rightarrow> lstate \<times> gstate \<Rightarrow> lstate \<times> gstate" where
  "update_lcounter n \<equiv> apfst (\<lambda>l. l \<lparr> lcounter := n \<rparr>)"

definition update_addr :: "L \<Rightarrow> lstate \<times> gstate \<Rightarrow> lstate \<times> gstate" where
  "update_addr l \<equiv> apfst (\<lambda>s. s \<lparr> addr := l\<rparr>)"

definition update_writer :: "bool \<Rightarrow> lstate \<times> gstate \<Rightarrow> lstate \<times> gstate" where
  "update_writer b \<equiv> apfst (\<lambda>l. l \<lparr> writer := b \<rparr>)"

definition lstarts :: "(T \<Rightarrow> lstate) set" where
  "lstarts \<equiv> {s. (\<forall>t. status (s t) = NotStarted \<and> writer (s t) = False)}"

definition tml_pre :: "pc event \<Rightarrow> lstate \<times> gstate \<Rightarrow> bool" where
  "tml_pre e s \<equiv>
      e \<noteq> External Cancel
    \<and> (e = External Abort \<longrightarrow> status (fst s) = AbortPending)
    \<and> (case e of
           External a \<Rightarrow> ext_enabled (status (fst s)) a
         | Internal a \<Rightarrow> status (fst s) = Pending a)"

notation fcomp (infixl "\<circ>>" 60)

fun ext_eff :: "action \<Rightarrow> lstate \<times> gstate \<Rightarrow> lstate \<times> gstate" where
  "ext_eff BeginInv = update_status (Pending Begin1)"
| "ext_eff BeginResp = update_status Ready"
| "ext_eff CommitInv = update_status (Pending Commit1)"
| "ext_eff CommitResp = update_status Committed"
| "ext_eff Abort = update_status Aborted"
| "ext_eff (ReadInv l) = update_addr l \<circ>> update_status (Pending Read1)"
| "ext_eff (ReadResp v) = update_status Ready"
| "ext_eff (WriteInv l v) = (update_addr l \<circ>> update_val v \<circ>> update_status (Pending Write1))"
| "ext_eff WriteResp = update_status Ready"
| "ext_eff Cancel = undefined"

fun int_eff :: "pc \<Rightarrow> lstate \<times> gstate \<Rightarrow> lstate \<times> gstate" where
  "int_eff Begin1 = (\<lambda>s. update_loc (glb (snd s)) (update_status (Pending Begin2) s))"
| "int_eff Begin2 = (\<lambda>s. update_lcounter (counter (snd s)) (update_status (Pending Begin3) s))"
| "int_eff Begin3 = (\<lambda>s. if even (loc (fst s))
                         then update_status BeginResponding s
                         else update_status (Pending Begin1) s)"
| "int_eff Commit1 = (\<lambda>s. if odd (loc (fst s))
                          then update_status (Pending Commit2) s
                          else update_status CommitResponding s)"
| "int_eff Commit2 = apsnd (glb_update (op + 1)) \<circ>> update_writer False \<circ>> update_status CommitResponding"
| "int_eff Read1 = (\<lambda>s. update_val (store (snd s) (addr (fst s))) (update_status (Pending Read2) s))"
| "int_eff Read2 = (\<lambda>s. if loc (fst s) = glb (snd s)
                        then update_status (Pending Read3) s
                        else update_status AbortPending s)"
| "int_eff Read3 = (\<lambda>s. if lcounter (fst s) = counter (snd s)
                        then update_status (ReadResponding (val (fst s))) s
                        else update_status AbortPending s)"
| "int_eff Write1 = (\<lambda>s. if even (loc (fst s))
                         then update_status (Pending Write2) s
                         else update_status (Pending Write5) s)"
| "int_eff Write2 = (\<lambda>s. if (loc (fst s) = glb (snd s) \<and> lcounter (fst s) = counter (snd s))
                        then apsnd (glb_update (op + 1)) (update_writer True (update_status (Pending Write4) s))
                        else update_status AbortPending s)"
| "int_eff Write4 = (\<lambda>s. (update_loc (Suc (loc (fst s))) \<circ>> update_status (Pending Write5)) s)"
| "int_eff Write5 = (\<lambda>s. (apsnd (update_store (addr (fst s)) (val (fst s))) \<circ>> update_status WriteResponding) s)"

fun tml_eff :: "pc event \<Rightarrow> lstate \<times> gstate \<Rightarrow> lstate \<times> gstate"
  where
  "tml_eff (Internal a) = int_eff a"
| "tml_eff (External a) = ext_eff a"

definition TML :: "(T, lstate, gstate, pc) RGA" where
  "TML \<equiv> \<lparr> RGA.local_starts = lstarts, RGA.global_start = gstart, RGA.pre = tml_pre, RGA.eff = tml_eff, RGA.environment = Id\<rparr>"

definition is_writer :: "T \<Rightarrow> (T, lstate, gstate) state \<Rightarrow> bool" where
  "is_writer t s \<equiv> writer (local t s) = True \<and> lvar loc s t = glb (global s) \<and> lvar lcounter s t = counter (global s)"

definition writer_unique :: "(T, lstate, gstate) state \<Rightarrow> bool" where
  "writer_unique s \<equiv> ((\<exists>t. writer (local t s) = True) \<longrightarrow> (\<exists>!t. writer (local t s) = True))"

definition WRITER_UNIQUE :: "(T, lstate, gstate) state \<Rightarrow> bool" where
  "WRITER_UNIQUE s = writer_unique s"

lemma writer_uniqueness: "writer_unique s \<longleftrightarrow> (\<forall>t t'. lvar writer s t \<and> lvar writer s t' \<longrightarrow> t = t')"
  by (auto simp add: writer_unique_def WRITER_UNIQUE_def local_def lvar_def)

definition WRITER_EX :: "(T, lstate, gstate) state \<Rightarrow> bool" where
  "WRITER_EX s \<equiv> (\<exists>t. writer (fst s t))"

definition the_writer :: "(T, lstate, gstate) state \<Rightarrow> T option" where
  "the_writer s \<equiv> if (\<exists>!t. lvar writer s t = True) then Some (THE t. lvar writer s t = True) else None" 

definition global_inv :: "(T, lstate, gstate) state \<Rightarrow> bool" where
  "global_inv s \<equiv> (even (glb (global s)) \<longleftrightarrow> (\<forall>t. writer (local t s) = False)) \<and> writer_unique s"

definition is_ready :: "T \<Rightarrow> (T, lstate, gstate) state \<Rightarrow> bool" where
  "is_ready t s \<equiv> 
      lvar loc s t \<le> glb (global s) \<and> lvar lcounter s t \<le> gvar counter s
    \<and> (odd (lvar loc s t) \<longrightarrow> is_writer t s)"

definition even_non_writer :: "T \<Rightarrow> (T, lstate, gstate) state \<Rightarrow> bool" where
  "even_non_writer t s \<equiv> even (lvar loc s t) \<longrightarrow> lvar writer s t = False"

definition txn_inv :: "T \<Rightarrow> pc event \<Rightarrow> (T, lstate, gstate) state \<Rightarrow> bool"
  where
  "txn_inv t e s \<equiv>
   tml_pre e (view t s) \<longrightarrow>
   (case e of
      External BeginInv \<Rightarrow> lvar writer s t = False
      |
      Internal Begin1 \<Rightarrow> lvar writer s t = False
      |
      Internal Begin2 \<Rightarrow> lvar loc s t \<le> gvar glb s \<and> lvar writer s t = False
      |
      Internal Begin3 \<Rightarrow> lvar loc s t \<le> gvar glb s \<and> lvar lcounter s t \<le> gvar counter s \<and> lvar writer s t = False
      |
      External BeginResp \<Rightarrow> (lvar loc s t \<le> gvar glb s \<and> lvar lcounter s t \<le> gvar counter s \<and> even (lvar loc s t) \<and> lvar writer s t = False)
      |
      Internal Read1 \<Rightarrow> is_ready t s \<and> even_non_writer t s
      |
      Internal Read2 \<Rightarrow> (lvar loc s t = gvar glb s \<and> lvar lcounter s t = gvar counter s \<and> (odd (lvar loc s t) \<longrightarrow> is_writer t s) \<and> even_non_writer t s)
                        \<or>
                        (lvar loc s t < gvar glb s \<and> lvar writer s t = False)
                        \<or>
                        (lvar loc s t \<le> gvar glb s \<and> lvar lcounter s t < gvar counter s \<and> lvar writer s t = False)
      |
      Internal Read3 \<Rightarrow> (lvar loc s t \<le> gvar glb s \<and> lvar lcounter s t = gvar counter s \<and> (odd (lvar loc s t) \<longrightarrow> is_writer t s) \<and> even_non_writer t s)
                        \<or>
                        (lvar loc s t \<le> gvar glb s \<and> lvar lcounter s t < gvar counter s \<and> lvar writer s t = False)
      |     
      External (ReadResp v) \<Rightarrow> v = lvar val s t \<and> is_ready t s \<and> even_non_writer t s
      |
      Internal Write1 \<Rightarrow> is_ready t s \<and> even_non_writer t s
      |
      Internal Write2 \<Rightarrow> (lvar loc s t = gvar glb s \<and> lvar lcounter s t = gvar counter s \<and> even (lvar loc s t))
                         \<or>
                         (lvar loc s t < gvar glb s \<and> lvar lcounter s t \<le> gvar counter s)
                         \<or>
                         (lvar lcounter s t < gvar counter s \<and> lvar loc s t \<le> gvar glb s)
      |
      Internal Write4 \<Rightarrow> (even (lvar loc s t) \<and> writer (local t s) = True \<and> 1 + lvar loc s t = gvar glb s \<and> lvar lcounter s t = gvar counter s)
      |
      Internal Write5 \<Rightarrow> odd(lvar loc s t) \<and> is_writer t s
      |
      External WriteResp \<Rightarrow> odd(lvar loc s t) \<and> is_writer t s
      |
      Internal Commit1 \<Rightarrow> is_ready t s \<and> even_non_writer t s
      |
      Internal Commit2 \<Rightarrow> odd(lvar loc s t) \<and> is_writer t s
      |
      External CommitResp \<Rightarrow> lvar writer s t = False
      |
      External Abort \<Rightarrow> True
      |
      External Cancel \<Rightarrow> False
      |
      _ \<Rightarrow> is_ready t s \<and> even_non_writer t s)"


lemmas unfold_updates =
  update_status_def
  update_loc_def update_addr_def update_val_def
  update_store_def
  update_lcounter_def
  update_writer_def

lemmas unfold_tml =
  starts_def
  tml_pre_def
  unfold_updates

lemmas splits =
  action.split pc.split

lemma Event_split:
  "\<lbrakk>b = External BeginInv \<Longrightarrow> P;
    b = Internal Begin1 \<Longrightarrow> P;
    b = Internal Begin2 \<Longrightarrow> P;
    b = Internal Begin3 \<Longrightarrow> P;
    b = External BeginResp \<Longrightarrow> P;
    \<And> l. b = External (ReadInv l) \<Longrightarrow> P;
    b = Internal Read1 \<Longrightarrow> P;
    b = Internal Read2 \<Longrightarrow> P;
    b = Internal Read3 \<Longrightarrow> P;
    \<And> v. b = External (ReadResp v) \<Longrightarrow> P;
    \<And> l v. b = External (WriteInv l v) \<Longrightarrow> P;
    b = Internal Write1 \<Longrightarrow> P;
    b = Internal Write2 \<Longrightarrow> P;
    b = Internal Write4 \<Longrightarrow> P;
    b = Internal Write5 \<Longrightarrow> P;
    b = External WriteResp \<Longrightarrow> P;
    b = External CommitInv \<Longrightarrow> P;
    b = Internal Commit1 \<Longrightarrow> P;
    b = Internal Commit2 \<Longrightarrow> P;
    b = External CommitResp \<Longrightarrow> P;
    b = External Cancel \<Longrightarrow> P;
    b = External Abort \<Longrightarrow> P \<rbrakk>
   \<Longrightarrow>
    P"
  apply (cases rule: event.exhaust[where y=b])
  apply (auto simp add: action.exhaust event.exhaust pc.exhaust
              split: action.split event.split pc.split)
  using ext_eff.cases apply blast
  using pc.exhaust by blast

definition local_tml_eff :: "T \<Rightarrow> pc event \<Rightarrow> (T \<Rightarrow> lstate) \<times> gstate \<Rightarrow> (T \<Rightarrow> lstate) \<times> gstate" where
  "local_tml_eff t a = localize t (tml_eff a)" 

definition local_tml_pre :: "T \<Rightarrow> pc event \<Rightarrow> (T, lstate, gstate) state \<Rightarrow> bool" where
  "local_tml_pre t a s = tml_pre a (view t s)"

lemmas locality_tml =
  local_tml_eff_def
  local_tml_pre_def
  view_def
  local_def
  localize_def
  global_def
  lvar_def
  gvar_def

lemmas tml_simps =
  unfold_tml
  ext_enabled_def
  Utilities.all_utilities
  txn_inv_def
  is_ready_def
  is_writer_def
  global_inv_def
  even_non_writer_def
  writer_uniqueness

(* Begin *)

lemma txn_inv_pres_begin_inv_self:
  "\<lbrakk>global_inv s;
    txn_inv at (External BeginInv) s;
    local_tml_pre at (External BeginInv) s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_tml_eff at (External BeginInv) s)"
   by (cases rule: Event_split[of b]) (auto simp add: tml_simps locality_tml)

lemma txn_inv_pres_begin_inv_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at (External BeginInv) s;
    t \<noteq> at;
    local_tml_pre at (External BeginInv) s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_tml_eff at (External BeginInv) s)"
   by (cases rule: Event_split[of b]) (auto simp add: tml_simps locality_tml)

lemma txn_inv_pres_begin1_self:
  "\<lbrakk>global_inv s;
    txn_inv at (Internal Begin1) s;
    local_tml_pre at (Internal Begin1) s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_tml_eff at (Internal Begin1) s)"
   by (cases rule: Event_split[of b]) (auto simp add: tml_simps locality_tml)

lemma txn_inv_pres_begin2_self:
  "\<lbrakk>global_inv s;
    txn_inv at (Internal Begin2) s;
    local_tml_pre at (Internal Begin2) s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_tml_eff at (Internal Begin2) s)"
   by (cases rule: Event_split[of b]) (auto simp add: tml_simps locality_tml)

lemma txn_inv_pres_begin3_self:
  "\<lbrakk>global_inv s;
    txn_inv at (Internal Begin3) s;
    local_tml_pre at (Internal Begin3) s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_tml_eff at (Internal Begin3) s)"
   by (cases rule: Event_split[of b]) (auto simp add: tml_simps locality_tml)

lemma txn_inv_pres_begin1_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at (Internal Begin1) s;
    t \<noteq> at;
    local_tml_pre at (Internal Begin1) s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_tml_eff at (Internal Begin1) s)"
   by (cases rule: Event_split[of b]) (auto simp add: tml_simps locality_tml)

lemma txn_inv_pres_begin2_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at (Internal Begin2) s;
    t \<noteq> at;
    local_tml_pre at (Internal Begin2) s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_tml_eff at (Internal Begin2) s)"
   by (cases rule: Event_split[of b]) (auto simp add: tml_simps locality_tml)

lemma txn_inv_pres_begin3_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at (Internal Begin3) s;
    t \<noteq> at;
    local_tml_pre at (Internal Begin3) s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_tml_eff at (Internal Begin3) s)"
   by (cases rule: Event_split[of b]) (auto simp add: tml_simps locality_tml)

lemma txn_inv_pres_begin_resp_self:
  "\<lbrakk>global_inv s;
    txn_inv at (External BeginResp) s;
    local_tml_pre at (External BeginResp) s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_tml_eff at (External BeginResp) s)"
   apply (cases rule: Event_split[of b])
   by (auto simp add: tml_simps locality_tml)

lemma txn_inv_pres_begin_resp_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at (External BeginResp) s;
    t \<noteq> at;
    local_tml_pre at (External BeginResp) s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_tml_eff at (External BeginResp) s)"
   by (cases rule: Event_split[of b]) (auto simp add: tml_simps locality_tml)

(* Read *)

lemma txn_inv_pres_read_inv_self:
  "\<lbrakk>global_inv s;
    txn_inv at (External (ReadInv l)) s;
    local_tml_pre at (External (ReadInv l)) s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_tml_eff at (External (ReadInv l)) s)"
   by (cases rule: Event_split[of b]) (auto simp add: tml_simps locality_tml)

lemma txn_inv_pres_read_inv_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at (External (ReadInv l)) s;
    t \<noteq> at;
    local_tml_pre at (External (ReadInv l)) s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_tml_eff at (External (ReadInv l)) s)"
   by (cases rule: Event_split[of b]) (auto simp add: tml_simps locality_tml)

lemma txn_inv_pres_read1_self:
  "\<lbrakk>global_inv s;
    txn_inv at (Internal Read1) s;
    local_tml_pre at (Internal Read1) s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_tml_eff at (Internal Read1) s)"
   by (cases rule: Event_split[of b]) (auto simp add: tml_simps locality_tml)

lemma txn_inv_pres_read1_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at (Internal Read1) s;
    t \<noteq> at;
    local_tml_pre at (Internal Read1) s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_tml_eff at (Internal Read1) s)"
   by (cases rule: Event_split[of b]) (auto simp add: tml_simps locality_tml)

lemma txn_inv_pres_read2_self:
  "\<lbrakk>global_inv s;
    txn_inv at (Internal Read2) s;
    local_tml_pre at (Internal Read2) s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_tml_eff at (Internal Read2) s)"
   by (cases rule: Event_split[of b]) (auto simp add: tml_simps locality_tml)

lemma txn_inv_pres_read2_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at (Internal Read2) s;
    t \<noteq> at;
    local_tml_pre at (Internal Read2) s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_tml_eff at (Internal Read2) s)"
   apply (cases rule: Event_split[of b])
   apply (simp_all add: tml_simps locality_tml)
   by blast+

lemma txn_inv_pres_read3_self:
  "\<lbrakk>global_inv s;
    txn_inv at (Internal Read3) s;
    local_tml_pre at (Internal Read3) s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_tml_eff at (Internal Read3) s)"
   apply (cases rule: Event_split[of b]) 
   by (auto simp add: tml_simps locality_tml)

lemma txn_inv_pres_read3_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at (Internal Read3) s;
    t \<noteq> at;
    local_tml_pre at (Internal Read3) s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_tml_eff at (Internal Read3) s)"
   apply (cases rule: Event_split[of b])
   apply (simp_all add: tml_simps locality_tml)
   by blast+

lemma txn_inv_pres_read_resp_self:
  "\<lbrakk>global_inv s;
    txn_inv at (External (ReadResp v)) s;
    local_tml_pre at (External (ReadResp v)) s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_tml_eff at (External (ReadResp v)) s)"
   by (cases rule: Event_split[of b]) (simp_all add: tml_simps locality_tml)

lemma txn_inv_pres_read_resp_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at (External (ReadResp v)) s;
    t \<noteq> at;
    local_tml_pre at (External (ReadResp v)) s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_tml_eff at (External (ReadResp v)) s)"
   by (cases rule: Event_split[of b]) (auto simp add: tml_simps locality_tml)

(* Write *)

lemma txn_inv_pres_write_inv_self:
  "\<lbrakk>global_inv s;
    txn_inv at (External (WriteInv l v)) s;
    local_tml_pre at (External (WriteInv l v)) s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_tml_eff at (External (WriteInv l v)) s)"
   by (cases rule: Event_split[of b]) (auto simp add: tml_simps locality_tml)

lemma txn_inv_pres_write_inv_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at (External (WriteInv l v)) s;
    t \<noteq> at;
    local_tml_pre at (External (WriteInv l v)) s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_tml_eff at (External (WriteInv l v)) s)"
   by (cases rule: Event_split[of b]) (auto simp add: tml_simps locality_tml)

lemma txn_inv_pres_write1_self:
  "\<lbrakk>global_inv s;
    txn_inv at (Internal Write1) s;
    local_tml_pre at (Internal Write1) s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_tml_eff at (Internal Write1) s)"
   apply (cases rule: Event_split[of b])
   apply (simp_all add: tml_simps locality_tml)
   using le_neq_trans by blast

lemma txn_inv_pres_write1_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at (Internal Write1) s;
    t \<noteq> at;
    local_tml_pre at (Internal Write1) s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_tml_eff at (Internal Write1) s)"
   apply (cases rule: Event_split[of b])
   apply (simp_all add: tml_simps locality_tml)
   by (linarith | metis)+

lemma txn_inv_pres_write2_self:
  "\<lbrakk>global_inv s;
    txn_inv at (Internal Write2) s;
    local_tml_pre at (Internal Write2) s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_tml_eff at (Internal Write2) s)"
   apply (cases rule: Event_split[of b])
   apply (simp_all add: tml_simps locality_tml)
   by auto

lemma txn_inv_pres_write2_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at (Internal Write2) s;
    t \<noteq> at;
    local_tml_pre at (Internal Write2) s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_tml_eff at (Internal Write2) s)"
   apply (cases rule: Event_split[of b])
   apply (simp_all add: tml_simps locality_tml)
   apply linarith+
   apply fastforce
   apply fastforce
   apply force
   apply force
   apply force
   apply force
   apply force
   apply force
   apply force
   apply force
   apply force
   apply force
   apply force
   by force

lemma txn_inv_pres_write4_self:
  "\<lbrakk>global_inv s;
    txn_inv at (Internal Write4) s;
    local_tml_pre at (Internal Write4) s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_tml_eff at (Internal Write4) s)"
   apply (cases rule: Event_split[of b])
   apply (simp_all add: tml_simps locality_tml)
   using even_Suc by fastforce

lemma txn_inv_pres_write4_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at (Internal Write4) s;
    t \<noteq> at;
    local_tml_pre at (Internal Write4) s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_tml_eff at (Internal Write4) s)"
   apply (cases rule: Event_split[of b])
   apply (simp_all add: tml_simps locality_tml)
   by auto

lemma txn_inv_pres_write5_self:
  "\<lbrakk>global_inv s;
    txn_inv at (Internal Write5) s;
    local_tml_pre at (Internal Write5) s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_tml_eff at (Internal Write5) s)"
   apply (cases rule: Event_split[of b])
   apply (simp_all add: tml_simps locality_tml)
   using even_Suc by fastforce

lemma txn_inv_pres_write5_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at (Internal Write5) s;
    t \<noteq> at;
    local_tml_pre at (Internal Write5) s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_tml_eff at (Internal Write5) s)"
   apply (cases rule: Event_split[of b])
   apply (simp_all add: tml_simps locality_tml)
   by auto

lemma txn_inv_pres_write_resp_self:
  "\<lbrakk>global_inv s;
    txn_inv at (External WriteResp) s;
    local_tml_pre at (External WriteResp) s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_tml_eff at (External WriteResp) s)"
   by (cases rule: Event_split[of b]) (simp_all add: tml_simps locality_tml)

lemma txn_inv_pres_write_resp_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at (External WriteResp) s;
    t \<noteq> at;
    local_tml_pre at (External WriteResp) s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_tml_eff at (External WriteResp) s)"
   by (cases rule: Event_split[of b]) (auto simp add: tml_simps locality_tml)

(* Commit *)

lemma txn_inv_pres_commit_inv_self:
  "\<lbrakk>global_inv s;
    txn_inv at (External CommitInv) s;
    local_tml_pre at (External CommitInv) s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_tml_eff at (External CommitInv) s)"
   apply (cases rule: Event_split[of b])
   by (simp_all add: tml_simps locality_tml)

lemma txn_inv_pres_commit_inv_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at (External CommitInv) s;
    t \<noteq> at;
    local_tml_pre at (External CommitInv) s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_tml_eff at (External CommitInv) s)"
   by (cases rule: Event_split[of b]) (auto simp add: tml_simps locality_tml)

lemma txn_inv_pres_commit1_self:
  "\<lbrakk>global_inv s;
    txn_inv at (Internal Commit1) s;
    local_tml_pre at (Internal Commit1) s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_tml_eff at (Internal Commit1) s)"
   apply (cases rule: Event_split[of b])
   by (simp_all add: tml_simps locality_tml)

lemma txn_inv_pres_commit1_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at (Internal Commit1) s;
    t \<noteq> at;
    local_tml_pre at (Internal Commit1) s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_tml_eff at (Internal Commit1) s)"
   by (cases rule: Event_split[of b]) (auto simp add: tml_simps locality_tml)

lemma txn_inv_pres_commit2_self:
  "\<lbrakk>global_inv s;
    txn_inv at (Internal Commit2) s;
    local_tml_pre at (Internal Commit2) s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_tml_eff at (Internal Commit2) s)"
   by (cases rule: Event_split[of b]) (simp_all add: tml_simps locality_tml)

lemma txn_inv_pres_commit2_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at (Internal Commit2) s;
    t \<noteq> at;
    local_tml_pre at (Internal Commit2) s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_tml_eff at (Internal Commit2) s)"
   by (cases rule: Event_split[of b]) (auto simp add: tml_simps locality_tml)

lemma txn_inv_pres_commit_resp_self:
  "\<lbrakk>global_inv s;
    txn_inv at (External CommitResp) s;
    local_tml_pre at (External CommitResp) s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_tml_eff at (External CommitResp) s)"
   by (cases rule: Event_split[of b]) (simp_all add: tml_simps locality_tml)

lemma txn_inv_pres_commit_resp_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at (External CommitResp) s;
    t \<noteq> at;
    local_tml_pre at (External CommitResp) s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_tml_eff at (External CommitResp) s)"
   by (cases rule: Event_split[of b]) (auto simp add: tml_simps locality_tml)

lemma txn_inv_pres_cancel_self:
  "\<lbrakk>global_inv s;
    txn_inv at (External Cancel) s;
    local_tml_pre at (External Cancel) s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_tml_eff at (External Cancel) s)"
   by (cases rule: Event_split[of b]) (auto simp add: tml_simps locality_tml)

lemma txn_inv_pres_cancel_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at (External Cancel) s;
    t \<noteq> at;
    local_tml_pre at (External Cancel) s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_tml_eff at (External Cancel) s)"
   by (cases rule: Event_split[of b]) (simp_all add: tml_simps locality_tml)

lemma txn_inv_pres_abort_self:
  "\<lbrakk>global_inv s;
    txn_inv at (External Abort) s;
    local_tml_pre at (External Abort) s\<rbrakk>
   \<Longrightarrow>
   txn_inv at b (local_tml_eff at (External Abort) s)"
   by (cases rule: Event_split[of b]) (simp_all add: tml_simps locality_tml)

lemma txn_inv_pres_abort_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at (External Abort) s;
    t \<noteq> at;
    local_tml_pre at (External Abort) s\<rbrakk>
   \<Longrightarrow>
   txn_inv t b (local_tml_eff at (External Abort) s)"
   by (cases rule: Event_split[of b]) (auto simp add: tml_simps locality_tml)

lemmas intro_self =
  txn_inv_pres_write1_self
  txn_inv_pres_write_inv_self
  txn_inv_pres_read_resp_self
  txn_inv_pres_read3_self
  txn_inv_pres_read2_self
  txn_inv_pres_read1_self
  txn_inv_pres_read_inv_self
  txn_inv_pres_begin_resp_self
  txn_inv_pres_begin3_self
  txn_inv_pres_begin2_self
  txn_inv_pres_begin1_self
  txn_inv_pres_begin_inv_self
  txn_inv_pres_write5_self
  txn_inv_pres_write4_self
  txn_inv_pres_write2_self
  txn_inv_pres_write_resp_self
  txn_inv_pres_commit_inv_self
  txn_inv_pres_commit_resp_self
  txn_inv_pres_commit2_self
  txn_inv_pres_commit1_self
  txn_inv_pres_abort_self
  txn_inv_pres_cancel_self

lemmas intro_other =
  txn_inv_pres_write1_other
  txn_inv_pres_write_inv_other
  txn_inv_pres_read_resp_other
  txn_inv_pres_read3_other
  txn_inv_pres_read2_other
  txn_inv_pres_read1_other
  txn_inv_pres_read_inv_other
  txn_inv_pres_begin_resp_other
  txn_inv_pres_begin3_other
  txn_inv_pres_begin2_other
  txn_inv_pres_begin1_other
  txn_inv_pres_begin_inv_other
  txn_inv_pres_write5_other
  txn_inv_pres_write4_other
  txn_inv_pres_write2_other
  txn_inv_pres_write_resp_other
  txn_inv_pres_commit_inv_other
  txn_inv_pres_commit_resp_other
  txn_inv_pres_commit2_other
  txn_inv_pres_commit1_other
  txn_inv_pres_abort_other
  txn_inv_pres_cancel_other

lemma txn_inv_pres_other:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    t \<noteq> at;
    local_tml_pre at a s\<rbrakk>
  \<Longrightarrow> txn_inv t b (local_tml_eff at a s)"
  by (rule_tac b = a in Event_split; simp; intro intro_other; assumption)

lemma txn_inv_pres_self:
  "\<lbrakk>global_inv s;
    txn_inv at a s;
    local_tml_pre at a s\<rbrakk>
  \<Longrightarrow> txn_inv at b (local_tml_eff at a s)"
  by (rule_tac b = a in Event_split; simp; intro intro_self; assumption)

lemma txn_inv_pres:
  "\<lbrakk>global_inv s;
    txn_inv t b s;
    txn_inv at a s;
    local_tml_pre at a s\<rbrakk>
  \<Longrightarrow> txn_inv t b (local_tml_eff at a s)"
  using txn_inv_pres_other txn_inv_pres_self by blast

lemma txn_inv_starts: "(s, g) \<in> starts (interference \<rhd> TML) \<Longrightarrow> txn_inv t e (s, g)"
  apply (rule_tac b = e in Event_split)
  by (simp_all add: TML_def Rely_def image_def locality_tml tml_simps gstart_def lstarts_def)

lemma eff_TML_env [simp]: "eff (TML \<lparr> environment := R \<rparr>) a = eff TML a"
  by (simp add: TML_def)

lemma pre_TML_env [simp]: "pre (TML \<lparr> environment := R \<rparr>) a = pre TML a"
  by (simp add: TML_def)

lemma loc_TML: "localize t (eff TML a) s = local_tml_eff t a s"
  by (simp add: TML_def local_tml_eff_def)

lemma pre_TML: "pre TML a (view t s) = local_tml_pre t a s"
  by (simp add: TML_def local_tml_pre_def)

lemma global_inv_pres:
  "\<lbrakk>global_inv s;
    txn_inv t a s;
    local_tml_pre t a s\<rbrakk>
  \<Longrightarrow> global_inv (local_tml_eff t a s)"
  apply (rule_tac b = a in Event_split)
  apply (simp_all add: global_inv_def tml_simps locality_tml)
  apply metis
  by (smt less_asym')+

lemma tml_lpres: "local_preserved (interference \<rhd> TML) (\<lambda>s. global_inv s \<and> (\<forall>t e. txn_inv t e s)) s"
  by (simp add: local_preserved_def pre_TML loc_TML Rely_def global_inv_pres txn_inv_pres)

lemma tml_gpres: "global_preserved (interference \<rhd> TML) (\<lambda>s. global_inv s \<and> (\<forall>t e. txn_inv t e s)) s"
  apply (simp add: global_preserved_def interference_simple Rely_def)
  apply (intro impI allI conjI; elim conjE)
  apply (simp add: tml_simps locality_tml)
  apply auto[1]
  apply (erule_tac x = t in allE)
  apply (erule_tac x = e in allE)
  apply (rule_tac b = e in Event_split)
  apply (simp_all add: tml_simps locality_tml)
  apply auto[3]
  apply (smt le_eq_less_or_eq less_le_trans)
  apply (smt le_eq_less_or_eq less_le_trans)
  apply (smt le_eq_less_or_eq less_le_trans)
  apply (smt le_eq_less_or_eq less_le_trans)
  apply auto[5]
  apply auto[5]
  done

theorem TML_invariant:
  "invariant (ioa (interference \<rhd> TML)) (\<lambda> s. global_inv s \<and> (\<forall> t e. txn_inv t e s))"
  apply (safe intro!: invariant_intro)
  apply (simp add: starts_def Rely_def lstarts_def TML_def image_def global_inv_def gstart_def locality_tml writer_unique_def)
  apply (rule txn_inv_starts; assumption)
  apply (simp add: preserved_def)
  using tml_gpres tml_lpres by blast

lemma tml_reach_inv: "reach (interference \<rhd> TML) s \<Longrightarrow> global_inv s \<and> txn_inv t e s"
  using TML_invariant invariant_elim by blast

end