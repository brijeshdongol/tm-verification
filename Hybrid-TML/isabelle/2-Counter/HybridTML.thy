theory HybridTML
imports TMLCorrect HWTMCorrect
begin

lemma refl_intI: "refl interference"
  by (metis interference_unstar refl_rtrancl)

lemma [simp]: "(a, a) \<in> interference"
  by (simp add: interference_simple)

lemma refl_glbI: "refl glb_change"
  by (simp add: glb_change_def refl_on_def)

lemma refl_tms2I: "refl tms2_interference"
  by (metis refl_rtrancl tms2_interference_pow_inv)

lemma interference_simp: "(a, b) \<in> {(g, g'). P g g'} \<longleftrightarrow> P a b"
  by auto

lemma gstate_eq:
  fixes s :: gstate
  shows "store s = store s' \<Longrightarrow> glb s = glb s' \<Longrightarrow> counter s = counter s' \<Longrightarrow> s = s'"
  apply (cases s)
  apply (cases s')
  by auto

lemma tml_intf_int:
  assumes "local_tml_pre t (Internal i) s"
  and "TML.global_inv s" and "TML.txn_inv t (Internal i) s"
  shows "(snd s, snd (local_tml_eff t (Internal i) s)) \<in> glb_change"
  using assms
  apply (cases i)
  by (simp_all add: tml_simps locality_tml glb_change_def)

lemma tml_intf_ext:
  assumes "local_tml_pre t (External e) s"
  and "TML.global_inv s" and "TML.txn_inv t (External e) s"
  shows "(snd s, snd (local_tml_eff t (External e) s)) \<in> glb_change"
  using assms
  apply (cases e)
  by (simp_all add: tml_simps locality_tml glb_change_def)

lemma TML_guarantee: "guarantee (interference \<rhd> TML) glb_change"
  apply (simp only: guarantee_def tml_lpre tml_leff, intro allI impI, erule conjE)
  apply (drule_tac t = t and e = a in tml_reach_inv)
  by (metis (full_types) tml_eff.cases tml_intf_ext tml_intf_int)

lemma interference_10:
  assumes "local_hw_pre t (Internal Commit3) s"
  and "HWTM.txn_inv t (Internal Commit3) s"
  shows "(snd s, snd (local_hw_eff t (Internal Commit3) s)) \<in> interference"
  using assms
  apply (simp add: interference_simple)
  apply (cases "\<not> validate (view t s)")
  apply (rule disjI1)
  apply (simp add: hw_simps locality_hw)
  apply blast
  apply (rule disjI2)
  apply (intro conjI impI)
  by (simp_all add: hw_simps locality_hw)

lemma hw_intf_int:
  assumes "local_hw_pre t (Internal ic) s"
  and "HWTM.txn_inv t (Internal ic) s"
  shows "(snd s, snd (local_hw_eff t (Internal ic) s)) \<in> interference"
  using assms
  apply (cases ic)
  prefer 10
  apply (simp add: interference_10)
  by (simp_all add: interference_simple ap_emptyness hw_simps locality_hw option.case_eq_if)

lemma hw_intf_ext:
  assumes "local_hw_pre t (External e) s"
  and "HWTM.txn_inv t (External e) s"
  shows "(snd s, snd (local_hw_eff t (External e) s)) \<in> interference"
  using assms
  by (cases e) (simp_all add: interference_simple hw_simps locality_hw option.case_eq_if)

lemma HWTM_guarantee: "guarantee (glb_change \<rhd> HWTM) interference"
  apply (simp only: guarantee_def cnc_lpre cnc_leff)
  apply (intro impI allI, erule conjE)
  apply (rename_tac t a s)
  apply (drule_tac t = t and e = a in hw_reach_inv)
  by (metis (full_types) event.exhaust hw_intf_ext hw_intf_int)
                                     
lemma [simp]: "\<exists>S. snd s = snd s \<bowtie> S"
  apply (rule_tac x = "[]" in exI)
  by (simp add: stores_append_def)
                           
lemma TMS2_guarantee: "guarantee (tms2_interference \<rhd> TMS2) tms2_interference"
  apply (simp only: guarantee_def abs_leff abs_lpre)
  apply (intro impI allI, erule conjE)
  apply (rename_tac t a s)
  apply (drule_tac t = t and e = a in reachable_invariant_tms2_interference)
  apply (rule_tac b = a in TMS2.Event_split)
  apply (simp_all add: unfold_tms2 locality_tms2 tms2_interference_def when_fn_def)
  apply (simp only: write_back_def stores_append_def)
  by blast

lemma latest_store2: "apply_partial (latest_store ag) (write_set as) = latest_store (write_back (write_set as) ag)"
  by (simp add: write_back_def)

lemma no_unique_ex: "\<not> (\<exists>!t. P t) \<longleftrightarrow> (\<forall>t. \<not> P t \<or> (\<exists>t'. t \<noteq> t' \<and> P t'))"
  by auto metis

lemma if_SN: "(if x then Some y else None) = Some y' \<Longrightarrow> x \<and> y = y'"
  by (cases x) auto

lemma HYBRID_NIL:
  "non_interference_left HWTMCorrect.sim_rel TMLCorrect.sim_rel TMLCorrect.step_correspondence (interference \<rhd> TML) (glb_change \<rhd> HWTM) (tms2_interference \<rhd> TMS2)"
proof (simp only: non_interference_left_def, intro conjI)
  show "non_interference_left_ext HWTMCorrect.sim_rel TMLCorrect.sim_rel (interference \<rhd> TML) (tms2_interference \<rhd> TMS2)"
    apply (simp only: non_interference_left_ext_def tml_lpre tml_leff abs_leff abs_lpre)
    apply (intro impI allI)
    apply (erule conjE)+
    apply (drule_tac t = t and e = "External e" in tml_reach_inv)
    apply (drule_tac t = t and e = "External e" in reachable_invariant_tms2_interference)
    apply (erule conjE)
  proof -
    fix cs cg as ag t e
    assume "HWTMCorrect.sim_rel (lsprojr cs, cg) (lsprojr as, ag)"
    and "local_tml_pre t (External e) (lsprojl cs, cg)"
    and "local_tms_pre t (External e) (lsprojl as, ag)"
    and "TMS2.txn_inv t (External e) (lsprojl as, ag)"
    and "TML.global_inv (lsprojl cs, cg)"
    and "TML.txn_inv t (External e) (lsprojl cs, cg)"
    
    thus "HWTMCorrect.sim_rel (lsprojr cs, snd (local_tml_eff t (External e) (lsprojl cs, cg)))
                              (lsprojr as, snd (local_tms_eff t (External e) (lsprojl as, ag)))"
      by (cases e) (simp_all add: tml_simps locality_tml unfold_tms2 locality_tms2)
  qed
next
  show "non_interference_left_int HWTMCorrect.sim_rel TMLCorrect.sim_rel TMLCorrect.step_correspondence (interference \<rhd> TML) (glb_change \<rhd> HWTM) (tms2_interference \<rhd> TMS2)"
    apply (simp only: non_interference_left_int_def tml_lpre tml_leff abs_leff abs_lpre)
    apply (intro impI allI)
    apply (erule conjE)+
    apply (drule_tac t = t and e = "Internal ic" in tml_reach_inv)
    apply (drule_tac t = t and e = "Internal ia" in reachable_invariant_tms2_interference)
    apply (erule conjE)
  proof -
    fix cs cg as ag t ic ia
    assume "HWTMCorrect.sim_rel (lsprojr cs, cg) (lsprojr as, ag)"
    and "TMLCorrect.sim_rel (lsprojl cs, cg) (lsprojl as, ag)"
    and "local_tml_pre t (Internal ic) (lsprojl cs, cg)"
    and "local_tms_pre t (Internal ia) (lsprojl as, ag)"
    and "TMLCorrect.step_correspondence (lsprojl cs, cg) t ic = Some ia"
    and "TMS2.txn_inv t (Internal ia) (lsprojl as, ag)"
    and "TML.global_inv (lsprojl cs, cg)"
    and "TML.txn_inv t (Internal ic) (lsprojl cs, cg)"

    thus "HWTMCorrect.sim_rel (lsprojr cs, snd (local_tml_eff t (Internal ic) (lsprojl cs, cg)))
                               (lsprojr as, snd (local_tms_eff t (Internal ia) (lsprojl as, ag)))"
      apply (cases ia)

      apply (simp_all add: TMLCorrect.scf_simps)
      apply (simp_all add: HWTMCorrect.sim_rel_def TMLCorrect.sim_rel_def TMS2.txn_inv_def tml_simps locality_tml unfold_tms2 locality_tms2)

      apply (intro conjI)
        apply (simp add: HWTMCorrect.global_rel_def TMLCorrect.global_rel_def)
        apply (cases "even (glb cg)")
        apply blast
        apply (simp add: write_count_def write_back_def max_index_def locality_tml tml_simps locality_tms2 unfold_tms2 latest_store_def)
        apply (simp add: writes_def the_writer_def)
        apply (intro impI allI conjI)
        apply (smt fst_conv local_def lvar_def the_equality)
        apply simp
        apply (rule ap_emptyness[symmetric])
        apply (simp add: no_unique_ex locality_tml)
        apply blast

        apply (intro allI)
        apply (rename_tac t' a)
        apply (elim conjE)
        apply (erule_tac x = t' in allE, erule_tac x = a in allE)
        apply (simp add: HWTMCorrect.txn_rel_def)
        apply (intro conjI)
        apply (drule conjunct1)
        apply (simp add: in_flight_def locality_hw)
        apply (rule_tac b = a in HWTM.Event_split; simp add: locality_hw hw_simps)

      apply (intro conjI)
        apply (simp add: HWTMCorrect.global_rel_def TMLCorrect.global_rel_def)
        apply (simp add: write_count_def write_back_def max_index_def locality_tml tml_simps locality_tms2 unfold_tms2 latest_store_def when_fn_def)
        apply blast

        apply (intro allI)
        apply (rename_tac t' a)
        apply (elim conjE)
        apply (erule_tac x = t' in allE, erule_tac x = a in allE)
        apply (simp add: HWTMCorrect.txn_rel_def)
        apply (intro conjI)
        apply (drule conjunct1)
        apply (simp add: in_flight_def locality_hw)
        apply (rule_tac b = a in HWTM.Event_split; simp add: locality_hw hw_simps)
      done
  qed
next
  show "non_interference_left_stutter HWTMCorrect.sim_rel TMLCorrect.sim_rel TMLCorrect.step_correspondence (interference \<rhd> TML) (tms2_interference \<rhd> TMS2)"
    apply (simp only: non_interference_left_stutter_def tml_lpre tml_leff abs_leff abs_lpre)
    apply (intro impI allI)
    apply (erule conjE)+
    apply (drule_tac t = t and e = "Internal ic" in tml_reach_inv)
    apply (erule conjE)
  proof -
    fix cs cg as ag t ic
    assume "HWTMCorrect.sim_rel (lsprojr cs, cg) (lsprojr as, ag)"
    and "TMLCorrect.sim_rel (lsprojl cs, cg) (lsprojl as, ag)"
    and "local_tml_pre t (Internal ic) (lsprojl cs, cg)"
    and "TMLCorrect.step_correspondence (lsprojl cs, cg) t ic = None"
    and "TML.global_inv (lsprojl cs, cg)"
    and "TML.txn_inv t (Internal ic) (lsprojl cs, cg)"
    
    thus "HWTMCorrect.sim_rel (lsprojr cs, snd (local_tml_eff t (Internal ic) (lsprojl cs, cg)))
                               (lsprojr as, ag)"
      apply -
      apply (simp only: TMLCorrect.scf_None)
      apply (elim disjE conjE)
      apply (simp_all add: HWTMCorrect.sim_rel_def HWTMCorrect.global_rel_def TML.global_inv_def tml_simps locality_tml)
      apply (elim conjE exE )
      apply (intro allI conjI impI)
      apply (simp add: write_count_def)
      apply simp
      apply (rename_tac t' a)
      apply (erule_tac x = t' in allE) back
      apply (erule_tac x = a in allE)
      apply (rule_tac b = a in HWTM.Event_split)
      by (simp_all add: HWTMCorrect.txn_rel_def hw_simps locality_hw HWTMCorrect.in_flight_def)
  qed
qed

lemma helper_NIR: "ws' = Map.empty \<Longrightarrow> apply_partial (latest_store ag) ws = apply_partial (latest_store (write_back ws ag)) ws'"
  by (simp add: ap_emptyness write_back_def latest_store_def store_at_def max_index_def)

lemma store_at_wb_n: "n \<le> max_index ag \<Longrightarrow> store_at ag n = store_at (write_back ws ag) n"
  by (cases n) (simp_all add: store_at_def write_back_def max_index_def nth_append)

lemma validity_prop_NIR:
  assumes "HWTMCorrect.sim_rel (lsprojr cs, cg) (lsprojr as, ag)"
  and "local_hw_pre t (Internal Commit3) (lsprojr cs, cg)"
  and "local_tms_pre t (Internal DoCommitWriter) (lsprojr as, ag)"
  and "HWTM.txn_inv t (Internal Commit3) (lsprojr cs, cg)"
  and "TMS2.txn_inv t (Internal DoCommitWriter) (lsprojr as, ag)"
  and "TML.global_inv (lsprojl cs, cg)"
  and "TMLCorrect.global_rel (lsprojl cs, cg) (lsprojl as, ag)"
  and "validate (view t (lsprojr cs, cg))"
  and "validity_prop (lsprojl cs, cg) (lsprojl as, ag) t'"
  shows "validity_prop (lsprojl cs, snd (local_hw_eff t (Internal Commit3) (lsprojr cs, cg)))
                       (lsprojl as, snd (local_tms_eff t (Internal DoCommitWriter) (lsprojr as, ag))) t'"
  using assms
  apply (simp add: validity_prop_def)
  apply (intro conjI impI allI; elim conjE exE)
  apply (simp add: hw_simps locality_hw)
  apply (simp add: hw_simps locality_hw unfold_tms2 local_tms_eff_def max_index_wb)
  apply (simp add: hw_simps locality_hw unfold_tms2 local_tms_eff_def read_consistent_def)
  using add_leD1 store_at_wb_n by presburger

lemma in_flight_NIR:
  assumes "HWTMCorrect.sim_rel (lsprojr cs, cg) (lsprojr as, ag)"
  and "local_hw_pre t (Internal Commit3) (lsprojr cs, cg)"
  and "local_tms_pre t (Internal DoCommitWriter) (lsprojr as, ag)"
  and "HWTM.txn_inv t (Internal Commit3) (lsprojr cs, cg)"
  and "TMS2.txn_inv t (Internal DoCommitWriter) (lsprojr as, ag)"
  and "TML.global_inv (lsprojl cs, cg)"
  and "TMLCorrect.global_rel (lsprojl cs, cg) (lsprojl as, ag)"
  and "validate (view t (lsprojr cs, cg))"
  and "TMLCorrect.in_flight (lsprojl cs, cg) (lsprojl as, ag) t'"
  shows "TMLCorrect.in_flight (lsprojl cs, snd (local_hw_eff t (Internal Commit3) (lsprojr cs, cg)))
                              (lsprojl as, snd (local_tms_eff t (Internal DoCommitWriter) (lsprojr as, ag))) t'"
  using assms
  apply (simp add: TMLCorrect.in_flight_def)
  apply (intro conjI impI allI; elim conjE exE)

  apply (simp add: hw_simps locality_hw)
  using validity_prop_NIR apply blast
  by (simp_all add: hw_simps locality_hw)

lemma HYBRID_NIR:
  "non_interference_right TMLCorrect.sim_rel HWTMCorrect.sim_rel HWTMCorrect.step_correspondence (glb_change \<rhd> HWTM) (interference \<rhd> TML) (tms2_interference \<rhd> TMS2)"
proof (simp only: non_interference_right_def, intro conjI)
  show "non_interference_right_ext TMLCorrect.sim_rel HWTMCorrect.sim_rel (glb_change \<rhd> HWTM) (tms2_interference \<rhd> TMS2)"
    apply (simp only: non_interference_right_ext_def cnc_lpre cnc_leff abs_leff abs_lpre)
    apply (intro impI allI)
    apply (erule conjE)+
    apply (drule_tac t = t and e = "External e" in hw_reach_inv)
    apply (drule_tac t = t and e = "External e" in reachable_invariant_tms2_interference)
  proof -
    fix cs cg as ag t e
    assume "TMLCorrect.sim_rel (lsprojl cs, cg) (lsprojl as, ag)"
    and "HWTMCorrect.sim_rel (lsprojr cs, cg) (lsprojr as, ag)"
    and "local_hw_pre t (External e) (lsprojr cs, cg)"
    and "local_tms_pre t (External e) (lsprojr as, ag)"
    and "HWTM.txn_inv t (External e) (lsprojr cs, cg)"
    and "TMS2.txn_inv t (External e) (lsprojr as, ag)"
    
    thus "TMLCorrect.sim_rel (lsprojl cs, snd (local_hw_eff t (External e) (lsprojr cs, cg)))
                             (lsprojl as, snd (local_tms_eff t (External e) (lsprojr as, ag)))"
      by (cases e) (simp_all add: hw_simps locality_hw unfold_tms2 locality_tms2)
  qed
next
  show "non_interference_right_int TMLCorrect.sim_rel HWTMCorrect.sim_rel HWTMCorrect.step_correspondence (glb_change \<rhd> HWTM) (interference \<rhd> TML) (tms2_interference \<rhd> TMS2)"
    apply (simp only: non_interference_right_int_def cnc_lpre cnc_leff abs_leff abs_lpre)
    apply (intro impI allI)
    apply (erule conjE)+
    apply (drule_tac t = t and e = "Internal ic" in hw_reach_inv)
    apply (frule_tac t = t and e = undefined in tml_reach_inv)
    apply (drule conjunct1)
    apply (drule_tac t = t and e = "Internal ia" in reachable_invariant_tms2_interference)
  proof -
    fix cs cg as ag t ic ia
    assume "TMLCorrect.sim_rel (lsprojl cs, cg) (lsprojl as, ag)"
    and "HWTMCorrect.sim_rel (lsprojr cs, cg) (lsprojr as, ag)"
    and "local_hw_pre t (Internal ic) (lsprojr cs, cg)"
    and "local_tms_pre t (Internal ia) (lsprojr as, ag)"
    and "HWTMCorrect.step_correspondence (lsprojr cs, cg) t ic = Some ia"
    and "HWTM.txn_inv t (Internal ic) (lsprojr cs, cg)"
    and "TMS2.txn_inv t (Internal ia) (lsprojr as, ag)"
    and "TML.global_inv (lsprojl cs, cg)"
    and "reach (interference \<rhd> TML) (lsprojl cs, cg)"

    thus  "TMLCorrect.sim_rel (lsprojl cs, snd (local_hw_eff t (Internal ic) (lsprojr cs, cg)))
                              (lsprojl as, snd (local_tms_eff t (Internal ia) (lsprojr as, ag)))"
      apply -
      apply (cases ia)

      apply (simp_all add: HWTMCorrect.scf_simps)

      apply (simp add: TMLCorrect.sim_rel_def)
      apply (intro conjI)
      apply (simp add: TMLCorrect.global_rel_def hw_simps locality_hw unfold_tms2 locality_tms2)
      apply (intro conjI)
      apply (rule writers_ap)
      apply (simp add: the_writer_def locality_tml)
      apply (simp add: write_count_def)
      apply (intro allI)
      apply (rename_tac a t')
      apply (erule conjE)
      apply (erule_tac x = a in allE, erule_tac x = t' in allE)
      apply (rule_tac b = a in TML.Event_split;
             simp add: TMLCorrect.txn_rel_def hw_simps locality_hw unfold_tms2 locality_tms2 tml_simps
                       TMLCorrect.in_flight_def validity_prop_def;
             force)

      apply (simp add: TMLCorrect.sim_rel_def)
      apply (elim conjE)
      apply (intro conjI)
      apply (simp add: TMLCorrect.global_rel_def unfold_tms2 hw_simps locality_hw locality_tms2 HWTMCorrect.sim_rel_def)
      apply (elim conjE)
      apply (erule_tac x = t in allE, erule_tac x = "Internal Commit3" in allE)
      apply (simp add: HWTMCorrect.txn_rel_def HWTMCorrect.in_flight_def HWTMCorrect.global_rel_def locality_hw)
      apply (elim conjE)
      apply (intro conjI)
      apply (simp add: writes_def the_writer_def)
      apply (simp add: TML.global_inv_def locality_tml)
      apply (metis ap_empty helper_NIR)
      apply (simp add: write_count_def max_index_wb)
      apply (intro allI)
      apply (rename_tac a t')
      apply (erule_tac x = a in allE, erule_tac x = t' in allE)
      apply (rule_tac b = a in TML.Event_split; simp add: TMLCorrect.txn_rel_def;
             (intro conjI impI in_flight_NIR validity_prop_NIR)?; blast?; simp add: tml_simps locality_tml hw_simps locality_hw)
      apply (simp add: TMLCorrect.in_flight_def validity_prop_def TMS2.txn_inv_def unfold_tms2 locality_tms2 TMLCorrect.global_rel_def locality_tml)
      using write_count_def apply auto[1]
      apply (simp add: TMLCorrect.in_flight_def write_count_def validity_prop_def TMS2.txn_inv_def unfold_tms2 locality_tms2 TMLCorrect.global_rel_def locality_tml)
      apply (subgoal_tac "odd (loc (lsprojl cs t')) \<or> even (loc (lsprojl cs t'))")
      apply (erule disjE)
      apply simp
      apply simp
      apply (elim disjE conjE)
      apply simp
      apply simp
      apply (subgoal_tac "lcounter (lsprojl cs t') \<le> counter cg")
      apply linarith
      apply (subgoal_tac "TML.txn_inv t' (Internal Read3) (lsprojl cs, cg)")
      apply (simp add: TML.txn_inv_def locality_tml tml_simps)
      using tml_reach_inv apply blast
      apply blast

      apply (simp add: TMLCorrect.sim_rel_def)
      apply (elim conjE)
      apply (intro conjI impI)
      apply (cases "hw_write_set (lsprojr cs t) (hw_addr (lsprojr cs t))")
      apply (simp add: TMLCorrect.global_rel_def HWTMCorrect.sim_rel_def HWTMCorrect.global_rel_def hw_simps locality_hw unfold_tms2 locality_tms2 when_fn_def dom_def)
      apply (simp add: TMLCorrect.global_rel_def HWTMCorrect.sim_rel_def HWTMCorrect.global_rel_def hw_simps locality_hw unfold_tms2 locality_tms2 when_fn_def dom_def)
      apply (intro allI)
      apply (rename_tac l v a t')
      apply (erule_tac x = a in allE, erule_tac x = t' in allE)
      apply (cases "hw_write_set (lsprojr cs t) (hw_addr (lsprojr cs t))")
      apply (rule_tac b = a in TML.Event_split;
             simp add: TMLCorrect.txn_rel_def hw_simps locality_hw unfold_tms2 locality_tms2 tml_simps
                       TMLCorrect.in_flight_def validity_prop_def read_consistent_def when_fn_def option.case_eq_if)
      apply (rule_tac b = a in TML.Event_split; simp add: TMLCorrect.txn_rel_def;
             (intro conjI impI in_flight_NIR validity_prop_NIR)?; blast?; simp add: tml_simps locality_tml hw_simps locality_hw)
      apply (simp add: TMLCorrect.txn_rel_def hw_simps locality_hw unfold_tms2 locality_tms2 tml_simps
                       TMLCorrect.in_flight_def validity_prop_def read_consistent_def when_fn_def option.case_eq_if)+
      done
  qed
next
  show "non_interference_right_stutter TMLCorrect.sim_rel HWTMCorrect.sim_rel HWTMCorrect.step_correspondence (glb_change \<rhd> HWTM) (tms2_interference \<rhd> TMS2)"
    apply (simp only: non_interference_right_stutter_def cnc_lpre cnc_leff abs_leff abs_lpre)
    apply (intro impI allI)
    apply (erule conjE)+
    apply (drule_tac t = t and e = "Internal ic" in hw_reach_inv)
  proof -
    fix cs cg as ag t ic
    assume "TMLCorrect.sim_rel (lsprojl cs, cg) (lsprojl as, ag)"
    and "HWTMCorrect.sim_rel (lsprojr cs, cg) (lsprojr as, ag)"
    and "local_hw_pre t (Internal ic) (lsprojr cs, cg)"
    and "HWTMCorrect.step_correspondence (lsprojr cs, cg) t ic = None"
    and "HWTM.txn_inv t (Internal ic) (lsprojr cs, cg)"

    thus "TMLCorrect.sim_rel (lsprojl cs, snd (local_hw_eff t (Internal ic) (lsprojr cs, cg))) (lsprojl as, ag)"
      apply -
      apply (simp only: HWTMCorrect.scf_None)
      apply (elim disjE conjE)
      apply (simp_all add: TMLCorrect.sim_rel_def TMLCorrect.global_rel_def hw_simps locality_hw)
      by blast+
  qed
qed

theorem HYBRID_SIM:
  shows "standard_simulation (TML \<parallel> HWTM) (TMS2 \<parallel> TMS2)
    (par_sc TMLCorrect.step_correspondence HWTMCorrect.step_correspondence)
    (par_sim_rel TMLCorrect.sim_rel HWTMCorrect.sim_rel)"
  apply (rule PARALLEL)
  apply (simp add: compatible_def Rely_def TML_def HWTM_def)
  apply (rule refl_glbI)
  apply (rule refl_intI)
  apply (rule refl_tms2I)
  apply (rule TML_guarantee)
  apply (rule HWTM_guarantee)
  apply (rule TMS2_guarantee)
  using TML_weak_simulation apply blast
  using HWTM_weak_simulation apply blast
  apply (rule HYBRID_NIR)
  by (rule HYBRID_NIL)

corollary hybrid_trace_inclusion:
  "traces (ioa (TML \<parallel> HWTM)) \<subseteq> traces (ioa (SumT TMS2))"
proof -
  have env_TMS2_refl: "refl (environment TMS2)"
    by (simp add: refl_on_def)

  have "traces (ioa (TML \<parallel> HWTM)) \<subseteq> traces (ioa (TMS2 \<parallel> TMS2))"
    using HYBRID_SIM standard_simulation_trace_inclusion by blast
  also have "... \<subseteq> traces (ioa (SumT TMS2))"
    using PAR_REFL[OF env_TMS2_refl] standard_simulation_trace_inclusion by blast
  finally show ?thesis .
qed

end