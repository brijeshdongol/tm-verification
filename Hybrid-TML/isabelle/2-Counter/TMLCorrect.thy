theory TMLCorrect
imports TML TMS2
begin

definition step_correspondence :: "(T, lstate, gstate) state \<Rightarrow> T \<Rightarrow> TML.pc \<Rightarrow> internal_action option"
  where
  "step_correspondence cs t a \<equiv>
    case a of
        Read1 \<Rightarrow> if lvar loc cs t = glb (global cs) \<and> lvar lcounter cs t = counter (global cs)
                then Some (DoRead (lvar addr cs t) (write_count (global cs)))
                else None
      | Write5 \<Rightarrow> if lvar loc cs t = glb (global cs) \<and> lvar lcounter cs t = counter (global cs)
                  then Some (DoWrite (lvar addr cs t) (lvar val cs t))
                  else None
      | Commit1 \<Rightarrow> if even (lvar loc cs t)
                  then Some DoCommitReadOnly
                  else None
      | Commit2 \<Rightarrow> Some DoCommitWriter
      | _ \<Rightarrow> None"

lemma scf1: "step_correspondence cs t ia = Some (DoRead l n) \<longleftrightarrow>
  (ia = Read1 \<and> l = lvar addr cs t \<and> n = write_count (global cs) \<and> lvar loc cs t = glb (global cs) \<and> lvar lcounter cs t = counter (global cs))"
  by (cases ia) (auto simp add: step_correspondence_def)

lemma scf2: "step_correspondence cs t ia = Some (DoWrite l v) \<longleftrightarrow>
  (ia = Write5 \<and> l = lvar addr cs t \<and> v = lvar val cs t \<and> lvar loc cs t = glb (global cs) \<and> lvar lcounter cs t = counter (global cs))"
  by (cases ia) (auto simp add: step_correspondence_def)

lemma scf3: "step_correspondence cs t ia = Some DoCommitReadOnly \<longleftrightarrow> (ia = Commit1 \<and> even (lvar loc cs t))"
  by (cases ia) (auto simp add: step_correspondence_def)

lemma scf4: "step_correspondence cs t ia = Some DoCommitWriter \<longleftrightarrow> ia = Commit2"
  by (cases ia) (auto simp add: step_correspondence_def)

lemmas scf_simps = scf1 scf2 scf3 scf4

lemma scf_None: "step_correspondence cs t ia = None \<longleftrightarrow>
  (((lvar loc cs t \<noteq> glb (global cs) \<or> lvar lcounter cs t \<noteq> counter (global cs)) \<and> ia = Read1)
  \<or> ((lvar loc cs t \<noteq> glb (global cs) \<or> lvar lcounter cs t \<noteq> counter (global cs)) \<and> ia = Write5)
  \<or> ia = Read2 \<or> ia = Read3 \<or> ia = Write1 \<or> ia = Write2 \<or> ia = Write4 \<or> (ia = Commit1 \<and> odd (lvar loc cs t)) \<or> ia = Begin1 \<or> ia = Begin3 \<or> ia = Begin2)"
  by (cases ia) (auto simp add: step_correspondence_def)

type_synonym tml_state = "(T, lstate, gstate) state"

type_synonym tms2_state = "(T, Local, stores_state) state"


definition writes :: "tml_state \<Rightarrow> tms2_state \<Rightarrow> L \<Rightarrow> V option"
  where
  "writes cs0 as0 \<equiv>
    case (the_writer cs0) of Some w \<Rightarrow> lvar write_set as0 w | None \<Rightarrow> Map.empty"

lemma the_writer_Some: "the_writer cs = Some t \<Longrightarrow> writes cs as = lvar write_set as t"
  by (auto simp add: writes_def)

lemma the_writer_None: "the_writer cs = None \<Longrightarrow> writes cs as = Map.empty"
  by (auto simp add: writes_def)

definition global_rel :: "tml_state \<Rightarrow> tms2_state \<Rightarrow> bool"
  where
  "global_rel cs0 as0 \<equiv>
     gvar store cs0 = apply_partial (gvar latest_store as0) (writes cs0 as0)
     \<and>
     gvar write_count cs0 = gvar max_index as0"

definition validity_prop :: "tml_state \<Rightarrow> tms2_state \<Rightarrow> T \<Rightarrow> bool"
  where
  "validity_prop cs as t \<equiv>
          lvar begin_index as t \<le> lvar loc cs t div 2 + lvar lcounter cs t
        \<and> lvar loc cs t div 2 + lvar lcounter cs t \<le> gvar max_index as (* Added for hybrid *)
        \<and> (read_consistent (gvar store_at as (lvar loc cs t div 2 + lvar lcounter cs t)) (lvar read_set as t))"

definition in_flight :: "tml_state \<Rightarrow> tms2_state \<Rightarrow> T \<Rightarrow> bool"
  where
  "in_flight cs as t \<equiv>
      (even (lvar loc cs t) \<longleftrightarrow> lvar write_set as t = Map.empty)
      \<and> validity_prop cs as t
      \<and> (odd (lvar loc cs t) \<longrightarrow> the_writer cs = Some t \<and> lvar loc cs t = gvar glb cs \<and> lvar lcounter cs t = gvar counter cs)" (* Had to add this *)

definition even_ws_empty :: "tml_state \<Rightarrow> tms2_state \<Rightarrow> T \<Rightarrow> bool" where
  "even_ws_empty cs as t \<equiv> even (lvar loc cs t) \<longleftrightarrow> lvar write_set as t = Map.empty"

definition odd_is_writer :: "tml_state \<Rightarrow> tms2_state \<Rightarrow> T \<Rightarrow> bool" where
  "odd_is_writer cs as t \<equiv> odd (lvar loc cs t) \<longrightarrow> the_writer cs = Some t \<and> lvar loc cs t = gvar glb cs \<and> lvar lcounter cs t = gvar counter cs"

lemma in_flight_var: "in_flight cs as t \<longleftrightarrow> even_ws_empty cs as t \<and> validity_prop cs as t \<and> odd_is_writer cs as t"
  by (simp add: in_flight_def even_ws_empty_def odd_is_writer_def)

definition txn_rel :: "TML.pc event \<Rightarrow> tml_state \<Rightarrow> tms2_state \<Rightarrow> T \<Rightarrow> bool"
  where
  "txn_rel e cs0 as0 t \<equiv>
    tml_pre e (view t cs0) \<longrightarrow>
   (case e of
      External BeginInv \<Rightarrow> lvar status as0 t = NotStarted
      |
      Internal Begin1 \<Rightarrow>   lvar status as0 t = BeginResponding
                        \<and> (lvar begin_index as0 t \<le> gvar glb cs0 div 2 + gvar counter cs0)
      |
      Internal Begin2 \<Rightarrow> lvar status as0 t = BeginResponding
                        \<and> (lvar begin_index as0 t \<le> lvar loc cs0 t div 2 + gvar counter cs0)
      |
      Internal Begin3 \<Rightarrow>   lvar status as0 t = BeginResponding
                         \<and> (lvar begin_index as0 t \<le> lvar loc cs0 t div 2 + lvar lcounter cs0 t)
      |
      External BeginResp \<Rightarrow> lvar status as0 t = BeginResponding
                         \<and> (lvar begin_index as0 t \<le> lvar loc cs0 t div 2 + lvar lcounter cs0 t)
      |
      Internal Read1 \<Rightarrow>   lvar status as0 t = Pending (ReadPending (lvar addr cs0 t))
                        \<and> in_flight cs0 as0 t
      |
      Internal Read2 \<Rightarrow> ((lvar status as0 t = ReadResponding (lvar val cs0 t))
                         \<or>
                         (lvar status as0 t = Pending(ReadPending (lvar addr cs0 t))
                           \<and> (lvar loc cs0 t \<noteq> gvar glb cs0 \<or> lvar lcounter cs0 t \<noteq> gvar counter cs0)))
                        \<and> in_flight cs0 as0 t
      |
      Internal Read3 \<Rightarrow> ((lvar status as0 t = ReadResponding (lvar val cs0 t))
                         \<or>
                         (lvar status as0 t = Pending(ReadPending (lvar addr cs0 t))
                           \<and> ((*lvar loc cs0 t \<noteq> gvar glb cs0 \<or>*) lvar lcounter cs0 t \<noteq> gvar counter cs0)))
                        \<and> in_flight cs0 as0 t
      |
      External (ReadResp v) \<Rightarrow>   lvar status as0 t = ReadResponding (lvar val cs0 t)
                        \<and> in_flight cs0 as0 t
      |
      Internal Write1 \<Rightarrow>   lvar status as0 t = Pending (WritePending (lvar addr cs0 t) (lvar val cs0 t))
                        \<and> in_flight cs0 as0 t
      |
      Internal Write2 \<Rightarrow>   lvar status as0 t = Pending (WritePending (lvar addr cs0 t) (lvar val cs0 t))
                         \<and> lvar write_set as0 t = Map.empty
                         \<and> validity_prop cs0 as0 t
      |
      Internal Write4 \<Rightarrow> lvar status as0 t = Pending (WritePending (lvar addr cs0 t) (lvar val cs0 t))
                         \<and> validity_prop cs0 as0 t
      |
      Internal Write5 \<Rightarrow> lvar status as0 t = Pending (WritePending (lvar addr cs0 t) (lvar val cs0 t))
                        \<and> validity_prop cs0 as0 t
      |
      External WriteResp \<Rightarrow> lvar status as0 t = WriteResponding
                        \<and> validity_prop cs0 as0 t
      |
      Internal Commit1 \<Rightarrow>  lvar status as0 t = Pending CommitPending
                        \<and> in_flight cs0 as0 t
      |
      Internal Commit2 \<Rightarrow> lvar status as0 t = Pending CommitPending
                        \<and> in_flight cs0 as0 t
      |
      External CommitResp \<Rightarrow> lvar status as0 t = CommitResponding
      |
      External Abort \<Rightarrow> lvar status as0 t \<notin> {NotStarted, Ready, CommitResponding, Committed, Aborted}
      |
      External Cancel \<Rightarrow> False
      |
      _ \<Rightarrow> (  lvar status as0 t = Ready
            \<and> in_flight cs0 as0 t))"

lemma latest_store1[simp]: "latest_store (fst ag, snd ag @ [X]) = X"
  by (simp add: latest_store_def store_at_def max_index_def)

lemma store_wb: "n \<le> max_index g \<Longrightarrow> store_at (write_back ws g) n = store_at g n"
  by (cases n) (simp_all add: write_back_def store_at_def max_index_def nth_append)

definition sim_rel :: "tml_state \<Rightarrow> tms2_state \<Rightarrow> bool"
  where
  "sim_rel cs as \<equiv> global_rel cs as \<and> (\<forall> a t. txn_rel a cs as t)"

lemma tml_lpre: "local_pre (interference \<rhd> TML) = local_tml_pre"
  by (rule ext)+ (simp add: local_tml_pre_def local_pre_def TML_def Rely_def)

lemma tml_leff: "local_eff (interference \<rhd> TML) = local_tml_eff"
  by (rule ext)+ (simp add: local_tml_eff_def local_eff_def TML_def Rely_def)

lemma same_writer: "the_writer cs = the_writer cs' \<Longrightarrow> writes cs as = writes cs' as"
  by (rule ext) (simp add: writes_def)

lemma same_writer_ap: "the_writer cs = the_writer cs' \<Longrightarrow> apply_partial ls (writes cs as) = apply_partial ls (writes cs' as)"
  by (rule ext) (simp add: writes_def)

lemma become_writer: "the_writer cs = None \<Longrightarrow> the_writer cs' = Some t \<Longrightarrow> lvar write_set as t = Map.empty \<Longrightarrow> writes cs as = writes cs' as"
  by (rule ext) (simp add: writes_def)

lemma become_writer_ap: "the_writer cs = None \<Longrightarrow> the_writer cs' = Some t \<Longrightarrow> lvar write_set as t = Map.empty \<Longrightarrow> apply_partial ls (writes cs as) = apply_partial ls (writes cs' as)"
  by (rule ext) (simp add: apply_partial_def writes_def)

lemma writers: "(the_writer cs = None \<and> the_writer cs' = None) \<or> (\<exists>t. the_writer cs = Some t \<and> the_writer cs' = Some t \<and> lvar write_set as t = lvar write_set as' t) \<Longrightarrow> writes cs as = writes cs' as'"
  apply (rule ext)
  apply (erule disjE)
  apply (simp add: writes_def locality_tml)
  apply (erule exE)
  by (simp add: writes_def locality_tml)

lemma writers_ap: "(the_writer cs = None \<and> the_writer cs' = None) \<or> (\<exists>t. the_writer cs = Some t \<and> the_writer cs' = Some t \<and> lvar write_set as t = lvar write_set as' t) \<Longrightarrow> apply_partial ls (writes cs as) = apply_partial ls (writes cs' as')"
  using writes_def by auto

lemma writes_update: "the_writer cs = Some t \<Longrightarrow> the_writer cs' = Some t \<Longrightarrow> (lvar write_set as t)(l \<mapsto> v) = lvar write_set as' t \<Longrightarrow> (apply_partial ls (writes cs as)) (l := v) = apply_partial ls (writes cs' as')"
  by (simp add: writes_def apply_partial_simp[symmetric])

lemma internal_tms_pre:
  assumes "global_inv cs"
  and "TML.txn_inv at (Internal ic) cs"
  and "TMS2.txn_inv at (Internal ia) as"
  and "global_rel cs as"
  and "txn_rel (Internal ic) cs as at"
  and "local_tml_pre at (Internal ic) cs"
  and "step_correspondence cs at ic = Some ia"
  shows "local_tms_pre at (Internal ia) as"
  using assms
  apply -
  apply (cases ia; simp add: scf_simps; (elim conjE)?)
  apply (simp add: tml_simps unfold_tms2 locality_tml local_tms_pre_def)
  apply (simp add: latest_store_def write_count_def tml_simps unfold_tms2 locality_tml txn_rel_def global_rel_def TMS2.txn_inv_def in_flight_def validity_prop_def local_tms_pre_def)
  apply (simp add: local_tms_pre_def unfold_tms2)
  apply (simp add: latest_store_def write_count_def tml_simps unfold_tms2 locality_tml txn_rel_def global_rel_def TMS2.txn_inv_def in_flight_def validity_prop_def local_tms_pre_def)
  apply blast
  apply (simp add: latest_store_def write_count_def tml_simps unfold_tms2 locality_tml txn_rel_def global_rel_def TMS2.txn_inv_def in_flight_def validity_prop_def local_tms_pre_def)
  by (simp add: latest_store_def write_count_def tml_simps unfold_tms2 locality_tml txn_rel_def global_rel_def TMS2.txn_inv_def in_flight_def validity_prop_def local_tms_pre_def)

lemma internal_global_rel:
  assumes "global_inv cs"
  and "TML.txn_inv at (Internal ic) cs"
  and "TMS2.txn_inv at (Internal ia) as" 
  and "global_rel cs as"
  and "txn_rel (Internal ic) cs as at"
  and "local_tml_pre at (Internal ic) cs"
  and "step_correspondence cs at ic = Some ia"
  shows "global_rel (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as)"
  using assms
  apply -
  apply (cases ia; simp add: scf_simps; (elim conjE)?)
  apply (simp_all add: global_rel_def)
  apply (simp_all add: locality_tml tml_simps unfold_tms2 txn_rel_def local_tms_eff_def TMS2.txn_inv_def write_count_def max_index_wb dom_def cong del: if_weak_cong)
  apply (rule writers_ap)
  apply (cases "\<exists>t. t \<noteq> at \<and> writer (fst cs t)")
  apply (rule disjI2)
  apply (simp add: the_writer_def locality_tml)
  apply blast
  apply (rule disjI1)
  apply (simp add: the_writer_def locality_tml)
  apply force
  apply (subst the_writer_Some[where t = at])
  apply (simp add: the_writer_def locality_tml)
  apply (meson the_equality)
  apply (subst the_writer_None)
  apply (simp add: the_writer_def locality_tml)
  apply blast
  apply (simp add: locality_tml write_back_def)
  apply auto[1]
  apply (intro impI conjI)
  apply (rule writers_ap)
  apply (simp add: the_writer_def locality_tml)
  apply (rule writers_ap)
  apply (simp add: the_writer_def locality_tml)
  apply (rule writes_update[where t = at])
  apply (simp add: the_writer_def locality_tml)
  apply (meson the_equality)
  apply (simp add: the_writer_def locality_tml)
  apply (meson the_equality)
  by (simp add: the_writer_def locality_tml update_partial_def)

lemma read_consistent_update_partial: "read_consistent (store_at ag wc) rs \<Longrightarrow> read_consistent (store_at ag wc) (update_partial l (store_at ag  wc l) rs)"
  by (simp add: update_partial_def read_consistent_def)

lemma internal_validity_prop_self:
  assumes "global_inv cs"
  and "TML.txn_inv at (Internal ic) cs"
  and "TMS2.txn_inv at (Internal ia) as"
  and "global_rel cs as"
  and "validity_prop cs as at"
  and "local_tml_pre at (Internal ic) cs"
  and "local_tms_pre at (Internal ia) as"
  and "step_correspondence cs at ic = Some ia"
  shows "validity_prop (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) at"
  using assms
  apply (cases ia; simp add: scf_simps)
  apply (simp_all add: validity_prop_def max_index_wb tml_simps locality_tml local_tms_eff_def unfold_tms2)
  using mi_rc_wb apply blast
  apply (simp add: dom_def value_for_def value_at_def write_count_def)
  by (simp add: read_consistent_update_partial)

lemma internal_validity_prop_other:
  assumes "global_inv cs"
  and "TML.txn_inv at (Internal ic) cs"
  and "TMS2.txn_inv at (Internal ia) as"
  and "global_rel cs as"
  and "validity_prop cs as t"
  and "local_tml_pre at (Internal ic) cs"
  and "local_tms_pre at (Internal ia) as"
  and "step_correspondence cs at ic = Some ia"
  and "at \<noteq> t"
  shows "validity_prop (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
  using assms
  apply (cases ia; simp add: scf_simps)
  apply (simp_all add: validity_prop_def max_index_wb tml_simps locality_tml local_tms_eff_def unfold_tms2)
  using mi_rc_wb by blast

lemma internal_validity_prop:
  assumes "global_inv cs"
  and "TML.txn_inv at (Internal ic) cs"
  and "TMS2.txn_inv at (Internal ia) as"
  and "global_rel cs as"
  and "validity_prop cs as t"
  and "local_tml_pre at (Internal ic) cs"
  and "local_tms_pre at (Internal ia) as"
  and "step_correspondence cs at ic = Some ia"
  shows "validity_prop (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
  using assms internal_validity_prop_other internal_validity_prop_self by blast

lemma if_Some: "(if B then Some a else None) = Some c \<longleftrightarrow> B \<and> a = c"
  by simp

lemmas sim_simps =
  scf_simps
  locality_tml
  tml_simps
  local_tms_eff_def
  unfold_tms2
  dom_def
  the_writer_def
  update_partial_def
  local_tms_pre_def
  if_Some

lemma internal_in_flight_self:
  assumes "global_inv cs"
  and "TML.txn_inv at (Internal ic) cs"
  and "TMS2.txn_inv at (Internal ia) as"
  and "global_rel cs as"
  and "in_flight cs as at"
  and "local_tml_pre at (Internal ic) cs"
  and "local_tms_pre at (Internal ia) as"
  and "step_correspondence cs at ic = Some ia"
  and "ic \<noteq> Commit2"
  shows "in_flight (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) at"
  using assms
  apply -
  apply (cases ia; simp add: scf_simps in_flight_def; intro conjI)
  apply (rule internal_validity_prop | simp add: sim_simps)
  apply ((rule internal_validity_prop; blast?; simp add: sim_simps) | simp add: sim_simps)
  apply ((rule internal_validity_prop; blast?; simp add: sim_simps; (meson the_equality)?) | (simp add: sim_simps; (meson the_equality)?))
  apply ((rule internal_validity_prop; blast?; simp add: sim_simps) | (simp add: sim_simps; blast))
  apply ((rule internal_validity_prop; blast?; simp add: sim_simps) | (simp add: sim_simps; blast))
  apply ((rule internal_validity_prop; blast?; simp add: sim_simps) | (simp add: sim_simps))
  apply (meson the_equality)
  by ((rule internal_validity_prop; blast?; simp add: sim_simps) | (simp add: sim_simps; blast))+

lemma internal_in_flight_other:
  assumes "global_inv cs"
  and "TML.txn_inv at (Internal ic) cs"
  and "TMS2.txn_inv at (Internal ia) as"
  and "global_rel cs as"
  and "in_flight cs as t"
  and "local_tml_pre at (Internal ic) cs"
  and "local_tms_pre at (Internal ia) as"
  and "step_correspondence cs at ic = Some ia"
  and "at \<noteq> t"
  shows "in_flight (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
  using assms
  apply -
  apply (cases ia; simp add: scf_simps in_flight_def; intro conjI)
  apply ((rule internal_validity_prop; blast?; simp add: sim_simps) | simp add: sim_simps)+
  apply (subgoal_tac "\<not> writer (fst cs t)")
  apply (metis the_equality)
  apply blast
  by ((rule internal_validity_prop; blast?; simp add: sim_simps) | simp add: sim_simps)+

lemma writes_loc_None: "writer (fst cs t) \<Longrightarrow> write_set (fst as t) l = None \<Longrightarrow> writes cs as l = None"
  apply (simp add: writes_def the_writer_def locality_tml)
  using the_equality by fastforce

lemma internal_in_flight:
  assumes "global_inv cs"
  and "TML.txn_inv at (Internal ic) cs"
  and "TMS2.txn_inv at (Internal ia) as"
  and "global_rel cs as"
  and "in_flight cs as t"
  and "local_tml_pre at (Internal ic) cs"
  and "local_tms_pre at (Internal ia) as"
  and "step_correspondence cs at ic = Some ia"
  and "\<not> (ic = Commit2 \<and> at = t)"
  shows "in_flight (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
  using assms internal_in_flight_other internal_in_flight_self by blast

lemma stutter_even_ws_empty_other:
  assumes "global_inv cs"
  and "TML.txn_inv at (Internal ic) cs"
  and "global_rel cs as"
  and "even_ws_empty cs as t"
  and "local_tml_pre at (Internal ic) cs"
  and "step_correspondence cs at ic = None"
  and "at \<noteq> t"
  shows "even_ws_empty (local_tml_eff at (Internal ic) cs) as t"
  using assms
  by (simp add: scf_None; elim disjE) (simp; simp add: tml_simps locality_tml even_ws_empty_def)+

lemma stutter_validity_prop_other:
  assumes "global_inv cs"
  and "TML.txn_inv at (Internal ic) cs"
  and "global_rel cs as"
  and "validity_prop cs as t"
  and "local_tml_pre at (Internal ic) cs"
  and "step_correspondence cs at ic = None"
  and "at \<noteq> t"
  shows "validity_prop (local_tml_eff at (Internal ic) cs) as t"
  using assms
  by (simp add: scf_None; elim disjE) (simp; simp add: tml_simps locality_tml validity_prop_def)+

lemma stutter_odd_is_writer_other:
  assumes "global_inv cs"
  and "TML.txn_inv at (Internal ic) cs"
  and "global_rel cs as"
  and "odd_is_writer cs as t"
  and "local_tml_pre at (Internal ic) cs"
  and "step_correspondence cs at ic = None"
  and "at \<noteq> t"
  shows "odd_is_writer (local_tml_eff at (Internal ic) cs) as t"
  using assms
  apply -
  apply (simp add: scf_None; elim disjE)
  apply (simp_all add: tml_simps locality_tml odd_is_writer_def the_writer_def if_Some)
  apply linarith
  by (metis (no_types, hide_lams) less_not_refl2)

lemma stutter_in_flight_other:
  assumes "global_inv cs"
  and "TML.txn_inv at (Internal ic) cs"
  and "global_rel cs as"
  and "in_flight cs as t"
  and "local_tml_pre at (Internal ic) cs"
  and "step_correspondence cs at ic = None"
  and "at \<noteq> t"
  shows "in_flight (local_tml_eff at (Internal ic) cs) as t"
  using assms
  apply (simp only: in_flight_var; elim conjE; intro conjI)
  apply (rule stutter_even_ws_empty_other; assumption)
  apply (rule stutter_validity_prop_other; assumption)
  apply (rule stutter_odd_is_writer_other; assumption)
  done

lemma stutter_even_ws_empty_self:
  assumes "global_inv cs"
  and "TML.txn_inv t (Internal ic) cs"
  and "global_rel cs as"
  and "even_ws_empty cs as t"
  and "local_tml_pre t (Internal ic) cs"
  and "step_correspondence cs t ic = None"
  and "ic \<notin> {Begin1, Begin2, Write4}"
  shows "even_ws_empty (local_tml_eff t (Internal ic) cs) as t"
  using assms
  apply (simp add: scf_None; elim disjE)
  apply (simp; simp add: tml_simps locality_tml even_ws_empty_def)  
  apply (simp; simp add: tml_simps locality_tml even_ws_empty_def)
  apply (simp; simp add: tml_simps locality_tml even_ws_empty_def)
  apply auto[1]
  apply (simp; simp add: tml_simps locality_tml even_ws_empty_def)
  apply (simp; simp add: tml_simps locality_tml even_ws_empty_def)
  apply auto[1]
  apply (simp; simp add: tml_simps locality_tml even_ws_empty_def)
  apply auto[1]
  apply (simp; simp add: tml_simps locality_tml even_ws_empty_def)
  by (simp; simp add: tml_simps locality_tml even_ws_empty_def)

lemma upd_writer: "(\<forall>t'. t' \<noteq> t \<longrightarrow> \<not> writer (fst (f cs) t')) \<Longrightarrow> writer (fst (f cs) t) \<Longrightarrow> the_writer cs = Some t \<Longrightarrow> the_writer (f cs) = Some t"
  apply (simp add: the_writer_def locality_tml)
  by (metis the_equality)

lemma upd_writer2: "writer_unique (f cs) \<Longrightarrow> writer (fst (f cs) t) \<Longrightarrow> the_writer cs = Some t \<Longrightarrow> the_writer (f cs) = Some t"
  apply (simp add: the_writer_def locality_tml writer_unique_def)
  by (metis the_equality)

lemma the_writer_is_writer: "writer_unique cs \<Longrightarrow> the_writer cs = Some t \<Longrightarrow> writer (fst cs t)"
  apply (auto simp add: the_writer_def locality_tml writer_unique_def cong del: if_weak_cong)
  apply (meson option.distinct(1))
  by (metis option.inject the_equality)

lemma stutter_odd_is_writer_self:
  assumes "global_inv cs"
  and "TML.txn_inv t (Internal ic) cs"
  and "global_rel cs as"
  and "odd_is_writer cs as t"
  and "local_tml_pre t (Internal ic) cs"
  and "step_correspondence cs t ic = None"
  and "ic \<notin> {Begin1, Begin2, Write4}"
  shows "odd_is_writer (local_tml_eff t (Internal ic) cs) as t"
  using assms
  apply (simp add: scf_None; elim disjE)
  apply (simp; simp add: tml_simps locality_tml odd_is_writer_def)
  apply (intro impI)
  apply (simp add: the_writer_def)
  apply (simp; simp add: tml_simps locality_tml odd_is_writer_def)

  apply simp
  apply (simp add: odd_is_writer_def)
  apply (simp (no_asm) add: tml_simps locality_tml odd_is_writer_def cong del: if_weak_cong)
  apply (intro conjI impI)
  apply (rule upd_writer) back
  apply simp
  apply (simp add: global_inv_def writer_unique_def locality_tml)
  apply (subgoal_tac "writer (fst cs t)")
  apply auto[1]
  using assms(1) global_inv_def the_writer_is_writer apply blast
  apply simp
  apply (simp add: global_inv_def local_def lvar_def the_writer_is_writer)
  apply (simp add: local_def lvar_def)
  apply (simp add: global_def gvar_def local_def lvar_def)
  apply (simp add: global_def gvar_def local_def lvar_def)

  apply simp
  apply (simp add: odd_is_writer_def)
  apply (simp (no_asm) add: tml_simps locality_tml odd_is_writer_def cong del: if_weak_cong)
  apply (intro conjI impI)
  apply (rule upd_writer) back
  apply simp
  apply (simp add: global_inv_def writer_unique_def locality_tml)
  apply (subgoal_tac "writer (fst cs t)")
  apply auto[1]
  using assms(1) global_inv_def the_writer_is_writer apply blast
  apply simp
  apply (simp add: global_inv_def local_def lvar_def the_writer_is_writer)
  apply (simp add: local_def lvar_def)
  apply (simp add: global_def gvar_def local_def lvar_def)
  apply (simp add: global_def gvar_def local_def lvar_def)

  apply simp
  apply (simp add: odd_is_writer_def)
  apply (simp (no_asm) add: tml_simps locality_tml odd_is_writer_def cong del: if_weak_cong)
  apply (intro conjI impI)
  apply (rule upd_writer) back
  apply simp
  apply (simp add: global_inv_def writer_unique_def locality_tml)
  apply (subgoal_tac "writer (fst cs t)")
  apply auto[1]
  using assms(1) global_inv_def the_writer_is_writer apply blast
  apply simp
  apply (simp add: global_inv_def local_def lvar_def the_writer_is_writer)
  apply (simp add: local_def lvar_def)
  apply (simp add: global_def gvar_def local_def lvar_def)
  apply (simp add: global_def gvar_def local_def lvar_def)

  apply simp
  apply (simp add: odd_is_writer_def)
  apply (simp (no_asm) add: tml_simps locality_tml odd_is_writer_def cong del: if_weak_cong)
  apply (intro conjI impI)
  apply (simp add: tml_simps locality_tml)
  apply (rule upd_writer) back
  apply simp
  apply (simp add: global_inv_def writer_unique_def locality_tml)
  apply (subgoal_tac "writer (fst cs t)")
  apply auto[1]
  apply (simp add: global_inv_def local_def lvar_def the_writer_is_writer)
  apply (simp add: local_def lvar_def)
  apply (simp add: local_def lvar_def)
  apply (simp add: gvar_def global_def local_def lvar_def)
  apply (simp add: gvar_def global_def local_def lvar_def)

  apply simp
  apply (simp add: odd_is_writer_def)
  apply (simp (no_asm) add: tml_simps locality_tml odd_is_writer_def cong del: if_weak_cong)
  apply (intro conjI impI)
  apply (simp add: tml_simps locality_tml)
  apply (rule upd_writer) back
  apply simp
  apply auto[1]
  apply (simp add: global_inv_def writer_unique_def locality_tml)
  apply blast
  apply (simp add: global_inv_def local_def lvar_def the_writer_is_writer)
  apply (simp add: gvar_def global_def local_def lvar_def)
  apply (simp add: gvar_def global_def local_def lvar_def)

  apply simp
  apply (simp add: odd_is_writer_def)
  apply (simp (no_asm) add: tml_simps locality_tml odd_is_writer_def cong del: if_weak_cong)
  apply (intro conjI impI)
  apply (rule upd_writer) back
  apply simp
  apply auto[1]
  apply (simp add: lvar_def)
  apply (simp add: locality_tml)
  apply (simp add: locality_tml)
  apply (metis global_inv_def local_def the_writer_is_writer writer_unique_def)
  apply simp
  apply (simp add: global_inv_def local_def lvar_def the_writer_is_writer)
  apply (simp add: local_def lvar_def)
  apply (simp add: global_def gvar_def local_def lvar_def)
  apply (simp add: global_def gvar_def local_def lvar_def)
  done

lemma stutter_validity_prop_self:
  assumes "global_inv cs"
  and "TML.txn_inv t (Internal ic) cs"
  and "global_rel cs as"
  and "validity_prop cs as t"
  and "local_tml_pre t (Internal ic) cs"
  and "step_correspondence cs t ic = None"
  and "ic \<notin> {Begin1, Begin2}"
  shows "validity_prop (local_tml_eff t (Internal ic) cs) as t"
  using assms
  apply -
  apply (simp add: scf_None; elim disjE)
  apply simp_all
  apply (simp_all add: tml_simps locality_tml validity_prop_def read_consistent_def option.case_eq_if)
  apply linarith+
  by (metis even_Suc_div_two)+

lemma stutter_in_flight_self:
  assumes "global_inv cs"
  and "TML.txn_inv t (Internal ic) cs"
  and "global_rel cs as"
  and "in_flight cs as t"
  and "local_tml_pre t (Internal ic) cs"
  and "step_correspondence cs t ic = None"
  and "ic \<notin> {Begin1, Begin2, Write4}"
  shows "in_flight (local_tml_eff t (Internal ic) cs) as t"
  using assms
  apply (simp only: in_flight_var; elim conjE; intro conjI)
  apply (rule stutter_even_ws_empty_self; assumption)
  apply (rule stutter_validity_prop_self; assumption?)
  apply simp
  apply (rule stutter_odd_is_writer_self; assumption)
  done

method other_solver =
  ((simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps),
   (rule internal_in_flight_other; assumption?),
   (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps))+

lemma status_choice: "lstate.status (fst (if A then B else C)) = stat \<longleftrightarrow> (A \<and> lstate.status (fst B) = stat) \<or> (\<not> A \<and> lstate.status (fst C) = stat)"
  by auto

lemma internal_txn_rel:
  assumes "global_inv cs"
  and "TML.txn_inv at (Internal ic) cs"
  and "TML.txn_inv t a cs"
  and "TMS2.txn_inv at (Internal ia) as"
  and "global_rel cs as"
  and "txn_rel (Internal ic) cs as at"
  and "txn_rel a cs as t"
  and "local_tml_pre at (Internal ic) cs"
  and "local_tms_pre at (Internal ia) as"
  and "step_correspondence cs at ic = Some ia"
  shows "txn_rel a (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
proof (rule_tac b = a in TML.Event_split; simp)
  assume "a = External BeginInv"
  from this and assms
  show "txn_rel (External BeginInv) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
    apply (cases ia; simp add: scf_simps; (elim conjE)?)
    apply (simp_all add: txn_rel_def)
    apply (simp add: tml_simps local_tms_eff_def locality_tml)
    apply (simp add: tml_simps local_tms_eff_def locality_tml)
    apply (simp (no_asm) add: tml_simps local_tms_eff_def locality_tml)
    apply (simp add: ext_enabled_def local_def lvar_def tml_pre_def view_def)
    by (simp add: tml_simps local_tms_eff_def locality_tml)
next
  assume "a = Internal Begin1"
  from this and assms
  show "txn_rel (Internal Begin1) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
    apply (cases ia; simp add: scf_simps; (elim conjE)?)
    apply (simp_all add: txn_rel_def)
    apply (simp add: tml_simps local_tms_eff_def locality_tml)
    apply (simp add: tml_simps local_tms_eff_def locality_tml)
    apply linarith
    apply (simp (no_asm) add: tml_simps local_tms_eff_def locality_tml)
    apply (simp add: ext_enabled_def local_def lvar_def tml_pre_def view_def)
    apply (simp add: global_def gvar_def)
    by (simp add: tml_simps local_tms_eff_def locality_tml)
next
  assume "a = Internal Begin2"
  from this and assms
  show "txn_rel (Internal Begin2) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
    apply (cases ia; simp add: scf_simps; (elim conjE)?)
    apply (simp_all add: txn_rel_def)
    by (simp_all add: tml_simps local_tms_eff_def locality_tml)
next
  assume "a = Internal Begin3"
  from this and assms
  show "txn_rel (Internal Begin3) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
    apply (cases ia; simp add: scf_simps; (elim conjE)?)
    apply (simp_all add: txn_rel_def)
    by (simp_all add: tml_simps local_tms_eff_def locality_tml)
next
  assume "a = External BeginResp"
  from this and assms
  show "txn_rel (External BeginResp) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
    apply (cases ia; simp add: scf_simps; (elim conjE)?)
    apply (simp_all add: txn_rel_def)
    by (simp_all add: tml_simps local_tms_eff_def locality_tml)
next
  fix l
  assume "a = External (ReadInv l)"
  from this and assms
  show "txn_rel (External (ReadInv l)) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
    apply -
    apply (cases ia; simp add: scf_simps; (elim conjE)?)
    apply (simp_all add: txn_rel_def)
    apply (simp_all add: tml_simps local_tms_eff_def locality_tml in_flight_def validity_prop_def le_Suc_eq read_consistent_def max_index_wb unfold_tms2 option.case_eq_if cong del: if_weak_cong)
    apply (simp_all add: the_writer_def if_Some locality_tml dom_def)
    apply (intro allI conjI impI)
    apply linarith+
    apply (intro allI conjI impI)
    apply (subst mi_store_wb)
    apply blast
    by auto
next
  assume "a = Internal Read1"
  from this and assms
  show "txn_rel (Internal Read1) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
    apply (cases ia; simp add: scf_simps; (elim conjE)?)
    apply (simp_all add: txn_rel_def)
    apply (simp_all add: tml_simps local_tms_eff_def locality_tml in_flight_def validity_prop_def le_Suc_eq read_consistent_def max_index_wb unfold_tms2 option.case_eq_if cong del: if_weak_cong)
    apply (simp_all add: the_writer_def if_Some locality_tml dom_def)
    apply (intro allI conjI impI)
    apply linarith+
    apply (intro allI conjI impI)
    apply (subst mi_store_wb)
    apply auto[1]
    apply simp
    apply blast
    apply (intro allI conjI impI)
    by auto
next
  assume [simp]: "a = Internal Read2"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (Internal Read2) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
      apply -
      apply (cases ia; simp add: scf_simps; (elim conjE)?; simp add: txn_rel_def; intro conjI impI; (rule internal_in_flight_self; blast?)?)
      apply simp_all
      apply (simp_all only: tml_simps local_tms_eff_def locality_tml le_Suc_eq read_consistent_def max_index_wb unfold_tms2 option.case_eq_if cong del: if_weak_cong)
      apply simp_all
      apply (simp_all only: value_for_def value_at_def latest_store_def global_rel_def tml_simps local_tms_eff_def locality_tml le_Suc_eq read_consistent_def max_index_wb unfold_tms2 option.case_eq_if cong del: if_weak_cong)
      apply simp_all
      apply (metis (no_types, lifting) assms(1) global_inv_def in_flight_def local_def lvar_def option.distinct(1) option.exhaust_sel the_writer_None the_writer_Some the_writer_is_writer)
      using assms(10)
      by simp
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (Internal Read2) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
      apply -
      apply (cases ia; simp add: scf_simps; (elim conjE)?; simp add: txn_rel_def; intro conjI impI; (rule internal_in_flight_other; blast?)?)
      apply (simp_all add: tml_simps local_tms_eff_def locality_tml le_Suc_eq read_consistent_def max_index_wb unfold_tms2 option.case_eq_if cong del: if_weak_cong)
      using assms(10)
      apply simp_all
      by linarith
  }
  ultimately show "txn_rel (Internal Read2) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t" by blast
next
  assume [simp]: "a = Internal Read3"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (Internal Read3) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
      apply -
      apply (cases ia; simp add: scf_simps; (elim conjE)?; simp add: txn_rel_def; intro conjI impI; (rule internal_in_flight_self; blast?)?)
      by (simp_all add: tml_simps local_tms_eff_def locality_tml le_Suc_eq read_consistent_def max_index_wb unfold_tms2 option.case_eq_if)
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (Internal Read3) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
      apply -
      apply (cases ia; simp add: scf_simps; (elim conjE)?; simp add: txn_rel_def; intro conjI impI; (rule internal_in_flight_other; blast?)?)
      apply (simp_all add: tml_simps local_tms_eff_def locality_tml le_Suc_eq read_consistent_def max_index_wb unfold_tms2 option.case_eq_if cong del: if_weak_cong)
      using assms(10)
      by simp_all
  }
  ultimately show "txn_rel (Internal Read3) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t" by blast
next
  fix v
  assume [simp]: "a = External (ReadResp v)"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (External (ReadResp v)) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
      apply -
      apply (cases ia; simp add: scf_simps; (elim conjE)?; simp add: txn_rel_def; intro conjI impI; (rule internal_in_flight_self; blast?)?)
      apply (simp_all only: tml_simps local_tms_eff_def locality_tml le_Suc_eq read_consistent_def max_index_wb unfold_tms2 option.case_eq_if)
      apply simp_all
      apply (simp_all only: tml_simps local_tms_eff_def locality_tml le_Suc_eq read_consistent_def max_index_wb unfold_tms2 option.case_eq_if)
      by simp_all
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (External (ReadResp v)) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
      apply -
      apply (cases ia; simp add: scf_simps; (elim conjE)?; simp add: txn_rel_def; intro conjI impI; (rule internal_in_flight_other; blast?)?)
      apply (simp_all add: tml_simps local_tms_eff_def locality_tml le_Suc_eq read_consistent_def max_index_wb unfold_tms2 option.case_eq_if cong del: if_weak_cong)
      using assms(10)
      by simp_all
  }
  ultimately show "txn_rel (External (ReadResp v)) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t" by blast
next
  fix l v
  assume [simp]: "a = External (WriteInv l v)"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (External (WriteInv l v)) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
      apply (cases ia; simp add: scf_simps; (elim conjE)?; simp add: txn_rel_def; intro impI conjI)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp)
      apply (rule internal_in_flight_self; assumption?)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (simp add: locality_tml)
      apply (simp; simp add: tml_simps locality_tml)
      apply (rule internal_in_flight_self; assumption?)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (simp add: locality_tml)
      apply (simp add: tml_simps locality_tml)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_in_flight_self; assumption?)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply simp
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_in_flight_self; assumption?)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      by simp
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (External (WriteInv l v)) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
      by (cases ia; simp add: scf_simps; (elim conjE)?; simp add: txn_rel_def; intro impI conjI) other_solver
  }
  ultimately
  show "txn_rel (External (WriteInv l v)) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
    by blast
next
  assume [simp]: "a = Internal Write1"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (Internal Write1) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
      apply (cases ia; simp add: scf_simps; (elim conjE)?; simp add: txn_rel_def; intro impI conjI)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp)
      apply (rule internal_in_flight_self; assumption?)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (simp add: locality_tml)
      apply simp
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_in_flight_self; assumption?)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (simp add: locality_tml)
      apply (simp add: tml_simps locality_tml)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_in_flight_self; assumption?)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply simp
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_in_flight_self; assumption?)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      by simp
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (Internal Write1) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
      by (cases ia; simp add: scf_simps; (elim conjE)?; simp add: txn_rel_def; intro impI conjI) other_solver
  }
  ultimately
  show "txn_rel (Internal Write1) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
    by blast
next
  assume [simp]: "a = Internal Write2"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (Internal Write2) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
      apply (cases ia; simp add: scf_simps; (elim conjE)?; simp add: txn_rel_def; intro impI conjI)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_validity_prop; assumption?)
      using in_flight_def local_tml_pre_def apply blast
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_validity_prop; assumption?)
      using in_flight_def local_tml_pre_def apply blast
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_validity_prop; assumption?)
      using in_flight_def local_tml_pre_def apply blast
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_validity_prop; assumption?)
      using in_flight_def local_tml_pre_def by blast
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (Internal Write2) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
      apply (cases ia; simp add: scf_simps; (elim conjE)?; simp add: txn_rel_def; intro impI conjI)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_validity_prop; assumption?)
      apply (subgoal_tac "tml_pre (Internal Write2) (view t cs)")
      apply simp
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)

      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_validity_prop; assumption?)
      apply (subgoal_tac "tml_pre (Internal Write2) (view t cs)")
      apply simp
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)

      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_validity_prop; assumption?)
      apply (subgoal_tac "tml_pre (Internal Write2) (view t cs)")
      apply simp
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)

      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_validity_prop; assumption?)
      apply (subgoal_tac "tml_pre (Internal Write2) (view t cs)")
      apply simp
      by (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
  }
  ultimately
  show "txn_rel (Internal Write2) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
    by blast
next
  assume [simp]: "a = Internal Write4"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (Internal Write4) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
      apply (cases ia; simp add: scf_simps; (elim conjE)?; simp add: txn_rel_def; intro impI conjI)
      by (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)+
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (Internal Write4) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
      apply (cases ia; simp add: scf_simps; (elim conjE)?; simp add: txn_rel_def; intro impI conjI)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_validity_prop; assumption?)
      apply (subgoal_tac "tml_pre (Internal Write4) (view t cs)")
      apply simp
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_validity_prop; assumption?)
      apply (subgoal_tac "tml_pre (Internal Write4) (view t cs)")
      apply simp
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_validity_prop; assumption?)
      apply (subgoal_tac "tml_pre (Internal Write4) (view t cs)")
      apply simp
      by (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)+
  }
  ultimately
  show "txn_rel (Internal Write4) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
    by blast
next
  assume [simp]: "a = Internal Write5"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (Internal Write5) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
      apply (cases ia; simp add: scf_simps; (elim conjE)?; simp add: txn_rel_def; intro impI conjI)
      by (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)+
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (Internal Write5) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
      apply (cases ia; simp add: scf_simps; (elim conjE)?; simp add: txn_rel_def; intro impI conjI)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_validity_prop; assumption?)
      apply (subgoal_tac "tml_pre (Internal Write5) (view t cs)")
      apply simp
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_validity_prop; assumption?)
      apply (subgoal_tac "tml_pre (Internal Write5) (view t cs)")
      apply simp
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_validity_prop; assumption?)
      apply (subgoal_tac "tml_pre (Internal Write5) (view t cs)")
      apply simp
      by (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)+
  }
  ultimately
  show "txn_rel (Internal Write5) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
    by blast
next
  assume [simp]: "a = External WriteResp"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (External WriteResp) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
      apply (cases ia; simp add: scf_simps; (elim conjE)?; simp add: txn_rel_def; intro impI conjI)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_validity_prop; assumption?)
      apply (subgoal_tac "tml_pre (External WriteResp) (view t cs)")
      apply simp
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_validity_prop; assumption?)
      apply (subgoal_tac "tml_pre (External WriteResp) (view t cs)")
      apply simp
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_validity_prop; assumption?)
      apply (subgoal_tac "tml_pre (External WriteResp) (view t cs)")
      apply simp
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (simp only: tml_simps local_tms_eff_def locality_tml unfold_tms2; simp add: tml_simps)
      apply (rule internal_validity_prop; assumption?)
      apply (subgoal_tac "tml_pre (Internal Write5) (view t cs)")
      apply simp
      by (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (External WriteResp) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
      apply (cases ia; simp add: scf_simps; (elim conjE)?; simp add: txn_rel_def; intro impI conjI)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_validity_prop; assumption?)
      apply (subgoal_tac "tml_pre (External WriteResp) (view t cs)")
      apply simp
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_validity_prop; assumption?)
      apply (subgoal_tac "tml_pre (External WriteResp) (view t cs)")
      apply simp
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_validity_prop; assumption?)
      apply (subgoal_tac "tml_pre (External WriteResp) (view t cs)")
      apply simp
      by (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)+
  }
  ultimately
  show "txn_rel (External WriteResp) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
    by blast
next
  assume [simp]: "a = External CommitInv"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (External CommitInv) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
      apply -
      apply (cases ia; simp add: scf_simps; (elim conjE)?; simp add: txn_rel_def; intro impI conjI; (rule internal_in_flight; blast?)?)
      apply simp_all
      apply (simp_all only: tml_simps local_tms_eff_def locality_tml)
      apply simp_all
      apply (simp_all only: tml_simps local_tms_eff_def locality_tml)
      by simp_all
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (External CommitInv) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
      apply -
      apply (cases ia; simp add: scf_simps; (elim conjE)?; simp add: txn_rel_def; intro impI conjI; (rule internal_in_flight_other; blast?)?)
      apply (simp_all add: tml_simps local_tms_eff_def locality_tml)
      using assms(10)
      by simp_all
  }
  ultimately show "txn_rel (External CommitInv) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t" by blast
next
  assume a_def [simp]: "a = Internal Commit1"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (Internal Commit1) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
      apply (cases ia; simp add: scf_simps; (elim conjE)?; simp add: txn_rel_def; intro impI conjI)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp)
      apply (rule internal_in_flight_self; assumption?)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (simp add: locality_tml)
      apply simp
      apply (simp add: tml_simps locality_tml)
      apply (rule internal_in_flight_self; assumption?)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (simp add: locality_tml)
      apply (simp add: tml_simps locality_tml)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_in_flight_self; assumption?)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply simp
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_in_flight_self; assumption?)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      by simp
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (Internal Commit1) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
      by (cases ia; simp add: scf_simps; (elim conjE)?; simp add: txn_rel_def; intro impI conjI) other_solver
  }
  ultimately show "txn_rel (Internal Commit1) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
    by blast
next
  assume a_def [simp]: "a = Internal Commit2"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (Internal Commit2) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
      apply (cases ia; simp add: scf_simps; (elim conjE)?; simp add: txn_rel_def; intro impI conjI)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp)
      apply (rule internal_in_flight_self; assumption?)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (simp add: locality_tml)
      apply (simp add: tml_simps locality_tml)
      apply (rule internal_in_flight_self; assumption?)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (simp add: locality_tml)
      apply (simp add: tml_simps locality_tml)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_in_flight_self; assumption?)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply simp
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      apply (rule internal_in_flight_self; assumption?)
      apply (simp only: tml_simps local_tms_eff_def locality_tml; simp add: tml_simps)
      by simp
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (Internal Commit2) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
      by (cases ia; simp add: scf_simps; (elim conjE)?; simp add: txn_rel_def; intro impI conjI) other_solver
  }
  ultimately show "txn_rel (Internal Commit2) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
    by blast
next
  assume a_def [simp]: "a = External CommitResp"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (External CommitResp) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
      apply (cases ia; simp add: scf_simps; (elim conjE)?; simp add: txn_rel_def; intro impI conjI)
      by (simp only: tml_simps local_tms_eff_def locality_tml unfold_tms2; simp add: tml_simps)+
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (External CommitResp) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
      apply (cases ia; simp add: scf_simps; (elim conjE)?; simp add: txn_rel_def; intro impI conjI)
      by (simp only: tml_simps local_tms_eff_def locality_tml unfold_tms2; simp add: tml_simps)+
  }
  ultimately show "txn_rel (External CommitResp) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
    by blast
next
  assume "a = External Cancel"
  from this and assms
  show "txn_rel (External Cancel) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
    by (cases ia; simp add: scf_simps txn_rel_def tml_simps)    
next
  assume "a = External Abort"
  from this and assms
  show "txn_rel (External Abort) (local_tml_eff at (Internal ic) cs) (local_tms_eff at (Internal ia) as) t"
    by (cases ia; simp add: scf_simps txn_rel_def tml_simps local_tms_eff_def unfold_tms2 locality_tml)
qed

lemma stutter_txn_rel:
  assumes "global_inv cs"
  and "TML.txn_inv at (Internal ic) cs"
  and "TML.txn_inv t a cs"
  and "global_rel cs as"
  and "txn_rel (Internal ic) cs as at"
  and "txn_rel a cs as t"
  and "local_tml_pre at (Internal ic) cs"
  and "step_correspondence cs at ic = None"
  shows "txn_rel a (local_tml_eff at (Internal ic) cs) as t"
proof (rule_tac b = a in TML.Event_split; simp)
  assume "a = External BeginInv"
  from this and assms
  show "txn_rel (External BeginInv) (local_tml_eff at (Internal ic) cs) as t"
    by (simp add: scf_None; elim disjE) (simp add: txn_rel_def locality_tml; simp add: tml_simps)+
next
  assume "a = Internal Begin1"
  from this and assms
  show "txn_rel (Internal Begin1) (local_tml_eff at (Internal ic) cs) as t"
    apply (simp add: scf_None; elim disjE)
    apply (simp add: txn_rel_def locality_tml; simp add: tml_simps)
    apply (simp add: txn_rel_def locality_tml; simp add: tml_simps)
    apply (simp add: txn_rel_def locality_tml; simp add: tml_simps)
    apply (simp add: txn_rel_def locality_tml; simp add: tml_simps)
    apply (simp add: txn_rel_def locality_tml; simp add: tml_simps)
    apply (cases "at = t")
    apply (simp add: tml_simps txn_rel_def locality_tml)
    apply (simp add: tml_simps txn_rel_def locality_tml)
    apply linarith
    apply (simp add: txn_rel_def locality_tml; simp add: tml_simps)
    apply (simp add: txn_rel_def locality_tml; simp add: tml_simps)
    apply (simp add: txn_rel_def locality_tml; simp add: tml_simps)
    apply (simp add: txn_rel_def locality_tml; simp add: tml_simps locality_tml)
    apply (intro conjI impI)
    apply blast
    apply (rule_tac j = "loc (fst cs t) div 2 + lcounter (fst cs t)" in le_trans)
    apply blast
    apply (simp add: add_le_mono div_le_mono)
    by (simp add: txn_rel_def locality_tml; simp add: tml_simps)
next
  assume "a = Internal Begin2"
  from this and assms
  show "txn_rel (Internal Begin2) (local_tml_eff at (Internal ic) cs) as t"
    apply (simp add: scf_None; elim disjE)
    apply (simp add: txn_rel_def locality_tml; simp add: tml_simps)
    apply (simp add: txn_rel_def locality_tml; simp add: tml_simps)
    apply (simp add: txn_rel_def locality_tml; simp add: tml_simps)
    apply (cases "at = t")
    apply (simp add: tml_simps txn_rel_def locality_tml)
    apply (simp add: tml_simps txn_rel_def locality_tml)
    apply linarith
    apply (simp add: txn_rel_def locality_tml; simp add: tml_simps)
    apply (simp add: txn_rel_def locality_tml; simp add: tml_simps)
    apply (cases "at = t")
    apply (simp add: tml_simps txn_rel_def locality_tml)
    apply (simp add: tml_simps txn_rel_def locality_tml)
    apply (simp add: txn_rel_def locality_tml; simp add: tml_simps)
    apply (simp add: txn_rel_def locality_tml; simp add: tml_simps)
    apply (cases "at = t")
    apply (simp add: tml_simps txn_rel_def locality_tml)
    apply (simp add: tml_simps txn_rel_def locality_tml)
    by (simp add: txn_rel_def locality_tml; simp add: tml_simps)+
next
  assume "a = Internal Begin3"
  from this and assms
  show "txn_rel (Internal Begin3) (local_tml_eff at (Internal ic) cs) as t"
    apply (simp add: scf_None; elim disjE)
    apply (simp add: txn_rel_def locality_tml; simp add: tml_simps locality_tml)+
    apply auto[1]
    apply (cases "at = t")
    apply (simp add: tml_simps txn_rel_def locality_tml)
    apply (simp add: tml_simps txn_rel_def locality_tml)
    apply (simp add: txn_rel_def locality_tml; simp add: tml_simps locality_tml)
    apply (cases "at = t")
    apply (simp add: tml_simps txn_rel_def locality_tml)
    apply (simp add: tml_simps txn_rel_def locality_tml)
    apply (simp add: txn_rel_def locality_tml; simp add: tml_simps locality_tml)
    apply (cases "at = t")
    apply (simp add: tml_simps txn_rel_def locality_tml)
    apply (simp add: tml_simps txn_rel_def locality_tml)
    apply (simp add: txn_rel_def locality_tml; simp add: tml_simps locality_tml)
    apply (simp add: txn_rel_def locality_tml; simp add: tml_simps locality_tml)
    apply (simp add: txn_rel_def locality_tml; simp add: tml_simps locality_tml)
    apply (cases "at = t")
    apply (simp add: tml_simps txn_rel_def locality_tml)
    by (simp add: tml_simps txn_rel_def locality_tml)
next
  assume [simp]: "a = External BeginResp"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (External BeginResp) (local_tml_eff at (Internal ic) cs) as t"
      by (simp add: scf_None; elim disjE) (simp add: txn_rel_def locality_tml; simp add: tml_simps)+
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (External BeginResp) (local_tml_eff at (Internal ic) cs) as t"
      by (simp add: scf_None; elim disjE) (simp add: txn_rel_def locality_tml; simp add: tml_simps)+
  }
  ultimately show "txn_rel (External BeginResp) (local_tml_eff at (Internal ic) cs) as t" by blast
next
  fix l
  assume [simp]: "a = External (ReadInv l)"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (External (ReadInv l)) (local_tml_eff at (Internal ic) cs) as t"
      by (simp add: scf_None; elim disjE) (simp add: txn_rel_def locality_tml; simp add: tml_simps)+
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (External (ReadInv l)) (local_tml_eff at (Internal ic) cs) as t"
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def; intro impI conjI)
      apply (simp add: txn_rel_def locality_tml; simp add: tml_simps)
      apply (rule stutter_in_flight_other; assumption?)
      apply (simp add: tml_simps locality_tml)
      apply (simp add: tml_simps locality_tml)
      apply (rule stutter_in_flight_other; assumption?)
      apply (simp add: tml_simps locality_tml)
      apply (simp add: tml_simps locality_tml)
      apply (rule stutter_in_flight_other; assumption?)
      apply (simp add: tml_simps locality_tml)
      apply (simp add: tml_simps locality_tml)
      apply (rule stutter_in_flight_other; assumption?)
      apply (simp add: tml_simps locality_tml)
      apply (simp add: tml_simps locality_tml)
      apply (rule stutter_in_flight_other; assumption?)
      apply (simp add: tml_simps locality_tml)
      apply (simp add: tml_simps locality_tml)
      apply (rule stutter_in_flight_other; assumption?)
      apply (simp add: tml_simps locality_tml)
      apply (simp add: tml_simps locality_tml)
      apply (rule stutter_in_flight_other; assumption?)
      apply (simp add: tml_simps locality_tml)
      apply (simp add: tml_simps locality_tml)
      apply (rule stutter_in_flight_other; assumption?)
      apply (simp add: tml_simps locality_tml)
      apply (simp add: tml_simps locality_tml)
      apply (rule stutter_in_flight_other; assumption?)
      apply (simp add: tml_simps locality_tml)
      apply (simp add: tml_simps locality_tml)
      apply (rule stutter_in_flight_other; assumption?)
      apply (simp add: tml_simps locality_tml)
      apply (simp add: tml_simps locality_tml)
      apply (rule stutter_in_flight_other; assumption?)
      by (simp add: tml_simps locality_tml)
  }
  ultimately show "txn_rel (External (ReadInv l)) (local_tml_eff at (Internal ic) cs) as t" by blast
next
  assume [simp]: "a = Internal Read1"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (Internal Read1) (local_tml_eff at (Internal ic) cs) as t"
      by (simp add: scf_None; elim disjE) (simp add: txn_rel_def locality_tml; simp add: tml_simps)+
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (Internal Read1) (local_tml_eff at (Internal ic) cs) as t"
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def; intro impI conjI)
      apply (simp add: txn_rel_def locality_tml; simp add: tml_simps)
      apply (rule stutter_in_flight_other; assumption?)
      apply (simp add: tml_simps locality_tml)
      apply (simp add: tml_simps locality_tml)
      apply (rule stutter_in_flight_other; assumption?)
      apply (simp add: tml_simps locality_tml)
      apply (simp add: tml_simps locality_tml)
      apply (rule stutter_in_flight_other; assumption?)
      apply (simp add: tml_simps locality_tml)
      apply (simp add: tml_simps locality_tml)
      apply (rule stutter_in_flight_other; assumption?)
      apply (simp add: tml_simps locality_tml)
      apply (simp add: tml_simps locality_tml)
      apply (rule stutter_in_flight_other; assumption?)
      apply (simp add: tml_simps locality_tml)
      apply (simp add: tml_simps locality_tml)
      apply (rule stutter_in_flight_other; assumption?)
      apply (simp add: tml_simps locality_tml)
      apply (simp add: tml_simps locality_tml)
      apply (rule stutter_in_flight_other; assumption?)
      apply (simp add: tml_simps locality_tml)
      apply (simp add: tml_simps locality_tml)
      apply (rule stutter_in_flight_other; assumption?)
      apply (simp add: tml_simps locality_tml)
      apply (simp add: tml_simps locality_tml)
      apply (rule stutter_in_flight_other; assumption?)
      apply (simp add: tml_simps locality_tml)
      apply (simp add: tml_simps locality_tml)
      apply (rule stutter_in_flight_other; assumption?)
      apply (simp add: tml_simps locality_tml)
      apply (simp add: tml_simps locality_tml)
      apply (rule stutter_in_flight_other; assumption?)
      by (simp add: tml_simps locality_tml)
  }
  ultimately show "txn_rel (Internal Read1) (local_tml_eff at (Internal ic) cs) as t" by blast
next
  assume [simp]: "a = Internal Read2"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (Internal Read2) (local_tml_eff at (Internal ic) cs) as t"
      apply -
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def; intro conjI impI; (rule stutter_in_flight_self; blast?)?)
      apply simp_all
      apply (simp_all add: tml_simps locality_tml status_choice)
      using assms(8) by blast
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (Internal Read2) (local_tml_eff at (Internal ic) cs) as t"
      apply -
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def; intro impI conjI; (rule stutter_in_flight_other; blast?)?)
      apply (simp_all add: tml_simps locality_tml)
      using assms(8)
      apply simp_all
      by linarith+
  }
  ultimately show "txn_rel (Internal Read2) (local_tml_eff at (Internal ic) cs) as t" by blast
next
assume [simp]: "a = Internal Read3"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (Internal Read3) (local_tml_eff at (Internal ic) cs) as t"
      apply -
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def; intro conjI impI; (rule stutter_in_flight_self; blast?)?)
      apply simp_all
      apply (simp_all add: tml_simps locality_tml status_choice)
      using assms(8) by blast 
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (Internal Read3) (local_tml_eff at (Internal ic) cs) as t"
      apply -
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def; intro impI conjI; (rule stutter_in_flight_other; blast?)?)
      apply (simp_all add: tml_simps locality_tml)
      using assms(8)
      by simp_all
  }
  ultimately show "txn_rel (Internal Read3) (local_tml_eff at (Internal ic) cs) as t" by blast
next
  fix v
  assume [simp]: "a = External (ReadResp v)"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (External (ReadResp v)) (local_tml_eff at (Internal ic) cs) as t"
      apply -
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def; intro conjI impI; (rule stutter_in_flight_self; blast?)?)
      apply simp_all
      apply (simp_all only: tml_simps locality_tml status_choice)
      apply simp_all
      apply (simp_all only: tml_simps locality_tml status_choice)
      apply simp_all
      using assms(8) by blast
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (External (ReadResp v)) (local_tml_eff at (Internal ic) cs) as t"
      apply -
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def; intro impI conjI; (rule stutter_in_flight_other; blast?)?)
      apply (simp_all add: tml_simps locality_tml status_choice)
      using assms(8)
      by simp_all
  }
  ultimately show "txn_rel (External (ReadResp v)) (local_tml_eff at (Internal ic) cs) as t" by blast
next
  fix l v
  assume [simp]: "a = External (WriteInv l v)"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (External (WriteInv l v)) (local_tml_eff at (Internal ic) cs) as t"
      apply -
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def; intro conjI impI; (rule stutter_in_flight_self; blast?)?)
      apply simp_all
      apply (simp_all only: tml_simps locality_tml status_choice)
      apply simp_all
      apply (simp_all only: tml_simps locality_tml status_choice)
      by simp_all
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (External (WriteInv l v)) (local_tml_eff at (Internal ic) cs) as t"
      apply -
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def; intro impI conjI; (rule stutter_in_flight_other; blast?)?)
      apply (simp_all add: tml_simps locality_tml status_choice)
      using assms(8)
      by simp_all
  }
  ultimately show "txn_rel (External (WriteInv l v)) (local_tml_eff at (Internal ic) cs) as t" by blast
next
  assume [simp]: "a = Internal Write1"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (Internal Write1) (local_tml_eff at (Internal ic) cs) as t"
      apply -
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def; intro conjI impI; (rule stutter_in_flight_self; blast?)?)
      apply simp_all
      apply (simp_all only: tml_simps locality_tml status_choice)
      apply simp_all
      apply (simp_all only: tml_simps locality_tml status_choice)
      by simp_all
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (Internal Write1) (local_tml_eff at (Internal ic) cs) as t"
      apply -
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def; intro impI conjI; (rule stutter_in_flight_other; blast?)?)
      apply (simp_all add: tml_simps locality_tml status_choice)
      using assms(8)
      by simp_all
  }
  ultimately show "txn_rel (Internal Write1) (local_tml_eff at (Internal ic) cs) as t" by blast
next
  assume [simp]: "a = Internal Write2"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (Internal Write2) (local_tml_eff at (Internal ic) cs) as t"
      apply -
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def; intro conjI impI; (rule stutter_validity_prop_self; blast?)?)
      apply (simp_all add: tml_simps locality_tml status_choice in_flight_def)
      using assms(8)
      by simp
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (Internal Write2) (local_tml_eff at (Internal ic) cs) as t"
      apply -
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def; intro impI conjI; (rule stutter_validity_prop_other; blast?)?)
      apply (simp_all add: tml_simps locality_tml status_choice)
      using assms(8)
      by simp_all
  }
  ultimately show "txn_rel (Internal Write2) (local_tml_eff at (Internal ic) cs) as t" by blast
next
  assume [simp]: "a = Internal Write4"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (Internal Write4) (local_tml_eff at (Internal ic) cs) as t"
      apply -
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def; intro conjI impI; (rule stutter_validity_prop_self; blast?)?)
      apply (simp_all add: tml_simps locality_tml status_choice in_flight_def)
      using assms(8)
      by simp
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (Internal Write4) (local_tml_eff at (Internal ic) cs) as t"
      apply -
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def; intro impI conjI; (rule stutter_validity_prop_other; blast?)?)
      apply (simp_all add: tml_simps locality_tml status_choice)
      using assms(8)
      by simp_all
  }
  ultimately show "txn_rel (Internal Write4) (local_tml_eff at (Internal ic) cs) as t" by blast
next
  assume [simp]: "a = Internal Write5"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (Internal Write5) (local_tml_eff at (Internal ic) cs) as t"
      apply -
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def; intro conjI impI; (rule stutter_validity_prop_self; blast?)?)
      apply simp_all
      apply (simp_all only: tml_simps locality_tml status_choice in_flight_def)
      apply simp_all
      apply (simp_all only: tml_simps locality_tml status_choice in_flight_def)
      apply simp_all
      using assms(8)
      by simp_all
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (Internal Write5) (local_tml_eff at (Internal ic) cs) as t"
      apply -
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def; intro impI conjI; (rule stutter_validity_prop_other; blast?)?)
      apply (simp_all add: tml_simps locality_tml status_choice)
      using assms(8)
      by simp_all
  }
  ultimately show "txn_rel (Internal Write5) (local_tml_eff at (Internal ic) cs) as t" by blast
next
  assume [simp]: "a = External WriteResp"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (External WriteResp) (local_tml_eff at (Internal ic) cs) as t"
      apply -
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def; intro conjI impI; (rule stutter_validity_prop_self; blast?)?)
      apply simp_all
      apply (simp_all only: tml_simps locality_tml status_choice in_flight_def)
      apply simp_all
      apply (simp_all only: tml_simps locality_tml status_choice in_flight_def)
      by simp_all
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (External WriteResp) (local_tml_eff at (Internal ic) cs) as t"
      apply -
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def; intro impI conjI; (rule stutter_validity_prop_other; blast?)?)
      apply (simp_all add: tml_simps locality_tml status_choice)
      using assms(8)
      by simp_all
  }
  ultimately show "txn_rel (External WriteResp) (local_tml_eff at (Internal ic) cs) as t" by blast
next
  assume [simp]: "a = External CommitInv"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (External CommitInv) (local_tml_eff at (Internal ic) cs) as t"
      apply -
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def; intro impI conjI; (rule stutter_in_flight_self; blast?)?)
      apply simp_all
      apply (simp_all only: tml_simps locality_tml status_choice)
      apply simp_all
      apply (simp_all only: tml_simps locality_tml status_choice)
      by simp_all
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (External CommitInv) (local_tml_eff at (Internal ic) cs) as t"
      apply -
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def; intro impI conjI; (rule stutter_in_flight_other; blast?)?)
      apply (simp_all add: tml_simps locality_tml)
      using assms(8)
      by simp_all
  }
  ultimately show "txn_rel (External CommitInv) (local_tml_eff at (Internal ic) cs) as t" by blast
next
  assume [simp]: "a = Internal Commit1"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (Internal Commit1) (local_tml_eff at (Internal ic) cs) as t"
      apply -
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def; intro impI conjI; (rule stutter_in_flight_self; blast?)?)
      apply simp_all
      apply (simp_all only: tml_simps locality_tml status_choice)
      apply simp_all
      apply (simp_all only: tml_simps locality_tml status_choice)
      by simp_all
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (Internal Commit1) (local_tml_eff at (Internal ic) cs) as t"
      apply -
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def; intro impI conjI; (rule stutter_in_flight_other; blast?)?)
      apply (simp_all add: tml_simps locality_tml)
      using assms(8)
      by simp_all
  }
  ultimately show "txn_rel (Internal Commit1) (local_tml_eff at (Internal ic) cs) as t" by blast
next
  assume [simp]: "a = Internal Commit2"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (Internal Commit2) (local_tml_eff at (Internal ic) cs) as t"
      apply -
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def; intro impI conjI; (rule stutter_in_flight_self; blast?)?)
      apply simp_all
      apply (simp_all only: tml_simps locality_tml status_choice)
      apply simp_all
      apply (simp_all only: tml_simps locality_tml status_choice)
      apply simp_all
      using assms(8)
      by simp
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (Internal Commit2) (local_tml_eff at (Internal ic) cs) as t"
      apply -
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def; intro impI conjI; (rule stutter_in_flight_other; blast?)?)
      apply (simp_all add: tml_simps locality_tml)
      using assms(8)
      by simp_all
  }
  ultimately show "txn_rel (Internal Commit2) (local_tml_eff at (Internal ic) cs) as t" by blast
next
  assume [simp]: "a = External CommitResp"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (External CommitResp) (local_tml_eff at (Internal ic) cs) as t"
      apply -
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def; intro impI conjI)
      apply (simp_all only: tml_simps locality_tml status_choice)
      apply simp_all
      apply (simp_all only: tml_simps locality_tml status_choice)
      by simp_all
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (External CommitResp) (local_tml_eff at (Internal ic) cs) as t"
      apply -
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def; intro impI conjI)
      apply (simp only: tml_simps locality_tml)
      by (simp_all add: tml_simps locality_tml)
  }
  ultimately show "txn_rel (External CommitResp) (local_tml_eff at (Internal ic) cs) as t" by blast
next
  assume "a = External Cancel"
  from this and assms
  show "txn_rel (External Cancel) (local_tml_eff at (Internal ic) cs) as t"
    by (simp add: scf_None; elim disjE; simp add: txn_rel_def) (simp_all add: tml_simps)
next
  assume [simp]: "a = External Abort"
  {
    assume "at = t"
    from this and assms
    have "txn_rel (External Abort) (local_tml_eff at (Internal ic) cs) as t"
      apply (simp add: scf_None; elim disjE; simp add: txn_rel_def)
      apply (simp add: local_tml_pre_def tml_pre_def ext_enabled_def)+
      apply auto[1]
      apply (simp add: local_tml_pre_def tml_pre_def ext_enabled_def)
      apply auto[1]
      by (simp add: local_tml_pre_def tml_pre_def ext_enabled_def)+
  }
  moreover
  {
    assume "at \<noteq> t"
    from this and assms
    have "txn_rel (External Abort) (local_tml_eff at (Internal ic) cs) as t"
      by (simp add: scf_None; elim disjE; simp add: txn_rel_def) (simp only: tml_simps locality_tml; simp)+
  }
  ultimately show "txn_rel (External Abort) (local_tml_eff at (Internal ic) cs) as t" by blast
qed

lemma stutter_global_rel:
  assumes "global_inv cs"
  and "TML.txn_inv at (Internal ic) cs"
  and "global_rel cs as"
  and "txn_rel (Internal ic) cs as at"
  and "local_tml_pre at (Internal ic) cs"
  and "step_correspondence cs at ic = None"
  shows "global_rel (local_tml_eff at (Internal ic) cs) as"
  using assms
  apply -
  apply (simp add: scf_None; elim disjE; simp add: global_rel_def)
  apply (simp add: tml_simps locality_tml txn_rel_def cong del: if_weak_cong)
  apply (rule same_writer_ap)
  apply (simp add: the_writer_def locality_tml)
  apply (simp add: tml_simps locality_tml txn_rel_def cong del: if_weak_cong)
  apply (simp add: tml_simps locality_tml txn_rel_def cong del: if_weak_cong; intro conjI impI)
  apply (rule same_writer_ap)
  apply (simp add: the_writer_def locality_tml)
  apply (rule same_writer_ap)
  apply (simp add: the_writer_def locality_tml)
  apply (simp add: tml_simps locality_tml txn_rel_def cong del: if_weak_cong; intro conjI impI)
  apply (rule same_writer_ap)
  apply (simp add: the_writer_def locality_tml)
  apply (rule same_writer_ap)
  apply (simp add: the_writer_def locality_tml)
  apply (simp add: tml_simps locality_tml txn_rel_def cong del: if_weak_cong; intro conjI impI)
  apply (rule same_writer_ap)
  apply (simp add: the_writer_def locality_tml)
  apply (rule same_writer_ap)
  apply (simp add: the_writer_def locality_tml)
  apply (simp add: tml_simps locality_tml txn_rel_def cong del: if_weak_cong; intro conjI impI)
  apply (rule become_writer_ap[where t = at])
  apply (simp add: the_writer_def locality_tml)
  apply blast
  apply (simp add: the_writer_def locality_tml)
  apply (simp add: the_writer_def locality_tml)
  apply (simp add: write_count_def)
  apply (rule same_writer_ap)
  apply (simp add: the_writer_def locality_tml)
  apply (simp add: tml_simps locality_tml txn_rel_def cong del: if_weak_cong)
  apply (rule same_writer_ap)
  apply (simp add: the_writer_def locality_tml)
  apply (simp add: tml_simps locality_tml txn_rel_def cong del: if_weak_cong)
  apply (rule same_writer_ap)
  apply (simp add: the_writer_def locality_tml)
  apply (simp add: tml_simps locality_tml txn_rel_def cong del: if_weak_cong)
  apply (rule same_writer_ap)
  apply (simp add: the_writer_def locality_tml)
  apply (simp add: tml_simps locality_tml txn_rel_def cong del: if_weak_cong; intro conjI impI)
  apply (rule same_writer_ap)
  apply (simp add: the_writer_def locality_tml)
  apply (rule same_writer_ap)
  apply (simp add: the_writer_def locality_tml)
  apply (simp add: tml_simps locality_tml txn_rel_def cong del: if_weak_cong)
  apply (rule same_writer_ap)
  by (simp add: the_writer_def locality_tml)

lemma tml_sim_int_step:
  "standard_sim_int_step (interference \<rhd> TML) (tms2_interference \<rhd> TMS2) step_correspondence sim_rel"
  apply (simp only: standard_sim_int_step_def sim_rel_def tml_leff tml_lpre abs_leff abs_lpre)
  apply (intro conjI impI allI; elim conjE)
  using reachable_invariant_tms2_interference tml_reach_inv
  apply -
  apply (rule internal_tms_pre; blast)
  apply (rule internal_global_rel; blast)
  apply (rule internal_txn_rel; blast?)
  by (rule internal_tms_pre; blast)

lemma tml_sim_stutter:
  "standard_sim_stutter (interference \<rhd> TML) (tms2_interference \<rhd> TMS2) step_correspondence sim_rel"
  apply (simp only: standard_sim_stutter_def sim_rel_def tml_leff tml_lpre abs_leff abs_lpre)
  apply (intro conjI impI allI; elim conjE)
  using reachable_invariant_tms2_interference tml_reach_inv
  apply -
  apply (simp add: stutter_global_rel)
  using stutter_txn_rel apply auto[1]
  done

lemma external_validity_prop_other:
  assumes "global_inv cs"
  and "TML.txn_inv at (External e) cs"
  and "TMS2.txn_inv at (External e) as"
  and "global_rel cs as"
  and "validity_prop cs as t"
  and "local_tml_pre at (External e) cs"
  and "local_tms_pre at (External e) as"
  and "at \<noteq> t"
  shows "validity_prop (local_tml_eff at (External e) cs) (local_tms_eff at (External e) as) t"
  using assms
  by (cases e) (simp_all add: validity_prop_def max_index_wb tml_simps locality_tml local_tms_eff_def unfold_tms2)

lemma external_in_flight_other:
  assumes "global_inv cs"
  and "TML.txn_inv at (External e) cs"
  and "TMS2.txn_inv at (External e) as"
  and "global_rel cs as"
  and "in_flight cs as t"
  and "local_tml_pre at (External e) cs"
  and "local_tms_pre at (External e) as"
  and "at \<noteq> t"
  shows "in_flight (local_tml_eff at (External e) cs) (local_tms_eff at (External e) as) t"
  using assms
  apply (cases e; simp add: in_flight_def; intro impI conjI external_validity_prop_other; blast?)
  by (simp_all add: in_flight_def max_index_wb tml_simps locality_tml local_tms_eff_def unfold_tms2 the_writer_def if_Some)

lemma external_txn_rel_other:
  assumes "global_inv cs"
  and "TML.txn_inv at (External e) cs"
  and "TML.txn_inv t a cs"
  and "TMS2.txn_inv at (External e) as"
  and "global_rel cs as"
  and "txn_rel (External e) cs as at"
  and "txn_rel a cs as t"
  and "local_tml_pre at (External e) cs"
  and "local_tms_pre at (External e) as"
  and "at \<noteq> t"
  shows "txn_rel a (local_tml_eff at (External e) cs) (local_tms_eff at (External e) as) t"
  using assms
  apply (rule_tac b = a in TML.Event_split; cases e; simp add: txn_rel_def;
         (intro impI conjI external_in_flight_other external_validity_prop_other)?; blast?)
  by (simp_all add: tml_simps locality_tml local_tms_eff_def unfold_tms2)

lemma external_even_ws_empty_self:
  assumes "global_inv cs"
  and "TML.txn_inv at (External e) cs"
  and "TMS2.txn_inv at (External e) as"
  and "global_rel cs as"
  and "even_ws_empty cs as at"
  and "local_tml_pre at (External e) cs"
  and "local_tms_pre at (External e) as"
  shows "even_ws_empty (local_tml_eff at (External e) cs) (local_tms_eff at (External e) as) at"
  using assms
  by (cases e) (simp_all add: even_ws_empty_def max_index_wb tml_simps locality_tml local_tms_eff_def unfold_tms2)

lemma external_odd_is_writer_self:
  assumes "global_inv cs"
  and "TML.txn_inv at (External e) cs"
  and "TMS2.txn_inv at (External e) as"
  and "global_rel cs as"
  and "odd_is_writer cs as at"
  and "local_tml_pre at (External e) cs"
  and "local_tms_pre at (External e) as"
  shows "odd_is_writer (local_tml_eff at (External e) cs) (local_tms_eff at (External e) as) at"
  using assms
  apply (cases e)
  by (simp_all add: odd_is_writer_def max_index_wb tml_simps locality_tml local_tms_eff_def unfold_tms2 the_writer_def locality_tml if_Some)

lemma external_validity_prop_self:
  assumes "global_inv cs"
  and "TML.txn_inv at (External e) cs"
  and "TMS2.txn_inv at (External e) as"
  and "global_rel cs as"
  and "validity_prop cs as at"
  and "local_tml_pre at (External e) cs"
  and "local_tms_pre at (External e) as"
  and "e \<noteq> BeginInv"
  shows "validity_prop (local_tml_eff at (External e) cs) (local_tms_eff at (External e) as) at"
  using assms
  by (cases e) (simp_all add: validity_prop_def max_index_wb tml_simps locality_tml local_tms_eff_def unfold_tms2)

lemma external_in_flight_self:
  assumes "global_inv cs"
  and "TML.txn_inv at (External e) cs"
  and "TMS2.txn_inv at (External e) as"
  and "global_rel cs as"
  and "in_flight cs as at"
  and "local_tml_pre at (External e) cs"
  and "local_tms_pre at (External e) as"
  and "e \<noteq> BeginInv"
  shows "in_flight (local_tml_eff at (External e) cs) (local_tms_eff at (External e) as) at"
  using assms
  apply -
  by (cases e; simp add: in_flight_var; intro conjI external_validity_prop_self external_odd_is_writer_self external_even_ws_empty_self; blast?)

lemma external_txn_rel_self:
  assumes "global_inv cs"
  and "TML.txn_inv at (External e) cs"
  and inv2: "TML.txn_inv at a cs"
  and "TMS2.txn_inv at (External e) as"
  and "global_rel cs as"
  and "txn_rel (External e) cs as at"
  and "local_tml_pre at (External e) cs"
  and "local_tms_pre at (External e) as"
  shows "txn_rel a (local_tml_eff at (External e) cs) (local_tms_eff at (External e) as) at"
  using assms(1) assms(2) assms(4) assms(5) assms(6) assms(7) assms(8)
  apply -
  apply (cases e; rule_tac b = a in TML.Event_split; simp add: txn_rel_def;
         (intro impI conjI external_in_flight_self)?; blast?)
  apply (simp_all add: tml_simps locality_tml local_tms_eff_def unfold_tms2 in_flight_def local_tms_pre_def global_rel_def TMS2.txn_inv_def)
  apply (simp add: global_rel_def locality_tml write_count_def)
  apply (simp_all add: validity_prop_def locality_tml write_count_def read_consistent_def)
  using div_le_mono apply linarith+
  using inv2
  apply (simp add: tml_simps locality_tml the_writer_def)
  apply (meson the_equality)
  apply (simp add: tml_simps locality_tml the_writer_def)
  apply (meson the_equality)
  apply (simp add: tml_simps locality_tml the_writer_def)
  by (meson the_equality)

lemma external_txn_rel:
  assumes "global_inv cs"
  and "TML.txn_inv at (External e) cs"
  and "TML.txn_inv t a cs"
  and "TMS2.txn_inv at (External e) as"
  and "global_rel cs as"
  and "txn_rel (External e) cs as at"
  and "txn_rel a cs as t"
  and "local_tml_pre at (External e) cs"
  and "local_tms_pre at (External e) as"
  shows "txn_rel a (local_tml_eff at (External e) cs) (local_tms_eff at (External e) as) t"
  using assms external_txn_rel_other external_txn_rel_self by blast

lemma external_tms_pre:
  assumes "global_inv cs"
  and "TML.txn_inv at (External e) cs"
  and "TMS2.txn_inv at (External e) as"
  and "global_rel cs as"
  and "txn_rel (External e) cs as at"
  and "local_tml_pre at (External e) cs"
  shows "local_tms_pre at (External e) as"
  using assms
  apply -
  apply (cases e)
  by (simp_all add: tml_simps locality_tml local_tms_pre_def unfold_tms2 txn_rel_def) 

lemma external_global_rel:
  assumes "global_inv cs"
  and "TML.txn_inv at (External e) cs"
  and "TMS2.txn_inv at (External e) as"
  and "global_rel cs as"
  and "txn_rel (External e) cs as at"
  and "local_tml_pre at (External e) cs"
  and "local_tms_pre at (External e) as"
  shows "global_rel (local_tml_eff at (External e) cs) (local_tms_eff at (External e) as)"
  using assms
  apply -
  apply (cases e)
  apply (simp_all add: tml_simps locality_tml local_tms_eff_def unfold_tms2 global_rel_def)
  apply (rule writers_ap, simp add: the_writer_def locality_tml unfold_tms2)+
  done

lemma tml_sim_ext_step:
  "standard_sim_ext_step (interference \<rhd> TML) (tms2_interference \<rhd> TMS2) sim_rel"
  apply (simp only: standard_sim_ext_step_def sim_rel_def tml_leff tml_lpre abs_leff abs_lpre)
  apply (intro conjI impI allI; elim conjE)
  using reachable_invariant_tms2_interference tml_reach_inv
  apply -
  using external_tms_pre apply blast
  using external_global_rel external_tms_pre apply blast
  apply (rule external_txn_rel; blast?)
  using external_tms_pre by blast

lemma TML_weak_simulation:
  "weak_standard_simulation (interference \<rhd> TML) (tms2_interference \<rhd> TMS2) step_correspondence sim_rel"
  unfolding weak_standard_simulation_def
  apply (simp only: tml_sim_int_step tml_sim_stutter tml_sim_ext_step)
  apply safe
  apply (rule_tac x = default_start in exI)
  apply (simp add: starts_def Rely_def image_def TML_def lstarts_def tms_starts_def default_start_def sim_rel_def
                   global_rel_def gstart_def locality_tml mem_initial_def initial_stores_def latest_store_def max_index_def
                   write_count_def writes_def the_writer_def)
  apply (intro allI)
  apply (rename_tac cs cg a t)
  apply (rule_tac b = a in TML.Event_split)
  by (simp_all add: txn_rel_def locality_tml tml_pre_def ext_enabled_def)

end