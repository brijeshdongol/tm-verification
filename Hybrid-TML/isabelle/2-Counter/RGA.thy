theory RGA
  imports "~~/src/HOL/HOLCF/IOA/IOA" Transactions

begin

type_synonym ('t, 's, 'g) state = "('t \<Rightarrow> 's) \<times> 'g"

record ('t, 's, 'g, 'i) RGA =
  local_starts :: "('t \<Rightarrow> 's) set"
  global_start :: "'g"
  pre :: "('i event \<Rightarrow> 's * 'g \<Rightarrow> bool)"
  eff :: "('i event \<Rightarrow> 's * 'g \<Rightarrow> 's * 'g)"
  environment :: "'g rel"

definition local :: "'t \<Rightarrow> ('t, 's, 'g) state \<Rightarrow> 's" where
  "local t s = fst s t"

definition global :: "('t, 's, 'g) state \<Rightarrow> 'g" where
  "global = snd"

definition view :: "'t \<Rightarrow> ('t, 's, 'g) state \<Rightarrow> ('s \<times> 'g)" where
  "view t s = (local t s, snd s)"

definition lvar :: "('s \<Rightarrow> 'v) \<Rightarrow> ('t, 's, 'g) state \<Rightarrow> 't \<Rightarrow> 'v" where
  "lvar access s t \<equiv> access (local t s)"

definition gvar :: "('g \<Rightarrow> 'v) \<Rightarrow> ('t, 's, 'g) state \<Rightarrow> 'v" where
  "gvar access s = access (global s)"

definition localize :: "'t \<Rightarrow> ('s \<times> 'g \<Rightarrow> 's \<times> 'g) \<Rightarrow> ('t, 's, 'g) state \<Rightarrow> ('t, 's, 'g) state" where
  "localize t f s \<equiv> (\<lambda>t'. if t = t' then fst (f (view t s)) else local t' s, snd (f (view t s)))"

primrec globalize :: "('g1 \<Rightarrow> 'g2) \<Rightarrow> ('t, 's, 'g1) state \<Rightarrow> ('t, 's, 'g2) state" where
  "globalize f (l, g) = (l, f g)" 

definition local_preserved :: "('t, 's, 'g, 'i) RGA \<Rightarrow> (('t, 's, 'g) state \<Rightarrow> bool) \<Rightarrow> ('t, 's, 'g) state \<Rightarrow> bool " where 
  "local_preserved aut P s \<equiv> \<forall>t a. P s \<and> pre aut a (view t s) \<longrightarrow> P (localize t (eff aut a) s)"

definition global_preserved :: "('t, 's, 'g, 'i) RGA \<Rightarrow> (('t, 's, 'g) state \<Rightarrow> bool) \<Rightarrow> ('t, 's, 'g) state \<Rightarrow> bool" where
  "global_preserved aut P s \<equiv> \<forall>g. (global s, g) \<in> environment aut \<and> P s \<longrightarrow> P (fst s, g)"

definition preserved :: "('t, 's, 'g, 'i) RGA \<Rightarrow> (('t, 's, 'g) state \<Rightarrow> bool) \<Rightarrow> ('t, 's, 'g) state \<Rightarrow> bool" where
  "preserved aut P s \<equiv> local_preserved aut P s \<and> global_preserved aut P s"

definition starts :: "('t, 's, 'g, 'i) RGA \<Rightarrow> (('t, 's, 'g) state) set" where
   "starts rga \<equiv> (\<lambda>x. Pair x (global_start rga)) ` (local_starts rga)"

definition "Environment \<equiv> Inr ()"

definition sig :: "('t, 's, 'g, 'i) RGA \<Rightarrow> 't ioa_event signature" where
  "sig aut \<equiv> ({}, {Inl (t, a)|t a. (\<exists>a'. a = External a')}, {Inl (t, a)|t a. a = Internal Tau} \<union> {Environment})"

definition local_eff :: "('t, 's, 'g, 'i) RGA \<Rightarrow> 't \<Rightarrow> 'i event \<Rightarrow> ('t, 's, 'g) state \<Rightarrow> ('t, 's, 'g) state" where
  "local_eff aut t a s = localize t (eff aut a) s"

definition local_pre :: "('t, 's, 'g, 'i) RGA \<Rightarrow> 't \<Rightarrow> 'i event \<Rightarrow> ('t, 's, 'g) state \<Rightarrow> bool" where
  "local_pre aut t a s = pre aut a (view t s)"

definition trans :: "('t, 's, 'g, 'i) RGA \<Rightarrow> (('t, 's, 'g) state \<times> 't ioa_event \<times> ('t, 's, 'g) state) set" where
  "trans aut \<equiv>
    {(s0, a, s1).
      case a of
        Inl (t, Internal Tau) \<Rightarrow> (\<exists>i. local_pre aut t (Internal i) s0 \<and> s1 = local_eff aut t (Internal i) s0)
      | Inr () \<Rightarrow> fst s0 = fst s1 \<and> (snd s0, snd s1) \<in> environment aut
      | Inl (t, External a') \<Rightarrow> (local_pre aut t (External a') s0 \<and> s1 = local_eff aut t (External a') s0)}"
                                                                    
definition ioa :: "('t, 's, 'g, 'i) RGA \<Rightarrow> ('t ioa_event, ('t, 's, 'g) state) ioa" where
  "ioa aut = (sig aut, starts aut, trans aut, {}, {})"

definition reach :: "('t, 's, 'g, 'i) RGA \<Rightarrow> ('t, 's, 'g) state \<Rightarrow> bool" where
  "reach aut s = reachable (ioa aut) s"

definition standard_sim_ext_step ::
  "('t, 'sc, 'gc, 'ic) RGA \<Rightarrow> ('t, 'sa, 'ga, 'ia) RGA \<Rightarrow> (('t, 'sc, 'gc) state \<Rightarrow> ('t, 'sa, 'ga) state \<Rightarrow> bool) \<Rightarrow> bool"
  where
  "standard_sim_ext_step C A R \<equiv>
    \<forall>cs as a t. reach C cs \<and> reach A as \<longrightarrow> R cs as \<and> local_pre C t (External a) cs \<longrightarrow>
     local_pre A t (External a) as \<and> R (local_eff C t (External a) cs) (local_eff A t (External a) as)"

definition standard_sim_env_step ::
  "('t, 'sc, 'gc, 'ic) RGA \<Rightarrow> ('t, 'sa, 'ga, 'ia) RGA \<Rightarrow> (('t, 'sc, 'gc) state \<Rightarrow> ('t, 'sa, 'ga) state \<Rightarrow> bool) \<Rightarrow> bool"
  where
  "standard_sim_env_step C A R \<equiv>
    \<forall>cs cg' as. reach C cs \<and> reach A as \<longrightarrow> R cs as \<and> (snd cs, cg') \<in> environment C \<longrightarrow>
     (\<exists>ag'. (snd as, ag') \<in> environment A \<and> R (fst cs, cg') (fst as, ag'))"

(*
definition sim_env_to_prog ::
  "('t, 'sc, 'gc, 'ic) RGA \<Rightarrow> ('t, 'sa, 'ga, 'ia) RGA \<Rightarrow> (('t, 'sc, 'gc) state \<Rightarrow> ('t, 'sa, 'ga) state \<Rightarrow> bool) \<Rightarrow> bool"
  where
  "sim_env_to_prog C A R \<equiv>
    \<forall>cs cg' as. reach C cs \<and> reach A as \<longrightarrow> R cs as \<and> (snd cs, cg') \<in> environment C \<longrightarrow>
     (\<exists>ag' actions. R (snd cs, cg') ("
*)

definition standard_sim_stutter ::
  "('t, 'sc, 'gc, 'ic) RGA \<Rightarrow> ('t, 'sa, 'ga, 'ia) RGA \<Rightarrow> (('t, 'sc, 'gc) state \<Rightarrow> 't \<Rightarrow> 'ic \<Rightarrow> 'ia option) \<Rightarrow> (('t, 'sc, 'gc) state \<Rightarrow> ('t, 'sa, 'ga) state \<Rightarrow> bool) \<Rightarrow> bool"
   where
   "standard_sim_stutter C A sc R \<equiv>
    \<forall> cs as ic at. reach C cs \<and> reach A as \<longrightarrow>
       R cs as \<and> local_pre C at (Internal ic) cs \<and> sc cs at ic = None
         \<longrightarrow>
          R (local_eff C at (Internal ic) cs) as"

(*
definition rg_sim_stutter ::
  "('t, 'sc, 'gc, 'ic) RGA \<Rightarrow> ('t, 'sa, 'ga, 'ia) RGA \<Rightarrow> (('t, 'sc, 'gc) state \<Rightarrow> 't \<Rightarrow> 'ic \<Rightarrow> 'ia option) \<Rightarrow> (('t, 'sc, 'gc) state \<Rightarrow> ('t, 'sa, 'ga) state \<Rightarrow> bool) \<Rightarrow> bool"
   where
   "rg_sim_stutter C A sc R \<equiv>
    \<forall>cs as ic at. reach C cs \<and> reach A as \<and> R cs as \<and> local_pre C at (Internal ic) cs \<and> sc cs at ic = None
     \<longrightarrow> R (local_eff C at (Internal ic) cs) as
         \<or> (\<exists>ag. (snd as, ag) \<in> environment A \<and> R (local_eff C at (Internal ic) cs) (fst as, ag))"
*)

definition standard_sim_int_step ::
  "('t, 'sc, 'gc, 'ic) RGA \<Rightarrow> ('t, 'sa, 'ga, 'ia) RGA \<Rightarrow> (('t, 'sc, 'gc) state \<Rightarrow> 't \<Rightarrow> 'ic \<Rightarrow> 'ia option) \<Rightarrow> (('t, 'sc, 'gc) state \<Rightarrow> ('t, 'sa, 'ga) state \<Rightarrow> bool) \<Rightarrow> bool"
   where
   "standard_sim_int_step C A sc R \<equiv>
    \<forall> cs as ic ia at. reach C cs \<and> reach A as \<longrightarrow>
      R cs as \<and> local_pre C at (Internal ic) cs \<and> sc cs at ic = Some ia
        \<longrightarrow>
          local_pre A at (Internal ia) as \<and> R (local_eff C at (Internal ic) cs) (local_eff A at (Internal ia) as)"

definition rg_sim_int_step ::
  "('t, 'sc, 'gc, 'ic) RGA \<Rightarrow> ('t, 'sa, 'ga, 'ia) RGA \<Rightarrow> (('t, 'sc, 'gc) state \<Rightarrow> 't \<Rightarrow> 'ic \<Rightarrow> 'ia option) \<Rightarrow> (('t, 'sc, 'gc) state \<Rightarrow> ('t, 'sa, 'ga) state \<Rightarrow> bool) \<Rightarrow> bool"
   where
   "rg_sim_int_step C A sc R \<equiv>
    \<forall>cs as ic ia at. reach C cs \<and> reach A as \<and> R cs as \<and> local_pre C at (Internal ic) cs \<and> sc cs at ic = Some ia
        \<longrightarrow> (local_pre A at (Internal ia) as \<and> R (local_eff C at (Internal ic) cs) (local_eff A at (Internal ia) as))
            \<or> (\<exists>ag. (snd as, ag) \<in> environment A \<and> R (local_eff C at (Internal ic) cs) (fst as, ag))"

definition standard_simulation ::
  "('t, 'sc, 'gc, 'ic) RGA \<Rightarrow> ('t, 'sa, 'ga, 'ia) RGA \<Rightarrow> (('t, 'sc, 'gc) state \<Rightarrow> 't \<Rightarrow> 'ic \<Rightarrow> 'ia option) \<Rightarrow> (('t, 'sc, 'gc) state \<Rightarrow> ('t, 'sa, 'ga) state \<Rightarrow> bool) \<Rightarrow> bool"
  where
  "standard_simulation C A sc R \<equiv>
    (\<forall> cs. cs \<in> starts C \<longrightarrow> (\<exists>as. R cs as \<and> as \<in> starts A))
    \<and>
    standard_sim_ext_step C A R
    \<and>
    standard_sim_env_step C A R
    \<and>
    standard_sim_stutter C A sc R
    \<and>
    standard_sim_int_step C A sc R"

definition rg_simulation ::
  "('t, 'sc, 'gc, 'ic) RGA \<Rightarrow> ('t, 'sa, 'ga, 'ia) RGA \<Rightarrow> (('t, 'sc, 'gc) state \<Rightarrow> 't \<Rightarrow> 'ic \<Rightarrow> 'ia option) \<Rightarrow> (('t, 'sc, 'gc) state \<Rightarrow> ('t, 'sa, 'ga) state \<Rightarrow> bool) \<Rightarrow> bool"
  where
  "rg_simulation C A sc R \<equiv>
    (\<forall> cs. cs \<in> starts C \<longrightarrow> (\<exists>as. R cs as \<and> as \<in> starts A))
    \<and>
    standard_sim_ext_step C A R
    \<and>
    standard_sim_env_step C A R
    \<and>
    standard_sim_stutter C A sc R
    \<and>
    rg_sim_int_step C A sc R"

definition weak_standard_simulation ::
  "('t, 'sc, 'gc, 'ic) RGA \<Rightarrow> ('t, 'sa, 'ga, 'ia) RGA \<Rightarrow> (('t, 'sc, 'gc) state \<Rightarrow> 't \<Rightarrow> 'ic \<Rightarrow> 'ia option) \<Rightarrow> (('t, 'sc, 'gc) state \<Rightarrow> ('t, 'sa, 'ga) state \<Rightarrow> bool) \<Rightarrow> bool"
  where
  "weak_standard_simulation C A sc R \<equiv>
    (\<forall> cs. cs \<in> starts C \<longrightarrow> (\<exists>as. R cs as \<and> as \<in> starts A))
    \<and>
    standard_sim_ext_step C A R
    \<and>
    standard_sim_stutter C A sc R
    \<and>
    standard_sim_int_step C A sc R"

definition standard_simulation_no_env ::
  "('t, 'sc, 'gc, 'ic) RGA \<Rightarrow> ('t, 'sa, 'ga, 'ia) RGA \<Rightarrow> (('t, 'sc, 'gc) state \<Rightarrow> 't \<Rightarrow> 'ic \<Rightarrow> 'ia option) \<Rightarrow> (('t, 'sc, 'gc) state \<Rightarrow> ('t, 'sa, 'ga) state \<Rightarrow> bool) \<Rightarrow> bool"
  where
  "standard_simulation_no_env C A sc R \<equiv>
    (\<forall> cs. cs \<in> starts C \<longrightarrow> (\<exists>as. R cs as \<and> as \<in> starts A))
    \<and>
    standard_sim_ext_step C A R
    \<and>
    standard_sim_stutter C A sc R
    \<and>
    standard_sim_int_step C A sc R"

lemma ssim_no_env: "standard_simulation C A sc R \<Longrightarrow> weak_standard_simulation C A sc R"
  by (simp add: standard_simulation_def weak_standard_simulation_def)

definition Rely :: "'g rel \<Rightarrow> ('t, 's, 'g, 'a) RGA \<Rightarrow> ('t, 's, 'g, 'a) RGA" (infix "\<rhd>" 85)where
  "Rely R aut = aut \<lparr> environment := R \<rparr>"

lemma ssim_stutter: "standard_simulation C A sc R \<Longrightarrow> standard_sim_stutter C A sc R"
  by (simp add: standard_simulation_def)

lemma ssim_int: "standard_simulation C A sc R \<Longrightarrow> standard_sim_int_step C A sc R"
  by (simp add: standard_simulation_def)

lemma ssim_env: "standard_simulation C A sc R \<Longrightarrow> standard_sim_env_step C A R"
  by (simp add: standard_simulation_def)

lemma ssim_ext: "standard_simulation C A sc R \<Longrightarrow> standard_sim_ext_step C A R"
  by (simp add: standard_simulation_def)

lemma rg_sim_stutter: "rg_simulation C A sc R \<Longrightarrow> standard_sim_stutter C A sc R"
  by (simp add: rg_simulation_def)

lemma rg_sim_int: "rg_simulation C A sc R \<Longrightarrow> rg_sim_int_step C A sc R"
  by (simp add: rg_simulation_def)

lemma rg_sim_env: "rg_simulation C A sc R \<Longrightarrow> standard_sim_env_step C A R"
  by (simp add: rg_simulation_def)

lemma rg_sim_ext: "rg_simulation C A sc R \<Longrightarrow> standard_sim_ext_step C A R"
  by (simp add: rg_simulation_def)

lemma wssim_stutter: "weak_standard_simulation C A sc R \<Longrightarrow> standard_sim_stutter C A sc R"
  by (simp add: weak_standard_simulation_def)

lemma wssim_int: "weak_standard_simulation C A sc R \<Longrightarrow> standard_sim_int_step C A sc R"
  by (simp add: weak_standard_simulation_def)

lemma wssim_ext: "weak_standard_simulation C A sc R \<Longrightarrow> standard_sim_ext_step C A R"
  by (simp add: weak_standard_simulation_def)

lemma standard_simulation_environment_move:
  assumes sim: "standard_simulation C A sc R"
  and reach_C: "reach C cs"
  and step: "fst cs = fst cs' \<and> (snd cs, snd cs') \<in> environment C"
  and rel: "R cs as"
  and reach_A: "reach A as"
  shows "\<exists>as'. R cs' as' \<and> reachable (ioa A) as' \<and> (\<exists>ex. move (ioa A) ex as Environment as')"
  using assms
  apply -
  apply (drule ssim_env)
  apply (simp only: standard_sim_env_step_def)
  apply (erule_tac x = cs in allE)
  apply (erule_tac x = "snd cs'" in allE)
  apply (erule_tac x = as in allE)
  apply (erule impE)
  apply blast
  apply (erule impE)
  apply blast
  apply (erule conjE)
proof -
  assume "\<exists>ag'. (snd as, ag') \<in> environment A \<and> R (fst cs, snd cs') (fst as, ag')"
  and "fst cs = fst cs'"
  and "(snd cs, snd cs') \<in> environment C"
  then obtain ag' where "(snd as, ag') \<in> environment A" and "R (fst cs, snd cs') (fst as, ag')"
    by auto
  show ?thesis
    apply (rule_tac x = "(fst as, ag')" in exI)
    apply (intro conjI)
    using \<open>R (fst cs, snd cs') (fst as, ag')\<close> local.step apply auto[1]
    apply (rule_tac s = as and a = "Environment" in reachable_n)
    using reach_A reach_def apply blast
    apply (simp add: trans_def ioa_def trans_of_def `(snd as, ag') \<in> environment A` Environment_def)
    apply (simp add: \<open>(snd as, ag') \<in> environment A\<close> case_unit_Unity)
    apply (rule_tac x = "(Environment,  (fst as, ag')) \<leadsto> nil" in exI)
    apply (simp add: move_def trans_of_def trans_def ioa_def `(snd as, ag') \<in> environment A` Environment_def)
    by (simp add: \<open>(snd as, ag') \<in> environment A\<close> case_unit_Unity)
qed

lemma standard_simulation_internal_move:
  assumes sim: "standard_simulation C A sc R"
  and reach_C: "reach C cs"
  and step: "local_pre C t (Internal i) cs \<and> cs' = local_eff C t (Internal i) cs"
  and rel: "R cs as"
  and reach_A: "reach A as"
  shows "\<exists>as'. R cs' as' \<and> reachable (ioa A) as' \<and> (\<exists>ex. move (ioa A) ex as (Inl (t, Internal Tau)) as')"
proof (cases "sc cs t i = None")
  assume none: "sc cs t i = None"
  from this and assms have stutter_rel: "R cs' as"
    by - (drule ssim_stutter, simp only: standard_sim_stutter_def)
  have stutter_move: "\<exists>ex. move (ioa A) ex as (Inl (t, Internal Tau)) as"
    apply(rule exI[where x="nil"])
    by (simp add: move_def ioa_def externals_def sig_def asig_triple_proj ioa_triple_proj)
  show ?thesis using stutter_move stutter_rel reach_A reach_def by blast
next
  assume some: "sc cs t i \<noteq> None"
  then obtain a where "sc cs t i = Some a"
    by auto
  from this and assms have step_rel: "R cs' (local_eff A t (Internal (the (sc cs t i))) as)"
    apply -
    apply (drule ssim_int)
    apply (simp only: standard_sim_int_step_def)
    by fastforce
  have atrans: "as \<midarrow>(Inl (t, Internal Tau))\<midarrow>ioa A \<rightarrow> (local_eff A t (Internal (the (sc cs t i))) as)"
    using some step sim reach_C reach_A rel
    apply(unfold trans_def ioa_def trans_of_def, simp)
    apply(rule exI[where x="the(sc cs t i)"])
    apply (auto simp only: trans_def ioa_def trans_of_def standard_simulation_def standard_sim_int_step_def)
    by (metis option.sel)
  have step_reach: "reachable (ioa A) (local_eff A t (Internal (the (sc cs t i))) as)"
    using reach_A atrans
    by (simp add: reach_def reachable.reachable_n)
  from atrans have step_move: "(\<exists>ex. move (ioa A) ex as (Inl (t, Internal Tau)) (local_eff A t (Internal (the (sc cs t i))) as))"
    apply -
    apply (rule exI[where x="[(Inl (t, Internal Tau), local_eff A t (Internal (the (sc cs t i))) as)!]"])
    by(simp add: move_def ioa_def trans_def)
  from step_rel step_reach step_move show ?thesis by blast
qed

lemma rg_simulation_environment_move:
  assumes sim: "rg_simulation C A sc R"
  and reach_C: "reach C cs"
  and step: "fst cs = fst cs' \<and> (snd cs, snd cs') \<in> environment C"
  and rel: "R cs as"
  and reach_A: "reach A as"
  shows "\<exists>as'. R cs' as' \<and> reachable (ioa A) as' \<and> (\<exists>ex. move (ioa A) ex as Environment as')"
  using assms
  apply -
  apply (drule rg_sim_env)
  apply (simp only: standard_sim_env_step_def)
  apply (erule_tac x = cs in allE)
  apply (erule_tac x = "snd cs'" in allE)
  apply (erule_tac x = as in allE)
  apply (erule impE)
  apply blast
  apply (erule impE)
  apply blast
  apply (erule conjE)
proof -
  assume "\<exists>ag'. (snd as, ag') \<in> environment A \<and> R (fst cs, snd cs') (fst as, ag')"
  and "fst cs = fst cs'"
  and "(snd cs, snd cs') \<in> environment C"
  then obtain ag' where "(snd as, ag') \<in> environment A" and "R (fst cs, snd cs') (fst as, ag')"
    by auto
  show ?thesis
    apply (rule_tac x = "(fst as, ag')" in exI)
    apply (intro conjI)
    using \<open>R (fst cs, snd cs') (fst as, ag')\<close> local.step apply auto[1]
    apply (rule_tac s = as and a = "Environment" in reachable_n)
    using reach_A reach_def apply blast
    apply (simp add: trans_def ioa_def trans_of_def `(snd as, ag') \<in> environment A` Environment_def)
    apply (simp add: \<open>(snd as, ag') \<in> environment A\<close> case_unit_Unity)
    apply (rule_tac x = "(Environment,  (fst as, ag')) \<leadsto> nil" in exI)
    apply (simp add: move_def trans_of_def trans_def ioa_def `(snd as, ag') \<in> environment A` Environment_def)
    by (simp add: \<open>(snd as, ag') \<in> environment A\<close> case_unit_Unity)
qed

lemma rg_simulation_internal_move:
  assumes sim: "rg_simulation C A sc R"
  and reach_C: "reach C cs"
  and step: "local_pre C t (Internal i) cs \<and> cs' = local_eff C t (Internal i) cs"
  and rel: "R cs as"
  and reach_A: "reach A as"
  shows "\<exists>as'. R cs' as' \<and> reachable (ioa A) as' \<and> (\<exists>ex. move (ioa A) ex as (Inl (t, Internal Tau)) as' \<or> move (ioa A) ex as Environment as')"
proof (cases "sc cs t i = None")
  assume none: "sc cs t i = None"
  from this and assms have stutter_rel: "R cs' as"
    by - (drule rg_sim_stutter, simp only: standard_sim_stutter_def)
  have stutter_move: "\<exists>ex. move (ioa A) ex as (Inl (t, Internal Tau)) as"
    apply(rule exI[where x="nil"])
    by (simp add: move_def ioa_def externals_def sig_def asig_triple_proj ioa_triple_proj)
  show ?thesis using stutter_move stutter_rel reach_A reach_def by blast
next
  assume some: "sc cs t i \<noteq> None"
  then obtain a where sc_Some: "sc cs t i = Some a"
    by auto
  from this and assms
  have "local_pre A t (Internal a) as \<and> R (local_eff C t (Internal i) cs) (local_eff A t (Internal a) as) \<or>
        (\<exists>ag. (snd as, ag) \<in> environment A \<and> R (local_eff C t (Internal i) cs) (fst as, ag))"
    by - (drule rg_sim_int, simp only: rg_sim_int_step_def)
  moreover
  {
    assume abs_pre: "local_pre A t (Internal a) as"
    and step_rel: "R (local_eff C t (Internal i) cs) (local_eff A t (Internal a) as)"
    have atrans: "as \<midarrow>(Inl (t, Internal Tau))\<midarrow>ioa A\<rightarrow> (local_eff A t (Internal a) as)"
      using some step sim reach_C reach_A rel abs_pre sc_Some
      apply(unfold trans_def ioa_def trans_of_def, simp)
      apply(rule exI[where x="the(sc cs t i)"])
      apply (auto simp only: trans_def ioa_def trans_of_def standard_simulation_def standard_sim_int_step_def)
      apply auto[1]
      by (metis option.sel)

    have step_reach: "reachable (ioa A) (local_eff A t (Internal a) as)"
      using reach_A atrans by (simp add: reach_def reachable.reachable_n)

    from atrans have step_move: "(\<exists>ex. move (ioa A) ex as (Inl (t, Internal Tau)) (local_eff A t (Internal a) as))"
      by (simp add: transition_is_ex)

    from step_move step_reach step and step_rel
    have ?thesis by blast
  }
  moreover
  {
    assume "\<exists>ag. (snd as, ag) \<in> environment A \<and> R (local_eff C t (Internal i) cs) (fst as, ag)"
    then obtain ag where abs_env: "(snd as, ag) \<in> environment A"
    and step_rel: "R (local_eff C t (Internal i) cs) (fst as, ag)"
      by blast

    have atrans: "as \<midarrow>Environment\<midarrow>ioa A\<rightarrow> (fst as, ag)"
      using some step sim reach_C reach_A rel abs_env sc_Some
      by(unfold trans_def ioa_def trans_of_def Environment_def case_unit_Unity, simp)

    hence step_reach: "reachable (ioa A) (fst as, ag)"
      by (meson reach_A reach_def reachable.reachable_n)

    from atrans have step_move: "(\<exists>ex. move (ioa A) ex as Environment (fst as, ag))"
      by (simp add: transition_is_ex)

    
    from step_move step_reach step and step_rel
    have ?thesis by blast
  }
  ultimately show ?thesis by blast
qed

lemma ioa_event_cases:
  assumes "\<And>t. ev = Inl (t, Internal Tau) \<Longrightarrow> P ev"
  and "\<And>t act. ev = Inl (t, External act) \<Longrightarrow> P ev"
  and "ev = Inr () \<Longrightarrow> P ev"
  shows "P ev"
  using assms
  apply (cases ev)
  apply simp_all
  by (metis event.exhaust hidden.exhaust prod.collapse)

lemma standard_simulation_move:
  assumes sim: "standard_simulation C A sc R"
  and reach_C: "reach C cs"
  and step: "(cs, p, cs') \<in> trans C"
  and rel: "R cs as"
  and reach_A: "reach A as"
  shows "\<exists>as' ex. (cs', as') \<in> {(x, y). R x y \<and> reachable (ioa A) y} \<and> move (ioa A) ex as p as'"
proof (rule_tac ev = p in ioa_event_cases)
  fix t
  assume [simp]: "p = Inl (t, Internal Tau)"
  from step have int_step: "\<exists>i. local_pre C t (Internal i) cs \<and> cs' = local_eff C t (Internal i) cs"
    by (auto simp add: trans_def)
  thus ?thesis
    apply -
    apply (drule exE[where Q="\<exists>as'. R cs' as' \<and> reachable (ioa A) as' \<and> (\<exists>ex. move (ioa A) ex as p as')"], simp_all)
    by (metis prod.collapse reach_A reach_C rel sim standard_simulation_internal_move)
next
  fix t a
  assume [simp]: "p = Inl (t, External a)"
  show ?thesis
  proof (rule exI[where x= "local_eff A t (External a) as"])
    have atrans: "as \<midarrow>p\<midarrow>(ioa A) \<rightarrow> local_eff A t (External a) as"
      using step
      by (auto simp add: ioa_def trans_of_def trans_def eff_def)
         (metis reach_A reach_C rel sim standard_sim_ext_step_def standard_simulation_def)
    have reach_A_post: "reachable (ioa A) (local_eff A t (External a) as)"
      using step reach_A atrans
      by (simp add: reach_def reachable.reachable_n)
    have sim_post: "(cs', local_eff A t (External a) as) \<in> {(x, y). R x y}"
      using sim standard_simulation_def standard_sim_ext_step_def step rel reach_A reach_C
      by (auto simp add: trans_def) (metis reach_C standard_sim_ext_step_def standard_simulation_def)
    from atrans reach_A_post sim_post
    show "\<exists>ex. (cs', local_eff A t (External a) as) \<in> {(x, y). R x y \<and> reachable (ioa A) y} \<and>
           move (ioa A) ex as p (local_eff A t (External a) as)"
       by (simp add:  transition_is_ex)
  qed
next
  assume [simp]: "p = Inr ()"
  from step have env_step: "fst cs = fst cs' \<and> (snd cs, snd cs') \<in> environment C"
    by (simp add: trans_def case_unit_Unity)
  from standard_simulation_environment_move[OF sim reach_C env_step rel reach_A]
  show ?thesis
    by (simp add: Environment_def)
qed

lemma standard_simulation_trace_inclusion:
  assumes sim: "standard_simulation C A sc R"
  shows "traces (ioa C) \<subseteq> traces (ioa A)"
proof (rule trace_inclusion_for_simulations[where R="{(cs, as) . R cs as \<and> reachable (ioa A) as}"])
  show "Automata.ext (ioa C) = Automata.ext (ioa A)"
    by (auto simp add: externals_def asig_outputs_def asig_inputs_def asig_of_def fst_def snd_def ioa_def sig_def)
  next
  show "is_simulation {(cs, as) . R cs as \<and> reachable (ioa A) as} (ioa C) (ioa A)"
  proof -
    {
      from sim
      and reachable_0[where C="ioa A"]
      have "\<forall>s\<in>(fst \<circ> snd) (ioa C). {(x, y). R x y \<and> reachable (ioa A) y} `` {s} \<inter> (fst \<circ> snd) (ioa A) \<noteq> {}"
        by (force simp add: standard_simulation_def ioa_def starts_of_def)
    }
    moreover
    {
      from sim
      and standard_simulation_move[where C=C and A=A and R=R and sc=sc]
      have "\<forall> s s' t a.
        reachable (ioa C) s \<and> s \<midarrow>a\<midarrow>(ioa C) \<rightarrow> t \<and> (s, s') \<in> {(x, y). R x y \<and> reachable (ioa A) y} \<longrightarrow> (\<exists>t' ex. (t, t') \<in> {(x, y). R x y \<and> reachable (ioa A) y} \<and> move (ioa A) ex s' a t')"
        by (unfold ioa_def trans_of_def reach_def, clarify, simp)
    }
    ultimately show ?thesis
      by (simp add: is_simulation_def starts_of_def)
    qed
qed


definition par_locals :: "('ta \<Rightarrow> 'sa) \<Rightarrow> ('tb \<Rightarrow> 'sb) \<Rightarrow> 'ta + 'tb \<Rightarrow> 'sa + 'sb" where
  "par_locals L1 L2 \<equiv> \<lambda>t.
     case t of
       Inl t' \<Rightarrow> Inl (L1 t')
     | Inr t' \<Rightarrow> Inr (L2 t')"

fun par_pre ::
  "('ia event \<Rightarrow> 'sa \<times> 'g \<Rightarrow> bool) \<Rightarrow> ('ib event \<Rightarrow> 'sb \<times> 'g \<Rightarrow> bool) \<Rightarrow> ('ia + 'ib) event \<Rightarrow> ('sa + 'sb) \<times> 'g \<Rightarrow> bool"
  where
  "par_pre Pa Pb (External a) (Inl l, g) = Pa (External a) (l, g)"
| "par_pre Pa Pb (External a) (Inr l, g) = Pb (External a) (l, g)"
| "par_pre Pa Pb (Internal (Inl i)) (Inl l, g) = Pa (Internal i) (l, g)"
| "par_pre Pa Pb (Internal (Inr i)) (Inl l, g) = False"
| "par_pre Pa Pb (Internal (Inl i)) (Inr l, g) = False"
| "par_pre Pa Pb (Internal (Inr i)) (Inr l, g) = Pb (Internal i) (l, g)"

fun par_eff ::
    "('ia event \<Rightarrow> 'sa \<times> 'g \<Rightarrow> 'sa \<times> 'g) \<Rightarrow> ('ib event \<Rightarrow> 'sb \<times> 'g \<Rightarrow> 'sb \<times> 'g) \<Rightarrow> ('ia + 'ib) event \<Rightarrow> ('sa + 'sb) \<times> 'g \<Rightarrow> ('sa + 'sb) \<times> 'g"
  where
  "par_eff fa fb (External a) (Inl l, g) = apfst Inl (fa (External a) (l, g))"
| "par_eff fa fb (External a) (Inr l, g) = apfst Inr (fb (External a) (l, g))"
| "par_eff fa fb (Internal (Inl i)) (Inl l, g) = apfst Inl (fa (Internal i) (l, g))"
| "par_eff fa fb (Internal (Inr i)) (Inl l, g) = (Inl l, g)"
| "par_eff fa fb (Internal (Inl i)) (Inr l, g) = (Inr l, g)"
| "par_eff fa fb (Internal (Inr i)) (Inr l, g) = apfst Inr (fb (Internal i) (l, g))"

no_notation Automata.par (infixr "\<parallel>" 10)

definition parallel :: "('ta, 'sa, 'g, 'ia) RGA \<Rightarrow> ('tb, 'sb, 'g, 'ib) RGA \<Rightarrow> ('ta + 'tb, 'sa + 'sb, 'g, 'ia + 'ib) RGA" (infixl "\<parallel>" 66) where
  "parallel Q W \<equiv> \<lparr> RGA.local_starts = {par_locals L1 L2 |L1 L2. L1 \<in> local_starts Q \<and> L2 \<in> local_starts W},
                    RGA.global_start = global_start Q,
                    RGA.pre = par_pre (pre Q) (pre W),
                    RGA.eff = par_eff (eff Q) (eff W),
                    RGA.environment = Id \<rparr>"

definition local_invariant :: "('t, 's, 'g, 'i) RGA \<Rightarrow> ('t \<Rightarrow> ('t, 's, 'g) state \<Rightarrow> bool) \<Rightarrow> bool"
  where
  "local_invariant aut I \<equiv> \<forall> s. reach aut s \<longrightarrow> (\<forall> t. I t s)"

definition noninterference_invariant :: "('t, 's, 'g, 'i) RGA \<Rightarrow> ('t \<Rightarrow> 't \<Rightarrow> ('t, 's, 'g) state \<Rightarrow> bool) \<Rightarrow> bool"
  where
  "noninterference_invariant aut I \<equiv> \<forall> s. reach aut s \<longrightarrow> (\<forall> t t'. t \<noteq> t' \<longrightarrow> I t t' s)"

definition global_invariant :: "('t, 's, 'g, 'i) RGA \<Rightarrow> ('g \<Rightarrow> bool) \<Rightarrow> bool" where
  "global_invariant aut I \<equiv> (\<forall>s. reach aut s \<longrightarrow> I (global s))"

lemma invariant_intro:
  "\<lbrakk>\<And> s. s \<in> starts aut \<Longrightarrow> P s;
    \<And> s. reach aut s \<Longrightarrow>  preserved aut P s\<rbrakk>
    \<Longrightarrow>
    invariant (ioa aut) P"
proof (rule invariantI [of "ioa aut" P])
  fix s
  assume start: "\<And>s. s \<in> starts aut \<Longrightarrow> P s"
  and ioa_start: "s \<in> starts_of (ioa aut)"
  from start ioa_start show "P s" by (auto simp add: starts_of_def ioa_def)
next
  fix s a t
  assume pres: "\<And> s. reach aut s \<Longrightarrow> preserved aut P s" and
         hyp: "P s" and
         ioa_reach: "reachable (ioa aut) s"
  from this have "reach aut s" and "preserved aut P s"
    by (simp add: reach_def) (simp add: ioa_reach pres reach_def)
  from this hyp
  show "s \<midarrow>a\<midarrow>ioa aut \<rightarrow> t \<longrightarrow> P t"
    apply -
    apply (rule_tac ev = a in ioa_event_cases)
    apply (simp_all add: trans_of_def ioa_def trans_def)
    defer
    apply (simp_all add: preserved_def local_eff_def local_pre_def local_preserved_def case_unit_Unity)
    apply (simp add: preserved_def global_preserved_def)
    apply (metis global_def prod.collapse)
    by metis
qed

(*

value local_preserved

lemma local_invariant_intro:
  "\<lbrakk>(\<And> s t. s \<in> starts aut \<Longrightarrow> P t s);
    (\<And> s. reach aut s \<Longrightarrow> local_preserved aut P s)
    \<rbrakk>
  \<Longrightarrow>
  local_invariant aut P"
apply(insert invariant_intro[where aut=aut and P="\<lambda>s. \<forall>t. P t s"])
apply(unfold local_preserved_def invariant_def local_invariant_def reach_def)
by blast

lemma noninterference_invariant_intro:
  "\<lbrakk>(\<And> s t t'. start aut s \<and> t \<noteq> t' \<Longrightarrow> P t t' s);
    (\<And> s. reach aut s \<Longrightarrow> noninterference_preserved aut P s)
    \<rbrakk>
  \<Longrightarrow>
  noninterference_invariant aut P"
apply(insert invariant_intro[where aut=aut and P="\<lambda> s. \<forall> t1 t2. t1\<noteq>t2 \<longrightarrow> P t1 t2 s"])
apply(unfold noninterference_preserved_def invariant_def noninterference_invariant_def reach_def)
apply(blast)
done
*)

lemma invariant_elim:
  "\<lbrakk> invariant (ioa aut) P; reach aut s \<rbrakk> \<Longrightarrow> P s"
by (simp add: invariantE reach_def)

(*
lemma local_invariant_elim:
  "\<lbrakk> local_invariant aut P; reach aut s \<rbrakk> \<Longrightarrow> P t s"
by (simp add: local_invariant_def invariantE reach_def)

lemma noninterference_invariant_elim:
  "\<lbrakk> noninterference_invariant aut P; t \<noteq> t'; reach aut s \<rbrakk> \<Longrightarrow> P t t' s"
by (simp add: noninterference_invariant_def invariantE reach_def)

*)

lemma invariant_strengthen:
  "\<lbrakk>invariant (ioa aut) P;
    (\<forall> s . P s \<longrightarrow> Q s)
    \<rbrakk>
    \<Longrightarrow>
    invariant (ioa aut) Q"
by (simp add: invariant_def)

lemma local_invariant_strengthen:
  "\<lbrakk>local_invariant aut P;
    (\<forall> s t . P s t \<longrightarrow> Q s t)
    \<rbrakk>
    \<Longrightarrow>
    local_invariant aut Q"
  by (simp add: local_invariant_def)

definition RelyUnion :: "'g rel \<Rightarrow> ('t, 's, 'g, 'i) RGA \<Rightarrow> ('t, 's, 'g, 'i) RGA" where
  "RelyUnion R aut = aut \<lparr> environment := environment aut \<union> rtrancl R \<rparr>"

definition sprojl :: "('t1 + 't2, 's1 + 's2, 'g) state \<Rightarrow> 't1 \<Rightarrow> 's1" where
  "sprojl s \<equiv> \<lambda>t. projl (fst s (Inl t))"

definition sprojr :: "('t1 + 't2, 's1 + 's2, 'g) state \<Rightarrow> 't2 \<Rightarrow> 's2" where
  "sprojr s \<equiv> \<lambda>t. projr (fst s (Inr t))"

definition lsprojl :: "('t1 + 't2 \<Rightarrow> 's1 + 's2) \<Rightarrow> 't1 \<Rightarrow> 's1" where
  "lsprojl s \<equiv> \<lambda>t. projl (s (Inl t))"

definition lsprojr :: "('t1 + 't2 \<Rightarrow> 's1 + 's2) \<Rightarrow> 't2 \<Rightarrow> 's2" where
  "lsprojr s \<equiv> \<lambda>t. projr (s (Inr t))"

definition state_projl :: "('t1 + 't2, 's1 + 's2, 'g) state \<Rightarrow> ('t1, 's1, 'g) state" where
  "state_projl s \<equiv> (sprojl s, snd s)"

definition state_projr :: "('t1 + 't2, 's1 + 's2, 'g) state \<Rightarrow> ('t2, 's2, 'g) state" where
  "state_projr s \<equiv> (sprojr s, snd s)"

definition par_sim_rel ::
  "(('t, 'sc1, 'gc) state \<Rightarrow> ('t, 'sa, 'ga) state \<Rightarrow> bool) \<Rightarrow> (('t, 'sc2, 'gc) state \<Rightarrow> ('t, 'sa, 'ga) state \<Rightarrow> bool) \<Rightarrow>
  ('t + 't, 'sc1 + 'sc2, 'gc) state \<Rightarrow> ('t + 't, 'sa + 'sa, 'ga) state \<Rightarrow> bool"
  where
  "par_sim_rel Rl Rr C A \<equiv> Rl (state_projl C) (state_projl A) \<and> Rr (state_projr C) (state_projr A)"

definition compatible :: "('ta, 'sa, 'g, 'ia) RGA \<Rightarrow> ('tb, 'sb, 'g, 'ib) RGA \<Rightarrow> bool" where
  "compatible A B \<equiv> global_start A = global_start B"

definition guarantee where
  "guarantee aut I' \<equiv> (\<forall>t a s. reach aut s \<and> local_pre aut t a s \<longrightarrow> (snd s, snd (local_eff aut t a s)) \<in> I')"

lemma use_guarantee: "guarantee aut I \<Longrightarrow> reach aut s \<Longrightarrow> local_pre aut t a s \<Longrightarrow> g = snd s \<Longrightarrow> g' = snd (local_eff aut t a s) \<Longrightarrow> (g, g') \<in> I"
  by (meson guarantee_def)

lemma lconc_rconc: "invariant (ioa (aut1 \<parallel> aut2)) (\<lambda>s. (\<forall>t. fst s (Inl t) = Inl (projl (fst s (Inl t)))) \<and> (\<forall>t. fst s (Inr t) = Inr (projr (fst s (Inr t)))))"
proof (rule invariantI)
  fix s
  assume "s \<in> starts_of (ioa (aut1 \<parallel> aut2))"
  thus "(\<forall>t. fst s (Inl t) = Inl (projl (fst s (Inl t)))) \<and> (\<forall>t. fst s (Inr t) = Inr (projr (fst s (Inr t))))"
    apply (simp add: starts_of_def ioa_def parallel_def starts_def image_def par_locals_def)
    apply (erule exE)
    apply (erule conjE)
    apply (erule exE)+
    by simp
next
  fix s s' a
  assume "reachable (ioa (aut1 \<parallel> aut2)) s"
  and lr: "(\<forall>t. fst s (Inl t) = Inl (projl (fst s (Inl t)))) \<and> (\<forall>t. fst s (Inr t) = Inr (projr (fst s (Inr t))))"

  hence lconc: "\<And>t. fst s (Inl t) = Inl (projl (fst s (Inl t)))"
  and rconc: "\<And>t. fst s (Inr t) = Inr (projr (fst s (Inr t)))"
    by - simp_all

  from lr show "s \<midarrow>a\<midarrow>ioa (aut1 \<parallel> aut2)\<rightarrow> s' \<longrightarrow> (\<forall>t. fst s' (Inl t) = Inl (projl (fst s' (Inl t)))) \<and> (\<forall>t. fst s' (Inr t) = Inr (projr (fst s' (Inr t))))"
    apply (rule_tac ev = a in ioa_event_cases)
    apply (simp_all add: ioa_def parallel_def starts_def image_def par_locals_def trans_of_def trans_def local_pre_def local_eff_def case_unit_Unity)
    apply clarify
    apply (simp add: localize_def local_def view_def)
    apply (subgoal_tac "(\<exists>i'. i = Inr i') \<or> (\<exists>i'. i = Inl i')")
    apply (erule disjE)
    apply (erule exE)
    apply simp
    apply safe
    apply (subst lconc, simp (no_asm))
    apply (subst lconc, simp (no_asm))
    apply (metis (no_types, lifting) par_pre.simps(4))
    apply (subst rconc, simp (no_asm))
    apply (metis (no_types, lifting) fst_apfst par_eff.simps(6) sum.sel(2))
    apply (metis (no_types, hide_lams) Inl_Inr_False fst_apfst par_eff.simps(3) sum.exhaust_sel)
    apply (metis (no_types, lifting) par_pre.simps(5))
    apply (metis sum.collapse(1) sum.collapse(2))
    apply (simp add: localize_def view_def local_def)
    apply safe
    apply (metis (no_types, hide_lams) Inl_Inr_False fst_apfst par_eff.simps(1) sum.collapse(1) sum.collapse(2))
    apply (simp add: localize_def view_def local_def)
    apply safe
    apply (metis (no_types, hide_lams) fst_apfst par_eff.simps(2) sum.collapse(2) sum.disc(2))
    by simp_all
qed

lemma par_reach:
  assumes "compatible C\<^sub>1 C\<^sub>2"
  and I1_refl: "refl I\<^sub>1"
  and I2_refl: "refl I\<^sub>2"
  and guar1: "guarantee (I\<^sub>2 \<rhd> C\<^sub>1) I\<^sub>1"
  and guar2: "guarantee (I\<^sub>1 \<rhd> C\<^sub>2) I\<^sub>2"
  shows "reach (C\<^sub>1 \<parallel> C\<^sub>2) (cs, cg) \<Longrightarrow> reach (I\<^sub>1 \<rhd> C\<^sub>2) (state_projr (cs, cg)) \<and> reach (I\<^sub>2 \<rhd> C\<^sub>1) (state_projl (cs, cg))"
proof (simp add: reach_def, induct rule: reachable.induct; intro conjI)
  fix s
  assume "s \<in> starts_of (ioa (C\<^sub>1 \<parallel> C\<^sub>2))"

  from this and assms show "reachable (ioa (I\<^sub>1 \<rhd> C\<^sub>2)) (state_projr s)"
    apply -
    apply (rule reachable_0)
    apply (simp add: parallel_def ioa_def starts_of_def starts_def image_def Rely_def state_projr_def)
    apply (erule exE)
    apply (erule conjE)
    apply (erule exE)+
    apply (erule conjE)+
    by (simp add: par_locals_def compatible_def sprojr_def)
next
  fix s
  assume "s \<in> starts_of (ioa (C\<^sub>1 \<parallel> C\<^sub>2))"
  from this and assms show "reachable (ioa (I\<^sub>2 \<rhd> C\<^sub>1)) (state_projl s)"
    apply -
    apply (rule reachable_0)
    apply (simp add: parallel_def ioa_def starts_of_def starts_def image_def Rely_def state_projl_def)
    apply (erule exE)
    apply (erule conjE)
    apply (erule exE)+
    apply (erule conjE)+
    by (simp add: par_locals_def compatible_def sprojl_def) 
next
  fix s a s'
  assume reach_s: "reachable (ioa (C\<^sub>1 \<parallel> C\<^sub>2)) s"
  and reach_proj: "reachable (ioa (I\<^sub>1 \<rhd> C\<^sub>2)) (state_projr s) \<and> reachable (ioa (I\<^sub>2 \<rhd> C\<^sub>1)) (state_projl s)"
  and trans: "s \<midarrow>a\<midarrow>ioa (C\<^sub>1 \<parallel> C\<^sub>2)\<rightarrow> s'"

  have reach_proj1: "reachable (ioa (I\<^sub>1 \<rhd> C\<^sub>2)) (state_projr s)"
    using reach_proj by blast
  have reach_proj2: "reachable (ioa (I\<^sub>2 \<rhd> C\<^sub>1)) (state_projl s)"
    using reach_proj by blast

  have lconc: "\<And>t. fst s (Inl t) = Inl (projl (fst s (Inl t)))"
    using invariantE[OF lconc_rconc reach_s] by blast

  have rconc: "\<And>t. fst s (Inr t) = Inr (projr (fst s (Inr t)))"
    using invariantE[OF lconc_rconc reach_s] by blast

  from trans
  show "reachable (ioa (I\<^sub>1 \<rhd> C\<^sub>2)) (state_projr s')"
    apply -
    apply (rule reachable_n[where s = "state_projr s"], simp add: reach_proj)
  proof (rule_tac ev = a in ioa_event_cases)
    fix t
    assume [simp]: "a = Inl (t, Internal Tau)"

    {
      fix t'
      assume t_def [simp]: "t = Inr t'"

      from trans
      have "state_projr s \<midarrow>(Inl (t', Internal Tau))\<midarrow>ioa (I\<^sub>1 \<rhd> C\<^sub>2)\<rightarrow> state_projr s'"
        apply (simp add: trans_of_def ioa_def parallel_def Rely_def RGA.trans_def local_pre_def local_eff_def)
        apply (simp add: view_def local_def localize_def state_projr_def sprojr_def)
        apply (erule exE)
        apply (rule_tac x = "projr i" in exI)
        apply (intro conjI)
        apply (erule rev_mp)
        apply (subst rconc)
        apply (metis (no_types, lifting) par_pre.simps(5) par_pre.simps(6) sum.exhaust_sel)
        apply (rule ext)
        apply (simp add: view_def local_def)
        apply (subst rconc)
        apply (smt fst_apfst par_eff.simps(6) par_pre.simps(5) rconc sum.exhaust_sel sum.sel(2))
        by (smt par_eff.simps(6) par_pre.simps(5) rconc snd_apfst snd_conv sum.exhaust_sel)
    }
    moreover
    {
      fix t'
      assume t_def [simp]: "t = Inl t'"

      from trans
      have "state_projr s \<midarrow>(Inr ())\<midarrow>ioa (I\<^sub>1 \<rhd> C\<^sub>2)\<rightarrow> state_projr s'"
        apply (simp add: trans_of_def ioa_def parallel_def trans_def case_unit_Unity)
        apply (erule exE)
        apply (simp add: local_pre_def local_eff_def state_projr_def view_def local_def)
        apply (erule rev_mp)
        apply (subst lconc)
        apply (subgoal_tac "(\<exists>i'. i = Inl i') \<or> (\<exists>i'. i = Inr i')")
        apply (erule disjE)
        apply (erule exE)
        apply (simp add: Rely_def)
        apply clarify
        apply (intro conjI)
        apply (simp add: localize_def sprojr_def local_def)
        apply (rule_tac s = "state_projl s" and t = "t'" and a = "Internal i'" in use_guarantee[OF guar1])
        apply (simp add: reach_def reach_proj)
        apply (simp add: local_pre_def view_def Rely_def state_projl_def local_def sprojl_def)
        apply (simp add: state_projl_def)
        apply (simp add: local_eff_def view_def Rely_def state_projl_def local_def sprojl_def)
        apply (simp add: localize_def view_def local_def)
        apply (subst lconc)
        apply simp
        apply (erule exE)
        apply simp
        using sum.exhaust_sel by blast
    }
    ultimately show "state_projr s \<midarrow>(case a of Inl (Inr t', e) \<Rightarrow> Inl (t', e) | Inl (Inl t', e) \<Rightarrow> Inr () | Inr () \<Rightarrow> Inr ())\<midarrow>ioa (I\<^sub>1 \<rhd> C\<^sub>2)\<rightarrow> state_projr s'"
      by simp (cases t; simp)
  next
    fix t ac
    assume [simp]: "a = Inl (t, External ac)"

    {
      fix t'
      assume t_def [simp]: "t = Inr t'"

      from trans
      have "state_projr s \<midarrow>(Inl (t', External ac))\<midarrow>ioa (I\<^sub>1 \<rhd> C\<^sub>2)\<rightarrow> state_projr s'"
        apply (simp add: trans_of_def ioa_def parallel_def Rely_def RGA.trans_def local_pre_def local_eff_def)
        apply (simp add: view_def local_def localize_def state_projr_def sprojr_def)
        apply (intro conjI)
        apply (erule rev_mp)
        apply (subst rconc)
        apply simp
        apply (rule ext)
        apply (simp add: view_def local_def)
        apply (subst rconc)
        apply simp
        by (metis (no_types, lifting) par_eff.simps(2) rconc snd_apfst)
    }
    moreover
    {
      fix t'
      assume t_def [simp]: "t = Inl t'"

      from trans
      have "state_projr s \<midarrow>(Inr ())\<midarrow>ioa (I\<^sub>1 \<rhd> C\<^sub>2)\<rightarrow> state_projr s'"
        apply (simp add: trans_of_def ioa_def parallel_def trans_def case_unit_Unity)
        apply (simp add: local_pre_def local_eff_def state_projr_def view_def local_def)
        apply (erule rev_mp)
        apply (subst lconc)
        apply clarify
        apply (intro conjI)
        apply (simp add: localize_def sprojr_def local_def)
        apply (simp add: Rely_def)
        apply (rule_tac s = "state_projl s" and t = "t'" and a = "External ac" in use_guarantee[OF guar1])
        apply (simp add: reach_def reach_proj)
        apply (simp add: local_pre_def view_def Rely_def state_projl_def local_def sprojl_def)
        apply (simp add: state_projl_def)
        apply (simp add: local_eff_def view_def Rely_def state_projl_def local_def sprojl_def)
        apply (simp add: localize_def view_def local_def)
        apply (subst lconc)
        by simp
    }
    ultimately show "state_projr s \<midarrow>(case a of Inl (Inr t', e) \<Rightarrow> Inl (t', e) | Inl (Inl t', e) \<Rightarrow> Inr () | Inr () \<Rightarrow> Inr ())\<midarrow>ioa (I\<^sub>1 \<rhd> C\<^sub>2)\<rightarrow> state_projr s'"
      by simp (cases t; simp)
  next
    assume [simp]: "a = Inr ()"
    from trans
    show "state_projr s \<midarrow>(case a of Inl (Inr t', e) \<Rightarrow> Inl (t', e) | Inl (Inl t', e) \<Rightarrow> Inr () | Inr () \<Rightarrow> Inr ())\<midarrow>ioa (I\<^sub>1 \<rhd> C\<^sub>2)\<rightarrow> state_projr s'"
      by (simp add: case_unit_Unity trans_of_def ioa_def trans_def Rely_def parallel_def state_projr_def sprojr_def) (meson I1_refl UNIV_I refl_onD)
  qed
next
  fix s a s'
  assume reach_s: "reachable (ioa (C\<^sub>1 \<parallel> C\<^sub>2)) s"
  and reach_proj: "reachable (ioa (I\<^sub>1 \<rhd> C\<^sub>2)) (state_projr s) \<and> reachable (ioa (I\<^sub>2 \<rhd> C\<^sub>1)) (state_projl s)"
  and trans: "s \<midarrow>a\<midarrow>ioa (C\<^sub>1 \<parallel> C\<^sub>2)\<rightarrow> s'"

  have reach_proj1: "reachable (ioa (I\<^sub>1 \<rhd> C\<^sub>2)) (state_projr s)"
    using reach_proj by blast
  have reach_proj2: "reachable (ioa (I\<^sub>2 \<rhd> C\<^sub>1)) (state_projl s)"
    using reach_proj by blast

  have lconc: "\<And>t. fst s (Inl t) = Inl (projl (fst s (Inl t)))"
    using invariantE[OF lconc_rconc reach_s] by blast

  have rconc: "\<And>t. fst s (Inr t) = Inr (projr (fst s (Inr t)))"
    using invariantE[OF lconc_rconc reach_s] by blast

  from trans
  show "reachable (ioa (I\<^sub>2 \<rhd> C\<^sub>1)) (state_projl s')"
    apply -
    apply (rule reachable_n[where s = "state_projl s"], simp add: reach_proj)
  proof (rule_tac ev = a in ioa_event_cases)
    fix t
    assume [simp]: "a = Inl (t, Internal Tau)"

    {
      fix t'
      assume t_def [simp]: "t = Inl t'"

      from trans
      have "state_projl s \<midarrow>(Inl (t', Internal Tau))\<midarrow>ioa (I\<^sub>2 \<rhd> C\<^sub>1)\<rightarrow> state_projl s'"
        apply (simp add: trans_of_def ioa_def parallel_def Rely_def RGA.trans_def local_pre_def local_eff_def)
        apply (simp add: view_def local_def localize_def state_projl_def sprojl_def)
        apply (erule exE)
        apply (rule_tac x = "projl i" in exI)
        apply (intro conjI)
        apply (erule rev_mp)
        apply (subst lconc)
        apply (metis (no_types, lifting) par_pre.simps(3) par_pre.simps(4) sum.exhaust_sel)
        apply (rule ext)
        apply (simp add: view_def local_def)
        apply (subst lconc)
        apply (metis (no_types, lifting) Inl_inject fst_apfst lconc par_eff.simps(3) par_pre.simps(4) sum.collapse(1) sum.disc(1) sum.exhaust_sel)
        by (metis (no_types, lifting) lconc par_eff.simps(3) par_pre.simps(4) sndI snd_apfst sum.exhaust_sel)
    }
    moreover
    {
      fix t'
      assume t_def [simp]: "t = Inr t'"

      from trans
      have "state_projl s \<midarrow>(Inr ())\<midarrow>ioa (I\<^sub>2 \<rhd> C\<^sub>1)\<rightarrow> state_projl s'"
        apply (simp add: trans_of_def ioa_def parallel_def trans_def case_unit_Unity)
        apply (erule exE)
        apply (simp add: local_pre_def local_eff_def state_projl_def view_def local_def)
        apply (erule rev_mp)
        apply (subst rconc)
        apply (subgoal_tac "(\<exists>i'. i = Inr i') \<or> (\<exists>i'. i = Inl i')")
        apply (erule disjE)
        apply (erule exE)
        apply (simp add: Rely_def)
        apply clarify
        apply (intro conjI)
        apply (simp add: localize_def sprojl_def local_def)
        apply (rule_tac s = "state_projr s" and t = "t'" and a = "Internal i'" in use_guarantee[OF guar2])
        apply (simp add: reach_def reach_proj)
        apply (simp add: local_pre_def view_def Rely_def state_projr_def local_def sprojr_def)
        apply (simp add: state_projr_def)
        apply (simp add: local_eff_def view_def Rely_def state_projr_def local_def sprojr_def)
        apply (simp add: localize_def view_def local_def)
        apply (subst rconc)
        apply simp
        apply (erule exE)
        apply simp
        using sum.exhaust_sel by blast
    }
    ultimately show "state_projl s \<midarrow>(case a of Inl (Inl t', e) \<Rightarrow> Inl (t', e) | Inl (Inr t', e) \<Rightarrow> Inr () | Inr () \<Rightarrow> Inr ())\<midarrow>ioa (I\<^sub>2 \<rhd> C\<^sub>1)\<rightarrow> state_projl s'"
      by simp (cases t; simp)
  next
    fix t ac
    assume [simp]: "a = Inl (t, External ac)"

    {
      fix t'
      assume t_def [simp]: "t = Inl t'"

      from trans
      have "state_projl s \<midarrow>(Inl (t', External ac))\<midarrow>ioa (I\<^sub>2 \<rhd> C\<^sub>1)\<rightarrow> state_projl s'"
        apply (simp add: trans_of_def ioa_def parallel_def Rely_def RGA.trans_def local_pre_def local_eff_def)
        apply (simp add: view_def local_def localize_def state_projl_def sprojl_def)
        apply (intro conjI)
        apply (erule rev_mp)
        apply (subst lconc)
        apply simp
        apply (rule ext)
        apply (simp add: view_def local_def)
        apply (subst lconc)
        apply simp
        by (metis (no_types, lifting) lconc par_eff.simps(1) snd_apfst)
    }
    moreover
    {
      fix t'
      assume t_def [simp]: "t = Inr t'"

      from trans
      have "state_projl s \<midarrow>(Inr ())\<midarrow>ioa (I\<^sub>2 \<rhd> C\<^sub>1)\<rightarrow> state_projl s'"
        apply (simp add: trans_of_def ioa_def parallel_def trans_def case_unit_Unity)
        apply (simp add: local_pre_def local_eff_def state_projl_def view_def local_def)
        apply (erule rev_mp)
        apply (subst rconc)
        apply clarify
        apply (intro conjI)
        apply (simp add: localize_def sprojl_def local_def)
        apply (simp add: Rely_def)
        apply (rule_tac s = "state_projr s" and t = "t'" and a = "External ac" in use_guarantee[OF guar2])
        apply (simp add: reach_def reach_proj)
        apply (simp add: local_pre_def view_def Rely_def state_projr_def local_def sprojr_def)
        apply (simp add: state_projr_def)
        apply (simp add: local_eff_def view_def Rely_def state_projr_def local_def sprojr_def)
        apply (simp add: localize_def view_def local_def)
        apply (subst rconc)
        by simp
    }
    ultimately show "state_projl s \<midarrow>(case a of Inl (Inl t', e) \<Rightarrow> Inl (t', e) | Inl (Inr t', e) \<Rightarrow> Inr () | Inr () \<Rightarrow> Inr ())\<midarrow>ioa (I\<^sub>2 \<rhd> C\<^sub>1)\<rightarrow> state_projl s'"
      by simp (cases t; simp)
  next
    assume [simp]: "a = Inr ()"
    from trans
    show "state_projl s \<midarrow>(case a of Inl (Inl t', e) \<Rightarrow> Inl (t', e) | Inl (Inr t', e) \<Rightarrow> Inr () | Inr () \<Rightarrow> Inr ())\<midarrow>ioa (I\<^sub>2 \<rhd> C\<^sub>1)\<rightarrow> state_projl s'"
      by (simp add: case_unit_Unity trans_of_def ioa_def trans_def Rely_def parallel_def state_projl_def sprojl_def) (meson I2_refl UNIV_I refl_onD)
  qed
qed

lemma projl_env_eff:
  assumes rconc: "\<And>t. cs (Inr t) = Inr (projr (cs (Inr t)))"
  shows "state_projl (local_eff (C\<^sub>1 \<parallel> C\<^sub>2) (Inr t) (External e) (cs, cg)) = (let cg' = snd (local_eff C\<^sub>2 t (External e) (lsprojr cs, cg)) in (lsprojl cs, cg'))"
  by (simp add: parallel_def local_eff_def localize_def state_projl_def sprojl_def lsprojl_def local_def view_def) (subst rconc, simp add: lsprojr_def)

lemma projr_env_eff:
  assumes lconc: "\<And>t. cs (Inl t) = Inl (projl (cs (Inl t)))"
  shows "state_projr (local_eff (C\<^sub>1 \<parallel> C\<^sub>2) (Inl t) (External e) (cs, cg)) = (let cg' = snd (local_eff C\<^sub>1 t (External e) (lsprojl cs, cg)) in (lsprojr cs, cg'))"
  by (simp add: parallel_def local_eff_def localize_def state_projr_def sprojr_def lsprojl_def local_def view_def) (subst lconc, simp add: lsprojr_def)

lemma projr_local_eff:
  assumes rconc: "\<And>t. cs (Inr t) = Inr (projr (cs (Inr t)))"
  shows "state_projr (local_eff (C\<^sub>1 \<parallel> C\<^sub>2) (Inr t) (External e) (cs, cg)) = local_eff C\<^sub>2 t (External e) (lsprojr cs, cg)"
  apply (simp add: local_eff_def parallel_def localize_def state_projr_def sprojr_def view_def local_def lsprojr_def)
  apply (intro conjI)
  apply (rule ext)
  apply (simp add: view_def local_def lsprojr_def)
  apply (subst rconc, simp)
  by (subst rconc, simp)

lemma projl_local_eff:
  assumes lconc: "\<And>t. cs (Inl t) = Inl (projl (cs (Inl t)))"
  shows "state_projl (local_eff (C\<^sub>1 \<parallel> C\<^sub>2) (Inl t) (External e) (cs, cg)) = local_eff C\<^sub>1 t (External e) (lsprojl cs, cg)"
  apply (simp add: local_eff_def parallel_def localize_def state_projl_def sprojl_def view_def local_def lsprojl_def)
  apply (intro conjI)
  apply (rule ext)
  apply (simp add: view_def local_def lsprojr_def)
  apply (subst lconc, simp)
  by (subst lconc, simp)

lemma projr_local_eff_int:
  assumes rconc: "\<And>t. cs (Inr t) = Inr (projr (cs (Inr t)))"
  shows "state_projr (local_eff (C\<^sub>1 \<parallel> C\<^sub>2) (Inr t) (Internal (Inr ic)) (cs, cg)) = local_eff C\<^sub>2 t (Internal ic) (lsprojr cs, cg)"
  apply (simp add: local_eff_def parallel_def localize_def state_projr_def sprojr_def view_def local_def lsprojr_def)
  apply (intro conjI)
  apply (rule ext)
  apply (simp add: view_def local_def lsprojr_def)
  apply (subst rconc, simp)
  by (subst rconc, simp)

lemma projl_local_eff_int:
  assumes lconc: "\<And>t. cs (Inl t) = Inl (projl (cs (Inl t)))"
  shows "state_projl (local_eff (C\<^sub>1 \<parallel> C\<^sub>2) (Inl t) (Internal (Inl ic)) (cs, cg)) = local_eff C\<^sub>1 t (Internal ic) (lsprojl cs, cg)"
  apply (simp add: local_eff_def parallel_def localize_def state_projl_def sprojl_def view_def local_def lsprojl_def)
  apply (intro conjI)
  apply (rule ext)
  apply (simp add: view_def local_def lsprojr_def)
  apply (subst lconc, simp)
  by (subst lconc, simp)

lemma projl_local_eff_int2:
  assumes rconc: "\<And>t. cs (Inr t) = Inr (projr (cs (Inr t)))"
  shows "state_projl (local_eff (C\<^sub>1 \<parallel> C\<^sub>2) (Inr t') (Internal (Inr b)) (cs, cg)) = (lsprojl cs, snd (local_eff C\<^sub>2 t' (Internal b) (lsprojr cs, cg)))"
  apply (simp add: local_eff_def parallel_def localize_def state_projl_def sprojl_def view_def local_def lsprojl_def)
  apply (subst rconc)
  by (simp add: lsprojr_def)

lemma projr_local_eff_int2:
  assumes lconc: "\<And>t. cs (Inl t) = Inl (projl (cs (Inl t)))"
  shows "state_projr (local_eff (C\<^sub>1 \<parallel> C\<^sub>2) (Inl t') (Internal (Inl b)) (cs, cg)) = (lsprojr cs, snd (local_eff C\<^sub>1 t' (Internal b) (lsprojl cs, cg)))"
  apply (simp add: local_eff_def parallel_def localize_def state_projr_def sprojr_def view_def local_def lsprojr_def)
  apply (subst lconc)
  by (simp add: lsprojl_def)

lemma use_ext_step:
  assumes "standard_sim_ext_step C A R"
  and "reach C cs" and "reach A as"
  and "R cs as" and "local_pre C t (External a) cs"
  shows "local_pre A t (External a) as" and "R (local_eff C t (External a) cs) (local_eff A t (External a) as)"
  using assms by (smt standard_sim_ext_step_def)+

lemma use_env_step:
  assumes "standard_sim_env_step C A R"
  and "reach C cs" and "reach A as"
  and "R cs as" and "(snd cs, cg') \<in> environment C"
  shows "\<exists>ag'. (snd as, ag') \<in> environment A \<and> R (fst cs, cg') (fst as, ag')"
  using assms by (smt standard_sim_env_step_def)

lemma use_stutter_step:
  assumes "standard_sim_stutter C A sc R"
  and "reach C cs" and "reach A as"
  and "R cs as" and "local_pre C t (Internal ic) cs"
  and "sc cs t ic = None"
  shows "R (local_eff C t (Internal ic) cs) as"
  using assms
  apply (cases cs)
  apply (cases as)
  by (simp add: standard_sim_stutter_def)

lemma [simp]: "local_eff (R \<rhd> C) = local_eff C"
  by (rule ext)+ (simp add: local_eff_def Rely_def)

fun par_sc ::
  "(('t \<Rightarrow> 'sc1) \<times> 'gc \<Rightarrow> 't \<Rightarrow> 'ic1 \<Rightarrow> 'ia option) \<Rightarrow>
  (('t \<Rightarrow> 'sc2) \<times> 'gc \<Rightarrow> 't \<Rightarrow> 'ic2 \<Rightarrow> 'ia option) \<Rightarrow>
  ('t + 't \<Rightarrow> 'sc1 + 'sc2) \<times> 'gc \<Rightarrow> 't + 't \<Rightarrow> 'ic1 + 'ic2 \<Rightarrow> ('ia + 'ia) option" where
  "par_sc sc\<^sub>1 sc\<^sub>2 s (Inl t) (Inl i) = map_option Inl (sc\<^sub>1 (state_projl s) t i)"
| "par_sc sc\<^sub>1 sc\<^sub>2 s (Inr t) (Inr i) = map_option Inr (sc\<^sub>2 (state_projr s) t i)"
| "par_sc sc\<^sub>1 sc\<^sub>2 s (Inl t) (Inr i) = None"
| "par_sc sc\<^sub>1 sc\<^sub>2 s (Inr t) (Inl i) = None"

definition non_interference_left_ext where
  "non_interference_left_ext R T C A \<equiv> \<forall>cs cg as ag t e.
    R (lsprojr cs, cg) (lsprojr as, ag) \<and>
    T (lsprojl cs, cg) (lsprojl as, ag) \<and>
    reach C (lsprojl cs, cg) \<and> reach A (lsprojl as, ag) \<and>
    local_pre C t (External e) (lsprojl cs, cg) \<and> local_pre A t (External e) (lsprojl as, ag) \<longrightarrow>
    R (lsprojr cs, snd (local_eff C t (External e) (lsprojl cs, cg))) (lsprojr as, snd (local_eff A t (External e) (lsprojl as, ag)))"

definition non_interference_left_int where
  "non_interference_left_int R T sc C K A \<equiv> \<forall>cs cg as ag t ic ia.
    R (lsprojr cs, cg) (lsprojr as, ag) \<and>
    T (lsprojl cs, cg) (lsprojl as, ag) \<and>
    reach C (lsprojl cs, cg) \<and> reach A (lsprojl as, ag) \<and> reach K (lsprojr cs, cg) \<and>
    local_pre C t (Internal ic) (lsprojl cs, cg) \<and> local_pre A t (Internal ia) (lsprojl as, ag) \<and>
    sc (lsprojl cs, cg) t ic = Some ia \<longrightarrow>
    R (lsprojr cs, snd (local_eff C t (Internal ic) (lsprojl cs, cg))) (lsprojr as, snd (local_eff A t (Internal ia) (lsprojl as, ag)))"

definition non_interference_left_stutter where
  "non_interference_left_stutter R T sc C A \<equiv> \<forall>cs cg as ag t ic.
    R (lsprojr cs, cg) (lsprojr as, ag) \<and>
    T (lsprojl cs, cg) (lsprojl as, ag) \<and>
    reach C (lsprojl cs, cg) \<and> reach A (lsprojl as, ag) \<and>
    local_pre C t (Internal ic) (lsprojl cs, cg) \<and>
    sc (lsprojl cs, cg) t ic = None \<longrightarrow>
    R (lsprojr cs, snd (local_eff C t (Internal ic) (lsprojl cs, cg))) (lsprojr as, ag)"

definition non_interference_left where
  "non_interference_left R T sc C K A \<equiv> non_interference_left_ext R T C A \<and> non_interference_left_int R T sc C K A \<and> non_interference_left_stutter R T sc C A"

(*
definition non_interference_left where
  "non_interference_left R sc C A \<equiv>
    (\<forall>cs cg as ag t e. R (lsprojr cs, cg) (lsprojr as, ag) \<longrightarrow> R (lsprojr cs, snd (local_eff C t (External e) (lsprojl cs, cg))) (lsprojr as, snd (local_eff A t (External e) (lsprojl as, ag)))) \<and>
    (\<forall>cs cg as ag t ic ia. R (lsprojr cs, cg) (lsprojr as, ag) \<and> sc (lsprojl cs, cg) t ic = Some ia \<longrightarrow> R (lsprojr cs, snd (local_eff C t (Internal ic) (lsprojl cs, cg))) (lsprojr as, snd (local_eff A t (Internal ia) (lsprojl as, ag)))) \<and>
    (\<forall>cs cg a t ic. R (lsprojr cs, cg) a \<and> sc (lsprojl cs, cg) t ic = None \<longrightarrow> R (lsprojr cs, snd (local_eff C t (Internal ic) (lsprojl cs, cg))) a)"
*)


definition non_interference_right_ext where
  "non_interference_right_ext R T C A \<equiv> \<forall>cs cg as ag t e.
    R (lsprojl cs, cg) (lsprojl as, ag) \<and>
    T (lsprojr cs, cg) (lsprojr as, ag) \<and>
    reach C (lsprojr cs, cg) \<and> reach A (lsprojr as, ag) \<and>
    local_pre C t (External e) (lsprojr cs, cg) \<and> local_pre A t (External e) (lsprojr as, ag) \<longrightarrow>
    R (lsprojl cs, snd (local_eff C t (External e) (lsprojr cs, cg))) (lsprojl as, snd (local_eff A t (External e) (lsprojr as, ag)))"

definition non_interference_right_int where
  "non_interference_right_int R T sc C K A \<equiv> \<forall>cs cg as ag t ic ia.
    R (lsprojl cs, cg) (lsprojl as, ag) \<and>
    T (lsprojr cs, cg) (lsprojr as, ag) \<and>
    reach C (lsprojr cs, cg) \<and> reach A (lsprojr as, ag) \<and> reach K (lsprojl cs, cg) \<and>
    local_pre C t (Internal ic) (lsprojr cs, cg) \<and> local_pre A t (Internal ia) (lsprojr as, ag) \<and>
    sc (lsprojr cs, cg) t ic = Some ia \<longrightarrow>
    R (lsprojl cs, snd (local_eff C t (Internal ic) (lsprojr cs, cg))) (lsprojl as, snd (local_eff A t (Internal ia) (lsprojr as, ag)))"

definition non_interference_right_stutter where
  "non_interference_right_stutter R T sc C A \<equiv> \<forall>cs cg as ag t ic.
    R (lsprojl cs, cg) (lsprojl as, ag) \<and>
    T (lsprojr cs, cg) (lsprojr as, ag) \<and>
    reach C (lsprojr cs, cg) \<and> reach A (lsprojr as, ag) \<and>
    local_pre C t (Internal ic) (lsprojr cs, cg) \<and>
    sc (lsprojr cs, cg) t ic = None \<longrightarrow>
    R (lsprojl cs, snd (local_eff C t (Internal ic) (lsprojr cs, cg))) (lsprojl as, ag)"

definition non_interference_right where
  "non_interference_right R T sc C K A \<equiv> non_interference_right_ext R T C A \<and> non_interference_right_int R T sc C K A \<and> non_interference_right_stutter R T sc C A"

lemma helperD: "A \<and> x = y \<Longrightarrow> A \<and> y = x"
  by auto

lemma abs_compat: "compatible A A"
  by (simp add: compatible_def)

theorem PARALLEL:
  fixes C\<^sub>1 :: "('t, 'sc1, 'gc, 'ic1) RGA"
  and C\<^sub>2 :: "('t, 'sc2, 'gc, 'ic2) RGA"
  and A :: "('t, 'sa, 'ga, 'ia) RGA"
  assumes compat: "compatible C\<^sub>1 C\<^sub>2"
  and I1_refl: "refl I\<^sub>1"
  and I2_refl: "refl I\<^sub>2"
  and IA_refl: "refl I\<^sub>A"
  and guar1: "guarantee (I\<^sub>2 \<rhd> C\<^sub>1) I\<^sub>1"
  and guar2: "guarantee (I\<^sub>1 \<rhd> C\<^sub>2) I\<^sub>2"
  and guarA: "guarantee (I\<^sub>A \<rhd> A) I\<^sub>A"
  and lsim: "weak_standard_simulation (I\<^sub>2 \<rhd> C\<^sub>1) (I\<^sub>A \<rhd> A) sc\<^sub>1 R\<^sub>1"
  and rsim: "weak_standard_simulation (I\<^sub>1 \<rhd> C\<^sub>2) (I\<^sub>A \<rhd> A) sc\<^sub>2 R\<^sub>2"
  and NIR: "non_interference_right R\<^sub>1 R\<^sub>2 sc\<^sub>2 (I\<^sub>1 \<rhd> C\<^sub>2) (I\<^sub>2 \<rhd> C\<^sub>1) (I\<^sub>A \<rhd> A)"
  and NIL: "non_interference_left R\<^sub>2 R\<^sub>1 sc\<^sub>1 (I\<^sub>2 \<rhd> C\<^sub>1) (I\<^sub>1 \<rhd> C\<^sub>2) (I\<^sub>A \<rhd> A)"
  shows "standard_simulation (C\<^sub>1 \<parallel> C\<^sub>2) (A \<parallel> A) (par_sc sc\<^sub>1 sc\<^sub>2) (par_sim_rel R\<^sub>1 R\<^sub>2)"
proof (simp add: standard_simulation_def, intro conjI)
  (* Once we do parallel composition there's no environment *)
  show "standard_sim_env_step (C\<^sub>1 \<parallel> C\<^sub>2) (A \<parallel> A) (par_sim_rel R\<^sub>1 R\<^sub>2)"
    by (simp add: standard_sim_env_step_def parallel_def)
next
  show "standard_sim_stutter (C\<^sub>1 \<parallel> C\<^sub>2) (A \<parallel> A) (par_sc sc\<^sub>1 sc\<^sub>2) (par_sim_rel R\<^sub>1 R\<^sub>2)"
  proof (simp add: standard_sim_stutter_def, intro allI impI, (erule conjE)+)
    fix cs cg as ag ic ia t
    assume reach_concrete: "reach (C\<^sub>1 \<parallel> C\<^sub>2) (cs, cg)"
    and reach_abstract: "reach (A \<parallel> A) (as, ag)"
    and par_SR: "par_sim_rel R\<^sub>1 R\<^sub>2 (cs, cg) (as, ag)"
    and par_pre: "local_pre (C\<^sub>1 \<parallel> C\<^sub>2) t (Internal ic) (cs, cg)"
    and par_sc_None: "par_sc sc\<^sub>1 sc\<^sub>2 (cs, cg) t ic = None"

    from lsim have stutterl: "standard_sim_stutter (I\<^sub>2 \<rhd> C\<^sub>1) (I\<^sub>A \<rhd> A) sc\<^sub>1 R\<^sub>1"
      by (simp add: weak_standard_simulation_def)

    from rsim have stutterr: "standard_sim_stutter (I\<^sub>1 \<rhd> C\<^sub>2) (I\<^sub>A \<rhd> A) sc\<^sub>2 R\<^sub>2"
      by (simp add: weak_standard_simulation_def)

    let ?goal = "par_sim_rel R\<^sub>1 R\<^sub>2 (local_eff (C\<^sub>1 \<parallel> C\<^sub>2) t (Internal ic) (cs, cg)) (as, ag)"

    have lconc: "\<And>t. cs (Inl t) = Inl (projl (cs (Inl t)))"
      using invariant_elim[OF lconc_rconc reach_concrete] by auto

    have rconc: "\<And>t. cs (Inr t) = Inr (projr (cs (Inr t)))"
      using invariant_elim[OF lconc_rconc reach_concrete] by auto

    have rconc_abs: "\<And>t. as (Inr t) = Inr (projr (as (Inr t)))"
      using invariant_elim[OF lconc_rconc reach_abstract] by auto

    have lconc_abs: "\<And>t. as (Inl t) = Inl (projl (as (Inl t)))"
      using invariant_elim[OF lconc_rconc reach_abstract] by auto

    from par_reach[OF abs_compat IA_refl IA_refl guarA guarA reach_abstract]
    have R_abs_reach: "reach (I\<^sub>A \<rhd> A) (lsprojr as, ag)"
      by (simp add: state_projr_def sprojr_def lsprojr_def)

    from par_reach[OF abs_compat IA_refl IA_refl guarA guarA reach_abstract]
    have L_abs_reach: "reach (I\<^sub>A \<rhd> A) (lsprojl as, ag)"
      by (simp add: state_projl_def sprojl_def lsprojl_def)

    from par_reach[OF compat I1_refl I2_refl guar1 guar2 reach_concrete]
    have R_reach: "reach (I\<^sub>1 \<rhd> C\<^sub>2) (lsprojr cs, cg)"
      by (simp add: state_projr_def sprojr_def lsprojr_def)

    from par_reach[OF compat I1_refl I2_refl guar1 guar2 reach_concrete]
    have L_reach: "reach (I\<^sub>2 \<rhd> C\<^sub>1) (lsprojl cs, cg)"
      by (simp add: state_projl_def sprojl_def lsprojl_def)

    {
      fix t'
      assume t_def [simp]: "t = Inr t'"

      from par_sc_None
      have ?goal
        apply (simp add: par_sim_rel_def)
        apply (cases ic)
        apply simp_all
        apply (simp add: local_eff_def parallel_def state_projl_def sprojl_def state_projr_def sprojr_def localize_def local_def view_def)
        apply (subst rconc)
        apply simp
        apply (intro conjI)
        using par_SR
        apply (simp add: par_sim_rel_def state_projl_def sprojl_def)
        apply (subst rconc)
        apply (simp cong del: if_weak_cong add: local_def view_def)
        apply (subst rconc)
        apply (simp cong del: if_weak_cong)
        using par_SR
        apply (simp add: par_sim_rel_def state_projr_def sprojr_def)
        apply (subgoal_tac "(\<lambda>t. projr (cs (Inr t))) = (\<lambda>t. projr (if t' = t then Inr (projr (cs (Inr t'))) else cs (Inr t)))")
        apply simp
        apply (rule ext)
        apply simp
        apply (subst projr_local_eff_int, rule rconc)
        apply (intro conjI)
        apply (simp add: state_projl_def sprojl_def lsprojl_def[symmetric])
        apply (simp cong del: if_weak_cong add: local_eff_def localize_def local_def view_def parallel_def)
        apply (subst rconc)
        apply (simp cong del: if_weak_cong add: lsprojl_def)
        apply (simp only: lsprojl_def[symmetric])
        apply (subst rconc)
        apply simp
        using NIR
        apply (simp add: non_interference_right_def non_interference_right_stutter_def state_projr_def sprojr_def lsprojr_def[symmetric])
        apply (drule conjunct2)+
        apply (erule_tac x = cs in allE)
        apply (erule_tac x = cg in allE)
        apply (erule_tac x = as in allE)
        apply (erule_tac x = ag in allE)
        apply (erule_tac x = t' in allE)
        apply (rename_tac ic')
        apply (erule_tac x = ic' in allE)
        apply (erule impE)
        apply (intro conjI)
        using par_SR
        apply (simp add: par_sim_rel_def state_projl_def sprojl_def lsprojl_def)
        using par_SR
        apply (simp add: par_sim_rel_def state_projr_def sprojr_def lsprojr_def)
        using R_reach apply blast
        using R_abs_reach apply blast
        apply (simp add: local_pre_def view_def local_def)
        using par_pre
        apply (simp add: parallel_def local_pre_def view_def local_def Rely_def)
        apply (erule rev_mp)+
        apply (subst rconc, simp add: lsprojr_def)
        apply assumption
        apply (simp add: local_eff_def localize_def view_def local_def lsprojr_def)
        apply (simp add: state_projr_def sprojr_def lsprojr_def[symmetric])
        apply (rule use_stutter_step[OF stutterr R_reach R_abs_reach, simplified])
        using par_SR
        apply (simp add: par_sim_rel_def state_projr_def sprojr_def lsprojr_def)
        using par_pre
        apply (simp add: parallel_def local_pre_def view_def local_def Rely_def)
        apply (erule rev_mp)+
        apply (subst rconc, simp add: lsprojr_def)
        by blast
    }
    moreover
    {
      fix t'
      assume t_def [simp]: "t = Inl t'"

      from par_sc_None par_pre
      have ?goal
        apply (simp add: par_sim_rel_def)
        apply (cases ic)
        defer
        apply simp_all
        apply (simp add: local_eff_def parallel_def state_projl_def sprojl_def state_projr_def sprojr_def localize_def local_def view_def)
        apply (subst lconc)
        apply simp
        apply (intro conjI)
        prefer 2
        using par_SR
        apply (simp add: par_sim_rel_def state_projr_def sprojr_def)
        apply (subst lconc)
        apply (simp cong del: if_weak_cong add: local_def view_def)
        apply (simp cong del: if_weak_cong add: local_def view_def)
        apply (subst lconc)
        apply (simp cong del: if_weak_cong)
        using par_SR
        apply (simp add: par_sim_rel_def state_projl_def sprojl_def)
        apply (subgoal_tac "(\<lambda>t. projl (cs (Inl t))) = (\<lambda>t. projl (if t' = t then Inl (projl (cs (Inl t'))) else cs (Inl t)))")
        apply simp
        apply (rule ext)
        apply simp
        apply (subst projl_local_eff_int, rule lconc)
        apply (intro conjI)
        prefer 2
        apply (simp add: state_projr_def sprojr_def lsprojr_def[symmetric])
        apply (simp cong del: if_weak_cong add: local_eff_def localize_def local_def view_def parallel_def)
        apply (subst lconc)
        apply (simp cong del: if_weak_cong add: lsprojr_def)
        apply (simp only: lsprojr_def[symmetric])
        apply (subst lconc)
        apply simp
        using NIL
        apply (simp add: non_interference_left_def non_interference_left_stutter_def state_projl_def sprojl_def lsprojl_def[symmetric])
        apply (drule conjunct2)+
        apply (erule_tac x = cs in allE)
        apply (erule_tac x = cg in allE)
        apply (erule_tac x = as in allE)
        apply (erule_tac x = ag in allE)
        apply (erule_tac x = t' in allE)
        apply (rename_tac ic')
        apply (erule_tac x = ic' in allE)
        apply (erule impE)
        apply simp
        apply (intro conjI)
        using par_SR
        apply (simp add: par_sim_rel_def state_projr_def sprojr_def lsprojr_def)
        using par_SR
        apply (simp add: par_sim_rel_def state_projl_def sprojl_def lsprojl_def)
        using L_reach apply blast
        using L_abs_reach apply blast
        apply (simp add: local_pre_def view_def local_def)
        apply (erule rev_mp)+
        apply (subst lconc)
        apply (simp add: Rely_def lsprojl_def)
        apply (simp add: local_eff_def localize_def view_def local_def lsprojl_def)
        apply (simp add: state_projl_def sprojl_def lsprojl_def[symmetric])
        apply (rule use_stutter_step[OF stutterl L_reach L_abs_reach, simplified])
        using par_SR
        apply (simp add: par_sim_rel_def state_projl_def sprojl_def lsprojl_def)
        using par_pre
        apply (simp add: parallel_def local_pre_def view_def local_def Rely_def)
        apply (erule rev_mp)+
        apply (subst lconc, simp add: lsprojl_def)
        by blast
    }
    ultimately show ?goal
      using sum.exhaust_sel by blast
  qed
next
  show "standard_sim_int_step (C\<^sub>1 \<parallel> C\<^sub>2) (A \<parallel> A) (par_sc sc\<^sub>1 sc\<^sub>2) (par_sim_rel R\<^sub>1 R\<^sub>2)"
  proof (simp add: standard_sim_int_step_def, intro allI impI, (erule conjE)+)
    fix cs cg as ag ic ia t
    assume reach_concrete: "reach (C\<^sub>1 \<parallel> C\<^sub>2) (cs, cg)"
    and reach_abstract: "reach (A \<parallel> A) (as, ag)"
    and par_SR: "par_sim_rel R\<^sub>1 R\<^sub>2 (cs, cg) (as, ag)"
    and par_pre: "local_pre (C\<^sub>1 \<parallel> C\<^sub>2) t (Internal ic) (cs, cg)"
    and par_sc_Some: "par_sc sc\<^sub>1 sc\<^sub>2 (cs, cg) t ic = Some ia"

    let ?goal1 = "local_pre (A \<parallel> A) t (Internal ia) (as, ag)"
    let ?goal2 = "par_sim_rel R\<^sub>1 R\<^sub>2 (local_eff (C\<^sub>1 \<parallel> C\<^sub>2) t (Internal ic) (cs, cg)) (local_eff (A \<parallel> A) t (Internal ia) (as, ag))"

    have lconc: "\<And>t. cs (Inl t) = Inl (projl (cs (Inl t)))"
      using invariant_elim[OF lconc_rconc reach_concrete] by auto

    have rconc: "\<And>t. cs (Inr t) = Inr (projr (cs (Inr t)))"
      using invariant_elim[OF lconc_rconc reach_concrete] by auto

    have rconc_abs: "\<And>t. as (Inr t) = Inr (projr (as (Inr t)))"
      using invariant_elim[OF lconc_rconc reach_abstract] by auto

    have lconc_abs: "\<And>t. as (Inl t) = Inl (projl (as (Inl t)))"
      using invariant_elim[OF lconc_rconc reach_abstract] by auto

    from par_reach[OF abs_compat IA_refl IA_refl guarA guarA reach_abstract]
    have R_abs_reach: "reach (I\<^sub>A \<rhd> A) (lsprojr as, ag)"
      by (simp add: state_projr_def sprojr_def lsprojr_def)

    from par_reach[OF abs_compat IA_refl IA_refl guarA guarA reach_abstract]
    have L_abs_reach: "reach (I\<^sub>A \<rhd> A) (lsprojl as, ag)"
      by (simp add: state_projl_def sprojl_def lsprojl_def)

    from par_reach[OF compat I1_refl I2_refl guar1 guar2 reach_concrete]
    have R_reach: "reach (I\<^sub>1 \<rhd> C\<^sub>2) (lsprojr cs, cg)"
      by (simp add: state_projr_def sprojr_def lsprojr_def)

    from par_reach[OF compat I1_refl I2_refl guar1 guar2 reach_concrete]
    have L_reach: "reach (I\<^sub>2 \<rhd> C\<^sub>1) (lsprojl cs, cg)"
      by (simp add: state_projl_def sprojl_def lsprojl_def)

    {
      fix t'
      assume t_def: "t = Inr t'"

      obtain ic' where ic_def [simp]: "ic = Inr ic'"
        by (metis not_None_eq par_sc.simps(4) par_sc_Some sum.exhaust_sel t_def)

      obtain ia' where ia_def [simp]: "ia = Inr ia'"
        using par_sc_Some t_def and ic_def by auto

      from t_def ic_def and par_pre
      have R_local_pre: "local_pre (I\<^sub>1 \<rhd> C\<^sub>2) t' (Internal ic') (lsprojr cs, cg)"
        apply (simp add: local_pre_def parallel_def view_def local_def lsprojr_def)
        apply (erule rev_mp)+
        apply (subst rconc)
        apply simp
        by (subst rconc, simp add: Rely_def)

      from t_def and par_SR
      have R_SR1: "R\<^sub>1 (lsprojl cs, cg) (lsprojl as, ag)"
        by (simp add: par_sim_rel_def lsprojl_def state_projl_def sprojl_def)

      from t_def and par_SR
      have R_SR: "R\<^sub>2 (lsprojr cs, cg) (lsprojr as, ag)"
        by (simp add: par_sim_rel_def lsprojr_def state_projr_def sprojr_def)

      have R_sc: "sc\<^sub>2 (lsprojr cs, cg) t' ic' = Some ia'"
        using par_sc_Some t_def ic_def and ia_def
        by (auto simp add: lsprojr_def state_projr_def sprojr_def)

      from R_SR R_sc R_local_pre R_reach R_abs_reach and rsim
      have rsim_1: "local_pre (I\<^sub>A \<rhd> A) t' (Internal ia') (lsprojr as, ag)"
      and rsim_2: "R\<^sub>2 (local_eff (I\<^sub>1 \<rhd> C\<^sub>2) t' (Internal ic') (lsprojr cs, cg)) (local_eff (I\<^sub>A \<rhd> A) t' (Internal ia') (lsprojr as, ag))"
        by - (simp_all add: standard_sim_int_step_def weak_standard_simulation_def)

      from t_def rsim_1
      have g1: ?goal1
        apply -
        apply (simp add: local_pre_def view_def local_def lsprojr_def parallel_def)
        by (subst rconc_abs, simp add: Rely_def)

      moreover from t_def par_sc_Some par_pre g1
      have ?goal2
        apply (simp add: par_sim_rel_def)
        apply (subst projr_local_eff_int, rule rconc)
        apply (subst projr_local_eff_int, rule rconc_abs)
        apply (intro conjI)
        apply (subst projl_local_eff_int2, rule rconc)
        apply (subst projl_local_eff_int2, rule rconc_abs)
        apply (simp add: state_projr_def sprojr_def)
        apply (simp add: lsprojr_def[symmetric])
        using NIR R_SR1
        apply (simp add: non_interference_right_def non_interference_right_int_def)
        apply (drule conjunct2, drule conjunct1)
        apply (erule_tac x = cs in allE, erule_tac x = cg in allE, erule_tac x = as in allE, erule_tac x = ag in allE)
        apply (erule_tac x = t' in allE, erule_tac x = ic' in allE, erule_tac x = ia' in allE)
        apply (erule impE)
        apply (intro conjI)
        apply assumption
        using par_SR
        apply (simp add: par_sim_rel_def state_projr_def sprojr_def lsprojr_def)
        using R_reach apply blast
        using R_abs_reach apply blast
        using L_reach apply blast
        apply (simp add: local_pre_def view_def local_def parallel_def)
        apply (erule rev_mp)+
        apply (subst rconc)
        apply (simp add: lsprojr_def projr_def Rely_def)
        apply (simp add: local_pre_def view_def local_def parallel_def)
        apply (erule rev_mp)+
        apply (subst rconc_abs)
        apply (simp add: lsprojr_def projr_def Rely_def)
        apply assumption
        apply simp
        using rsim_2
        by (simp add: Rely_def local_eff_def)

      ultimately have "?goal1 \<and> ?goal2" by blast
    }
    moreover
    {
      fix t'
      assume t_def: "t = Inl t'"

      obtain ic' where ic_def [simp]: "ic = Inl ic'"
        by (metis not_None_eq old.sum.exhaust par_sc.simps(3) par_sc_Some t_def)

      obtain ia' where ia_def [simp]: "ia = Inl ia'"
        using par_sc_Some t_def and ic_def by auto

      from t_def ic_def and par_pre
      have L_local_pre: "local_pre (I\<^sub>2 \<rhd> C\<^sub>1) t' (Internal ic') (lsprojl cs, cg)"
        apply (simp add: local_pre_def parallel_def view_def local_def lsprojl_def)
        apply (erule rev_mp)+
        apply (subst lconc)
        apply simp
        by (subst lconc, simp add: Rely_def)

      from t_def and par_SR
      have L_SR2: "R\<^sub>2 (lsprojr cs, cg) (lsprojr as, ag)"
        by (simp add: par_sim_rel_def lsprojr_def state_projr_def sprojr_def)

      from t_def and par_SR
      have L_SR: "R\<^sub>1 (lsprojl cs, cg) (lsprojl as, ag)"
        by (simp add: par_sim_rel_def lsprojl_def state_projl_def sprojl_def)

      have L_sc: "sc\<^sub>1 (lsprojl cs, cg) t' ic' = Some ia'"
        using par_sc_Some t_def ic_def and ia_def
        by (auto simp add: lsprojl_def state_projl_def sprojl_def)

      from L_SR L_sc L_local_pre L_reach L_abs_reach and lsim
      have rsim_1: "local_pre (I\<^sub>A \<rhd> A) t' (Internal ia') (lsprojl as, ag)"
      and rsim_2: "R\<^sub>1 (local_eff (I\<^sub>2 \<rhd> C\<^sub>1) t' (Internal ic') (lsprojl cs, cg)) (local_eff (I\<^sub>A \<rhd> A) t' (Internal ia') (lsprojl as, ag))"
        by - (simp_all add: standard_sim_int_step_def weak_standard_simulation_def)

      from t_def rsim_1
      have g1: ?goal1
        apply -
        apply (simp add: local_pre_def view_def local_def lsprojl_def parallel_def)
        by (subst lconc_abs, simp add: Rely_def)

      moreover from t_def par_sc_Some par_pre g1
      have ?goal2
        apply (simp add: par_sim_rel_def)
        apply (subst projl_local_eff_int, rule lconc)
        apply (subst projl_local_eff_int, rule lconc_abs)
        apply (intro conjI)
        using rsim_2
        apply (simp add: Rely_def local_eff_def)
        apply (subst projr_local_eff_int2, rule lconc)
        apply (subst projr_local_eff_int2, rule lconc_abs)
        using NIL L_SR2
        apply (simp add: non_interference_left_def non_interference_left_int_def state_projl_def sprojl_def)
        apply (drule conjunct2)
        apply (drule conjunct1)
        apply (erule_tac x = cs in allE, erule_tac x = cg in allE, erule_tac x = as in allE, erule_tac x = ag in allE)
        apply (erule_tac x = t' in allE, erule_tac x = ic' in allE, erule_tac x = ia' in allE)
        apply (erule impE)
        apply (intro conjI)
        apply assumption
        using par_SR
        apply (simp add: par_sim_rel_def state_projl_def sprojl_def lsprojl_def)
        using L_reach apply blast
        using L_abs_reach apply blast
        using R_reach apply blast
        apply (simp add: local_pre_def view_def local_def parallel_def)
        apply (erule rev_mp)+
        apply (subst lconc)
        apply (simp add: lsprojl_def projl_def Rely_def)
        apply (simp add: local_pre_def view_def local_def parallel_def)
        apply (erule rev_mp)+
        apply (subst lconc_abs)
        apply (simp add: lsprojl_def projl_def Rely_def)
        apply (simp add: lsprojl_def)
        by (simp only: lsprojl_def[symmetric])
      ultimately have "?goal1 \<and> ?goal2" by blast
    }
    ultimately show "?goal1 \<and> ?goal2"
      using sumE by blast
  qed
next
  show "standard_sim_ext_step (C\<^sub>1 \<parallel> C\<^sub>2) (A \<parallel> A) (par_sim_rel R\<^sub>1 R\<^sub>2)"
  proof (simp add: standard_sim_ext_step_def, intro allI impI, (erule conjE)+)
    fix cs cg as ag e t
    assume reach_concrete: "reach (C\<^sub>1 \<parallel> C\<^sub>2) (cs, cg)"
    and reach_abstract: "reach (A \<parallel> A) (as, ag)"
    and par_SR: "par_sim_rel R\<^sub>1 R\<^sub>2 (cs, cg) (as, ag)"
    and par_pre: "local_pre (C\<^sub>1 \<parallel> C\<^sub>2) t (External e) (cs, cg)"

    from par_SR
    have SR_1: "R\<^sub>1 (lsprojl cs, cg) (lsprojl as, ag)" and SR_2: "R\<^sub>2 (lsprojr cs, cg) (lsprojr as, ag)"
      by - (simp_all add: par_sim_rel_def state_projr_def state_projl_def sprojl_def sprojr_def lsprojr_def lsprojl_def)

    let ?goal1 = "local_pre (A \<parallel> A) t (External e) (as, ag)"
    let ?goal2 = "par_sim_rel R\<^sub>1 R\<^sub>2 (local_eff (C\<^sub>1 \<parallel> C\<^sub>2) t (External e) (cs, cg)) (local_eff (A \<parallel> A) t (External e) (as, ag))"


    have lconc: "\<And>t. cs (Inl t) = Inl (projl (cs (Inl t)))"
      using invariant_elim[OF lconc_rconc reach_concrete] by auto

    have rconc: "\<And>t. cs (Inr t) = Inr (projr (cs (Inr t)))"
      using invariant_elim[OF lconc_rconc reach_concrete] by auto

    have rconc_abs: "\<And>t. as (Inr t) = Inr (projr (as (Inr t)))"
      using invariant_elim[OF lconc_rconc reach_abstract] by auto

    have lconc_abs: "\<And>t. as (Inl t) = Inl (projl (as (Inl t)))"
      using invariant_elim[OF lconc_rconc reach_abstract] by auto

    from par_reach[OF abs_compat IA_refl IA_refl guarA guarA reach_abstract]
    have R_abs_reach: "reach (I\<^sub>A \<rhd> A) (lsprojr as, ag)"
      by (simp add: state_projr_def sprojr_def lsprojr_def)

    from par_reach[OF abs_compat IA_refl IA_refl guarA guarA reach_abstract]
    have L_abs_reach: "reach (I\<^sub>A \<rhd> A) (lsprojl as, ag)"
      by (simp add: state_projl_def sprojl_def lsprojl_def)

    from par_reach[OF compat I1_refl I2_refl guar1 guar2 reach_concrete]
    have R_reach: "reach (I\<^sub>1 \<rhd> C\<^sub>2) (lsprojr cs, cg)"
      by (simp add: state_projr_def sprojr_def lsprojr_def)

    from par_reach[OF compat I1_refl I2_refl guar1 guar2 reach_concrete]
    have L_reach: "reach (I\<^sub>2 \<rhd> C\<^sub>1) (lsprojl cs, cg)"
      by (simp add: state_projl_def sprojl_def lsprojl_def)

    have lsim: "standard_sim_ext_step (I\<^sub>2 \<rhd> C\<^sub>1) (I\<^sub>A \<rhd> A) R\<^sub>1"
      using lsim wssim_ext by auto

    have rsim: "standard_sim_ext_step (I\<^sub>1 \<rhd> C\<^sub>2) (I\<^sub>A \<rhd> A) R\<^sub>2"
      using rsim wssim_ext by blast

    {
      fix t'
      assume t_def: "t = Inr t'"

      from t_def and par_pre
      have R_local_pre: "local_pre (I\<^sub>1 \<rhd> C\<^sub>2) t' (External e) (lsprojr cs, cg)"
        apply (simp add: local_pre_def parallel_def view_def local_def lsprojr_def)
        apply (erule rev_mp)+
        apply (subst rconc)
        apply simp
        by (subst rconc, simp add: Rely_def)

      from t_def and par_SR
      have R_SR: "R\<^sub>2 (lsprojr cs, cg) (lsprojr as, ag)"
        by (simp add: par_sim_rel_def lsprojr_def state_projr_def sprojr_def)

      from R_SR R_local_pre R_reach R_abs_reach and rsim
      have rsim_1: "local_pre (I\<^sub>A \<rhd> A) t' (External e) (lsprojr as, ag)"
      and rsim_2: "R\<^sub>2 (local_eff (I\<^sub>1 \<rhd> C\<^sub>2) t' (External e) (lsprojr cs, cg)) (local_eff (I\<^sub>A \<rhd> A) t' (External e) (lsprojr as, ag))"
        by - (simp_all add: standard_sim_ext_step_def)

      from t_def rsim_1
      have g1: ?goal1
        apply -
        apply (simp add: local_pre_def view_def local_def lsprojr_def parallel_def)
        by (subst rconc_abs, simp add: Rely_def)

      moreover from t_def rsim_2 SR_1 par_pre g1
      have ?goal2
        apply (simp add: par_sim_rel_def)
        apply (intro conjI)
        defer
        apply (subst projr_local_eff, rule rconc)
        apply (subst projr_local_eff, rule rconc_abs)
        apply (simp add: Rely_def local_eff_def)
        apply (subst projl_env_eff, rule rconc)
        apply (subst projl_env_eff, rule rconc_abs)
        apply simp
        using NIR
        apply (simp add: non_interference_right_def non_interference_right_ext_def)
        apply (drule conjunct1)
        apply (erule_tac x = cs in allE, erule_tac x = cg in allE, erule_tac x = as in allE, erule_tac x = ag in allE)
        apply (erule_tac x = t' in allE, erule_tac x = e in allE)
        apply (erule impE)
        apply (intro conjI)
        apply assumption
        using par_SR
        apply (simp add: par_sim_rel_def state_projr_def sprojr_def lsprojr_def)
        using R_reach apply blast
        using R_abs_reach apply blast
        apply (simp add: local_pre_def view_def local_def parallel_def)
        apply (erule rev_mp)+
        apply (subst rconc)
        apply (simp add: lsprojr_def projr_def Rely_def)
        apply (simp add: local_pre_def view_def local_def parallel_def)
        apply (erule rev_mp)+
        apply (subst rconc_abs)
        apply (simp add: lsprojr_def projr_def Rely_def)
        by simp
       
      ultimately have "?goal1 \<and> ?goal2"
        by blast
    }
    moreover
    {
      fix t'
      assume t_def: "t = Inl t'"

      from t_def and par_pre
      have L_local_pre: "local_pre (I\<^sub>2 \<rhd> C\<^sub>1) t' (External e) (lsprojl cs, cg)"
        apply (simp add: local_pre_def parallel_def view_def local_def lsprojl_def)
        apply (erule rev_mp)+
        apply (subst lconc)
        apply simp
        by (subst lconc, simp add: Rely_def)

      from t_def and par_SR
      have L_SR: "R\<^sub>1 (lsprojl cs, cg) (lsprojl as, ag)"
        by (simp add: par_sim_rel_def lsprojl_def state_projl_def sprojl_def)

      from L_SR L_local_pre L_reach L_abs_reach and lsim
      have lsim_1: "local_pre (I\<^sub>A \<rhd> A) t' (External e) (lsprojl as, ag)"
      and lsim_2: "R\<^sub>1 (local_eff (I\<^sub>2 \<rhd> C\<^sub>1) t' (External e) (lsprojl cs, cg)) (local_eff (I\<^sub>A \<rhd> A) t' (External e) (lsprojl as, ag))"
        by - (simp_all add: standard_sim_ext_step_def)

      from t_def lsim_1
      have g1: ?goal1
        apply -
        apply (simp add: local_pre_def view_def local_def lsprojl_def parallel_def)
        by (subst lconc_abs, simp add: Rely_def)

      moreover from t_def lsim_2 SR_2 par_pre g1
      have ?goal2
        apply (simp add: par_sim_rel_def)
        apply (intro conjI)
        apply (subst projl_local_eff, rule lconc)
        apply (subst projl_local_eff, rule lconc_abs)
        apply (simp add: Rely_def local_eff_def)
        apply (subst projr_env_eff, rule lconc)
        apply (subst projr_env_eff, rule lconc_abs)
        apply simp
        using NIL
        apply (simp add: non_interference_left_def non_interference_left_ext_def)
        apply (drule conjunct1)
        apply (erule_tac x = cs in allE, erule_tac x = cg in allE, erule_tac x = as in allE, erule_tac x = ag in allE)
        apply (erule_tac x = t' in allE, erule_tac x = e in allE)
        apply (erule impE)
        apply (intro conjI)
        apply assumption
        using par_SR
        apply (simp add: par_sim_rel_def state_projl_def sprojl_def lsprojl_def)
        using L_reach apply blast
        using L_abs_reach apply blast
        apply (simp add: local_pre_def view_def local_def parallel_def)
        apply (erule rev_mp)+
        apply (subst lconc)
        apply (simp add: lsprojl_def projl_def Rely_def)
        apply (simp add: local_pre_def view_def local_def parallel_def)
        apply (erule rev_mp)+
        apply (subst lconc_abs)
        apply (simp add: lsprojl_def projl_def Rely_def)
        by simp

      ultimately have "?goal1 \<and> ?goal2"
        by blast
    }
    ultimately show "?goal1 \<and> ?goal2"
      using sum.exhaust_sel by blast
  qed
next
  from lsim rsim
  show "\<forall>cs cg. (cs, cg) \<in> starts (C\<^sub>1 \<parallel> C\<^sub>2) \<longrightarrow>
         (\<exists>as ag. par_sim_rel R\<^sub>1 R\<^sub>2 (cs, cg) (as, ag) \<and> (as, ag) \<in> starts (A \<parallel> A))"
    apply (simp add: starts_def parallel_def image_def par_locals_def weak_standard_simulation_def Rely_def)
    apply safe
    apply (erule_tac x = "L1" in allE)
    apply (erule_tac x = "L2" in allE)
    apply auto
    apply (rename_tac L1 L2 LA1 LA2)
    apply (rule_tac x = "case_sum (\<lambda>t'. Inl (LA1 t')) (\<lambda>t'. Inr (LA2 t'))" in exI)
    apply (intro conjI)
    apply (auto simp add: par_sim_rel_def state_projl_def sprojl_def state_projr_def sprojr_def)
    by (metis RGA.compatible_def compat)
qed

primrec unsum :: "'a + 'a \<Rightarrow> 'a" where
  "unsum (Inr x) = x"
| "unsum (Inl x) = x"

definition SumT :: "('t, 's ,'g, 'i) RGA \<Rightarrow> ('t + 't, 's, 'g, 'i) RGA" where
  "SumT aut \<equiv> \<lparr> RGA.local_starts = {unsum \<circ> par_locals L1 L2|L1 L2. L1 \<in> local_starts aut \<and> L2 \<in> local_starts aut},
                RGA.global_start = global_start aut,
                RGA.pre = pre aut,
                RGA.eff = eff aut,
                RGA.environment = environment aut \<rparr>"

lemma "unsum \<circ> par_locals f g = (\<lambda>x. case x of Inl y \<Rightarrow> f y | Inr z \<Rightarrow> g z)"
  by (rule ext, simp add: par_locals_def sum.case_eq_if)

lemma "{l \<circ> unsum|l. l \<in> local_starts aut} \<subseteq> {unsum \<circ> par_locals L1 L2|L1 L2. L1 \<in> local_starts aut \<and> L2 \<in> local_starts aut}"
  apply safe
  apply (rule_tac x = l in exI)+
  apply safe
  apply (rule ext)
  apply (auto simp add: par_locals_def)
  by (metis sum.case_eq_if sum.collapse(1) sum.collapse(2) unsum.simps(1) unsum.simps(2))

theorem PAR_REFL:
  assumes env_refl: "refl (environment A)"
  shows "standard_simulation (A \<parallel> A) (SumT A) (\<lambda>_ _ ic. Some (unsum ic)) (\<lambda>c a. (\<forall>t. unsum (fst c t) = fst a t) \<and> snd c = snd a)"
proof (simp add: standard_simulation_def, intro conjI)
  show "standard_sim_env_step (A \<parallel> A) (SumT A) (\<lambda>c a. (\<forall>t. unsum (fst c t) = fst a t) \<and> snd c = snd a)"
    by (simp add: standard_sim_env_step_def parallel_def SumT_def) (meson IdI env_refl refl_Id refl_onD refl_onD2)
next
  show "standard_sim_ext_step (A \<parallel> A) (SumT A) (\<lambda>c a. (\<forall>t. unsum (fst c t) = fst a t) \<and> snd c = snd a)"
  proof (simp add: standard_sim_ext_step_def, clarify, intro conjI)
    fix cs cg as g e t
    assume reach_concrete: "reach (A \<parallel> A) (cs, g)"
    and reach_abstract: "reach (SumT A) (as, g)"
    and "\<forall>t. unsum (cs t) = as t"
    and "local_pre (A \<parallel> A) t (External e) (cs, g)"
    note local_assms = this(3) this(4)

    have lconc: "\<And>t. cs (Inl t) = Inl (projl (cs (Inl t)))"
      using invariant_elim[OF lconc_rconc reach_concrete] by auto

    have rconc: "\<And>t. cs (Inr t) = Inr (projr (cs (Inr t)))"
      using invariant_elim[OF lconc_rconc reach_concrete] by auto

    from local_assms
    show "local_pre (SumT A) t (External e) (as, g)"
      apply (simp add: local_pre_def parallel_def SumT_def view_def local_def)
      apply (erule_tac x = t in allE)
      apply (cases t)
      apply simp_all
      apply (erule rev_mp)+
      apply (subst lconc)
      apply simp
      apply (subst lconc) back
      apply simp
      apply (erule rev_mp)+
      apply (subst rconc)
      apply simp
      apply (subst rconc) back
      by simp

    from local_assms
    show "\<forall>t'. unsum (fst (local_eff (A \<parallel> A) t (External e) (cs, g)) t') = fst (local_eff (SumT A) t (External e) (as, g)) t'"
      apply (simp add: local_eff_def parallel_def SumT_def view_def local_def local_pre_def localize_def)
      apply (erule_tac x = t in allE)
      apply (cases t)
      apply simp_all
      apply (erule rev_mp)+
      apply (subst lconc)
      apply simp
      apply (subst lconc) back
      apply simp
      apply (subst lconc) back
      apply simp
      apply (erule rev_mp)+
      apply (subst rconc)
      apply simp
      apply (subst rconc) back
      apply simp
      apply (subst rconc) back
      by simp

    from local_assms
    show "snd (local_eff (A \<parallel> A) t (External e) (cs, g)) = snd (local_eff (SumT A) t (External e) (as, g))"
      apply (simp add: local_eff_def parallel_def SumT_def view_def local_def local_pre_def localize_def)
      apply (erule_tac x = t in allE)
      apply (cases t)
      apply simp_all
      apply (erule rev_mp)+
      apply (subst lconc)
      apply simp
      apply (subst lconc) back
      apply simp
      apply (subst lconc) back
      apply simp
      apply (erule rev_mp)+
      apply (subst rconc)
      apply simp
      apply (subst rconc) back
      apply simp
      apply (subst rconc) back
      by simp
  qed
next
  show "standard_sim_stutter (A \<parallel> A) (SumT A) (\<lambda>_ _ ic. Some (unsum ic)) (\<lambda>c a. (\<forall>t. unsum (fst c t) = fst a t) \<and> snd c = snd a)"
    by (simp add: standard_sim_stutter_def)
next
  show "standard_sim_int_step (A \<parallel> A) (SumT A) (\<lambda>_ _ ic. Some (unsum ic)) (\<lambda>c a. (\<forall>t. unsum (fst c t) = fst a t) \<and> snd c = snd a)"
  proof (simp add: standard_sim_int_step_def, clarify, intro conjI)
    fix cs cg as g ic t
    assume reach_concrete: "reach (A \<parallel> A) (cs, g)"
    and reach_abstract: "reach (SumT A) (as, g)"
    and "\<forall>t. unsum (cs t) = as t"
    and "local_pre (A \<parallel> A) t (Internal ic) (cs, g)"
    note local_assms = this(3) this(4)    

    have lconc: "\<And>t. cs (Inl t) = Inl (projl (cs (Inl t)))"
      using invariant_elim[OF lconc_rconc reach_concrete] by auto

    have rconc: "\<And>t. cs (Inr t) = Inr (projr (cs (Inr t)))"
      using invariant_elim[OF lconc_rconc reach_concrete] by auto

    from local_assms
    show "local_pre (A \<parallel> A) t (Internal ic) (cs, g) \<Longrightarrow> local_pre (SumT A) t (Internal (unsum ic)) (as, g)"
      apply (simp add: local_pre_def parallel_def SumT_def view_def local_def)
      apply (erule_tac x = t in allE)
      apply (cases ic)
      apply simp_all
      apply (cases t)
      apply simp_all
      apply (erule rev_mp)+
      apply (subst lconc)
      apply simp
      apply (subst lconc) back
      apply simp
      apply (erule rev_mp)+
      apply (subst rconc)
      apply simp
      apply (subst rconc) back
      apply simp
      apply (cases t)
      apply simp_all
      apply (erule rev_mp)+
      apply (subst lconc)
      apply simp
      apply (subst lconc) back
      apply simp
      apply (erule rev_mp)+
      apply (subst rconc)
      apply simp
      apply (subst rconc) back
      by simp

    from local_assms
    show "\<forall>t'. unsum (fst (local_eff (A \<parallel> A) t (Internal ic) (cs, g)) t') = fst (local_eff (SumT A) t (Internal (unsum ic)) (as, g)) t'"
      apply (simp add: local_eff_def parallel_def SumT_def view_def local_def local_pre_def localize_def)
      apply (erule_tac x = t in allE)
      apply (cases ic)
      apply simp_all
      apply (cases t)
      apply simp_all
      apply (erule rev_mp)+
      apply (subst lconc)
      apply simp
      apply (subst lconc) back
      apply simp
      apply (subst lconc) back
      apply simp
      apply (erule rev_mp)+
      apply (subst rconc)
      apply simp
      apply (subst rconc) back
      apply simp
      apply (erule rev_mp)+
      apply (cases t)
      apply simp_all
      apply (erule rev_mp)+
      apply (subst lconc)
      apply simp
      apply (subst lconc) back
      apply simp
      apply (subst rconc) back
      apply simp
      apply (erule rev_mp)+
      apply (subst rconc)
      apply simp
      apply (subst rconc) back
      by simp

    from local_assms
    show "snd (local_eff (A \<parallel> A) t (Internal ic) (cs, g)) = snd (local_eff (SumT A) t (Internal (unsum ic)) (as, g))"
      apply (simp add: local_eff_def parallel_def SumT_def view_def local_def local_pre_def localize_def)
      apply (erule_tac x = t in allE)
      apply (cases ic)
      apply simp_all
      apply (cases t)
      apply simp_all
      apply (erule rev_mp)+
      apply (subst lconc)
      apply simp
      apply (subst lconc) back
      apply simp
      apply (subst lconc) back
      apply simp
      apply (erule rev_mp)+
      apply (subst rconc)
      apply simp
      apply (subst rconc) back
      apply simp
      apply (cases t)
      apply simp_all
      apply (erule rev_mp)+
      apply (subst lconc)
      apply simp
      apply (subst lconc) back
      apply simp
      apply (erule rev_mp)+
      apply (subst rconc)
      apply simp
      apply (subst rconc) back
      apply simp
      apply (subst rconc) back
      by simp
  qed
next
  show "\<forall>a b. (a, b) \<in> starts (A \<parallel> A) \<longrightarrow> (\<exists>c. (\<forall>t. unsum (a t) = c t) \<and> (c, b) \<in> starts (SumT A))"
    apply (auto simp add: parallel_def SumT_def starts_def image_def)
    apply (rule_tac x = "unsum \<circ> par_locals L1 L2" in exI)
    apply simp
    apply (rule_tac x = L1 in exI)
    apply (rule_tac x = L2 in exI)
    by blast
qed

end