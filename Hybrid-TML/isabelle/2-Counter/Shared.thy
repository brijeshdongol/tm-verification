theory Shared
imports Transactions Utilities RGA
begin

record gstate =
  glb :: nat
  counter :: nat
  store :: "L \<Rightarrow> V"

definition write_count :: "gstate \<Rightarrow> nat" where
  "write_count s \<equiv> (glb s div 2) + counter s"

definition gstart :: gstate where
  "gstart \<equiv> \<lparr> glb = 0, counter = 0, store = mem_initial\<rparr>"

definition update_store :: "L \<Rightarrow> V \<Rightarrow> gstate \<Rightarrow> gstate" where
  "update_store l v s \<equiv> s \<lparr> store := ((store s) (l := v)) \<rparr>"

definition interference :: "gstate rel" where
  "interference = rtrancl {(g, g'). (odd (glb g) \<longrightarrow> g = g') \<and> (even (glb g) \<and> store g' \<noteq> store g \<longrightarrow> counter g' > counter g) \<and> counter g' \<ge> counter g \<and> (glb g' = glb g)}"

lemma rtrancl_ka_induct1: "Id \<union> (R O S) \<subseteq> S \<Longrightarrow> R\<^sup>* \<subseteq> S"
  apply safe
  apply (rule_tac r = R and a = a in converse_rtrancl_induct)
  by blast+

lemma env_set_subI: "(\<And>a b. P a b \<Longrightarrow> Q a b) \<Longrightarrow> {(a, b). P a b} \<subseteq> {(a, b). Q a b}"
  by auto

lemma env_sub_comp: "{(a, b). P a b} O {(c, d). Q c d} = {(a, c). \<exists>b. P a b \<and> Q b c}"
  by auto

lemma interference_no_star:
  "rtrancl {(g, g'). (odd (glb g) \<longrightarrow> g = g') \<and> (even (glb g) \<and> store g' \<noteq> store g \<longrightarrow> counter g' > counter g) \<and> counter g' \<ge> counter g \<and> (glb g' = glb g)} =
  Id \<union> {(g, g'). (odd (glb g) \<longrightarrow> g = g') \<and> (even (glb g) \<and> store g' \<noteq> store g \<longrightarrow> counter g' > counter g) \<and> counter g' \<ge> counter g \<and> (glb g' = glb g)}"
  apply (rule antisym)
  apply (rule rtrancl_ka_induct1)
  apply (simp add: env_sub_comp)
  apply (intro le_supI2 env_set_subI)
  apply (erule exE)
  apply (rename_tac g g'' g')
  apply clarify
  using dual_order.strict_trans1 apply auto[1]
  by blast

lemma interference_unstar [simp]: "interference\<^sup>* = interference"
  unfolding interference_def by (metis (mono_tags, lifting) rtrancl_idemp)

lemma interference_simple:
  "interference = Id \<union> {(g, g'). (odd (glb g) \<longrightarrow> g = g') \<and> (even (glb g) \<and> store g' \<noteq> store g \<longrightarrow> counter g' > counter g) \<and> counter g' \<ge> counter g \<and> (glb g' = glb g)}"
  by (simp add: interference_def interference_no_star)

(*
definition glb_change :: "gstate rel" where
  "glb_change = Id \<union> {(g, g'). even (glb g) \<and> glb g < glb g' \<and> (odd (glb g'))} \<union> {(g, g'). odd (glb g) \<and> glb g = glb g'}"

lemma "rtrancl glb_change \<subseteq> glb_change"
  apply (rule rtrancl_ka_induct1)
  apply auto
  apply (simp add: glb_change_def)
  by (auto simp add: glb_change_def)
*)

definition glb_change :: "gstate rel" where
  "glb_change = {(g, g'). counter g = counter g' \<and> glb g \<le> glb g'}"

end